package com.yiyihealth.cherriesadmin.controller;



import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.yiyihealth.cherriesadmin.model.jkt.JktRegPointInParameter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


/**
 * @ClassName PubHospitalControllerTest
 * @Description TODO
 * @Author chen
 * @email chen18668070425@163.com
 * @Date 2021/5/21 22:52
 * @Version 1.0
 **/



@RunWith(SpringRunner.class)
@SpringBootTest()
@WebAppConfiguration
class PubHospitalControllerTest {


    private MockMvc mockMvc;
    /**
     * @Author chen
     * @Description //TODO 健康通
     * @Date  2021/5/21
     * @Param []
     * @return void
     **/
    @Test
    void regPoint() throws Exception {
        String parameter = "{\n" +
                "     \"deptName\": \"中医骨伤\",\n" +
                "     \"scheduleTime\": \"1\",\n" +
                "     \"papersId\": \"01\",\n" +
                "     \"patientName\": \"陈煜诚\",\n" +
                "     \"orderId\": 10701456546464644,\n" +
                "     \"deptId\": 86000014,\n" +
                "     \"targetType\": \"1\",\n" +
                "     \"papersNo\": \"411527199004257010\",\n" +
                "     \"pointNo\": \"14\",\n" +
                "     \"phoneNo\": \"18668070425\",\n" +
                "     \"doctorName\": \"史晓林\",\n" +
                "     \"pointId\": 86497810,\n" +
                "     \"labFlag\": \"1\",\n" +
                "     \"hospId\": \"MA2CF6M4233010315D1202\",\n" +
                "     \"doctorId\": 86000016,\n" +
                "     \"scheduleDate\": \"20210528\",\n" +
                "     \"scheduleId\": 86013070\n" +
                "}";
        JktRegPointInParameter inParameter = JSONUtil.toBean(parameter,JktRegPointInParameter.class);
        System.out.println(JSONUtil.parse(inParameter));
        /*
         * 1、mockMvc.perform执行一个请求。
         * 2、MockMvcRequestBuilders.get("XXX")构造一个请求。
         * 3、ResultActions.param添加请求传值
         * 4、ResultActions.accept(MediaType.TEXT_HTML_VALUE))设置返回类型
         * 5、ResultActions.andExpect添加执行完成后的断言。
         * 6、ResultActions.andDo添加一个结果处理器，表示要对结果做点什么事情
         *   比如此处使用MockMvcResultHandlers.print()输出整个响应结果信息。
         * 7、ResultActions.andReturn表示执行完成后返回相应的结果。
         */
        mockMvc = MockMvcBuilders.standaloneSetup(new PubHospitalController()).build();
        mockMvc.perform(MockMvcRequestBuilders.post("/hospital/regPoint")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON.toJSONString(inParameter)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print()).andReturn()
                .getResponse()
                .getContentAsString();
    }


    @Test
    void cancelReg() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(new PubHospitalController()).build();
        String parameter="{\n" +
                "    \"hospId\":\"MA2CF6M4233010315D1202\",\n" +
                "    \"orderId\":\"10701456546464645\",\n" +
                "    \"regId\":\"1070101_20201212_1_001\",\n" +
                "    \"regPassword\":\"3456345\"\n" +
                "}";
    }
}