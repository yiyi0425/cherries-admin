package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.service.PubDepartmentsService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class PubDepartmentsServiceImplTest {

    @Autowired
    private PubDepartmentsService pubDepartmentsService;

    @Test
    void extractHisData() throws Exception {
       try {
           pubDepartmentsService.ExtractHisData();
       }catch (Exception e){
           System.out.println(e.getMessage());
       }
    }

    /**
     * @Author chen
     * @Description //TODO 健康通
     * @Date  2021/5/22
     * @Param []
     * @return void
     **/
    @Test
    void jktUpload() throws Exception {
        pubDepartmentsService.jktUpload(57170L);
    }
}
