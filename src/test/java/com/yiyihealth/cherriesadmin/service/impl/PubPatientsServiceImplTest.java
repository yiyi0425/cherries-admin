package com.yiyihealth.cherriesadmin.service.impl;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.yiyihealth.cherriesadmin.core.http.HttpResult;
import com.yiyihealth.cherriesadmin.service.PubPatientsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

/**
 * com.yiyihealth.cherriesadmin.service.impl
 *
 * @description: TODO
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2020/9/29 0:17
 * @Version 1.0
 **/
@SpringBootTest
class PubPatientsServiceImplTest {

    @Autowired
    private PubPatientsService pubPatientsService;
    @Value("${remote.firstURL}")
    private String firstURL;
    @Value("${remote.secondURL}")
    private String secondURL;
    @Value("${remote.thirdURL}")
    private String thirdURL;

    @Test
    void extractHisData() throws Exception {
        pubPatientsService.ExtractHisData();
    }

    @Test
    void storedProcedure() throws Exception {
        String tokenStr = "";
        HashMap<String, Object> paramMap = new HashMap<>();
        String json = JSONUtil.parseObj("{}").toString();
        try {
            tokenStr = HttpUtil.post(firstURL+"/register/storedProcedure", "inData=8650696|毛奕能|男|0|330102198411240019|||13738137370||");
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("第三方接口异常");
        }
        HttpResult httpResult = JSONUtil.toBean(JSONUtil.parseObj(tokenStr),HttpResult.class);
        System.out.println(httpResult);
    }

    @Test
    void modifyName() throws Exception {
        String tokenStr = "";
        try {
            tokenStr = HttpUtil.post(firstURL+"/modifyName/storedProcedure", "inData=14695434|朱绳纬|18757015539");
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("第三方接口异常");
        }
        HttpResult httpResult = JSONUtil.toBean(JSONUtil.parseObj(tokenStr),HttpResult.class);
        System.out.println(httpResult);
    }
}
