package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.mapper.OrdSourceDetailMapper;
import com.yiyihealth.cherriesadmin.model.OptionItem;
import com.yiyihealth.cherriesadmin.model.OrdScheduling;
import com.yiyihealth.cherriesadmin.model.OrdSourceDetail;
import com.yiyihealth.cherriesadmin.model.PubHospital;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingWrapper;
import com.yiyihealth.cherriesadmin.service.OrdSchedulingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
class OrdSchedulingServiceImplTest {
    @Resource
    private OrdSchedulingService schedulingService;
    @Resource
    private RedisTemplate<Object, Object> redisCacheTemplate;
    @Resource
    private OrdSourceDetailMapper ordSourceDetailMapper;


    @Test
    void selectListForOptions(Long hospitalId) {
        LocalDateTime start = LocalDateTime.now();
        List<OptionItem<PubHospital>> data = schedulingService.selectListForOptions(hospitalId);
        data = getTreeData(data);
        LocalDateTime end = LocalDateTime.now();
        Duration duration = Duration.between(start, end);
        System.out.println("start-end: " + duration.getSeconds());
    }

    private List<OptionItem<PubHospital>> getTreeData(List<OptionItem<PubHospital>> hospitalOptionItemList) {
        for (OptionItem optionItem : hospitalOptionItemList) {
            String beanName = optionItem.getValue().getClass().getSimpleName();
            if ("OrdScheduling".equals(beanName) && !CollectionUtils.isEmpty(optionItem.getChildren())) {
                List<OptionItem> optionItemSourceDetailList = new ArrayList<>();
                for (OptionItem item : (List<OptionItem>) optionItem.getChildren()) {
                    //查询号源明细
                    OrdSourceDetail sourceDetail = (OrdSourceDetail) item.getValue();
                    String sourceDetailKey = "sourceDetail:" + sourceDetail.getDoctorId() + ":" + sourceDetail.getSchedulingId();
                    if (redisCacheTemplate.opsForSet().isMember(sourceDetailKey, sourceDetail.getId())) {
                        OptionItem<OrdSourceDetail> optionItemSourceDetail = new OptionItem<>();
                        optionItemSourceDetail.setValue(sourceDetail);
                        optionItemSourceDetail.setLabel(sourceDetail.getVisitTime() + " 第" + sourceDetail.getSerialNumber() + "号");
                        optionItemSourceDetailList.add(optionItemSourceDetail);
                    }
                }
                optionItem.setLabel("(" + optionItemSourceDetailList.size() + ")" + optionItem.getLabel().split("\\)")[1]);
                optionItem.setChildren(optionItemSourceDetailList);
            } else {
                this.getTreeData(optionItem.getChildren());
            }
        }
        return hospitalOptionItemList;
    }

    @BeforeEach
    void setUp() {
    }

    @Test
    void schedulingtasks() throws Exception {
        schedulingService.schedulingtasks();
    }

    @Test
    void selectListByHospitalIdDoctorId() {
        List<OrdSchedulingWrapper> ordSchedulingWrappers = schedulingService.selectListByHospitalIdDoctorId(57170L, 86000422L, "0", null);
        for (OrdSchedulingWrapper sd : ordSchedulingWrappers) {
            System.out.println(sd);
        }
    }

    /**
     * @return void
     * @Author chen
     * @Description //TODO 健康通
     * @Date 2021/5/22
     * @Param []
     **/
    @Test
    void jktSchedulingUpload() throws Exception {
        schedulingService.jktSchedulingUpload(57170L);
    }

    @Test
    void testSchedulingtasks() throws Exception {
        schedulingService.schedulingtasks();
    }

    @Test
    void schedulingCache() throws Exception {
        Map<String, Object> map = new HashMap<>();
        List<OrdScheduling> list = schedulingService.selectList(map);
        for (OrdScheduling scheduling : list) {
            schedulingService.schedulingCache(scheduling);
        }
    }
}
