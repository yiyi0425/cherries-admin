package com.yiyihealth.cherriesadmin.service.impl;


import com.yiyihealth.cherriesadmin.core.quartz.SchedulerQuartzJob;
import com.yiyihealth.cherriesadmin.model.OrdSchedulingTemplate;
import com.yiyihealth.cherriesadmin.service.OrdSchedulingTemplateService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.quartz.*;
import org.quartz.impl.DirectSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDateTime;


@RunWith(SpringRunner.class)
@SpringBootTest
class OrdSchedulingTemplateServiceImplTest {
    @Resource
    private OrdSchedulingTemplateService ordSchedulingTemplateService;
    @Autowired
    private RedisTemplate<Object, Object> redisCacheTemplate;

    @Test
    void testRedisTemplate(){
        Duration duration = Duration.between(LocalDateTime.now(),LocalDateTime.now().plusDays(3));
        redisCacheTemplate.opsForValue().set("scheduling:0014:1:123456789635874","80",duration);
        redisCacheTemplate.opsForValue().set("scheduling:0014:2:123456789635874","80",duration);
        redisCacheTemplate.opsForValue().set("scheduling:0014:3:123456789635874","80",duration);
        redisCacheTemplate.opsForValue().set("scheduling:0014:4:123456789635874","80",duration);
        System.out.println("ddd");
    }

    @Test
    void createScheduling() throws Exception {
        OrdSchedulingTemplate ordSchedulingTemplate = ordSchedulingTemplateService.selectByPrimaryKey(1312238536887505054L);
        ordSchedulingTemplateService.createScheduling(ordSchedulingTemplate,null);
    }

    @Test
    void createSchedulingBatch() throws Exception {
        ordSchedulingTemplateService.createSchedulingBatch();
    }

    @Test
    void extractHisData() throws Exception {
        ordSchedulingTemplateService.ExtractHisData();
    }

    @Test
    public void testTask() throws SchedulerException {
        DirectSchedulerFactory schedulerFactory = DirectSchedulerFactory.getInstance();
        // 表示以3个工作线程初始化工厂
        schedulerFactory.createVolatileScheduler(3);
        Scheduler scheduler = schedulerFactory.getScheduler();
        JobDetail jobDetail = JobBuilder.newJob(SchedulerQuartzJob.class).withIdentity("job2", "group2").build();
        //给自定义任务传值
        jobDetail.getJobDataMap().put("user", "Tom");  //给自定义任务传值
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("trigger1", "group1")
                .startNow()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInSeconds(2)
                        .repeatForever())
                .build();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}
