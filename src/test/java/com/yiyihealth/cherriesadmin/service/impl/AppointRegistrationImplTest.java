package com.yiyihealth.cherriesadmin.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;
/**
 * com.yiyihealth.cherriesadmin.service.impl
 * @description: TODO
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2020/11/8 17:07
 * @Version 1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
class AppointRegistrationImplTest {

     @Resource
     private AppointRegistrationImpl appointRegistration;

    @Test
    void funMain() throws Exception {
        String xml ="<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>\n" +
                "<data>\n" +
                "    <orgid>057170</orgid>\n" +
                "    <funcode>100208</funcode>\n" +
                "    <regid>641527055</regid>\n" +
                "    <numid>86022704</numid>\n" +
                "    <patid>6951348</patid>\n" +
                "    <visitdate>20201219</visitdate>\n" +
                "    <ampm>2</ampm>\n" +
                "    <schid>86000265</schid>\n" +
                "    <pass>59462850</pass>\n" +
                "    <no>5</no>\n" +
                "    <spid>9901</spid>\n" +
                "    <spname>浙江移动（12580）</spname>\n" +
                "    <oper>192.168.161.140</oper>\n" +
                "</data>";
                appointRegistration.funMain(xml);
    }
    @Test
    void test100202(){
        String xml ="<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>\n" +
                "<data>\n" +
                "    <orgid>057170</orgid>\n" +
                "    <funcode>100202</funcode>\n" +
                "    <docid>151</docid>\n" +
                "    <docname></docname>\n" +
                "</data>";
        appointRegistration.funMain(xml);
    }
    @Test
    void test100201(){
        String xml="<data>\n" +
                "    <orgid>057170</orgid>\n" +
                "    <funcode>100201</funcode>\n" +
                "    <deptid>H012</deptid>\n" +
                "</data>";
        appointRegistration.funMain(xml);
    }
    @Test
    void test100209(){
        String xml="<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>\n" +
                "<data>\n" +
                "    <funcode>100209</funcode>\n" +
                "    <orgid>0571GP</orgid>\n" +
                "    <regid>639028335</regid>\n" +
                "    <visitdate>20201211</visitdate>\n" +
                "    <pass>29746692</pass>\n" +
                "    <spid>9901</spid>\n" +
                "    <spname>浙江移动（12580）</spname>\n" +
                "    <oper>192.168.161.140</oper>\n" +
                "</data>";
        appointRegistration.funMain(xml);
    }
}
