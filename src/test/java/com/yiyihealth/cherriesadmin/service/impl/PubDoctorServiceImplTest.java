package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.service.PubDoctorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PubDoctorServiceImplTest {

    @Autowired
    private PubDoctorService pubDoctorService;

    @Test
    void createBean() throws Exception {
        pubDoctorService.ExtractHisData();
    }
    @Test
    void selectListByDepId(){
        pubDoctorService.selectListByDepId(86000007L,"0",null);
    }

    /**
     * @Author chen
     * @Description //TODO 健康通
     * @Date  2021/5/22
     * @Param []
     * @return void
     **/
    @Test
    void jktUpload() throws Exception {
        pubDoctorService.jktUpload(57170L);
    }
}
