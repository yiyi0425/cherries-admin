package com.yiyihealth.cherriesadmin.service.impl;



import com.alibaba.excel.EasyExcel;
import com.yiyihealth.cherriesadmin.core.utils.TestFileUtil;
import com.yiyihealth.cherriesadmin.model.OrdAppointment;
import com.yiyihealth.cherriesadmin.service.OrdAppointmentService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
class OrdAppointmentServiceImplTest {
    @Resource
    private OrdAppointmentService ordAppointmentService;

    @Test
    public void soures() throws Exception {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.clear();
        String fileName = TestFileUtil.getPath() + "complexHeadWrite" + System.currentTimeMillis() + ".xlsx";
        // 这里 需要指定写用哪个class去读，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        // 如果这里想使用03 则 传入excelType参数即可
        List<OrdAppointment> ordAppointments =  ordAppointmentService.selectList(paramMap);
        EasyExcel.write(fileName, OrdAppointment.class).sheet("模板").doWrite(ordAppointments);
    }

    @Test
    void uploadVisitInformation() throws Exception {
        ordAppointmentService.uploadVisitInformation();
    }

    @Test
    void selectDataCenter() {
        ordAppointmentService.selectDataCenter();
    }

    /**
     * @Author chen
     * @Description //TODO 健康通
     * @Date  2021/5/22
     * @Param []
     * @return void
     **/
    @Test
    void jktUploadVisitInformation() throws Exception {
        ordAppointmentService.jktUploadVisitInformation();
    }
}
