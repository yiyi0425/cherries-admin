package com.yiyihealth.cherriesadmin.service.impl;



import com.yiyihealth.cherriesadmin.service.PubHospitalService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;



/**
 * @ClassName PubHospitalServiceImplTest
 * @Description TODO
 * @Author chen
 * @email chen18668070425@163.com
 * @Date 2021/5/21 13:00
 * @Version 1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
class PubHospitalServiceImplTest {

    @Autowired
    private PubHospitalService hospitalService;

    /**
     * @Author chen
     * @Description //TODO 健康通
     * @Date  2021/5/22
     * @Param []
     * @return void
     **/
    @Test
    void jktUpload() throws Exception {
        hospitalService.jktUpload(57170L);
    }
}