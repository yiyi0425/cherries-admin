package com.yiyihealth.cherriesadmin;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.yiyihealth.cherriesadmin.core.utils.RSAUtil;
import com.yiyihealth.cherriesadmin.model.jkt.JktFunc10001;
import com.yiyihealth.cherriesadmin.model.jkt.JktInParameter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class JKTTest {

    private  Set<String> keySet = new TreeSet();
    private  String SIGN = "sign";
    public  String privateKey = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDFwopBlHKEHG6E534FCuKbEiYyP7IMKMTRXr4kDesNz57TLTf+43VeIsaE/ruqvGNroZ+1FcD9Pyfv70aYCsZwWVQed6b5WWnXff62Mv0FokRP3Cmy2oz8j4UYve6Y73j2em+54Cm/OvrJ/rkUrCNr1xUQbHzNfFtvyP3VPviFGxyQTcX1+360f+8M7aCPnxk2CPWkKCHRkrwIUPEAGCKdXNuU76hdAURKX5f8PC/o1idJXLGL3XaEBoCd82CuBYqJ9oMJSqVRhFcyysSvnC7b0fwB05LIjwa8FkFV84eJFj/HLfLnkIP0QGRphxkz1MD0Yq6zu+gH6PJtXfoNIfa/AgMBAAECggEBAL7v8eUXmumLOfoAOGBN/jSS9WYWsaWjx0QPIa5V8djH2SUzx5jFrJBTKzFFcZSLh8Jnrnd8pXTeAYVblOE/phDbbqhsBj9B4Ts6Xy8zU7CY1zPz8h7JfM5tE6ir2S64KCZyWGUy1ZvZKkLVOYWIHjsE00WkOWt84huwB8RYpEJ+E+BIQHR3RZ0GR1ep6MWMS3vc0xNXCyYU2ttVVgqw+CwGhGpCdKhNb9DUmEbXgR0Sz1yhp5twqIu9bN92TWps3wPX4930NXGU8ri4W18WQeJkOrJwyrppntQCBkGP7eP+vfLSDcjOmYkPjUKHfmOfRFLUMQVDdHTgPd1pYo1XRSECgYEA6mGfPTOZKE2o9TXN0CVsLFjWrt1jJLQoEG6g4Ojjb9WlbD1+dG7jyh8yljI5T1gJEVYjqJuaihILBWI9OI6e5tla/3nVgzH16+8IMGAkdFoQtC3D+2V5ReKzVUPnxyXjUOfp8UfyioQwPc4wQ7PcnenJ/fVGr6+yldm4tgLCNysCgYEA2AAv8n8g3lrAXeBPqmtPraskEnCR44I8uQgSWm+BM0kvuM9AAr0WkQrXIpgvOBCuF+kWjD5X+lQmIzGZuDKVtcu8F3MznEgy0lPyZmKW6M3StJFbOe7bU9E15EWczXa2kxNPdiVu79ex8JRL6B0uXdeZ/FRHYfdSTHfIBvRftL0CgYAri5eaN0vtQQBzDlv+n/VJB/ha5KbW2uHWq7cYEiyvSnJBeetUloWbt9AeN5aONzz4C5j0anR70n5ZsbbOGzFKkndln60bgty9yNssobAkLdeMojQXMPMnGMdYwBT/2vowveWo/kUBly2fTf+JlJEYhacRl9BaM7pqPNkkd769oQKBgQC4aBiHMC/Qhav7uXhqJymkPBsUvxReJp9O3OxGVzH3pgDfB9k/PU2Z0KZw+3o8pEjtUusUGlmj9tmEbGEvV8hmp6RSZODoCN+Sn2awMHVkFexQilRW9mw7d75Jv4CqDTK8B91sIXM/wmdA3GqdpOQj6+CkAxPaOHsDm7Oi9FkNBQKBgQC+75EaGYwE3n1zggz8pLK723mZSmvCnibZP/K4tTyKhGgB4K5FoMysfENxtywRL06SkYNa5uZF9MfzEoEoU3cu7jVQS+zcnFGlHzWAPvZ4bs4j4gFvdHWngky++hHnhNrI4R6gwfkWXRms921DaFjj2s20hILhyCj/sH9/xnTGfw==";
    public  String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxcKKQZRyhBxuhOd+BQrimxImMj+yDCjE0V6+JA3rDc+e0y03/uN1XiLGhP67qrxja6GftRXA/T8n7+9GmArGcFlUHnem+Vlp133+tjL9BaJET9wpstqM/I+FGL3umO949npvueApvzr6yf65FKwja9cVEGx8zXxbb8j91T74hRsckE3F9ft+tH/vDO2gj58ZNgj1pCgh0ZK8CFDxABginVzblO+oXQFESl+X/Dwv6NYnSVyxi912hAaAnfNgrgWKifaDCUqlUYRXMsrEr5wu29H8AdOSyI8GvBZBVfOHiRY/xy3y55CD9EBkaYcZM9TA9GKus7voB+jybV36DSH2vwIDAQAB";
    private RestTemplate restTemplate = new RestTemplate();

    @Test
    public void testMessage() throws UnsupportedEncodingException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {

        /**
         * 业务入参
         */
        JktFunc10001 fun10001In = new JktFunc10001();
        fun10001In.setHospId("MA2CF6M4233010315D1202");
        fun10001In.setHospName("孙泰和国医国药馆");
        fun10001In.setLocationCode("3301020000000000000000");
        fun10001In.setHospLevelCode("23");
        fun10001In.setHospImgUrl("");
        fun10001In.setTelephone("11111");
        fun10001In.setHospType("6");
        fun10001In.setHospState("0");
        fun10001In.setIntroduction("");
        fun10001In.setServiceAddress("");

        List<JSON> jktFun10001InList = new ArrayList<>();
        System.out.println(sortMap(BeanUtil.beanToMap(fun10001In)));
        jktFun10001InList.add(JSONUtil.parse(fun10001In));
        Map<String,Object> map =  new HashMap<>();
        map.put("list",jktFun10001InList);




        JktInParameter params = new JktInParameter();
        params.setMerchantId("2021042296");
        params.setLogTraceId(IdUtil.simpleUUID());
        params.setInterfaceMethod("10001");

        params.setBizContent(JSONUtil.parseObj(map));

        /**
         * 签名字符串
         */
        StringBuilder sign = new StringBuilder();
        sign.append("bizContent=");
        sign.append(params.getBizContent());
        sign.append("&");
        sign.append("interfaceMethod=");
        sign.append(params.getInterfaceMethod());
        sign.append("&");
        sign.append("logTraceId=");
        sign.append(params.getLogTraceId());
        sign.append("&");
        sign.append("merchantId=");
        sign.append(params.getMerchantId());

        /**
         * 签名
         */
        params.setSign(RSAUtil.sign(sign.toString(),privateKey));

        System.out.println("verify-------privateKey-------"+privateKey);
        System.out.println(RSAUtil.verify(sign.toString(),params.getSign(),publicKey));;


        /**
         * http请求对象
         */
        HttpRequest httpRequest = HttpRequest.post("http://183.136.187.224:18180/gateway/api/entry")
                .header("merchantId",params.getMerchantId())
                .header("interfaceMethod",params.getInterfaceMethod())
                .header("logTraceId",params.getLogTraceId())
                .header("sign",params.getSign());

        /**
         * 业务入参JSON
         */
        JSONObject bodyJson = JSONUtil.parseObj(params, false, true);
        System.out.println(params.getSign());
        bodyJson.remove("merchantId");
        bodyJson.remove("interfaceMethod");
        bodyJson.remove("sign");
        System.out.println(bodyJson.toStringPretty());

        String result = httpRequest.body(bodyJson.toStringPretty()).execute().body();

        System.out.println(result);

    }
    public int compare(Map.Entry<String, String> o1, Map.Entry<String, String> o2) {
        return (o1.getKey().compareTo(o2.getKey()));
    }

    public static List<Map.Entry<String, Object>> sortMap(Map<String, Object> map) {
        List<Map.Entry<String, Object>> infos = new ArrayList<Map.Entry<String, Object>>(map.entrySet());

        // 重写集合的排序方法：按字母顺序
        Collections.sort(infos, new Comparator<Map.Entry<String, Object>>() {
            @Override
            public int compare(Map.Entry<String, Object> o1, Map.Entry<String, Object> o2) {
                return (o1.getKey().compareTo(o2.getKey()));
            }
        });

        return infos;
    }
}
