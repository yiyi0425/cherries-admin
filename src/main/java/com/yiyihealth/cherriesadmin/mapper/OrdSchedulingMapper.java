package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.OrdScheduling;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 日常排版(OrdScheduling)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:05
 */
@Mapper
@Component
public interface OrdSchedulingMapper extends EntityMapper<OrdScheduling, OrdSchedulingWrapper, Long> {

}
