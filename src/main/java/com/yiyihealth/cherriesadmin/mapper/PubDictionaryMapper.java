package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.PubDictionary;
import com.yiyihealth.cherriesadmin.model.wrapper.PubDictionaryWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 基础字典表(PubDictionary)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:06
 */
@Mapper
@Component
public interface PubDictionaryMapper extends EntityMapper<PubDictionary, PubDictionaryWrapper, Long> {

}
