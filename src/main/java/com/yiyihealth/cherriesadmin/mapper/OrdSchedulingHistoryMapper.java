package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.OrdSchedulingHistory;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingHistoryWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 历史排班表(OrdSchedulingHistory)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:06
 */
@Mapper
@Component
public interface OrdSchedulingHistoryMapper extends EntityMapper<OrdSchedulingHistory, OrdSchedulingHistoryWrapper, Long> {

}
