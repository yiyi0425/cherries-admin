package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.PubPatients;
import com.yiyihealth.cherriesadmin.model.wrapper.PubPatientsWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 患者信息表(PubPatients)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:06
 */
@Mapper
@Component
public interface PubPatientsMapper extends EntityMapper<PubPatients, PubPatientsWrapper, Long> {

    PubPatients selectpatientsFuzzyquery(Map<String,Object> map);

    PubPatients selectByIDCard(String mobileNo);
}
