package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.PaPatientinfo;
import com.yiyihealth.cherriesadmin.model.wrapper.PaPatientinfoWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * HIS人员信息表(PaPatientinfo)表数据库访问层
 *
 * @author chen
 * @since 2020-10-11 18:26:36
 */
@Mapper
@Component
public interface PaPatientinfoMapper extends EntityMapper<PaPatientinfo, PaPatientinfoWrapper, Long> {

}