package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.PubHospital;
import com.yiyihealth.cherriesadmin.model.wrapper.PubHospitalWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 医疗机构信息表(PubHospital)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:06
 */
@Mapper
@Component
public interface PubHospitalMapper extends EntityMapper<PubHospital, PubHospitalWrapper, Long> {

    /**
     * 根据医院名称查询医院信息列表
     * @param hosName
     * @return
     */
    List<PubHospital> selectListByHosName(@Param("hosName")String hosName);

    /**
     * 查询所有未注销的医院信息列表
     * @param status
     * @return
     */
    List<PubHospital> selectListByStatus(@Param("status")String status);
}
