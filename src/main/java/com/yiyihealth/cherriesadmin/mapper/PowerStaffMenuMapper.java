package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.PowerStaffMenu;
import com.yiyihealth.cherriesadmin.model.wrapper.PowerStaffMenuWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * (PowerStaffMenu)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:06
 */
@Mapper
@Component
public interface PowerStaffMenuMapper extends EntityMapper<PowerStaffMenu, PowerStaffMenuWrapper, Long> {

}
