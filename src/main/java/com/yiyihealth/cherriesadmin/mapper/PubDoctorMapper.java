package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.PubDoctor;
import com.yiyihealth.cherriesadmin.model.wrapper.PubDoctorWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 医疗机构与人员信息关系表
 * (PubDoctor)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:06
 */
@Mapper
@Component
public interface PubDoctorMapper extends EntityMapper<PubDoctor, PubDoctorWrapper, Long> {

}
