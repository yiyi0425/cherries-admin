package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.OutRegPlan;
import com.yiyihealth.cherriesadmin.model.wrapper.OutRegPlanWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * (OutRegPlan)表数据库访问层
 *
 * @author chen
 * @since 2020-10-13 16:49:17
 */
@Mapper
@Component
public interface OutRegPlanMapper extends EntityMapper<OutRegPlan, OutRegPlanWrapper, Long> {

}
