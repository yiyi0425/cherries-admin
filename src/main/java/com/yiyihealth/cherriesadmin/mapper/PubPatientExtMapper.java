package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.PubPatientExt;
import com.yiyihealth.cherriesadmin.model.wrapper.PubPatientExtWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 省平台患者信息(PubPatientExt)表数据库访问层
 *
 * @author chen
 * @since 2020-10-22 10:17:56
 */
@Mapper
@Component
public interface PubPatientExtMapper extends EntityMapper<PubPatientExt, PubPatientExtWrapper, Long> {

    /**
     * 根据平台ID查询患者信息
     * @param platFromId
     * @return
     */
    PubPatientExt selectByPlatFromId(String platFromId);
}
