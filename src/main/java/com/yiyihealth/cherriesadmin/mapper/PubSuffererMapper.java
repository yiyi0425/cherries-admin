package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.PubSufferer;
import com.yiyihealth.cherriesadmin.model.wrapper.PubSuffererWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * (PubSufferer)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:07
 */
@Mapper
@Component
public interface PubSuffererMapper extends EntityMapper<PubSufferer, PubSuffererWrapper, Long> {

    PubSufferer selectByIdentityNum(String identityNum);
}
