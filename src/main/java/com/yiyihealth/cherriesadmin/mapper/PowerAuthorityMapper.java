package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.PowerAuthority;
import com.yiyihealth.cherriesadmin.model.wrapper.PowerAuthorityWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 菜单表 路由和一级菜单图标 按钮(PowerAuthority)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:06
 */
@Mapper
@Component
public interface PowerAuthorityMapper extends EntityMapper<PowerAuthority, PowerAuthorityWrapper, Long> {

}
