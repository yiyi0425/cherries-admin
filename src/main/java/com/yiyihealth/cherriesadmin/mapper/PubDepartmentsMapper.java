package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.PubDepartments;
import com.yiyihealth.cherriesadmin.model.wrapper.PubDepartmentsWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 执行科室科室(PubDepartments)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:06
 */
@Mapper
@Component
public interface PubDepartmentsMapper extends EntityMapper<PubDepartments, PubDepartmentsWrapper, Long> {

}
