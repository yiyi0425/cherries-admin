package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.PubStaffInfo;
import com.yiyihealth.cherriesadmin.model.wrapper.PubStaffInfoWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 职工信息表(PubStaffInfo)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:07
 */
@Mapper
@Component
public interface PubStaffInfoMapper extends EntityMapper<PubStaffInfo, PubStaffInfoWrapper, Long> {

    /**
     * 用户登录验证
     * @param userName
     * @param password
     * @return
     */
    PubStaffInfo selectListByLoginNumPassword(@Param("sffLoginNum") String userName, @Param("password")String password);
}
