package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.OrdSchedulingCancel;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingCancelWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 停诊表(OrdSchedulingCancel)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:05
 */
@Mapper
@Component
public interface OrdSchedulingCancelMapper extends EntityMapper<OrdSchedulingCancel, OrdSchedulingCancelWrapper, Long> {

}
