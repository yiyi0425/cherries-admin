package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.OrdSchedulingTemplate;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingTemplateWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 排版模板(OrdSchedulingTemplate)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:06
 */
@Mapper
@Component
public interface OrdSchedulingTemplateMapper extends EntityMapper<OrdSchedulingTemplate, OrdSchedulingTemplateWrapper, Long> {

}
