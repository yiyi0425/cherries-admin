package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.OrdAppointment;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdAppointmentWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 预约订单记录(OrdAppointment)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:05
 */
@Mapper
@Component
public interface OrdAppointmentMapper extends EntityMapper<OrdAppointment, OrdAppointmentWrapper, Long> {

   OrdAppointment selectByPrimaryKeypat(Long id);

    OrdAppointment selectByAppId(Long appId);
}
