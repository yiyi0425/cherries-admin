package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.OrdSourceDetailHistory;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSourceDetailHistoryWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 号源明细信息表(OrdSourceDetailHistory)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:06
 */
@Mapper
@Component
public interface OrdSourceDetailHistoryMapper extends EntityMapper<OrdSourceDetailHistory, OrdSourceDetailHistoryWrapper, Long> {

}
