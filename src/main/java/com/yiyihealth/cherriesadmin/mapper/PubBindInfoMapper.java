package com.yiyihealth.cherriesadmin.mapper;

import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.model.PubBindInfo;
import com.yiyihealth.cherriesadmin.model.wrapper.PubBindInfoWrapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 诊疗项目套餐表(PubBindInfo)表数据库访问层
 *
 * @author chen
 * @since 2020-09-14 20:03:06
 */
@Mapper
@Component
public interface PubBindInfoMapper extends EntityMapper<PubBindInfo, PubBindInfoWrapper, Long> {

}
