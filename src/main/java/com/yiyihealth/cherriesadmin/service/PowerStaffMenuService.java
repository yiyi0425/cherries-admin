package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PowerStaffMenu;
import com.yiyihealth.cherriesadmin.model.wrapper.PowerStaffMenuWrapper;

/**
 * (PowerStaffMenu)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface PowerStaffMenuService extends
        BaseService<PowerStaffMenu, PowerStaffMenuWrapper, Long> {
}
