package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PaPatientinfo;
import com.yiyihealth.cherriesadmin.model.wrapper.PaPatientinfoWrapper;

/**
 * HIS人员信息表(PaPatientinfo)表服务接口
 *
 * @author chen
 * @since 2020-10-11 18:26:34
 */
public interface PaPatientinfoService extends
        BaseService<PaPatientinfo, PaPatientinfoWrapper, Long> {
}