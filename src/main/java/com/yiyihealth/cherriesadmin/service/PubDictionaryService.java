package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PubDictionary;
import com.yiyihealth.cherriesadmin.model.wrapper.PubDictionaryWrapper;

/**
 * 基础字典表(PubDictionary)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface PubDictionaryService extends
        BaseService<PubDictionary, PubDictionaryWrapper, Long> {
}
