package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PubPatientExt;
import com.yiyihealth.cherriesadmin.model.wrapper.PubPatientExtWrapper;

/**
 * 省平台患者信息(PubPatientExt)表服务接口
 *
 * @author chen
 * @since 2020-10-22 10:14:31
 */
public interface PubPatientExtService extends
        BaseService<PubPatientExt, PubPatientExtWrapper, Long> {
}
