package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.OutRegPlan;
import com.yiyihealth.cherriesadmin.model.wrapper.OutRegPlanWrapper;

/**
 * (OutRegPlan)表服务接口
 *
 * @author chen
 * @since 2020-10-13 16:49:16
 */
public interface OutRegPlanService extends
        BaseService<OutRegPlan, OutRegPlanWrapper, Long> {
}
