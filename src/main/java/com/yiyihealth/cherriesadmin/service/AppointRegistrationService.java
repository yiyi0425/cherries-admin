package com.yiyihealth.cherriesadmin.service;


import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * 表示这是一个@WebService服务接口
 */

@WebService
public interface AppointRegistrationService {

    /**
     * @WebMethod注解，表示服务发布时被注解的方法也会随之发布
     * @param XmlRequest
     * @return
     */
    @WebMethod
    public String funMain(String XmlRequest) throws Exception;
}
