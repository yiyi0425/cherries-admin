package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.DataCenter;
import com.yiyihealth.cherriesadmin.model.OrdAppointment;
import com.yiyihealth.cherriesadmin.model.ZoneDataCenter;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdAppointmentWrapper;

import java.util.List;
import java.util.Map;

/**
 * 预约订单记录(OrdAppointment)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface OrdAppointmentService extends
        BaseService<OrdAppointment, OrdAppointmentWrapper, Long> {

    OrdAppointment selectByPrimaryKeypat(Long id);


    /**
     * 后台取消预约
     * @param record
     * @return
     * @throws Exception
     */
    String cancelByBack(OrdAppointment record) throws Exception;

    /**
     * 后台预约
     * @param record
     * @return
     * @throws Exception
     */
    String createByBack(OrdAppointment record) throws Exception;

    /**
     * H5页面预约
     * @param record
     * @param sourceDetailId
     * @return
     * @throws Exception
     */
    String createByPatIdSourceDetailId(OrdAppointment record, Long sourceDetailId) throws Exception;

    /**
     * H5取消预约
     * @param appointment
     * @return
     * @throws Exception
     */
    String delete(OrdAppointment appointment) throws Exception;

    /**
     * 抽取HIS患者信息数据
     */
    void extractHisData() throws Exception;

    /**
     * 上传就诊信息
     */
    public void uploadVisitInformation() throws Exception;
    public void jktUploadVisitInformation() throws Exception;

    /**
     * H5根据用户信息查询预约记录
     * @param patId
     * @return
     */
    List<OrdAppointmentWrapper> selectListByPatId(Long patId);

    List<OrdAppointmentWrapper>  selectListBySufferId(Long patId);

    List selectStatis(Map map);

    List priselectStatis(Map map);

    /**
     * 根据医生统计
     */
    List selectStatisDoctor(Map map);
    /**
     * 根据科室统计
     */
    List selectStatisDepartments(Map map);
    DataCenter selectDataCenter();

    ZoneDataCenter selectZoneDataCenterCount(Long hospitalId);
}
