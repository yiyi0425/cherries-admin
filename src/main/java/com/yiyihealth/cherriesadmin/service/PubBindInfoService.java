package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PubBindInfo;
import com.yiyihealth.cherriesadmin.model.wrapper.PubBindInfoWrapper;

/**
 * 诊疗项目套餐表(PubBindInfo)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface PubBindInfoService extends
        BaseService<PubBindInfo, PubBindInfoWrapper, Long> {
}
