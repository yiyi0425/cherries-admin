package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.OptionItem;
import com.yiyihealth.cherriesadmin.model.OrdScheduling;
import com.yiyihealth.cherriesadmin.model.PubHospital;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingWrapper;
import org.dom4j.DocumentException;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.List;

/**
 * 日常排版(OrdScheduling)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface OrdSchedulingService extends
        BaseService<OrdScheduling, OrdSchedulingWrapper, Long> {
    /**
     * 医生停诊
     * @param id
     * @param reason
     * @return
     */
    String doctorStopWork(Long id, String reason) throws Exception;

    /**
     * 号源池缓存重新生成
     * @return
     */
    String schedulingCache(OrdScheduling ordScheduling) throws Exception;

    boolean schedulingtasks() throws Exception;

    List<OrdSchedulingWrapper> selectListByHospitalIdDoctorId(Long hospitalId, Long doctorId, String status, Long patId);

    List<OptionItem<PubHospital>> selectListForOptions(Long hospitalId);

    String jktSchedulingUpload(Long hospitalId) throws Exception;
    void jktSchedulingUpload() throws Exception;

    void jktSchedulingUploadSingle(Long id) throws Exception;
}
