package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.OptionItem;
import com.yiyihealth.cherriesadmin.model.OrdSourceDetail;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSourceDetailWrapper;

import java.util.List;

/**
 * 号源明细信息表(OrdSourceDetail)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface OrdSourceDetailService extends
        BaseService<OrdSourceDetail, OrdSourceDetailWrapper, Long> {



    List<OptionItem> selectListForOptions(Long schedulingId);
}
