package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.OrdSchedulingHistory;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingHistoryWrapper;

/**
 * 历史排班表(OrdSchedulingHistory)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface OrdSchedulingHistoryService extends
        BaseService<OrdSchedulingHistory, OrdSchedulingHistoryWrapper, Long> {
}
