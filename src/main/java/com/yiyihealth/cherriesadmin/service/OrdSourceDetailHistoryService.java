package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.OrdSourceDetailHistory;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSourceDetailHistoryWrapper;

/**
 * 号源明细信息表(OrdSourceDetailHistory)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface OrdSourceDetailHistoryService extends
        BaseService<OrdSourceDetailHistory, OrdSourceDetailHistoryWrapper, Long> {
}
