package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PubDepartments;
import com.yiyihealth.cherriesadmin.model.PubDictionary;
import com.yiyihealth.cherriesadmin.model.wrapper.PubDepartmentsWrapper;

import java.util.List;
import java.util.Map;

/**
 * 执行科室科室(PubDepartments)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface PubDepartmentsService extends
        BaseService<PubDepartments, PubDepartmentsWrapper, Long> {

    List<PubDepartments> selectListByDepNameStatus(String depName, String status);

    List<PubDepartments> selectListByDepFatherId(String status, Long hospitalId, Long depFatherId);

    /**
     *
     * @param status
     * @param hospitalId
     * @param depFatherId
     * @return
     */
    List<PubDepartmentsWrapper> selectListWrapperByDepFatherId(String status, Long hospitalId, Long depFatherId);

    List<PubDepartments> selectListByHospitalId(Long hospitalId, String status);

    /**
     * 抽取HIS科室信息信息数据
     */
    void ExtractHisData() throws Exception;

    List<PubDictionary> selectListByDepFather(Long dicType, Long hospitalId);

    List<PubDepartmentsWrapper> selectListByDepFatherId(Long depFatherId, Long hospitalId);
    List<PubDepartments> selectOption(Map map);

    void jktUpload(Long hospitalId) throws Exception;
}
