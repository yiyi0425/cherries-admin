package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PowerAuthority;
import com.yiyihealth.cherriesadmin.model.wrapper.PowerAuthorityWrapper;

/**
 * 菜单表 路由和一级菜单图标 按钮(PowerAuthority)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface PowerAuthorityService extends
        BaseService<PowerAuthority, PowerAuthorityWrapper, Long> {
}
