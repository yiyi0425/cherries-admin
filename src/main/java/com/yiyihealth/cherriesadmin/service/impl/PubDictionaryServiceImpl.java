package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.PubDictionaryMapper;
import com.yiyihealth.cherriesadmin.model.PubDictionary;
import com.yiyihealth.cherriesadmin.model.wrapper.PubDictionaryWrapper;
import com.yiyihealth.cherriesadmin.service.PubDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 基础字典表(PubDictionary)表服务实现类
 *
 * @author chen
 * @since 2020-09-14 20:03:30
 */
@Service("pubDictionaryService")
public class PubDictionaryServiceImpl extends BaseServiceImpl<PubDictionary, PubDictionaryWrapper, Long> implements PubDictionaryService {
    @Resource
    private PubDictionaryMapper pubDictionaryMapper;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(pubDictionaryMapper);
    }

}
