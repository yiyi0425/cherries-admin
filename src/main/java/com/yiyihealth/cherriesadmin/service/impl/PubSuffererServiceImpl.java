package com.yiyihealth.cherriesadmin.service.impl;

import cn.hutool.core.util.IdcardUtil;
import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.PubSuffererMapper;
import com.yiyihealth.cherriesadmin.model.PubSufferer;
import com.yiyihealth.cherriesadmin.model.wrapper.PubSuffererWrapper;
import com.yiyihealth.cherriesadmin.service.PubSuffererService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * (PubSufferer)表服务实现类
 *
 * @author chen
 * @since 2020-09-14 20:03:33
 */
@Service("pubSuffererService")
public class PubSuffererServiceImpl extends BaseServiceImpl<PubSufferer, PubSuffererWrapper, Long> implements PubSuffererService {
    @Resource
    private PubSuffererMapper pubSuffererMapper;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(pubSuffererMapper);
    }

    @Override
    public PubSufferer insert(PubSufferer record) throws Exception {
        if (IdcardUtil.isValidCard(record.getPatIdentityNum())){
            record.setPatSex(IdcardUtil.getGenderByIdCard(record.getPatIdentityNum()));
        }
        if (Optional.ofNullable(record.getId()).isPresent()){
            return super.updateByPrimaryKeySelective(record);
        }
        return super.insert(record);
    }

    @Override
    public PubSufferer selectByIdentityNum(String IdentityNum) {
        return pubSuffererMapper.selectByIdentityNum(IdentityNum);
    }
}
