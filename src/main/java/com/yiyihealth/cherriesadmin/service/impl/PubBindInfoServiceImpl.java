package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.PubBindInfoMapper;
import com.yiyihealth.cherriesadmin.model.PubBindInfo;
import com.yiyihealth.cherriesadmin.model.wrapper.PubBindInfoWrapper;
import com.yiyihealth.cherriesadmin.service.PubBindInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 诊疗项目套餐表(PubBindInfo)表服务实现类
 *
 * @author chen
 * @since 2020-09-14 20:03:30
 */
@Service("pubBindInfoService")
public class PubBindInfoServiceImpl extends BaseServiceImpl<PubBindInfo, PubBindInfoWrapper, Long> implements PubBindInfoService {
    @Resource
    private PubBindInfoMapper pubBindInfoMapper;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(pubBindInfoMapper);
    }

}
