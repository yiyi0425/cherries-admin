package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.PubStaffInfoMapper;
import com.yiyihealth.cherriesadmin.model.PubStaffInfo;
import com.yiyihealth.cherriesadmin.model.wrapper.PubStaffInfoWrapper;
import com.yiyihealth.cherriesadmin.service.PubStaffInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 职工信息表(PubStaffInfo)表服务实现类
 *
 * @author chen
 * @since 2020-09-14 20:03:30
 */
@Service("pubStaffInfoService")
public class PubStaffInfoServiceImpl extends BaseServiceImpl<PubStaffInfo, PubStaffInfoWrapper, Long> implements PubStaffInfoService {
    @Resource
    private PubStaffInfoMapper pubStaffInfoMapper;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(pubStaffInfoMapper);
    }

    @Override
    public PubStaffInfo selectListByLoginNumPassword(String userName, String password) {
        return pubStaffInfoMapper.selectListByLoginNumPassword(userName,password);
    }
}
