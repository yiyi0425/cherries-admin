package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.OrdSchedulingHistoryMapper;
import com.yiyihealth.cherriesadmin.model.OrdSchedulingHistory;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingHistoryWrapper;
import com.yiyihealth.cherriesadmin.service.OrdSchedulingHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 历史排班表(OrdSchedulingHistory)表服务实现类
 *
 * @author chen
 * @since 2020-09-14 20:03:29
 */
@Transactional
@Service("ordSchedulingHistoryService")
public class OrdSchedulingHistoryServiceImpl extends BaseServiceImpl<OrdSchedulingHistory, OrdSchedulingHistoryWrapper, Long> implements OrdSchedulingHistoryService {
    @Resource
    private OrdSchedulingHistoryMapper ordSchedulingHistoryMapper;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(ordSchedulingHistoryMapper);
    }

}
