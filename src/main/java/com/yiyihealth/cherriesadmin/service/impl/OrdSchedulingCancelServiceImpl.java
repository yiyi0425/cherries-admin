package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.OrdSchedulingCancelMapper;
import com.yiyihealth.cherriesadmin.model.OrdSchedulingCancel;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingCancelWrapper;
import com.yiyihealth.cherriesadmin.service.OrdSchedulingCancelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 停诊表(OrdSchedulingCancel)表服务实现类
 *
 * @author chen
 * @since 2020-09-14 20:03:29
 */
@Transactional
@Service
public class OrdSchedulingCancelServiceImpl extends BaseServiceImpl<OrdSchedulingCancel, OrdSchedulingCancelWrapper, Long> implements OrdSchedulingCancelService {
    @Resource
    private OrdSchedulingCancelMapper ordSchedulingCancelMapper;

    @Autowired
    public void setEntityMapper() {
        super.setEntityMapper(ordSchedulingCancelMapper);
    }

}
