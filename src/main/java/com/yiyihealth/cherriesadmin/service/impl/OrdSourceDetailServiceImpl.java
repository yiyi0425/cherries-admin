package com.yiyihealth.cherriesadmin.service.impl;

import cn.hutool.core.date.Week;
import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.OrdSourceDetailHistoryMapper;
import com.yiyihealth.cherriesadmin.mapper.OrdSourceDetailMapper;
import com.yiyihealth.cherriesadmin.mapper.PubHospitalMapper;
import com.yiyihealth.cherriesadmin.model.*;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSourceDetailWrapper;
import com.yiyihealth.cherriesadmin.service.OrdSourceDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 号源明细信息表(OrdSourceDetail)表服务实现类
 *
 * @author chen
 * @since 2020-09-14 20:03:29
 */
@Transactional
@Service("ordSourceDetailService")
public class OrdSourceDetailServiceImpl extends BaseServiceImpl<OrdSourceDetail, OrdSourceDetailWrapper, Long> implements OrdSourceDetailService {
    @Resource
    private OrdSourceDetailMapper ordSourceDetailMapper;

    @Resource
    private PubHospitalMapper hospitalMapper;
    @Resource
    private RedisTemplate<Object, Object> redisCacheTemplate;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(ordSourceDetailMapper);
    }



    /**
     *
     * @return
     * @param schedulingId
     */
    @Override
    public List<OptionItem> selectListForOptions(Long schedulingId) {
        Map<String, Object> schedulingMap = new HashMap<String, Object>(8);
        schedulingMap.put("schedulingId",schedulingId);
        schedulingMap.put("status","0");
        List<OrdSourceDetail> sourceDetailList = ordSourceDetailMapper.selectList(schedulingMap);
        List<OptionItem> optionItemSourceDetailList = new ArrayList<>();
        for (OrdSourceDetail sourceDetail:sourceDetailList){
            OptionItem<OrdSourceDetail> optionItemSourceDetail=new OptionItem<>();
            optionItemSourceDetail.setValue(sourceDetail);
            optionItemSourceDetail.setLabel(sourceDetail.getVisitTime()+" 第"+sourceDetail.getSerialNumber()+"号");
            optionItemSourceDetailList.add(optionItemSourceDetail);
        }
        return optionItemSourceDetailList;
    }

    @Override
    public OrdSourceDetailWrapper selectWrapperByPrimaryKey(Long aLong) {
        OrdSourceDetailWrapper sourceDetail = ordSourceDetailMapper.selectWrapperByPrimaryKey(aLong);
        PubHospital hospital = hospitalMapper.selectByPrimaryKey(sourceDetail.getHospitalId());
        sourceDetail.setHospitalWrapper(hospital.getHosName());
        int week = sourceDetail.getSchedulingDate().getDayOfWeek().getValue()+1>7?1:sourceDetail.getSchedulingDate().getDayOfWeek().getValue()+1;
        sourceDetail.setWeekWrapper(Week.of(week).toChinese());
        if (sourceDetail.getTimeState().equals(1)){
            sourceDetail.setTimeStateWrapper("上午");
        }else if (sourceDetail.getTimeState().equals(2)){
            sourceDetail.setTimeStateWrapper("下午");
        }else if (sourceDetail.getTimeState().equals(3)){
            sourceDetail.setTimeStateWrapper("夜门诊");
        }
        return sourceDetail;
    }

    @Override
    public OrdSourceDetail updateByPrimaryKeySelective(OrdSourceDetail record) {

        return super.updateByPrimaryKeySelective(record);
    }

}
