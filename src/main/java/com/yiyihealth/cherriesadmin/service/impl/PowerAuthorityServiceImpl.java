package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.PowerAuthorityMapper;
import com.yiyihealth.cherriesadmin.model.PowerAuthority;
import com.yiyihealth.cherriesadmin.model.wrapper.PowerAuthorityWrapper;
import com.yiyihealth.cherriesadmin.service.PowerAuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 菜单表 路由和一级菜单图标 按钮(PowerAuthority)表服务实现类
 *
 * @author chen
 * @since 2020-09-14 20:03:30
 */
@Service("powerAuthorityService")
public class PowerAuthorityServiceImpl extends BaseServiceImpl<PowerAuthority, PowerAuthorityWrapper, Long> implements PowerAuthorityService {
    @Resource
    private PowerAuthorityMapper powerAuthorityMapper;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(powerAuthorityMapper);
    }

}
