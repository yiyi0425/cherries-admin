package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.PubPatientExtMapper;
import com.yiyihealth.cherriesadmin.model.PubPatientExt;
import com.yiyihealth.cherriesadmin.model.wrapper.PubPatientExtWrapper;
import com.yiyihealth.cherriesadmin.service.PubPatientExtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 省平台患者信息(PubPatientExt)表服务实现类
 *
 * @author chen
 * @since 2020-10-22 10:14:31
 */
@Service("pubPatientExtService")
public class PubPatientExtServiceImpl extends BaseServiceImpl<PubPatientExt, PubPatientExtWrapper, Long> implements PubPatientExtService {
    @Resource
    private PubPatientExtMapper pubPatientExtMapper;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(pubPatientExtMapper);
    }

}
