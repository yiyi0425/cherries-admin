package com.yiyihealth.cherriesadmin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.core.utils.JKTUtil;
import com.yiyihealth.cherriesadmin.core.webservice.MainServiceImplPortType;
import com.yiyihealth.cherriesadmin.core.webservice.MainServiceImplPortTypeService;
import com.yiyihealth.cherriesadmin.mapper.PubHospitalMapper;
import com.yiyihealth.cherriesadmin.model.PubHospital;
import com.yiyihealth.cherriesadmin.model.jkt.JThealth;
import com.yiyihealth.cherriesadmin.model.jkt.JktFunc10001;
import com.yiyihealth.cherriesadmin.model.wrapper.PubHospitalWrapper;
import com.yiyihealth.cherriesadmin.service.PubHospitalService;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.yiyihealth.cherriesadmin.core.utils.JKTUtil.sortMap;

/**
 * 医疗机构信息表(PubHospital)表服务实现类
 *
 * @author chen
 * @since 2020-09-14 20:03:30
 */
@CacheConfig(cacheNames = "pubHospitalService")
@Service("pubHospitalService")
public class PubHospitalServiceImpl extends BaseServiceImpl<PubHospital, PubHospitalWrapper, Long> implements PubHospitalService {
    @Resource
    private PubHospitalMapper pubHospitalMapper;

    @Resource
    private JktFunc10001 jktFunc10001;
    @Resource
    private JThealth jThealth;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(pubHospitalMapper);
    }

    @Cacheable
    @Override
    public List<PubHospital> selectListByHosName(String hosName) {
        return pubHospitalMapper.selectListByHosName(hosName);
    }

    @Cacheable
    @Override
    public List<PubHospital> selectListByStatus(String status) {
        return pubHospitalMapper.selectListByStatus(status);
    }

    @Override
    public void upload(Long hospitalId) throws Exception {
        MainServiceImplPortType service = new MainServiceImplPortTypeService().getMainServiceImplPortTypePort();
        Map<String, Object> map = new HashMap<>();
        map.put("id",hospitalId);
        List<PubHospital> hospitalWrapperList = pubHospitalMapper.selectList(map);
        for (PubHospital hospital : hospitalWrapperList) {
            try {
                Document document = DocumentHelper.createDocument();
                Element root = document.addElement("data");
                Element funcode = root.addElement("funcode");
                funcode.setText("200101");
                Element orgid = root.addElement("orgid");
                orgid.setText(hospital.getHosMarkCode().trim());

                Element orgname = root.addElement("orgname");
                orgname.setText(hospital.getHosName());

                Element alias = root.addElement("alias");
                alias.setText(hospital.getHosShortName());

                Element description = root.addElement("description");
                description.setText(hospital.getDescription());

                Element address = root.addElement("address");
                address.setText(hospital.getHosContactAddress());

                Element level = root.addElement("level");
                level.setText("00");

                Element nature = root.addElement("nature");
                nature.setText(hospital.getHosType());

                Element category = root.addElement("category");
                category.setText("A1");

                Element tel = root.addElement("tel");
                tel.setText(hospital.getHosPhone());

                Element web = root.addElement("web");
                web.setText(hospital.getHosWeb());

                Element state = root.addElement("state");
                state.setText("0");

                String strResult = "";


                strResult = service.funMain(document.asXML());
                Document documentResult = DocumentHelper.parseText(strResult);
                //获取根节点
                Element rootElt = documentResult.getRootElement();
                //获取根节点名称
                String rootName = rootElt.getName();
                //获取子节点
                Element stateElt = rootElt.element("state");

                if (!stateElt.getTextTrim().equals("0")) {
                    throw new Exception("inparam: "+document.asXML()+"    outparam: "+strResult);
                }
            } catch (Exception e) {
                throw new Exception("预约挂号：" + hospital.getHosName() + e.getMessage());
            }
        }
    }

    @Override
    public void jktUpload(Long hospitalId) throws Exception {
        Map<String, Object> map = new HashMap<>();
        if (!hospitalId.toString().equals("0")) {
            map.put("id",hospitalId);
        }

        List<PubHospital> hospitalWrapperList = pubHospitalMapper.selectList(map);
        for (PubHospital hospital : hospitalWrapperList) {
            try {

                jktFunc10001.setTelephone(hospital.getHosPhone());
                jktFunc10001.setHospState(hospital.getStatus());
                /*jktFunc10001.setIntroduction(hospital.getDescription());*/
                List<JSON> jktFun10001InList = new ArrayList<>();
                sortMap(BeanUtil.beanToMap(jktFunc10001));
                jktFun10001InList.add(JSONUtil.parse(jktFunc10001));
                Map<String,Object> FuncMap =  new HashMap<>();
                FuncMap.put("list",jktFun10001InList);
                JKTUtil.jThealthPost("10001",FuncMap, jThealth);
            } catch (Exception e) {
                throw new Exception("预约挂号：" + hospital.getHosName() + e.getMessage());
            }
        }
    }
}
