package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.OrdSchedulingTemplateMapper;
import com.yiyihealth.cherriesadmin.mapper.OutRegPlanMapper;
import com.yiyihealth.cherriesadmin.mapper.PubDoctorMapper;
import com.yiyihealth.cherriesadmin.model.OrdSchedulingTemplate;
import com.yiyihealth.cherriesadmin.model.OutRegPlan;
import com.yiyihealth.cherriesadmin.model.PubDoctor;
import com.yiyihealth.cherriesadmin.model.wrapper.OutRegPlanWrapper;
import com.yiyihealth.cherriesadmin.service.OrdSchedulingTemplateService;
import com.yiyihealth.cherriesadmin.service.OutRegPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * (OutRegPlan)表服务实现类
 *
 * @author chen
 * @since 2020-10-13 16:49:16
 */
@Service("outRegPlanService")
public class OutRegPlanServiceImpl extends BaseServiceImpl<OutRegPlan, OutRegPlanWrapper, Long> implements OutRegPlanService {
    @Resource
    private OutRegPlanMapper outRegPlanMapper;
    @Resource
    private OrdSchedulingTemplateMapper ordSchedulingTemplateMapper;
    @Resource
    private PubDoctorMapper doctorMapper;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(outRegPlanMapper);
    }

//    @Cacheable(value = "OutRegPlan")
    @Override
    public List<OutRegPlanWrapper> selectListWrapper(Map<String, Object> map) {
        List<OutRegPlanWrapper> outRegPlanWrapperList = outRegPlanMapper.selectListWrapper(map);
        List<OutRegPlanWrapper>  outRegPlanWrappers = new ArrayList<>();
        Integer num=0;
        for (OutRegPlanWrapper outRegPlanWrapper:outRegPlanWrapperList){
            if (num>20){
                continue;
            }
            num++;
            map.clear();
            map.put("originalSchedule",outRegPlanWrapper.getSystemid());
            List<OrdSchedulingTemplate> ordSchedulingTemplateList = ordSchedulingTemplateMapper.selectList(map);
            if (CollectionUtils.isEmpty(ordSchedulingTemplateList)){
                continue;
            }else {
                OrdSchedulingTemplate schedulingTemplate = ordSchedulingTemplateList.get(0);
                outRegPlanWrapper.setDepId(schedulingTemplate.getDepId());
                outRegPlanWrapper.setDepName(schedulingTemplate.getDepName());
                outRegPlanWrapper.setDoctorId(schedulingTemplate.getDoctorId());
                outRegPlanWrapper.setDoctorName(schedulingTemplate.getDoctorName());
                outRegPlanWrapper.setBinId(schedulingTemplate.getBinId());
                outRegPlanWrapper.setBinName(schedulingTemplate.getBinName());

                PubDoctor doctor = doctorMapper.selectByPrimaryKey(schedulingTemplate.getDoctorId());
                outRegPlanWrapper.setDoctorSync(doctor.getSync());
                if (Optional.ofNullable(schedulingTemplate.getDoctorPracticeScope()).isPresent()){
                    outRegPlanWrapper.setDoctorPracticeScope(schedulingTemplate.getDoctorPracticeScope());
                }else {
                    outRegPlanWrapper.setDoctorPracticeScope(doctor.getDoctorPracticeScope());
                }
                outRegPlanWrapper.setStart(outRegPlanWrapper.getKaishisj().toLocalTime());
                outRegPlanWrapper.setEnd(outRegPlanWrapper.getJieshusj().toLocalTime());
                outRegPlanWrappers.add(outRegPlanWrapper);
            }
        }
        return super.selectListWrapper(map);
    }
}
