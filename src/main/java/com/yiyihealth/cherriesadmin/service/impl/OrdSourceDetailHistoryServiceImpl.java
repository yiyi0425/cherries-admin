package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.OrdSourceDetailHistoryMapper;
import com.yiyihealth.cherriesadmin.model.OrdSourceDetailHistory;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSourceDetailHistoryWrapper;
import com.yiyihealth.cherriesadmin.service.OrdSourceDetailHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 号源明细信息表(OrdSourceDetailHistory)表服务实现类
 *
 * @author chen
 * @since 2020-09-14 20:03:29
 */
@Transactional
@Service("ordSourceDetailHistoryService")
public class OrdSourceDetailHistoryServiceImpl extends BaseServiceImpl<OrdSourceDetailHistory, OrdSourceDetailHistoryWrapper, Long> implements OrdSourceDetailHistoryService {
    @Resource
    private OrdSourceDetailHistoryMapper ordSourceDetailHistoryMapper;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(ordSourceDetailHistoryMapper);
    }

}
