package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.PaPatientinfoMapper;
import com.yiyihealth.cherriesadmin.model.PaPatientinfo;
import com.yiyihealth.cherriesadmin.model.wrapper.PaPatientinfoWrapper;
import com.yiyihealth.cherriesadmin.service.PaPatientinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * HIS人员信息表(PaPatientinfo)表服务实现类
 *
 * @author chen
 * @since 2020-10-11 18:26:34
 */
@Service("paPatientinfoService")
public class PaPatientinfoServiceImpl extends BaseServiceImpl<PaPatientinfo, PaPatientinfoWrapper, Long> implements PaPatientinfoService {
    @Resource
    private PaPatientinfoMapper paPatientinfoMapper;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(paPatientinfoMapper);
    }

}