package com.yiyihealth.cherriesadmin.service.impl;

import com.yiyihealth.cherriesadmin.core.service.impl.BaseServiceImpl;
import com.yiyihealth.cherriesadmin.mapper.PowerStaffMenuMapper;
import com.yiyihealth.cherriesadmin.model.PowerStaffMenu;
import com.yiyihealth.cherriesadmin.model.wrapper.PowerStaffMenuWrapper;
import com.yiyihealth.cherriesadmin.service.PowerStaffMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * (PowerStaffMenu)表服务实现类
 *
 * @author chen
 * @since 2020-09-14 20:03:30
 */
@Service("powerStaffMenuService")
public class PowerStaffMenuServiceImpl extends BaseServiceImpl<PowerStaffMenu, PowerStaffMenuWrapper, Long> implements PowerStaffMenuService {
    @Resource
    private PowerStaffMenuMapper powerStaffMenuMapper;

    @Autowired
    private void setEntityMapper() {
        super.setEntityMapper(powerStaffMenuMapper);
    }

}
