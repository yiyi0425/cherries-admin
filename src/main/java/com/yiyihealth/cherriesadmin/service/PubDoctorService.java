package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PubDoctor;
import com.yiyihealth.cherriesadmin.model.wrapper.PubDoctorWrapper;

import java.util.List;

/**
 * 医疗机构与人员信息关系表
 * (PubDoctor)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface PubDoctorService extends
        BaseService<PubDoctor, PubDoctorWrapper, Long> {
    List<PubDoctorWrapper> selectListByDepId(Long id, String status, Long patId);

    /**
     * 抽取HIS医生信息信息数据
     */
    void ExtractHisData() throws Exception;

    void jktUpload(Long hospitalId) throws Exception;
}
