package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PubPatients;
import com.yiyihealth.cherriesadmin.model.wrapper.PubPatientsWrapper;

import java.util.Map;

/**
 * 患者信息表(PubPatients)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:18
 */
public interface PubPatientsService extends
        BaseService<PubPatients, PubPatientsWrapper, Long> {

    /**
     * 抽取HIS患者信息数据
     */
    void ExtractHisData() throws Exception;

    PubPatients selectpatientsFuzzyquery(Map<String,Object> map);

    PubPatients modifyPatient(PubPatients patients) throws Exception;
}
