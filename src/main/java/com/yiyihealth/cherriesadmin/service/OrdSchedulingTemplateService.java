package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.OrdSchedulingTemplate;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingTemplateWrapper;

import java.time.LocalDate;

/**
 * 排版模板(OrdSchedulingTemplate)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface OrdSchedulingTemplateService extends
        BaseService<OrdSchedulingTemplate, OrdSchedulingTemplateWrapper, Long> {

    /**
     * 生成排班
     * @return
     */
    String createScheduling(OrdSchedulingTemplate record, LocalDate visiteDate ) throws Exception;

    String createSchedulingBatch(Long doctorId) throws Exception;
    String createSchedulingBatch() throws Exception;


    /**
     * 抽取HIS患者信息数据
     */
    String ExtractHisData() throws Exception;
}
