package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PubHospital;
import com.yiyihealth.cherriesadmin.model.wrapper.PubHospitalWrapper;

import java.util.List;

/**
 * 医疗机构信息表(PubHospital)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface PubHospitalService extends
        BaseService<PubHospital, PubHospitalWrapper, Long> {

    /**
     * 根据医院名称查询医院信息列表
     * @param hosName
     * @return
     */
    List<PubHospital> selectListByHosName(String hosName);

    /**
     * 查询所有未注销的医院信息列表
     * @param status
     * @return
     */
    List<PubHospital> selectListByStatus(String status);

    void upload(Long hospitalId) throws Exception;

    void jktUpload(Long hospitalId) throws Exception;
}
