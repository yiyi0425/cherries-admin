package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PubStaffInfo;
import com.yiyihealth.cherriesadmin.model.wrapper.PubStaffInfoWrapper;

/**
 * 职工信息表(PubStaffInfo)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:18
 */
public interface PubStaffInfoService extends
        BaseService<PubStaffInfo, PubStaffInfoWrapper, Long> {
    /**
     * 用户登录验证
     * @param userName
     * @param password
     * @return
     */
    PubStaffInfo selectListByLoginNumPassword(String userName, String password);
}
