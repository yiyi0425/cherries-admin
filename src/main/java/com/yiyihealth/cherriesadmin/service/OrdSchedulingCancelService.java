package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.OrdSchedulingCancel;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingCancelWrapper;

/**
 * 停诊表(OrdSchedulingCancel)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:17
 */
public interface OrdSchedulingCancelService extends
        BaseService<OrdSchedulingCancel, OrdSchedulingCancelWrapper, Long> {
}
