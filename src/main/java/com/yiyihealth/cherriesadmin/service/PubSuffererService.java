package com.yiyihealth.cherriesadmin.service;

import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PubSufferer;
import com.yiyihealth.cherriesadmin.model.wrapper.PubSuffererWrapper;

/**
 * (PubSufferer)表服务接口
 *
 * @author chen
 * @since 2020-09-14 20:03:18
 */
public interface PubSuffererService extends
        BaseService<PubSufferer, PubSuffererWrapper, Long> {
    PubSufferer selectByIdentityNum(String IdentityNum);
}
