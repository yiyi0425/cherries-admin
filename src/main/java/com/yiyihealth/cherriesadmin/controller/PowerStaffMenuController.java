package com.yiyihealth.cherriesadmin.controller;

import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.PowerStaffMenu;
import com.yiyihealth.cherriesadmin.model.wrapper.PowerStaffMenuWrapper;
import com.yiyihealth.cherriesadmin.service.PowerStaffMenuService;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * (PowerStaffMenu)表控制层
 *
 * @author chen
 * @since 2020-09-14 20:03:49
 */

@RestController
@RequestMapping("powerStaffMenu")
public class PowerStaffMenuController extends BaseController<PowerStaffMenu, PowerStaffMenuWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private PowerStaffMenuService powerStaffMenuService;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(powerStaffMenuService);
    }


}
