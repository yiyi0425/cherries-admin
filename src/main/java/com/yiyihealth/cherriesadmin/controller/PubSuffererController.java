package com.yiyihealth.cherriesadmin.controller;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.yiyihealth.cherriesadmin.core.http.HttpResult;
import com.yiyihealth.cherriesadmin.core.page.PageRequest;
import com.yiyihealth.cherriesadmin.core.page.PageUtils;
import com.yiyihealth.cherriesadmin.core.page.Param;
import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.PubPatients;
import com.yiyihealth.cherriesadmin.model.PubSufferer;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdAppointmentWrapper;
import com.yiyihealth.cherriesadmin.model.wrapper.PubSuffererWrapper;
import com.yiyihealth.cherriesadmin.service.PubPatientsService;
import com.yiyihealth.cherriesadmin.service.PubSuffererService;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Insert;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * (PubSufferer)表控制层
 *
 * @author chen
 * @since 2020-09-14 20:03:52
 */
@RestController
@RequestMapping("pubSufferer")
public class PubSuffererController extends BaseController<PubSufferer, PubSuffererWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private PubSuffererService pubSuffererService;
    @Resource
    private PubPatientsService pubPatientsService;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(pubSuffererService);
    }


    @Override
    public HttpResult create( PubSufferer record) throws Exception {
        Map<String,Object> map = new HashMap<>(7);
        map.put("patId",record.getPatId());
        map.put("patIdentityNum",record.getPatIdentityNum());
        List<PubSufferer> sufferers = pubSuffererService.selectList(map);
        if (!CollectionUtils.isEmpty(sufferers)){
            return HttpResult.failure("此用户已绑定身份证号为"+record.getPatIdentityNum()+"就诊人，就诊人姓名: "+sufferers.get(0).getPatName());
        }
        return super.create(record);
    }

    @ApiOperation(value = "H5根据患者ID查询就诊人列表")
    @GetMapping("/selectListByPatId")
    @ResponseBody
    public HttpResult selectListByPatId(@RequestParam Long patId) throws Exception {
        Map<String,Object> map = new HashMap<>(7);
        map.put("patId",patId);
        return HttpResult.success(pubSuffererService.selectList(map));
    }

    @Override
    public HttpResult selectList(@RequestBody Map<String, Object> map) {
        List<PubSuffererWrapper> suffererWrapperList = new ArrayList<>();
        List<PubSuffererWrapper> pubSuffererList = pubSuffererService.selectListWrapper(map);
        for (PubSuffererWrapper suffererWrapper:pubSuffererList){
            if (!Optional.ofNullable(suffererWrapper.getPatIdentityNum()).isPresent()){
                continue;
            }
            if (Optional.ofNullable(suffererWrapper.getPatIdentityNum()).isPresent() && IdcardUtil.isValidCard(suffererWrapper.getPatIdentityNum())){
                suffererWrapper.setPatSex(IdcardUtil.getGenderByIdCard(suffererWrapper.getPatIdentityNum()));
            }
            if (Optional.ofNullable(suffererWrapper.getPatSex()).isPresent()){
                suffererWrapper.setPatSexWrapper(suffererWrapper.getPatSex().equals(1)?"男":"女");
            }
            System.out.println(suffererWrapper.getPatId());
            PubPatients patients = pubPatientsService.selectByPrimaryKey(suffererWrapper.getPatId());
            if (Optional.ofNullable(patients).isPresent()){
                suffererWrapper.setSufferName(patients.getPatName());
                suffererWrapper.setPhoneWrapper(patients.getPatPhone());
            }

            if (suffererWrapperList.contains(suffererWrapper)){

            }else {
                suffererWrapperList.add(suffererWrapper);
            }

        }
        return HttpResult.success(suffererWrapperList);
    }

    @Override
    public HttpResult selectListPage(@RequestBody PageRequest pageRequest) {
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        Map<String, Object> map = new HashMap<>();
        for (Param param:pageRequest.getParams()){
            if (!StrUtil.isBlank(param.getValue())){
                map.put(param.getName(),param.getValue());
            }
        }
        PageInfo<PubSuffererWrapper> pageInfo = pubSuffererService.selectListWrapperPage(map,pageNum,pageSize);
        List<PubSuffererWrapper> suffererWrapperList = new ArrayList<>();
        List<PubSuffererWrapper> pubSuffererList = pageInfo.getList();
        for (PubSuffererWrapper suffererWrapper:pubSuffererList){
            if (!Optional.ofNullable(suffererWrapper.getPatIdentityNum()).isPresent()){
                continue;
            }
            if (Optional.ofNullable(suffererWrapper.getPatIdentityNum()).isPresent() && IdcardUtil.isValidCard(suffererWrapper.getPatIdentityNum())){
                suffererWrapper.setPatSex(IdcardUtil.getGenderByIdCard(suffererWrapper.getPatIdentityNum()));
            }
            if (Optional.ofNullable(suffererWrapper.getPatSex()).isPresent()){
                suffererWrapper.setPatSexWrapper(suffererWrapper.getPatSex().equals(1)?"男":"女");
            }
            suffererWrapperList.add(suffererWrapper);
            PubPatients patients = pubPatientsService.selectByPrimaryKey(suffererWrapper.getPatId());
            if (Optional.ofNullable(patients).isPresent()){
                suffererWrapper.setSufferName(patients.getPatName());
                suffererWrapper.setPhoneWrapper(patients.getPatPhone());
            }
        }
        pageInfo.setList(suffererWrapperList);
        return HttpResult.success(PageUtils.getPageResult(pageRequest,pageInfo));
    }

}
