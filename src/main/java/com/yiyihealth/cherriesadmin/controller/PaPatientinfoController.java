package com.yiyihealth.cherriesadmin.controller;

import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.PaPatientinfo;
import com.yiyihealth.cherriesadmin.model.wrapper.PaPatientinfoWrapper;
import com.yiyihealth.cherriesadmin.service.PaPatientinfoService;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * HIS人员信息表(PaPatientinfo)表控制层
 *
 * @author chen
 * @since 2020-10-11 18:26:34
 */
@RestController
@RequestMapping("paPatientinfo")
public class PaPatientinfoController extends BaseController<PaPatientinfo, PaPatientinfoWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private PaPatientinfoService paPatientinfoService;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(paPatientinfoService);
    }


}