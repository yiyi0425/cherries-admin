package com.yiyihealth.cherriesadmin.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.date.DateUtil;
import cn.hutool.system.SystemUtil;
import com.yiyihealth.cherriesadmin.core.http.HttpResult;
import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.PubStaffInfo;
import com.yiyihealth.cherriesadmin.model.wrapper.PubStaffInfoWrapper;
import com.yiyihealth.cherriesadmin.service.PubStaffInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 职工信息表(PubStaffInfo)表控制层
 *
 * @author chen
 * @since 2020-09-14 20:03:51
 */
@Api(tags = "订单管理类")
@RestController
@RequestMapping("pubStaffInfo")
public class PubStaffInfoController extends BaseController<PubStaffInfo, PubStaffInfoWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private PubStaffInfoService pubStaffInfoService;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(pubStaffInfoService);
    }


    @GetMapping("/verifyCode")
    public String verifyCode(HttpServletRequest request, HttpServletResponse response){
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(200,100);
        //图形验证码写出，可以写出到文件，也可以写出到流
        String imgName=DateUtil.current(true)+".png";
        String imgUrl="";
        if (SystemUtil.getOsInfo().isLinux()){
            imgUrl="/home/img/"+imgName;
        }else {
            imgUrl="D:/test/"+imgName;
        }
        lineCaptcha.write(imgUrl);
        //将VerifyCode绑定session
        request.getSession().setAttribute("VerifyCode", lineCaptcha.getCode());
        //验证图形验证码的有效性，返回boolean值
        lineCaptcha.verify(lineCaptcha.getCode());

        return "static/"+imgName;
    }

    @ApiOperation(value = "登录方法接口")
    @RequestMapping(value="login")
    public PubStaffInfo login(@RequestParam("userName")String userName, @RequestParam("password") String password, @RequestParam("appId") Long appId ,
                           @RequestParam("unitId") Long unitId, @RequestParam(value="resource", required=false, defaultValue="pc") String resource,  @RequestParam("code") String code,HttpServletRequest request) throws Exception {
        PubStaffInfo staffInfo  = null;
        String verifyCode=(String) request.getSession().getAttribute("VerifyCode");
//        if (!verifyCode.equals(code)){
////            throw new Exception("输入验证码不正确，请重新输入！");
//        }
        try {
            staffInfo  = pubStaffInfoService.selectListByLoginNumPassword(userName.trim(),password.trim());
            if (!Optional.ofNullable(staffInfo).isPresent()){
                throw new Exception("登录账户或者密码错误，请重新输入！");
            }
            //设置token
//            UUID uuid = UUID.randomUUID();
//            staffInfo.setToken(uuid.toString());
//            staffInfo.setAppId(appId);
        }catch (Exception e){
            throw new Exception("登录账户或者密码错误，请重新输入！");
        }
        System.out.println(staffInfo);
        return staffInfo;
    }
    @Override
    @ResponseBody
    public HttpResult create(@RequestBody PubStaffInfo record) throws Exception {
        Map<String, Object> sMap = new HashMap<>(16);
        sMap.put("sffLoginNum", record.getSffLoginNum().trim());
        List<PubStaffInfo> staffInfoList = pubStaffInfoService.selectList(sMap);
        if (!CollectionUtils.isEmpty(staffInfoList)){
            return HttpResult.failure(record.getSffLoginNum().trim()+"工号已存在，请确认？");
        }
        return super.create(record);
    }
}
