package com.yiyihealth.cherriesadmin.controller;

import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.PubPatientExt;
import com.yiyihealth.cherriesadmin.model.wrapper.PubPatientExtWrapper;
import com.yiyihealth.cherriesadmin.service.PubPatientExtService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 省平台患者信息(PubPatientExt)表控制层
 *
 * @author chen
 * @since 2020-10-22 10:14:32
 */
@Api(tags = "省平台患者信息类")
@RestController
@RequestMapping("pubPatientExt")
public class PubPatientExtController extends BaseController<PubPatientExt, PubPatientExtWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private PubPatientExtService pubPatientExtService;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(pubPatientExtService);
    }


}
