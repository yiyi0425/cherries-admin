package com.yiyihealth.cherriesadmin.controller;

import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.OrdSchedulingHistory;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingHistoryWrapper;
import com.yiyihealth.cherriesadmin.service.OrdSchedulingHistoryService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 历史排班表(OrdSchedulingHistory)表控制层
 *
 * @author chen
 * @since 2020-09-14 20:03:47
 */
@Api(tags = "历史排班表类")
@RestController
@RequestMapping("ordSchedulingHistory")
public class OrdSchedulingHistoryController extends BaseController<OrdSchedulingHistory, OrdSchedulingHistoryWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private OrdSchedulingHistoryService ordSchedulingHistoryService;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(ordSchedulingHistoryService);
    }


}
