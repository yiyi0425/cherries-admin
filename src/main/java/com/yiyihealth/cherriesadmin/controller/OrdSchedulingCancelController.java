package com.yiyihealth.cherriesadmin.controller;

import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.OrdSchedulingCancel;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSchedulingCancelWrapper;
import com.yiyihealth.cherriesadmin.service.OrdSchedulingCancelService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 停诊表(OrdSchedulingCancel)表控制层
 *
 * @author chen
 * @since 2020-09-25 11:00:23
 */
@Api(tags = "停诊表类")
@RestController
@RequestMapping("ordSchedulingCancel")
public class OrdSchedulingCancelController extends BaseController<OrdSchedulingCancel, OrdSchedulingCancelWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private OrdSchedulingCancelService ordSchedulingCancelService;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(ordSchedulingCancelService);
    }



}
