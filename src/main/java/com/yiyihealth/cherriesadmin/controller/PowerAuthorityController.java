package com.yiyihealth.cherriesadmin.controller;

import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.PowerAuthority;
import com.yiyihealth.cherriesadmin.model.wrapper.PowerAuthorityWrapper;
import com.yiyihealth.cherriesadmin.service.PowerAuthorityService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 菜单表 路由和一级菜单图标 按钮(PowerAuthority)表控制层
 *
 * @author chen
 * @since 2020-09-14 20:03:48
 */
@Api(tags = "菜单表类")
@RestController
@RequestMapping("powerAuthority")
public class PowerAuthorityController extends BaseController<PowerAuthority, PowerAuthorityWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private PowerAuthorityService powerAuthorityService;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(powerAuthorityService);
    }


}
