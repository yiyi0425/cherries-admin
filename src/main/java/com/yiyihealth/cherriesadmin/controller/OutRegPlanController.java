package com.yiyihealth.cherriesadmin.controller;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.yiyihealth.cherriesadmin.core.http.HttpResult;
import com.yiyihealth.cherriesadmin.core.page.PageRequest;
import com.yiyihealth.cherriesadmin.core.page.PageUtils;
import com.yiyihealth.cherriesadmin.core.page.Param;
import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.*;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdAppointmentWrapper;
import com.yiyihealth.cherriesadmin.model.wrapper.OutRegPlanWrapper;
import com.yiyihealth.cherriesadmin.model.wrapper.TimeState;
import com.yiyihealth.cherriesadmin.service.OrdSchedulingTemplateService;
import com.yiyihealth.cherriesadmin.service.OutRegPlanService;
import com.yiyihealth.cherriesadmin.service.PubDoctorService;
import io.swagger.annotations.Api;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * (OutRegPlan)表控制层
 *
 * @author chen
 * @since 2020-10-13 16:49:16
 */
@Api(tags = "类")
@RestController
@RequestMapping("outRegPlan")
public class OutRegPlanController extends BaseController<OutRegPlan, OutRegPlanWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private OutRegPlanService outRegPlanService;
    @Resource
    private OrdSchedulingTemplateService ordSchedulingTemplateService;
    @Resource
    private PubDoctorService doctorService;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(outRegPlanService);
    }


    @Override
    public HttpResult selectListWrapperPage(@RequestBody PageRequest pageRequest) {
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        Map<String, Object> map = new HashMap<>();
        for (Param param:pageRequest.getParams()){
            if (!StrUtil.isBlank(param.getValue())){
                map.put(param.getName(),param.getValue());
            }
        }
        String doctorName ="";
        if (map.containsKey("doctorName")){
            doctorName = map.get("doctorName").toString();
        }
        PageInfo<OutRegPlanWrapper> pageInfo = outRegPlanService.selectListWrapperPage(map,pageNum,pageSize);
        List<OutRegPlanWrapper> outRegPlanWrapperList = pageInfo.getList();
        List<OutRegPlanWrapper>  outRegPlanWrappers = new ArrayList<>();
        for (OutRegPlanWrapper outRegPlanWrapper:outRegPlanWrapperList){
            map.clear();
            map.put("originalSchedule",outRegPlanWrapper.getSystemid());
            map.put("hospitalId",outRegPlanWrapper.getHospitalId());
            map.put("status","0");
            List<OrdSchedulingTemplate> ordSchedulingTemplateList = ordSchedulingTemplateService.selectList(map);
            if (CollectionUtils.isEmpty(ordSchedulingTemplateList)){
                if (outRegPlanWrapper.getRiqi1() != null &&  outRegPlanWrapper.getRiqi1().equals("√")){
                    outRegPlanWrappers.add(outRegPlanWrapper);
                }else if (outRegPlanWrapper.getRiqi2() != null &&  outRegPlanWrapper.getRiqi2().equals("√")){
                    outRegPlanWrappers.add(outRegPlanWrapper);
                }else if (outRegPlanWrapper.getRiqi3() != null &&  outRegPlanWrapper.getRiqi3().equals("√")){
                    outRegPlanWrappers.add(outRegPlanWrapper);
                }else if (outRegPlanWrapper.getRiqi4() != null &&  outRegPlanWrapper.getRiqi4().equals("√")){
                    outRegPlanWrappers.add(outRegPlanWrapper);
                }else if (outRegPlanWrapper.getRiqi5() != null &&  outRegPlanWrapper.getRiqi5().equals("√")){
                    outRegPlanWrappers.add(outRegPlanWrapper);
                }else if (outRegPlanWrapper.getRiqi6() != null &&  outRegPlanWrapper.getRiqi6().equals("√")){
                    outRegPlanWrappers.add(outRegPlanWrapper);
                }else if (outRegPlanWrapper.getRiqi7() != null &&  outRegPlanWrapper.getRiqi7().equals("√")){
                    outRegPlanWrappers.add(outRegPlanWrapper);
                }else {
                    continue;
                }
            }else {
                OrdSchedulingTemplate schedulingTemplate = ordSchedulingTemplateList.get(0);
                outRegPlanWrapper.setDepId(schedulingTemplate.getDepId());
                outRegPlanWrapper.setDepName(schedulingTemplate.getDepName());
                outRegPlanWrapper.setDoctorId(schedulingTemplate.getDoctorId());
                outRegPlanWrapper.setDoctorName(schedulingTemplate.getDoctorName());
                outRegPlanWrapper.setBinId(schedulingTemplate.getBinId());
                outRegPlanWrapper.setBinName(schedulingTemplate.getBinName());

                PubDoctor doctor = doctorService.selectByPrimaryKey(schedulingTemplate.getDoctorId());
                outRegPlanWrapper.setDoctorSync(doctor.getSync());
                if (Optional.ofNullable(schedulingTemplate.getDoctorPracticeScope()).isPresent()){
                    outRegPlanWrapper.setDoctorPracticeScope(schedulingTemplate.getDoctorPracticeScope());
                }else {
                    outRegPlanWrapper.setDoctorPracticeScope(doctor.getDoctorPracticeScope());
                }
                outRegPlanWrapper.setStart(outRegPlanWrapper.getKaishisj().toLocalTime());
                outRegPlanWrapper.setEnd(outRegPlanWrapper.getJieshusj().toLocalTime());

                //具体状态
                for (OrdSchedulingTemplate ordSchedulingTemplate:ordSchedulingTemplateList){
                    if (ordSchedulingTemplate.getWeek().equals(1)){
                        TimeState timeState = outRegPlanWrapper.getRiqi1Wrapper();
                        if (!Optional.ofNullable(timeState).isPresent()){
                            timeState = new TimeState();
                        }
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(2)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(3)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi1Wrapper(timeState);
                    }else if (ordSchedulingTemplate.getWeek().equals(2)){
                        TimeState timeState = outRegPlanWrapper.getRiqi2Wrapper();
                        if (!Optional.ofNullable(timeState).isPresent()){
                            timeState = new TimeState();
                        }
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(2)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(3)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi2Wrapper(timeState);
                    }else if (ordSchedulingTemplate.getWeek().equals(3)){
                        TimeState timeState = outRegPlanWrapper.getRiqi3Wrapper();
                        if (!Optional.ofNullable(timeState).isPresent()){
                            timeState = new TimeState();
                        }
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(2)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(3)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi3Wrapper(timeState);
                    }else if (ordSchedulingTemplate.getWeek().equals(4)){
                        TimeState timeState = outRegPlanWrapper.getRiqi4Wrapper();
                        if (!Optional.ofNullable(timeState).isPresent()){
                            timeState = new TimeState();
                        }
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(2)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(3)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi4Wrapper(timeState);
                    }else if (ordSchedulingTemplate.getWeek().equals(5)){
                        TimeState timeState = outRegPlanWrapper.getRiqi5Wrapper();
                        if (!Optional.ofNullable(timeState).isPresent()){
                            timeState = new TimeState();
                        }
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(2)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(3)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi5Wrapper(timeState);
                    }else if (ordSchedulingTemplate.getWeek().equals(6)){
                        TimeState timeState = outRegPlanWrapper.getRiqi6Wrapper();
                        if (!Optional.ofNullable(timeState).isPresent()){
                            timeState = new TimeState();
                        }
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(2)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(3)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi6Wrapper(timeState);
                    }else if (ordSchedulingTemplate.getWeek().equals(7)){
                        TimeState timeState = outRegPlanWrapper.getRiqi7Wrapper();
                        if (!Optional.ofNullable(timeState).isPresent()){
                            timeState = new TimeState();
                        }
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(2)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(3)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi7Wrapper(timeState);
                    }
                }
                outRegPlanWrappers.add(outRegPlanWrapper);
            }
        }
        pageInfo.setList(outRegPlanWrappers);
        return HttpResult.success(PageUtils.getPageResult(pageRequest,pageInfo));
    }

    @Override
    public HttpResult selectListWrapper(Map<String, Object> map) {
        List<OutRegPlanWrapper> outRegPlanWrapperList = outRegPlanService.selectListWrapper(map);
        List<OutRegPlanWrapper>  outRegPlanWrappers = new ArrayList<>();
        for (OutRegPlanWrapper outRegPlanWrapper:outRegPlanWrapperList){
            map.clear();
            map.put("originalSchedule",outRegPlanWrapper.getSystemid());
            List<OrdSchedulingTemplate> ordSchedulingTemplateList = ordSchedulingTemplateService.selectList(map);
            if (CollectionUtils.isEmpty(ordSchedulingTemplateList)){
                continue;
            }else {
                OrdSchedulingTemplate schedulingTemplate = ordSchedulingTemplateList.get(0);
                outRegPlanWrapper.setDepId(schedulingTemplate.getDepId());
                outRegPlanWrapper.setDepName(schedulingTemplate.getDepName());
                outRegPlanWrapper.setDoctorId(schedulingTemplate.getDoctorId());
                outRegPlanWrapper.setDoctorName(schedulingTemplate.getDoctorName());
                outRegPlanWrapper.setBinId(schedulingTemplate.getBinId());
                outRegPlanWrapper.setBinName(schedulingTemplate.getBinName());

                PubDoctor doctor = doctorService.selectByPrimaryKey(schedulingTemplate.getDoctorId());
                outRegPlanWrapper.setDoctorSync(doctor.getSync());
                if (Optional.ofNullable(schedulingTemplate.getDoctorPracticeScope()).isPresent()){
                    outRegPlanWrapper.setDoctorPracticeScope(schedulingTemplate.getDoctorPracticeScope());
                }else {
                    outRegPlanWrapper.setDoctorPracticeScope(doctor.getDoctorPracticeScope());
                }
                outRegPlanWrapper.setStart(outRegPlanWrapper.getKaishisj().toLocalTime());
                outRegPlanWrapper.setEnd(outRegPlanWrapper.getJieshusj().toLocalTime());

                //具体状态
                for (OrdSchedulingTemplate ordSchedulingTemplate:ordSchedulingTemplateList){
                    if (ordSchedulingTemplate.getWeek().equals(1)){
                       TimeState timeState = outRegPlanWrapper.getRiqi1Wrapper();
                       if (ordSchedulingTemplate.getTimeState().equals(1)){
                           timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                       }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                           timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                       }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                           timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                       }
                       outRegPlanWrapper.setRiqi1Wrapper(timeState);
                    }else if (ordSchedulingTemplate.getWeek().equals(2)){
                        TimeState timeState = outRegPlanWrapper.getRiqi2Wrapper();
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi2Wrapper(timeState);
                    }else if (ordSchedulingTemplate.getWeek().equals(3)){
                        TimeState timeState = outRegPlanWrapper.getRiqi3Wrapper();
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi3Wrapper(timeState);
                    }else if (ordSchedulingTemplate.getWeek().equals(4)){
                        TimeState timeState = outRegPlanWrapper.getRiqi4Wrapper();
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi4Wrapper(timeState);
                    }else if (ordSchedulingTemplate.getWeek().equals(5)){
                        TimeState timeState = outRegPlanWrapper.getRiqi5Wrapper();
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi5Wrapper(timeState);
                    }else if (ordSchedulingTemplate.getWeek().equals(6)){
                        TimeState timeState = outRegPlanWrapper.getRiqi6Wrapper();
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi6Wrapper(timeState);
                    }else if (ordSchedulingTemplate.getWeek().equals(7)){
                        TimeState timeState = outRegPlanWrapper.getRiqi7Wrapper();
                        if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setSw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setXw(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }else  if (ordSchedulingTemplate.getTimeState().equals(1)){
                            timeState.setYmz(Integer.valueOf(ordSchedulingTemplate.getDoctorPracticeScope()));
                        }
                        outRegPlanWrapper.setRiqi7Wrapper(timeState);
                    }
                }
                outRegPlanWrappers.add(outRegPlanWrapper);
            }
        }
        return HttpResult.success(outRegPlanWrappers);
    }

}
