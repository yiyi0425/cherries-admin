package com.yiyihealth.cherriesadmin.controller;

import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.OrdSourceDetailHistory;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSourceDetailHistoryWrapper;
import com.yiyihealth.cherriesadmin.service.OrdSourceDetailHistoryService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 号源明细信息表(OrdSourceDetailHistory)表控制层
 *
 * @author chen
 * @since 2020-09-14 20:03:48
 */
@Api(tags = "号源信息历史表类")
@RestController
@RequestMapping("ordSourceDetailHistory")
public class OrdSourceDetailHistoryController extends BaseController<OrdSourceDetailHistory, OrdSourceDetailHistoryWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private OrdSourceDetailHistoryService ordSourceDetailHistoryService;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(ordSourceDetailHistoryService);
    }


}
