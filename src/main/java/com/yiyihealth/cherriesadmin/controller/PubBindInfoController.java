package com.yiyihealth.cherriesadmin.controller;

import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.PubBindInfo;
import com.yiyihealth.cherriesadmin.model.wrapper.PubBindInfoWrapper;
import com.yiyihealth.cherriesadmin.service.PubBindInfoService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 诊疗项目套餐表(PubBindInfo)表控制层
 *
 * @author chen
 * @since 2020-09-25 11:00:45
 */
@Api(tags = "诊疗项目套餐表类")
@RestController
@RequestMapping("pubBindInfo")
public class PubBindInfoController extends BaseController<PubBindInfo, PubBindInfoWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private PubBindInfoService pubBindInfoService;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(pubBindInfoService);
    }


}
