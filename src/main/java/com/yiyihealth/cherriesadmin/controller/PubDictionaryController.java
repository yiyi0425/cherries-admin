package com.yiyihealth.cherriesadmin.controller;

import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.PubDictionary;
import com.yiyihealth.cherriesadmin.model.wrapper.PubDictionaryWrapper;
import com.yiyihealth.cherriesadmin.service.PubDictionaryService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 基础字典表(PubDictionary)表控制层
 *
 * @author chen
 * @since 2020-09-25 11:00:53
 */
@Api(tags = "基础字典表类")
@RestController
@RequestMapping("pubDictionary")
public class PubDictionaryController extends BaseController<PubDictionary, PubDictionaryWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private PubDictionaryService pubDictionaryService;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(pubDictionaryService);
    }


}
