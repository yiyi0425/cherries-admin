package com.yiyihealth.cherriesadmin.controller;

import com.yiyihealth.cherriesadmin.core.http.HttpResult;
import com.yiyihealth.cherriesadmin.core.web.BaseController;
import com.yiyihealth.cherriesadmin.model.*;
import com.yiyihealth.cherriesadmin.model.wrapper.OrdSourceDetailWrapper;
import com.yiyihealth.cherriesadmin.service.OrdSchedulingService;
import com.yiyihealth.cherriesadmin.service.OrdSourceDetailService;
import com.yiyihealth.cherriesadmin.service.PubBindInfoService;
import com.yiyihealth.cherriesadmin.service.PubPatientsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * 号源明细信息表(OrdSourceDetail)表控制层
 *
 * @author chen
 * @since 2020-09-14 20:03:48
 */
@Api(tags = "号源信息表类")
@RestController
@RequestMapping("ordSourceDetail")
public class OrdSourceDetailController extends BaseController<OrdSourceDetail, OrdSourceDetailWrapper, Long> {
    /**
     * 服务对象
     */
    @Resource
    private OrdSourceDetailService sourceDetailService;
    @Resource
    private OrdSchedulingService schedulingService;
    @Resource
    private PubPatientsService pubPatientsService;
    @Resource
    private PubBindInfoService bindInfoService;
    @Resource
    private RedisTemplate<Object, Object> redisCacheTemplate;

    @Override
    @InitBinder()
    public void initBinder(WebDataBinder binder) {
        super.setBaseService(sourceDetailService);
    }


    @ApiOperation(value = "H5根据排班ID查询号源明细列表")
    @GetMapping("/selectListBySchId")
    @ResponseBody
    public HttpResult selectListBySchId(@RequestParam Long id,@RequestParam Long patId) throws Exception {
        OrdScheduling scheduling = schedulingService.selectByPrimaryKey(id);
        Map<String,Object> map = new HashMap<>(7);
        map.put("schedulingId",id);
        map.put("status","0");
        PubPatients pubPatients = pubPatientsService.selectByPrimaryKey(patId);
        if (Optional.ofNullable(pubPatients).isPresent()){
            if (Optional.ofNullable(pubPatients.getPatMemGrade()).isPresent()){
                if (pubPatients.getPatMemGrade().equals("1")){
                }else {
                    map.put("type","1");
                }
            }else {
                map.put("type","1");
            }
        }else {
            map.put("type","1");
        }
        String sourceDetailKey = "sourceDetail:" + scheduling.getDoctorId() + ":" + id;
        List<OrdSourceDetail> sourceDetailList = sourceDetailService.selectList(map);
        List<OrdSourceDetail> sourceDetails = new ArrayList<>();
        for (OrdSourceDetail sourceDetail:sourceDetailList){
            if (redisCacheTemplate.opsForSet().isMember(sourceDetailKey,sourceDetail.getId())){
                sourceDetails.add(sourceDetail);
            }
        }
        return HttpResult.success(sourceDetails);
    }

    @ApiOperation(value = "获取医生排班信息")
    @GetMapping("/selectListForOptions")
    @ResponseBody
    public List<OptionItem> selectListForOptions(@RequestParam Long schedulingId) throws Exception {
        return sourceDetailService.selectListForOptions(schedulingId);
    }

    @ResponseBody
    @Override
    public HttpResult selectWrapperByPrimaryKey(@RequestParam Long id) {
        OrdSourceDetailWrapper ordSourceDetailWrapper = sourceDetailService.selectWrapperByPrimaryKey(id);
        OrdScheduling ordScheduling = schedulingService.selectByPrimaryKey(ordSourceDetailWrapper.getSchedulingId());
        PubBindInfo bindInfo = bindInfoService.selectWrapperByPrimaryKey(ordScheduling.getBinId());
        if (!Optional.ofNullable(bindInfo.getGhf()).isPresent()){
            bindInfo.setGhf(0.00);
        }
        if (!Optional.ofNullable(bindInfo.getZlf()).isPresent()){
            bindInfo.setZlf(0.00);
        }
        if (Optional.ofNullable(ordScheduling.getRegistrationFee()).isPresent()&& ordScheduling.getRegistrationFee().equals("1")){
            ordSourceDetailWrapper.setFee(bindInfo.getGhf().toString());
        }else {
            ordSourceDetailWrapper.setFee((bindInfo.getGhf() + bindInfo.getZlf())+"" );
        }
        return HttpResult.success(ordSourceDetailWrapper);
    }
}
