package com.yiyihealth.cherriesadmin.model;


import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.yiyihealth.cherriesadmin.config.LocalDateConverter;
import com.yiyihealth.cherriesadmin.config.NumberConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;

/**
 * 预约订单记录(OrdAppointment)实体类
 *
 * @author chen
 * @since 2020-10-05 17:05:08
 */

@ApiModel(value = "预约订单记录对象(OrdAppointment)")
public class OrdAppointment implements Serializable {
    private static final long serialVersionUID = -39246722974938737L;

    @ExcelIgnore
    private Long id;

    /**
     * 排班编号
     */
    @ApiModelProperty(value = "排班编号")
    @ExcelIgnore
    private Long schedulingId;

    /**
     * 号源明细ID
     */
    @ApiModelProperty(value = "号源明细ID")
    @ExcelIgnore
    private Long sourceDetailId;

    /**
     * 号源编号
     */
    @ApiModelProperty(value = "号源编号")
    @ExcelProperty(value = "号源编号",converter = NumberConverter.class)
    private String serialNumber;

    /**
     * 号源时段
     */
    @ApiModelProperty(value = "号源时段")
    @ExcelIgnore
    private Integer timeState;

    /**
     * 科室编码
     */
    @ExcelIgnore
    @ApiModelProperty(value = "科室编码")
    private Long depId;

    /**
     * 科室名称
     */
    @ApiModelProperty(value = "科室名称")
    @ExcelProperty(value = "科室名称")
    private String depName;

    /**
     * 医生编码
     */
    @ApiModelProperty(value = "医生编码")
    @ExcelIgnore
    private Long doctorId;

    /**
     * 医生姓名
     */
    @ApiModelProperty(value = "医生姓名")
    @ExcelProperty(value = "医生姓名")
    private String doctorName;

    /**
     * 挂号类型编码
     */
    @ApiModelProperty(value = "挂号类型编码")
    @ExcelIgnore
    private Long binId;

    /**
     * 挂号类型名称
     */
    @ApiModelProperty(value = "挂号类型名称")
    @ExcelProperty(value = "挂号类型名称")
    private String binName;

    /**
     * 预约状态(0已预约 1已过期 2已取号 3患者取消 4医生停诊 5后台取消)
     */
    @ApiModelProperty(value = "预约状态(0已预约 1已过期 2已取号 3患者取消 4医生停诊 5后台取消)")
    @ExcelIgnore
    private Long appId;
    @ExcelIgnore
    private Integer appStatus;

    /**
     * 预约类型(0普通预约 1当日预约 )
     */
    @ApiModelProperty(value = "预约类型(0普通预约 1当日预约 )")
    @ExcelIgnore
    private String appType;

    /**
     * 平台患者编码ID
     */
    @ApiModelProperty(value = "平台患者编码ID")
    @ExcelIgnore
    private Long patId;
    @ApiModelProperty(value = "平台患者编码ID")
    @ExcelIgnore
    private Long sufferId;

    /**
     * 病历号
     */
    @ExcelIgnore
    @ApiModelProperty(value = "病历号")
    private String patCardNum;

    @ExcelProperty(value = "就诊人姓名")
    private String patName;

    @ExcelIgnore
    private String patSex;
    @ExcelProperty(value = "手机号")
    private String phone;

    @ExcelProperty(value = "身份证号")
    private String patIdentityNum;

    /**
     * 医保卡类型
     */
    @ApiModelProperty(value = "医保卡类型")
    @ExcelIgnore
    private String cardType;

    /**
     * 服务商编号
     */
    @ApiModelProperty(value = "服务商编号")
    @ExcelIgnore
    private String spNo;

    /**
     * 服务商
     */
    @ExcelProperty(value = "服务商")
    @ApiModelProperty(value = "服务商")
    private String spName;

    /**
     * 就诊时间点
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @ApiModelProperty(value = "就诊时间点")
    @ExcelProperty(value = "就诊时间",converter = LocalDateConverter.class)
    private LocalDate visitDate;

    /**
     * 取号密码
     */
    @ApiModelProperty(value = "取号密码")
    @ExcelProperty(value = "取号密码")
    private String password;

    /**
     * 取号代码
     */
    @ApiModelProperty(value = "取号代码")
    @ExcelIgnore
    private String takeCode;

    /**
     * 取号时间 默认提前30分钟取号
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @ApiModelProperty(value = "取号时间 默认提前30分钟取号")
    @ExcelIgnore
    private LocalDateTime takeTime;

    /**
     * 就诊时间
     */
    @JsonFormat(pattern = "HH:mm")
    @DateTimeFormat(pattern = "HH:mm")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    @ApiModelProperty(value = "就诊时间")
    @ExcelIgnore
    private LocalTime visitTime;

    /**
     * 所属机构
     */
    @ApiModelProperty(value = "所属机构")
    @ExcelIgnore
    private Long hospitalId;

    /**
     * 生成时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @ApiModelProperty(value = "生成时间")

    @ExcelProperty(value = "预约时间",converter = LocalDateConverter.class)
    private LocalDate createDate;

    /**
     * 取消时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @ApiModelProperty(value = "取消时间")
    @ExcelIgnore
    private LocalDateTime cancelDate;



    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @ApiModelProperty(value = "时间戳")
    @ExcelIgnore
    private LocalDateTime timeStamp;
    /**
     * 年龄
     */
    @ApiModelProperty(value = "年龄")
    @ExcelIgnore
    private String age;
    @ExcelIgnore
    private Long sffId;
    @ExcelIgnore
    private Long cancelSffId;

    public Long getCancelSffId() {
        return cancelSffId;
    }

    public void setCancelSffId(Long cancelSffId) {
        this.cancelSffId = cancelSffId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getSchedulingId() {
        return schedulingId;
    }

    public void setSchedulingId(Long schedulingId) {
        this.schedulingId = schedulingId;
    }


    public Long getSourceDetailId() {
        return sourceDetailId;
    }

    public void setSourceDetailId(Long sourceDetailId) {
        this.sourceDetailId = sourceDetailId;
    }


    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }


    public Integer getTimeState() {
        return timeState;
    }

    public void setTimeState(Integer timeState) {
        this.timeState = timeState;
    }


    public Long getDepId() {
        return depId;
    }

    public void setDepId(Long depId) {
        this.depId = depId;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }


    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }


    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }


    public Long getBinId() {
        return binId;
    }

    public void setBinId(Long binId) {
        this.binId = binId;
    }


    public String getBinName() {
        return binName;
    }

    public void setBinName(String binName) {
        this.binName = binName;
    }


    public Integer getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(Integer appStatus) {
        this.appStatus = appStatus;
    }


    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }


    public Long getPatId() {
        return patId;
    }

    public void setPatId(Long patId) {
        this.patId = patId;
    }


    public String getPatCardNum() {
        return patCardNum;
    }

    public void setPatCardNum(String patCardNum) {
        this.patCardNum = patCardNum;
    }


    public String getPatName() {
        return patName;
    }

    public void setPatName(String patName) {
        this.patName = patName;
    }


    public String getPatSex() {
        return patSex;
    }

    public void setPatSex(String patSex) {
        this.patSex = patSex;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getPatIdentityNum() {
        return patIdentityNum;
    }

    public void setPatIdentityNum(String patIdentityNum) {
        this.patIdentityNum = patIdentityNum;
    }


    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }


    public String getSpNo() {
        return spNo;
    }

    public void setSpNo(String spNo) {
        this.spNo = spNo;
    }


    public String getSpName() {
        return spName==null?"":spName;
    }

    public void setSpName(String spName) {
        this.spName = spName==null?"":spName;
    }


    public LocalDate getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(LocalDate visitDate) {
        this.visitDate = visitDate;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getTakeCode() {
        return takeCode;
    }

    public void setTakeCode(String takeCode) {
        this.takeCode = takeCode;
    }


    public LocalDateTime getTakeTime() {
        return takeTime;
    }

    public void setTakeTime(LocalDateTime takeTime) {
        this.takeTime = takeTime;
    }


    public LocalTime getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(LocalTime visitTime) {
        this.visitTime = visitTime;
    }


    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }


    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(LocalDateTime cancelDate) {
        this.cancelDate = cancelDate;
    }


    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public Long getSufferId() {
        return sufferId;
    }

    public void setSufferId(Long sufferId) {
        this.sufferId = sufferId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrdAppointment that = (OrdAppointment) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getSffId() {
        return sffId;
    }

    public void setSffId(Long sffId) {
        this.sffId = sffId;
    }
}
