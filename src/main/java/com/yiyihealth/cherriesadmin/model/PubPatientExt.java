package com.yiyihealth.cherriesadmin.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 省平台患者信息(PubPatientExt)实体类
 *
 * @author chen
 * @since 2020-10-22 10:17:55
 */

@ApiModel(value = "省平台患者信息对象(PubPatientExt)")
public class PubPatientExt implements Serializable {
    private static final long serialVersionUID = -72098120640080706L;

    private Long id;

    /**
     * 患者编号
     */
    @ApiModelProperty(value = "患者编号")
    private String patNo;

    /**
     * 患者姓名
     */
    @ApiModelProperty(value = "患者姓名")
    private String patName;

    /**
     * 性别代码
     */
    @ApiModelProperty(value = "性别代码")
    private String patSex;

    /**
     * 证件类型
     */
    @ApiModelProperty(value = "证件类型")
    private String idcardType;

    /**
     * 证件
     */
    @ApiModelProperty(value = "证件")
    private String idcard;

    /**
     * 医保类型
     */
    @ApiModelProperty(value = "医保类型")
    private String medcardType;

    /**
     * 医保编码
     */
    @ApiModelProperty(value = "医保编码")
    private String medcardNo;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String mobileNo;

    /**
     * 家庭地址
     */
    @ApiModelProperty(value = "家庭地址")
    private String address;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getPatNo() {
        return patNo;
    }

    public void setPatNo(String patNo) {
        this.patNo = patNo;
    }


    public String getPatName() {
        return patName;
    }

    public void setPatName(String patName) {
        this.patName = patName;
    }


    public String getPatSex() {
        return patSex;
    }

    public void setPatSex(String patSex) {
        this.patSex = patSex;
    }


    public String getIdcardType() {
        return idcardType;
    }

    public void setIdcardType(String idcardType) {
        this.idcardType = idcardType;
    }


    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }


    public String getMedcardType() {
        return medcardType;
    }

    public void setMedcardType(String medcardType) {
        this.medcardType = medcardType;
    }


    public String getMedcardNo() {
        return medcardNo;
    }

    public void setMedcardNo(String medcardNo) {
        this.medcardNo = medcardNo;
    }


    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
