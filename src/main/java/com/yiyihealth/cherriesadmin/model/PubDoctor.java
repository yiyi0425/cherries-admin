package com.yiyihealth.cherriesadmin.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 医疗机构与人员信息关系表(PubDoctor)实体类
 *
 * @author chen
 * @since 2020-10-28 17:07:21
 */

@ApiModel(value = "医疗机构与人员信息关系表对象(PubDoctor)")
public class PubDoctor implements Serializable {
    private static final long serialVersionUID = 369349913221595025L;

    private Long id;

    /**
     * 医生姓名
     */
    @ApiModelProperty(value = "医生姓名")
    private String doctorName;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private String doctorSex;

    /**
     * 出生年月
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @ApiModelProperty(value = "出生年月")
    private LocalDate birthday;

    /**
     * 所属科室
     */
    @ApiModelProperty(value = "所属科室")
    private Long depId;

    /**
     * 专业类别
     */
    @ApiModelProperty(value = "专业类别")
    private Integer doctorSpeciality;



    /**
     * 医生职称
     */
    @ApiModelProperty(value = "医生职称")
    private Long doctorProfessional;

    /**
     * 参加工作时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @ApiModelProperty(value = "参加工作时间")
    private LocalDateTime doctorWorkingTimes;

    /**
     * 医生状态(0-有效 1-无效)
     */
    @ApiModelProperty(value = "医生状态(0-有效 1-无效)")
    private String status;

    /**
     * 所属医院
     */
    @ApiModelProperty(value = "所属医院")
    private Long hospitalId;

    /**
     * 身份证号
     */
    @ApiModelProperty(value = "身份证号")
    private String patIdentityNum;

    /**
     * 医师执业证书编码
     */
    @ApiModelProperty(value = "医师执业证书编码")
    private String doctorPracticeCode;

    /**
     * 医师级别
     */
    @ApiModelProperty(value = "医师级别")
    private Long doctorDocGrade;

    /**
     * 预约开放渠道 0全部 1自有 2 12580
     */
    @ApiModelProperty(value = "预约开放渠道")
    private String doctorPracticeScope;

    /**
     * 同步
     */
    private String sync;

    /**
     * 执业科别
     */
    @ApiModelProperty(value = "执业科别")
    private String doctorPracticeDivision;

    /**
     * 医师资格证书编码
     */
    @ApiModelProperty(value = "医师资格证书编码")
    private String doctorQualificationCode;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String doctorSummary;

    /**
     * 医生擅长信息
     */
    @ApiModelProperty(value = "医生擅长信息")
    private String doctorGoodat;

    /**
     * 医院内码
     */
    @ApiModelProperty(value = "医院内码")
    private String doctorCode;

    /**
     * 图片地址
     */
    @ApiModelProperty(value = "图片地址")
    private String imageUrl;

    /**
     * 更新状态
     */
    @ApiModelProperty(value = "更新状态")
    private String updateStatus;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime timeStamp;

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }


    public String getDoctorSex() {
        return doctorSex;
    }

    public void setDoctorSex(String doctorSex) {
        this.doctorSex = doctorSex;
    }


    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }


    public Long getDepId() {
        return depId;
    }

    public void setDepId(Long depId) {
        this.depId = depId;
    }


    public Integer getDoctorSpeciality() {
        return doctorSpeciality;
    }

    public void setDoctorSpeciality(Integer doctorSpeciality) {
        this.doctorSpeciality = doctorSpeciality;
    }


    public Long getDoctorProfessional() {
        return doctorProfessional;
    }

    public void setDoctorProfessional(Long doctorProfessional) {
        this.doctorProfessional = doctorProfessional;
    }

    public LocalDateTime getDoctorWorkingTimes() {
        return doctorWorkingTimes;
    }

    public void setDoctorWorkingTimes(LocalDateTime doctorWorkingTimes) {
        this.doctorWorkingTimes = doctorWorkingTimes;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }


    public String getPatIdentityNum() {
        return patIdentityNum;
    }

    public void setPatIdentityNum(String patIdentityNum) {
        this.patIdentityNum = patIdentityNum;
    }


    public String getDoctorPracticeCode() {
        return doctorPracticeCode;
    }

    public void setDoctorPracticeCode(String doctorPracticeCode) {
        this.doctorPracticeCode = doctorPracticeCode;
    }


    public Long getDoctorDocGrade() {
        return doctorDocGrade;
    }

    public void setDoctorDocGrade(Long doctorDocGrade) {
        this.doctorDocGrade = doctorDocGrade;
    }


    public String getDoctorPracticeScope() {
        return doctorPracticeScope;
    }

    public void setDoctorPracticeScope(String doctorPracticeScope) {
        this.doctorPracticeScope = doctorPracticeScope;
    }


    public String getDoctorPracticeDivision() {
        return doctorPracticeDivision;
    }

    public void setDoctorPracticeDivision(String doctorPracticeDivision) {
        this.doctorPracticeDivision = doctorPracticeDivision;
    }


    public String getDoctorQualificationCode() {
        return doctorQualificationCode;
    }

    public void setDoctorQualificationCode(String doctorQualificationCode) {
        this.doctorQualificationCode = doctorQualificationCode;
    }


    public String getDoctorSummary() {
        return doctorSummary;
    }

    public void setDoctorSummary(String doctorSummary) {
        this.doctorSummary = doctorSummary;
    }


    public String getDoctorGoodat() {
        return doctorGoodat;
    }

    public void setDoctorGoodat(String doctorGoodat) {
        this.doctorGoodat = doctorGoodat;
    }


    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }


    public String getUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(String updateStatus) {
        this.updateStatus = updateStatus;
    }


    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }
}
