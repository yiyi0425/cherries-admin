package com.yiyihealth.cherriesadmin.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 停诊表(OrdSchedulingCancel)实体类
 *
 * @author chen
 * @since 2020-09-25 17:58:44
 */

@ApiModel(value = "停诊表对象(OrdSchedulingCancel)")
public class OrdSchedulingCancel implements Serializable {
    private static final long serialVersionUID = -59029169515041571L;

    private Long id;

    /**
     * 停诊开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @ApiModelProperty(value = "停诊开始时间")
    private LocalDateTime startDate;

    /**
     * 停诊结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @ApiModelProperty(value = "停诊结束时间")
    private LocalDateTime endDate;

    /**
     * 排班时段 早中晚 1 2 3
     */
    @ApiModelProperty(value = "排班时段 早中晚 1 2 3")
    private String timeState;

    /**
     * 停诊设置工号
     */
    @ApiModelProperty(value = "停诊设置工号")
    private Long sffId;

    /**
     * 停诊设置人员
     */
    @ApiModelProperty(value = "停诊设置人员")
    private String sffName;

    /**
     * 停诊原因编号
     */
    @ApiModelProperty(value = "停诊原因编号")
    private String stopServiceNo;

    /**
     * 停诊原因
     */
    @ApiModelProperty(value = "停诊原因")
    private String stopServiceReason;

    /**
     * 排班医生工号
     */
    @ApiModelProperty(value = "排班医生工号")
    private Long doctorId;

    private String week;

    /**
     * 排班医生姓名
     */
    @ApiModelProperty(value = "排班医生姓名")
    private String doctorName;

    /**
     * 科室ID
     */
    @ApiModelProperty(value = "科室ID")
    private Long depId;

    /**
     * 科室名称
     */
    @ApiModelProperty(value = "科室名称")
    private String depName;

    /**
     * 挂号类型名称
     */
    @ApiModelProperty(value = "挂号类型名称")
    private String binName;

    /**
     * 挂号类型ID
     */
    @ApiModelProperty(value = "挂号类型ID")
    private Long binId;

    /**
     * 排班ID
     */
    @ApiModelProperty(value = "排班ID")
    private Long schId;

    /**
     * 0排班正常 1排班停诊
     */
    @ApiModelProperty(value = "0排班正常 1排班停诊")
    private String status;

    private Long hospitalId;

    private Long templateId;

    /**
     * 0正常停诊 1奇数周停诊 2偶数周停诊
     */
    @ApiModelProperty(value = "0正常停诊 1奇数周停诊 2偶数周停诊")
    private String type;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }


    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }


    public String getTimeState() {
        return timeState;
    }

    public void setTimeState(String timeState) {
        this.timeState = timeState;
    }


    public Long getSffId() {
        return sffId;
    }

    public void setSffId(Long sffId) {
        this.sffId = sffId;
    }


    public String getSffName() {
        return sffName;
    }

    public void setSffName(String sffName) {
        this.sffName = sffName;
    }


    public String getStopServiceNo() {
        return stopServiceNo;
    }

    public void setStopServiceNo(String stopServiceNo) {
        this.stopServiceNo = stopServiceNo;
    }


    public String getStopServiceReason() {
        return stopServiceReason;
    }

    public void setStopServiceReason(String stopServiceReason) {
        this.stopServiceReason = stopServiceReason;
    }


    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }


    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }


    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }


    public Long getDepId() {
        return depId;
    }

    public void setDepId(Long depId) {
        this.depId = depId;
    }


    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }


    public String getBinName() {
        return binName;
    }

    public void setBinName(String binName) {
        this.binName = binName;
    }


    public Long getBinId() {
        return binId;
    }

    public void setBinId(Long binId) {
        this.binId = binId;
    }


    public Long getSchId() {
        return schId;
    }

    public void setSchId(Long schId) {
        this.schId = schId;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }


    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
