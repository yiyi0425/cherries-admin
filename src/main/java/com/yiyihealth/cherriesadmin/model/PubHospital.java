package com.yiyihealth.cherriesadmin.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 医疗机构信息表(PubHospital)实体类
 *
 * @author chen
 * @since 2020-09-25 17:58:46
 */

@ApiModel(value = "医疗机构信息表对象(PubHospital)")
public class PubHospital implements Serializable {
    private static final long serialVersionUID = -43090095792857050L;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private Long id;

    /**
     * 机构编码
     */
    @ApiModelProperty(value = "机构编码")
    private String hosCode;

    /**
     * 机构名称
     */
    @ApiModelProperty(value = "机构名称")
    private String hosName;

    /**
     * 拼音码
     */
    @ApiModelProperty(value = "拼音码")
    private String chinaSpell;

    /**
     * 五笔码
     */
    @ApiModelProperty(value = "五笔码")
    private String fiveStroke;

    /**
     * 医院等级
     */
    @ApiModelProperty(value = "医院等级")
    private String hosGrade;

    /**
     * 医疗机构简称
     */
    @ApiModelProperty(value = "医疗机构简称")
    private String hosShortName;

    /**
     * 中间服务地址
     */
    @ApiModelProperty(value = "中间服务地址")
    private String hosRegionalCode;

    /**
     * 医院性质
     */
    @ApiModelProperty(value = "医院性质")
    private String hosType;

    /**
     * 机构状态(0-正常预约1-暂停预约)
     */
    @ApiModelProperty(value = "机构状态(0-正常预约1-暂停预约)")
    private String status;

    /**
     * 医院介绍
     */
    @ApiModelProperty(value = "医院介绍")
    private String description;

    /**
     * 机构联系地址
     */
    @ApiModelProperty(value = "机构联系地址")
    private String hosContactAddress;

    /**
     * 机构联系电话
     */
    @ApiModelProperty(value = "机构联系电话")
    private String hosPhone;

    /**
     * 省平台机构编码
     */
    @ApiModelProperty(value = "省平台机构编码")
    private String hosMarkCode;

    /**
     * 官方网站
     */
    @ApiModelProperty(value = "官方网站")
    private String hosWeb;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getHosCode() {
        return hosCode;
    }

    public void setHosCode(String hosCode) {
        this.hosCode = hosCode;
    }


    public String getHosName() {
        return hosName;
    }

    public void setHosName(String hosName) {
        this.hosName = hosName;
    }


    public String getChinaSpell() {
        return chinaSpell;
    }

    public void setChinaSpell(String chinaSpell) {
        this.chinaSpell = chinaSpell;
    }


    public String getFiveStroke() {
        return fiveStroke;
    }

    public void setFiveStroke(String fiveStroke) {
        this.fiveStroke = fiveStroke;
    }


    public String getHosGrade() {
        return hosGrade;
    }

    public void setHosGrade(String hosGrade) {
        this.hosGrade = hosGrade;
    }


    public String getHosShortName() {
        return hosShortName;
    }

    public void setHosShortName(String hosShortName) {
        this.hosShortName = hosShortName;
    }


    public String getHosRegionalCode() {
        return hosRegionalCode;
    }

    public void setHosRegionalCode(String hosRegionalCode) {
        this.hosRegionalCode = hosRegionalCode;
    }


    public String getHosType() {
        return hosType;
    }

    public void setHosType(String hosType) {
        this.hosType = hosType;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getHosContactAddress() {
        return hosContactAddress;
    }

    public void setHosContactAddress(String hosContactAddress) {
        this.hosContactAddress = hosContactAddress;
    }


    public String getHosPhone() {
        return hosPhone;
    }

    public void setHosPhone(String hosPhone) {
        this.hosPhone = hosPhone;
    }


    public String getHosMarkCode() {
        return hosMarkCode;
    }

    public void setHosMarkCode(String hosMarkCode) {
        this.hosMarkCode = hosMarkCode;
    }


    public String getHosWeb() {
        return hosWeb;
    }

    public void setHosWeb(String hosWeb) {
        this.hosWeb = hosWeb;
    }


}
