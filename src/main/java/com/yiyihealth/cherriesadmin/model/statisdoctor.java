package com.yiyihealth.cherriesadmin.model;

import java.time.LocalDate;

public class statisdoctor {
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getDocname() {
        return docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }

    private LocalDate date;
    private Integer num;
    private String docname;
}
