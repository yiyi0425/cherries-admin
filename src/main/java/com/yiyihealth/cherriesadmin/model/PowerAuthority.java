package com.yiyihealth.cherriesadmin.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 菜单表 路由和一级菜单图标 按钮(PowerAuthority)实体类
 *
 * @author chen
 * @since 2020-09-25 17:58:45
 */

@ApiModel(value = "菜单表 路由和一级菜单图标 按钮对象(PowerAuthority)")
public class PowerAuthority implements Serializable {
    private static final long serialVersionUID = -69763275896122542L;

    /**
     * 菜单序号
     */
    @ApiModelProperty(value = "菜单序号")
    private Long id;

    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    /**
     * 菜单url
     */
    @ApiModelProperty(value = "菜单url")
    private String url;

    /**
     * 菜单图标
     */
    @ApiModelProperty(value = "菜单图标")
    private String ico;

    /**
     * 类型 0 菜单 1 功能按钮
     */
    @ApiModelProperty(value = "类型 0 菜单 1 功能按钮")
    private String type;

    /**
     * 资源描述
     */
    @ApiModelProperty(value = "资源描述")
    private String describe;

    /**
     * 上级菜单的序号
     */
    @ApiModelProperty(value = "上级菜单的序号")
    private Long fatherId;

    /**
     * 菜单等级 0 是一级 1 是二级  3是二级下按钮
     */
    @ApiModelProperty(value = "菜单等级 0 是一级 1 是二级  3是二级下按钮")
    private Integer grade;

    /**
     * 状态  0 启用 1 注销
     */
    @ApiModelProperty(value = "状态  0 启用 1 注销")
    private String state;

    /**
     * 排序，正序
     */
    @ApiModelProperty(value = "排序，正序")
    private Integer sort;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }


    public Long getFatherId() {
        return fatherId;
    }

    public void setFatherId(Long fatherId) {
        this.fatherId = fatherId;
    }


    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }


}
