package com.yiyihealth.cherriesadmin.model;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * @author 10560
 */
@Component
public class OptionItem<T> implements Serializable {
    private T value;
    private String label;
    private List<OptionItem> children;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<OptionItem> getChildren() {
        return children;
    }

    public void setChildren(List<OptionItem> children) {
        this.children = children;
    }
}
