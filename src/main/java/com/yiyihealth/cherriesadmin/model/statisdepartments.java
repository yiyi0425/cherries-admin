package com.yiyihealth.cherriesadmin.model;

import java.time.LocalDate;

public class statisdepartments {
    private LocalDate date;
    private Integer num;
    private String depName;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }
}
