package com.yiyihealth.cherriesadmin.model.his;


import java.io.Serializable;

/**
 * (PaPatientinfoext)实体类
 *
 * @author chen
 * @since 2020-09-29 13:59:46
 */

public class PaPatientinfoext implements Serializable {
    private static final long serialVersionUID = 994509969611455006L;

    private Integer binglilh;

    private String zhiye;

    private String guoji;

    private String minzu;

    private String shenfenzh;

    private String gongzuodw;

    private String lianxiren;

    private String guanxi;

    private String lianxiff;

    private String lianxiyb;

    private String hospitalId;

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Integer getBinglilh() {
        return binglilh;
    }

    public void setBinglilh(Integer binglilh) {
        this.binglilh = binglilh;
    }


    public String getZhiye() {
        return zhiye;
    }

    public void setZhiye(String zhiye) {
        this.zhiye = zhiye;
    }


    public String getGuoji() {
        return guoji;
    }

    public void setGuoji(String guoji) {
        this.guoji = guoji;
    }


    public String getMinzu() {
        return minzu;
    }

    public void setMinzu(String minzu) {
        this.minzu = minzu;
    }


    public String getShenfenzh() {
        return shenfenzh;
    }

    public void setShenfenzh(String shenfenzh) {
        this.shenfenzh = shenfenzh;
    }


    public String getGongzuodw() {
        return gongzuodw;
    }

    public void setGongzuodw(String gongzuodw) {
        this.gongzuodw = gongzuodw;
    }


    public String getLianxiren() {
        return lianxiren;
    }

    public void setLianxiren(String lianxiren) {
        this.lianxiren = lianxiren;
    }


    public String getGuanxi() {
        return guanxi;
    }

    public void setGuanxi(String guanxi) {
        this.guanxi = guanxi;
    }


    public String getLianxiff() {
        return lianxiff;
    }

    public void setLianxiff(String lianxiff) {
        this.lianxiff = lianxiff;
    }


    public String getLianxiyb() {
        return lianxiyb;
    }

    public void setLianxiyb(String lianxiyb) {
        this.lianxiyb = lianxiyb;
    }


}
