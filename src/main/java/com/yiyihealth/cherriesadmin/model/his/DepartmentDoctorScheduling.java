package com.yiyihealth.cherriesadmin.model.his;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * 科室排班查询
 *
 * @Author: 陈煜诚
 * @Date: 2019/6/1 13:55
 * @Version 1.0
 */

@SuppressWarnings("serial")
public class DepartmentDoctorScheduling implements Serializable {
    /**
     * 1. 排班编号 schid 20 N 排班在医院的唯一标识浙江省医院预约诊疗服务平台医院接入规范 web service（V1.0.3）
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private String schid;
    /**
     * 2. 医院编号 orgid 20 N 医院在平台的唯一编号
     */
    private String orgid;
    /**
     * 3. 医生编号 docid 20 Y 医生在医院的唯一标识，普通门诊该字段可以为 空
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private String docid;
    /**
     * 4. 医生姓名 docname 20 Y 普通门诊该字段可以为空
     */
    private String docname;
    /**
     * 5. 医生性别 docsex 2 N 男或女
     */
    private String docsex;
    /**
     * 6. 医生职称 title 3 N 见专业技术职务代码
     */
    private String title;
    /**
     * 7. 科室编号 deptid 20 N 医生所属科室在医院的唯一标识
     */
    private String depId;
    /**
     * 8. 科室名称 deptname 40 N
     */
    private String deptname;
    /**
     * 9. 排班日期 schdate 8 N 日期格式：YYYYMMDD
     */
    private String schdate;
    /**
     * 10. 上下午标志 ampm 1 N 1-上午 2-下午
     */
    private String ampm;
    /**
     * 11. 号源总数 numcount 3 N
     */
    private String numcount;
    /**
     * 12. 可预约数 numremain 3 N 剩余可预约的号源数量
     */
    private String numremain;
    /**
     * 13. 挂号类别 categor 1 N 1-普通 2-专家
     */
    private String categor;
    /**
     * 14. 挂号费 regfee 5 N 单位元
     */
    private String regfee;
    /**
     * 15. 诊疗费（诊查费） fee 5 N 单位元
     */
    private String fee;
    /**
     * 16. 排班状态 schstate 2 N 0-正常 1-停诊
     */
    private String schstate;



    public String getSchid() {
        return schid;
    }

    public void setSchid(String schid) {
        this.schid = schid;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getDocid() {
        return docid;
    }

    public void setDocid(String docid) {
        this.docid = docid;
    }

    public String getDocname() {
        return docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }

    public String getDocsex() {
        return docsex;
    }

    public void setDocsex(String docsex) {
        this.docsex = docsex;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDepId() {
        return depId;
    }

    public void setDepId(String depId) {
        this.depId = depId;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getSchdate() {
        return schdate;
    }

    public void setSchdate(String schdate) {
        this.schdate = schdate;
    }

    public String getAmpm() {
        return ampm;
    }

    public void setAmpm(String ampm) {
        this.ampm = ampm;
    }

    public String getNumcount() {
        return numcount;
    }

    public void setNumcount(String numcount) {
        this.numcount = numcount;
    }

    public String getNumremain() {
        return numremain;
    }

    public void setNumremain(String numremain) {
        this.numremain = numremain;
    }

    public String getCategor() {
        return categor;
    }

    public void setCategor(String categor) {
        this.categor = categor;
    }

    public String getRegfee() {
        return regfee;
    }

    public void setRegfee(String regfee) {
        this.regfee = regfee;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getSchstate() {
        return schstate;
    }

    public void setSchstate(String schstate) {
        this.schstate = schstate;
    }


}
