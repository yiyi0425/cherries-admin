package com.yiyihealth.cherriesadmin.model.his;


import java.io.Serializable;


/**
 * (OutRegisterNet)实体类
 *
 * @author chen
 * @since 2020-11-28 19:01:47
 */

public class OutRegisterNet implements Serializable {
    private static final long serialVersionUID = 184890704784446413L;

    private String serialno;

    private String userid;

    private String jiuzhenrq;

    private String amofpm;

    private String systemid;

    private String passwd;

    private String xuhao;

    private String gysbm;

    private String operatorid;

    private String hosid;

    private String hosname;

    private String canceltime;

    private Integer cancelflag;

    private Integer configflag;

    private String HospitalId;
    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getJiuzhenrq() {
        return jiuzhenrq;
    }

    public void setJiuzhenrq(String jiuzhenrq) {
        this.jiuzhenrq = jiuzhenrq;
    }

    public String getAmofpm() {
        return amofpm;
    }

    public void setAmofpm(String amofpm) {
        this.amofpm = amofpm;
    }

    public String getSystemid() {
        return systemid;
    }

    public void setSystemid(String systemid) {
        this.systemid = systemid;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getXuhao() {
        return xuhao;
    }

    public void setXuhao(String xuhao) {
        this.xuhao = xuhao;
    }

    public String getGysbm() {
        return gysbm;
    }

    public void setGysbm(String gysbm) {
        this.gysbm = gysbm;
    }

    public String getOperatorid() {
        return operatorid;
    }

    public void setOperatorid(String operatorid) {
        this.operatorid = operatorid;
    }

    public String getHosid() {
        return hosid;
    }

    public void setHosid(String hosid) {
        this.hosid = hosid;
    }

    public String getHosname() {
        return hosname;
    }

    public void setHosname(String hosname) {
        this.hosname = hosname;
    }

    public String getCanceltime() {
        return canceltime;
    }

    public void setCanceltime(String canceltime) {
        this.canceltime = canceltime;
    }

    public Integer getCancelflag() {
        return cancelflag;
    }

    public void setCancelflag(Integer cancelflag) {
        this.cancelflag = cancelflag;
    }

    public Integer getConfigflag() {
        return configflag;
    }

    public void setConfigflag(Integer configflag) {
        this.configflag = configflag;
    }

    public String getHospitalId() {
        return HospitalId;
    }

    public void setHospitalId(String hospitalId) {
        HospitalId = hospitalId;
    }
}