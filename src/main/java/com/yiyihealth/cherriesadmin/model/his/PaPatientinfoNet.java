package com.yiyihealth.cherriesadmin.model.his;

import java.io.Serializable;


/**
 * 预约患者信息注册表(PaPatientinfoNet)实体类
 *
 * @author chen
 * @since 2020-09-28 00:04:47
 */
public class PaPatientinfoNet implements Serializable {
    private static final long serialVersionUID = -99244560385344583L;

    private String serialno;

    /**
     * 患者平台ID
     */
    private String userid;

    /**
     * 患者姓名
     */
    private String username;

    /**
     * 性别
     */
    private String usersex;

    /**
     * 证件类型
     */
    private String identitype;

    /**
     * 身份证号码
     */
    private String identityno;

    /**
     * 卡类型
     */
    private String cardtype;

    /**
     * 卡号
     */
    private String cardno;

    /**
     * 电话
     */
    private String telephone;

    /**
     * 地址
     */
    private String addr;

    /**
     * 邮政编码
     */
    private String postcode;

    /**
     * 修改时间
     */
    private String modifytime;

    private String hospitalId;

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsersex() {
        return usersex;
    }

    public void setUsersex(String usersex) {
        this.usersex = usersex;
    }

    public String getIdentitype() {
        return identitype;
    }

    public void setIdentitype(String identitype) {
        this.identitype = identitype;
    }

    public String getIdentityno() {
        return identityno;
    }

    public void setIdentityno(String identityno) {
        this.identityno = identityno;
    }

    public String getCardtype() {
        return cardtype;
    }

    public void setCardtype(String cardtype) {
        this.cardtype = cardtype;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getModifytime() {
        return modifytime;
    }

    public void setModifytime(String modifytime) {
        this.modifytime = modifytime;
    }

}
