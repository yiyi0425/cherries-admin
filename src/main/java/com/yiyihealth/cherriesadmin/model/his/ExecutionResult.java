package com.yiyihealth.cherriesadmin.model.his;

import io.swagger.annotations.ApiModel;

/**
 * @author 10560
 */
@ApiModel(value = "调用HIS存储过程执行结果(ExecutionResult)")
public class ExecutionResult {
    /**
     *  String 见输入消息定义 否
     */
    private String inData;
    /**
     * 输出参数，0-取消成功；1-取消失败 否
     */
    private Integer outResult;
    /**
     * String 如果调用失败则返回错误信息 否
     */
    private String outData;

    public String getInData() {
        return inData;
    }

    public void setInData(String inData) {
        this.inData = inData;
    }

    public Integer getOutResult() {
        return outResult;
    }

    public void setOutResult(Integer outResult) {
        this.outResult = outResult;
    }

    public String getOutData() {
        return outData;
    }

    public void setOutData(String outData) {
        this.outData = outData;
    }
}
