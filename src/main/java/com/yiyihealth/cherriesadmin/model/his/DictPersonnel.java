package com.yiyihealth.cherriesadmin.model.his;

import java.io.Serializable;


/**
 * 职工信息表(DictPersonnel)实体类
 *
 * @author chen
 * @since 2020-09-27 23:49:59
 */
public class DictPersonnel implements Serializable {
    private static final long serialVersionUID = -60120045315825253L;

    /**
     * 职工编码
     */
    private String renyuanbm;

    /**
     * 职工姓名
     */
    private String xingming;

    /**
     * 职工性别
     */
    private String xingbie;

    /**
     * 拼音码
     */
    private String pinyinma;

    /**
     * 五笔码
     */
    private String wubima;

    /**
     * 自定义码
     */
    private String zidingma;

    /**
     * 账号
     */
    private String zhanghao;

    /**
     * 密码
     */
    private String mima;

    /**
     * 科室编码
     */
    private String keshibm;

    /**
     * 核算科室编码
     */
    private String hesuanksbm;

    /**
     * 封锁标志
     */
    private String fengsuobz;

    /**
     * 人员性质1医生
     */
    private Integer renyuanxz;

    private String ybname;

    private Integer yishengzb;

    private Integer transflag;

    /**
     * 医生医师服务编码
     */
    private String ybbm;

    /**
     * 医生职称
     */
    private Integer ybyszc;

    private String hospitalId;

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getRenyuanbm() {
        return renyuanbm;
    }

    public void setRenyuanbm(String renyuanbm) {
        this.renyuanbm = renyuanbm;
    }

    public String getXingming() {
        return xingming;
    }

    public void setXingming(String xingming) {
        this.xingming = xingming;
    }

    public String getXingbie() {
        return xingbie;
    }

    public void setXingbie(String xingbie) {
        this.xingbie = xingbie;
    }

    public String getPinyinma() {
        return pinyinma;
    }

    public void setPinyinma(String pinyinma) {
        this.pinyinma = pinyinma;
    }

    public String getWubima() {
        return wubima;
    }

    public void setWubima(String wubima) {
        this.wubima = wubima;
    }

    public String getZidingma() {
        return zidingma;
    }

    public void setZidingma(String zidingma) {
        this.zidingma = zidingma;
    }

    public String getZhanghao() {
        return zhanghao;
    }

    public void setZhanghao(String zhanghao) {
        this.zhanghao = zhanghao;
    }

    public String getMima() {
        return mima;
    }

    public void setMima(String mima) {
        this.mima = mima;
    }

    public String getKeshibm() {
        return keshibm;
    }

    public void setKeshibm(String keshibm) {
        this.keshibm = keshibm;
    }

    public String getHesuanksbm() {
        return hesuanksbm;
    }

    public void setHesuanksbm(String hesuanksbm) {
        this.hesuanksbm = hesuanksbm;
    }

    public String getFengsuobz() {
        return fengsuobz;
    }

    public void setFengsuobz(String fengsuobz) {
        this.fengsuobz = fengsuobz;
    }

    public Integer getRenyuanxz() {
        return renyuanxz;
    }

    public void setRenyuanxz(Integer renyuanxz) {
        this.renyuanxz = renyuanxz;
    }

    public String getYbname() {
        return ybname;
    }

    public void setYbname(String ybname) {
        this.ybname = ybname;
    }

    public Integer getYishengzb() {
        return yishengzb;
    }

    public void setYishengzb(Integer yishengzb) {
        this.yishengzb = yishengzb;
    }

    public Integer getTransflag() {
        return transflag;
    }

    public void setTransflag(Integer transflag) {
        this.transflag = transflag;
    }

    public String getYbbm() {
        return ybbm;
    }

    public void setYbbm(String ybbm) {
        this.ybbm = ybbm;
    }

    public Integer getYbyszc() {
        return ybyszc;
    }

    public void setYbyszc(Integer ybyszc) {
        this.ybyszc = ybyszc;
    }

}
