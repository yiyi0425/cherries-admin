package com.yiyihealth.cherriesadmin.model.his;

import java.io.Serializable;


/**
 * 科室信息表(DictDepart)实体类
 *
 * @author chen
 * @since 2020-09-27 23:18:59
 */
public class DictDepart implements Serializable {
    private static final long serialVersionUID = 861898423068067726L;

    /**
     * 科室编码
     */
    private String keshibm;

    /**
     * 科室名称
     */
    private String keshimc;

    /**
     * 拼音码
     */
    private String pinyinma;

    /**
     * 五笔码
     */
    private String wubima;

    private String zidingma;

    private Integer xingzhengbz;

    private Integer linchuangbz;

    private Integer yijibz;

    private Integer bingqubz;

    private Integer mzyfbz;

    private Integer zyyfbz;

    /**
     * 门诊标志
     */
    private Integer mzbz;

    /**
     * 药库标志
     */
    private Integer ykbz;

    /**
     * 检验标志
     */
    private Integer jybz;

    private Integer qitabz;

    private Integer zhuxiaobz;

    /**
     * 序号
     */
    private Integer xuhao;

    private Integer quankebz;

    private Integer transflag;

    private String keshiflbm;

    private String keshiflmc;

    private String hospitalId;

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getKeshibm() {
        return keshibm;
    }

    public void setKeshibm(String keshibm) {
        this.keshibm = keshibm;
    }

    public String getKeshimc() {
        return keshimc;
    }

    public void setKeshimc(String keshimc) {
        this.keshimc = keshimc;
    }

    public String getPinyinma() {
        return pinyinma;
    }

    public void setPinyinma(String pinyinma) {
        this.pinyinma = pinyinma;
    }

    public String getWubima() {
        return wubima;
    }

    public void setWubima(String wubima) {
        this.wubima = wubima;
    }

    public String getZidingma() {
        return zidingma;
    }

    public void setZidingma(String zidingma) {
        this.zidingma = zidingma;
    }

    public Integer getXingzhengbz() {
        return xingzhengbz;
    }

    public void setXingzhengbz(Integer xingzhengbz) {
        this.xingzhengbz = xingzhengbz;
    }

    public Integer getLinchuangbz() {
        return linchuangbz;
    }

    public void setLinchuangbz(Integer linchuangbz) {
        this.linchuangbz = linchuangbz;
    }

    public Integer getYijibz() {
        return yijibz;
    }

    public void setYijibz(Integer yijibz) {
        this.yijibz = yijibz;
    }

    public Integer getBingqubz() {
        return bingqubz;
    }

    public void setBingqubz(Integer bingqubz) {
        this.bingqubz = bingqubz;
    }

    public Integer getMzyfbz() {
        return mzyfbz;
    }

    public void setMzyfbz(Integer mzyfbz) {
        this.mzyfbz = mzyfbz;
    }

    public Integer getZyyfbz() {
        return zyyfbz;
    }

    public void setZyyfbz(Integer zyyfbz) {
        this.zyyfbz = zyyfbz;
    }

    public Integer getMzbz() {
        return mzbz;
    }

    public void setMzbz(Integer mzbz) {
        this.mzbz = mzbz;
    }

    public Integer getYkbz() {
        return ykbz;
    }

    public void setYkbz(Integer ykbz) {
        this.ykbz = ykbz;
    }

    public Integer getJybz() {
        return jybz;
    }

    public void setJybz(Integer jybz) {
        this.jybz = jybz;
    }

    public Integer getQitabz() {
        return qitabz;
    }

    public void setQitabz(Integer qitabz) {
        this.qitabz = qitabz;
    }

    public Integer getZhuxiaobz() {
        return zhuxiaobz;
    }

    public void setZhuxiaobz(Integer zhuxiaobz) {
        this.zhuxiaobz = zhuxiaobz;
    }

    public Integer getXuhao() {
        return xuhao;
    }

    public void setXuhao(Integer xuhao) {
        this.xuhao = xuhao;
    }

    public Integer getQuankebz() {
        return quankebz;
    }

    public void setQuankebz(Integer quankebz) {
        this.quankebz = quankebz;
    }

    public Integer getTransflag() {
        return transflag;
    }

    public void setTransflag(Integer transflag) {
        this.transflag = transflag;
    }

    public String getKeshiflbm() {
        return keshiflbm;
    }

    public void setKeshiflbm(String keshiflbm) {
        this.keshiflbm = keshiflbm;
    }

    public String getKeshiflmc() {
        return keshiflmc;
    }

    public void setKeshiflmc(String keshiflmc) {
        this.keshiflmc = keshiflmc;
    }

}
