package com.yiyihealth.cherriesadmin.model.wrapper;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.yiyihealth.cherriesadmin.model.OutRegPlan;
import io.swagger.annotations.ApiModelProperty;
import org.apache.tomcat.jni.Local;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalTime;
import java.util.Objects;

/**
 * (OutRegPlan)实体类
 *
 * @author chen
 * @since 2020-10-13 16:49:17
 */
public class OutRegPlanWrapper extends OutRegPlan {
    private static final long serialVersionUID = 653452546466001139L;

    /**
     * 预约开放渠道 0全部 1自有 2 12580
     */
    @ApiModelProperty(value = "预约开放渠道")
    private String doctorPracticeScope;
    /**
     * 同步
     */
    private String doctorSync;
    /**
     * 所属科室 pub_departments
     */
    private Long depId;

    /**
     * 科室名称
     */
    private String depName;

    /**
     * 挂号类型项目号
     */
    private Long binId;

    /**
     * 挂号类型项名称
     */
    private String binName;

    /**
     * 医生唯一号 pub_staff_info
     */
    private Long doctorId;


    private TimeState riqi1Wrapper;
    private TimeState riqi2Wrapper;
    private TimeState riqi3Wrapper;
    private TimeState riqi4Wrapper;
    private TimeState riqi5Wrapper;
    private TimeState riqi6Wrapper;
    private TimeState riqi7Wrapper;

    @JsonFormat(pattern = "HH:mm")
    @DateTimeFormat(pattern = "HH:mm")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    private LocalTime start;
    @JsonFormat(pattern = "HH:mm")
    @DateTimeFormat(pattern = "HH:mm")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    private LocalTime end;

    public Long getDepId() {
        return depId;
    }

    public void setDepId(Long depId) {
        this.depId = depId;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public Long getBinId() {
        return binId;
    }

    public void setBinId(Long binId) {
        this.binId = binId;
    }

    public String getBinName() {
        return binName;
    }

    public void setBinName(String binName) {
        this.binName = binName;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OutRegPlanWrapper)) return false;
        if (!super.equals(o)) return false;
        OutRegPlanWrapper that = (OutRegPlanWrapper) o;
        return Objects.equals(getDepId(), that.getDepId()) &&
                Objects.equals(getDepName(), that.getDepName()) &&
                Objects.equals(getBinId(), that.getBinId()) &&
                Objects.equals(getBinName(), that.getBinName()) &&
                Objects.equals(getDoctorId(), that.getDoctorId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getDepId(), getDepName(), getBinId(), getBinName(), getDoctorId());
    }

    public String getDoctorPracticeScope() {
        return doctorPracticeScope;
    }

    public void setDoctorPracticeScope(String doctorPracticeScope) {
        this.doctorPracticeScope = doctorPracticeScope;
    }

    public String getDoctorSync() {
        return doctorSync;
    }

    public void setDoctorSync(String doctorSync) {
        this.doctorSync = doctorSync;
    }

    public LocalTime getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    public TimeState getRiqi1Wrapper() {
        return riqi1Wrapper;
    }

    public void setRiqi1Wrapper(TimeState riqi1Wrapper) {
        this.riqi1Wrapper = riqi1Wrapper;
    }

    public TimeState getRiqi2Wrapper() {
        return riqi2Wrapper;
    }

    public void setRiqi2Wrapper(TimeState riqi2Wrapper) {
        this.riqi2Wrapper = riqi2Wrapper;
    }

    public TimeState getRiqi3Wrapper() {
        return riqi3Wrapper;
    }

    public void setRiqi3Wrapper(TimeState riqi3Wrapper) {
        this.riqi3Wrapper = riqi3Wrapper;
    }

    public TimeState getRiqi4Wrapper() {
        return riqi4Wrapper;
    }

    public void setRiqi4Wrapper(TimeState riqi4Wrapper) {
        this.riqi4Wrapper = riqi4Wrapper;
    }

    public TimeState getRiqi5Wrapper() {
        return riqi5Wrapper;
    }

    public void setRiqi5Wrapper(TimeState riqi5Wrapper) {
        this.riqi5Wrapper = riqi5Wrapper;
    }

    public TimeState getRiqi6Wrapper() {
        return riqi6Wrapper;
    }

    public void setRiqi6Wrapper(TimeState riqi6Wrapper) {
        this.riqi6Wrapper = riqi6Wrapper;
    }

    public TimeState getRiqi7Wrapper() {
        return riqi7Wrapper;
    }

    public void setRiqi7Wrapper(TimeState riqi7Wrapper) {
        this.riqi7Wrapper = riqi7Wrapper;
    }
}
