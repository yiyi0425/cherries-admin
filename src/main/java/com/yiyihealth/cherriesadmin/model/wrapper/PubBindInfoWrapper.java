package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.PubBindInfo;

/**
 * 诊疗项目套餐表(PubBindInfo)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:42
 */
public class PubBindInfoWrapper extends PubBindInfo {
    private static final long serialVersionUID = -84924164642558455L;

}
