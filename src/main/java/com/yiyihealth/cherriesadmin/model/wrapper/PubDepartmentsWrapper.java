package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.PubDepartments;

/**
 * 执行科室科室(PubDepartments)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:42
 */
public class PubDepartmentsWrapper extends PubDepartments {
    private static final long serialVersionUID = -52004523851157026L;

    /**
     * 科室医生数
     */
    private Integer doctorNum;

    public Integer getDoctorNum() {
        return doctorNum;
    }

    public void setDoctorNum(Integer doctorNum) {
        this.doctorNum = doctorNum;
    }
}
