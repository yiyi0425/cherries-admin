package com.yiyihealth.cherriesadmin.model.wrapper;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.yiyihealth.cherriesadmin.model.OrdAppointment;

/**
 * 预约订单记录(OrdAppointment)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:42
 */
public class OrdAppointmentWrapper extends OrdAppointment {
    private static final long serialVersionUID = -29741789836446994L;
    @ExcelProperty(value = "状态")
    private String appStatusWrapper;
    @ExcelProperty(value = "星期")
    private String weekWrapper;
    @ExcelProperty(value = "机构名称")
    private String hospitalIdWrapper;
    @ExcelProperty(value = "上下午")
    private String timeStateWrapper;
    @ExcelIgnore
    private String sufferName;

    @ExcelProperty(value = "预约操作员")
    private String sffName;
    @ExcelProperty(value = "取消操作员")
    private String cancelSffName;

    public String getSffName() {
        return sffName;
    }

    public void setSffName(String sffName) {
        this.sffName = sffName;
    }

    public String getCancelSffName() {
        return cancelSffName;
    }

    public void setCancelSffName(String cancelSffName) {
        this.cancelSffName = cancelSffName;
    }

    public String getAppStatusWrapper() {
        return appStatusWrapper;
    }

    public void setAppStatusWrapper(String appStatusWrapper) {
        this.appStatusWrapper = appStatusWrapper;
    }

    public String getWeekWrapper() {
        return weekWrapper;
    }

    public void setWeekWrapper(String weekWrapper) {
        this.weekWrapper = weekWrapper;
    }

    public String getHospitalIdWrapper() {
        return hospitalIdWrapper;
    }

    public void setHospitalIdWrapper(String hospitalIdWrapper) {
        this.hospitalIdWrapper = hospitalIdWrapper;
    }

    public String getSufferName() {
        return sufferName;
    }

    public void setSufferName(String sufferName) {
        this.sufferName = sufferName;
    }

    public String getTimeStateWrapper() {
        return timeStateWrapper;
    }

    public void setTimeStateWrapper(String timeStateWrapper) {
        this.timeStateWrapper = timeStateWrapper;
    }
}
