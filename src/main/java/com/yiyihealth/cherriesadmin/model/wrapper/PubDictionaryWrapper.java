package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.PubDictionary;

/**
 * 基础字典表(PubDictionary)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:43
 */
public class PubDictionaryWrapper extends PubDictionary {
    private static final long serialVersionUID = -94212302139618120L;

}
