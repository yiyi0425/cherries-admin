package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.OrdSchedulingCancel;

/**
 * 停诊表(OrdSchedulingCancel)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:42
 */
public class OrdSchedulingCancelWrapper extends OrdSchedulingCancel {
    private static final long serialVersionUID = 893848731830353148L;

}
