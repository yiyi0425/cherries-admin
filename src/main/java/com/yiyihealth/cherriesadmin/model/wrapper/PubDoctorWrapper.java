package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.PubDoctor;

/**
 * 医疗机构与人员信息关系表
 * (PubDoctor)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:43
 */
public class PubDoctorWrapper extends PubDoctor {
    private static final long serialVersionUID = 954495572403317842L;
    private Integer amount;
    private String depNameWrapper;
    public Integer getAmount() {
        return amount;
    }
    private String doctorProfessionalWrapper;

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getDepNameWrapper() {
        return depNameWrapper;
    }

    public void setDepNameWrapper(String depNameWrapper) {
        this.depNameWrapper = depNameWrapper;
    }

    public String getDoctorProfessionalWrapper() {
        return doctorProfessionalWrapper;
    }
    public void setDoctorProfessionalWrapper(String doctorProfessionalWrapper) {
        this.doctorProfessionalWrapper = doctorProfessionalWrapper;
    }
}
