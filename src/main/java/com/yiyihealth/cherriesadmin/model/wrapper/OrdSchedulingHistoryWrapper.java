package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.OrdSchedulingHistory;

/**
 * 历史排班表(OrdSchedulingHistory)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:42
 */
public class OrdSchedulingHistoryWrapper extends OrdSchedulingHistory {
    private static final long serialVersionUID = -41030957384999190L;

}
