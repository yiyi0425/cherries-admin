package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.PaPatientinfo;

/**
 * HIS人员信息表(PaPatientinfo)实体类
 *
 * @author chen
 * @since 2020-10-11 18:26:35
 */
public class PaPatientinfoWrapper extends PaPatientinfo {
    private static final long serialVersionUID = -29639396121357857L;

}