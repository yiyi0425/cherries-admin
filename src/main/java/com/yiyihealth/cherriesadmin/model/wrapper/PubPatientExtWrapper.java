package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.PubPatientExt;

/**
 * 省平台患者信息(PubPatientExt)实体类
 *
 * @author chen
 * @since 2020-10-22 10:14:34
 */
public class PubPatientExtWrapper extends PubPatientExt {
    private static final long serialVersionUID = -18171455631002428L;

}
