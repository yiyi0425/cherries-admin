package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.OrdSchedulingTemplate;

/**
 * 排版模板(OrdSchedulingTemplate)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:42
 */
public class OrdSchedulingTemplateWrapper extends OrdSchedulingTemplate {
    private static final long serialVersionUID = -29935053977878926L;

}
