package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.OrdSourceDetailHistory;

/**
 * 号源明细信息表(OrdSourceDetailHistory)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:42
 */
public class OrdSourceDetailHistoryWrapper extends OrdSourceDetailHistory {
    private static final long serialVersionUID = 395314470531307288L;

}
