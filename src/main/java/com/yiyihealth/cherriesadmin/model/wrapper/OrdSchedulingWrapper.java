package com.yiyihealth.cherriesadmin.model.wrapper;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.yiyihealth.cherriesadmin.model.OrdScheduling;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * 日常排版(OrdScheduling)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:42
 */
public class OrdSchedulingWrapper extends OrdScheduling {
    private static final long serialVersionUID = -45894649266031883L;
    /**
     * 排班时间（安排医生在那天看病）
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate schedulingDate;
    private String weekWrapper;
    private OrdScheduling schedulingSW;
    private OrdScheduling schedulingXW;
    private OrdScheduling schedulingWJ;

    public LocalDate getSchedulingDate() {
        return schedulingDate;
    }

    public void setSchedulingDate(LocalDate schedulingDate) {
        this.schedulingDate = schedulingDate;
    }

    public String getWeekWrapper() {
        return weekWrapper;
    }

    public void setWeekWrapper(String weekWrapper) {
        this.weekWrapper = weekWrapper;
    }

    public OrdScheduling getSchedulingSW() {
        return schedulingSW;
    }

    public void setSchedulingSW(OrdScheduling schedulingSW) {
        this.schedulingSW = schedulingSW;
    }

    public OrdScheduling getSchedulingXW() {
        return schedulingXW;
    }

    public void setSchedulingXW(OrdScheduling schedulingXW) {
        this.schedulingXW = schedulingXW;
    }

    public OrdScheduling getSchedulingWJ() {
        return schedulingWJ;
    }

    public void setSchedulingWJ(OrdScheduling schedulingWJ) {
        this.schedulingWJ = schedulingWJ;
    }
}
