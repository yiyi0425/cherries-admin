package com.yiyihealth.cherriesadmin.model.wrapper;

/**
 * com.yiyihealth.cherriesadmin.model.wrapper
 *
 * @description: TODO
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2020/11/29 16:10
 * @Version 1.0
 **/
public class TimeState {
    private Integer sw;
    private Integer xw;
    private Integer ymz;

    public Integer getSw() {
        return sw;
    }

    public void setSw(Integer sw) {
        this.sw = sw;
    }

    public Integer getXw() {
        return xw;
    }

    public void setXw(Integer xw) {
        this.xw = xw;
    }

    public Integer getYmz() {
        return ymz;
    }

    public void setYmz(Integer ymz) {
        this.ymz = ymz;
    }
}
