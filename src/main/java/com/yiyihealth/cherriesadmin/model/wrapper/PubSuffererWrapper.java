package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.PubSufferer;

import java.util.Objects;

/**
 * (PubSufferer)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:43
 */
public class PubSuffererWrapper extends PubSufferer {
    private static final long serialVersionUID = -60800207007979965L;
    private String patSexWrapper;
    private String sufferName;
    private String phoneWrapper;

    public String getPatSexWrapper() {
        return patSexWrapper;
    }

    public void setPatSexWrapper(String patSexWrapper) {
        this.patSexWrapper = patSexWrapper;
    }

    public String getSufferName() {
        return sufferName;
    }

    public void setSufferName(String sufferName) {
        this.sufferName = sufferName;
    }

    public String getPhoneWrapper() {
        return phoneWrapper;
    }

    public void setPhoneWrapper(String phoneWrapper) {
        this.phoneWrapper = phoneWrapper;
    }


}
