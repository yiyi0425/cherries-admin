package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.PubHospital;

/**
 * 医疗机构信息表(PubHospital)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:43
 */
public class PubHospitalWrapper extends PubHospital {
    private static final long serialVersionUID = -74576852060503917L;

}
