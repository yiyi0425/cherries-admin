package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.OrdSourceDetail;

/**
 * 号源明细信息表(OrdSourceDetail)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:42
 */
public class OrdSourceDetailWrapper extends OrdSourceDetail {
    private static final long serialVersionUID = 858000223776347273L;
    private String hospitalWrapper;

    public String getHospitalWrapper() {
        return hospitalWrapper;
    }
    public void setHospitalWrapper(String hospitalWrapper) {
        this.hospitalWrapper = hospitalWrapper;
    }

    private String weekWrapper;
    private String timeStateWrapper;
    private String fee ;

    public String getTimeStateWrapper() {
        return timeStateWrapper;
    }

    public void setTimeStateWrapper(String timeStateWrapper) {
        this.timeStateWrapper = timeStateWrapper;
    }

    public String getWeekWrapper() {
        return weekWrapper;
    }

    public void setWeekWrapper(String weekWrapper) {
        this.weekWrapper = weekWrapper;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }
}
