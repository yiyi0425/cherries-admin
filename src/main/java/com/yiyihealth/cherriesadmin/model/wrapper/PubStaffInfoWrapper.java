package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.PubStaffInfo;

/**
 * 职工信息表(PubStaffInfo)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:43
 */
public class PubStaffInfoWrapper extends PubStaffInfo {
    private static final long serialVersionUID = -56144305597314517L;

}
