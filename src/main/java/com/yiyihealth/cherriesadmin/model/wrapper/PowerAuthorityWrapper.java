package com.yiyihealth.cherriesadmin.model.wrapper;

import com.yiyihealth.cherriesadmin.model.PowerAuthority;

/**
 * 菜单表 路由和一级菜单图标 按钮(PowerAuthority)实体类
 *
 * @author chen
 * @since 2020-09-14 20:02:42
 */
public class PowerAuthorityWrapper extends PowerAuthority {
    private static final long serialVersionUID = -84437775025373881L;

}
