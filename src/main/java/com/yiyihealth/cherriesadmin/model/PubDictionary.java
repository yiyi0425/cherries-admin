package com.yiyihealth.cherriesadmin.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 基础字典表(PubDictionary)实体类
 *
 * @author chen
 * @since 2020-09-25 17:58:45
 */

@ApiModel(value = "基础字典表对象(PubDictionary)")
public class PubDictionary implements Serializable {
    private static final long serialVersionUID = 685128666647656571L;

    private Long id;

    /**
     * 字典代码
     */
    @ApiModelProperty(value = "字典代码")
    private String dicCode;

    /**
     * 字典类型
     */
    @ApiModelProperty(value = "字典类型")
    private Long dicType;

    /**
     * 字典名称
     */
    @ApiModelProperty(value = "字典名称")
    private String dicName;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer dicSort;

    /**
     * 字典状态
     */
    @ApiModelProperty(value = "字典状态")
    private String dicState;

    /**
     * 字典备注
     */
    @ApiModelProperty(value = "字典备注")
    private String dicSummary;

    /**
     * 拼音码
     */
    @ApiModelProperty(value = "拼音码")
    private String chinaSpell;

    /**
     * 五笔码
     */
    @ApiModelProperty(value = "五笔码")
    private String fiveStroke;

    private Long dicTypenew;

    @ApiModelProperty(value = "健康通国标父ID")
    private Long healthyId;

    List<PubDepartments> departmentsList = new ArrayList<>();

    public List<PubDepartments> getDepartmentsList() {
        return departmentsList;
    }

    public void setDepartmentsList(List<PubDepartments> departmentsList) {
        this.departmentsList = departmentsList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getDicCode() {
        return dicCode;
    }

    public void setDicCode(String dicCode) {
        this.dicCode = dicCode;
    }


    public Long getDicType() {
        return dicType;
    }

    public void setDicType(Long dicType) {
        this.dicType = dicType;
    }


    public String getDicName() {
        return dicName;
    }

    public void setDicName(String dicName) {
        this.dicName = dicName;
    }


    public Integer getDicSort() {
        return dicSort;
    }

    public void setDicSort(Integer dicSort) {
        this.dicSort = dicSort;
    }


    public String getDicState() {
        return dicState;
    }

    public void setDicState(String dicState) {
        this.dicState = dicState;
    }


    public String getDicSummary() {
        return dicSummary;
    }

    public void setDicSummary(String dicSummary) {
        this.dicSummary = dicSummary;
    }


    public String getChinaSpell() {
        return chinaSpell;
    }

    public void setChinaSpell(String chinaSpell) {
        this.chinaSpell = chinaSpell;
    }


    public String getFiveStroke() {
        return fiveStroke;
    }

    public void setFiveStroke(String fiveStroke) {
        this.fiveStroke = fiveStroke;
    }


    public Long getDicTypenew() {
        return dicTypenew;
    }

    public void setDicTypenew(Long dicTypenew) {
        this.dicTypenew = dicTypenew;
    }

    public Long getHealthyId() {
        return healthyId;
    }

    public void setHealthyId(Long healthyId) {
        this.healthyId = healthyId;
    }
}
