package com.yiyihealth.cherriesadmin.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * HIS人员信息表(PaPatientinfo)实体类
 *
 * @author chen
 * @since 2020-10-11 18:26:35
 */
public class PaPatientinfo implements Serializable {
    private static final long serialVersionUID = -49774790331066275L;

    /**
     * 流水号
     */
    private String serialno;

    /**
     * 病历号
     */
    private Integer binglilh;

    /**
     * 病历号
     */
    private String binglihao;

    /**
     * 住院号
     */
    private String zhuyuanhao;

    /**
     * 姓名
     */
    private String xingming;

    /**
     * 性别
     */
    private String xingbie;

    /**
     * 出生年月
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime chushengny;

    /**
     * 婚姻状态
     */
    private String hunyin;

    /**
     * 家庭住址
     */
    private String zhuzhi;

    /**
     * 邮政编码
     */
    private String youzhengbm;

    /**
     * 联系方式
     */
    private String lianxiff;

    /**
     * 病人类别
     */
    private String bingrenlb;

    /**
     * 流水号
     */
    private String leibiebm;

    /**
     * 流水号
     */
    private String jiekoulb;

    /**
     * 流水号
     */
    private String jiekoubm;

    /**
     * 修改日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime xiugaisj;

    /**
     * 就诊卡号
     */
    private String jiuzhenkh;

    /**
     * 流水号
     */
    private String babz;

    /**
     * 流水号
     */
    private String bamc;

    /**
     * openId
     */
    private Long openId;

    private Long Hospitalid;

    public Long getHospitalid() {
        return Hospitalid;
    }

    public void setHospitalid(Long hospitalid) {
        Hospitalid = hospitalid;
    }
    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public Integer getBinglilh() {
        return binglilh;
    }

    public void setBinglilh(Integer binglilh) {
        this.binglilh = binglilh;
    }

    public String getBinglihao() {
        return binglihao;
    }

    public void setBinglihao(String binglihao) {
        this.binglihao = binglihao;
    }

    public String getZhuyuanhao() {
        return zhuyuanhao;
    }

    public void setZhuyuanhao(String zhuyuanhao) {
        this.zhuyuanhao = zhuyuanhao;
    }

    public String getXingming() {
        return xingming;
    }

    public void setXingming(String xingming) {
        this.xingming = xingming;
    }

    public String getXingbie() {
        return xingbie;
    }

    public void setXingbie(String xingbie) {
        this.xingbie = xingbie;
    }

    public LocalDateTime getChushengny() {
        return chushengny;
    }

    public void setChushengny(LocalDateTime chushengny) {
        this.chushengny = chushengny;
    }

    public String getHunyin() {
        return hunyin;
    }

    public void setHunyin(String hunyin) {
        this.hunyin = hunyin;
    }

    public String getZhuzhi() {
        return zhuzhi;
    }

    public void setZhuzhi(String zhuzhi) {
        this.zhuzhi = zhuzhi;
    }

    public String getYouzhengbm() {
        return youzhengbm;
    }

    public void setYouzhengbm(String youzhengbm) {
        this.youzhengbm = youzhengbm;
    }

    public String getLianxiff() {
        return lianxiff;
    }

    public void setLianxiff(String lianxiff) {
        this.lianxiff = lianxiff;
    }

    public String getBingrenlb() {
        return bingrenlb;
    }

    public void setBingrenlb(String bingrenlb) {
        this.bingrenlb = bingrenlb;
    }

    public String getLeibiebm() {
        return leibiebm;
    }

    public void setLeibiebm(String leibiebm) {
        this.leibiebm = leibiebm;
    }

    public String getJiekoulb() {
        return jiekoulb;
    }

    public void setJiekoulb(String jiekoulb) {
        this.jiekoulb = jiekoulb;
    }

    public String getJiekoubm() {
        return jiekoubm;
    }

    public void setJiekoubm(String jiekoubm) {
        this.jiekoubm = jiekoubm;
    }

    public LocalDateTime getXiugaisj() {
        return xiugaisj;
    }

    public void setXiugaisj(LocalDateTime xiugaisj) {
        this.xiugaisj = xiugaisj;
    }

    public String getJiuzhenkh() {
        return jiuzhenkh;
    }

    public void setJiuzhenkh(String jiuzhenkh) {
        this.jiuzhenkh = jiuzhenkh;
    }

    public String getBabz() {
        return babz;
    }

    public void setBabz(String babz) {
        this.babz = babz;
    }

    public String getBamc() {
        return bamc;
    }

    public void setBamc(String bamc) {
        this.bamc = bamc;
    }

    public Long getOpenId() {
        return openId;
    }

    public void setOpenId(Long openId) {
        this.openId = openId;
    }

}