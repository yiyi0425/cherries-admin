package com.yiyihealth.cherriesadmin.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * (OutRegPlan)实体类
 *
 * @author chen
 * @since 2020-10-13 16:49:17
 */

public class OutRegPlan implements Serializable {
    private static final long serialVersionUID = -97430720201383149L;

    /**
     *
     */
    @ApiModelProperty(value = " ")
    private Long systemid;

    /**
     * ICD
     */
    @ApiModelProperty(value = "ICD")
    private String guahaolb;

    /**
     * ICD
     */
    @ApiModelProperty(value = "ICD")
    private String keshibm;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime kaishisj;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime jieshusj;

    private Double guahaofei;

    private Double zhenliaofei;

    /**
     * 标识
     */
    @ApiModelProperty(value = "标识")
    private String riqi1;

    /**
     * 标识
     */
    @ApiModelProperty(value = "标识")
    private String riqi2;

    /**
     * 标识
     */
    @ApiModelProperty(value = "标识")
    private String riqi3;

    /**
     * 标识
     */
    @ApiModelProperty(value = "标识")
    private String riqi4;

    /**
     * 标识
     */
    @ApiModelProperty(value = "标识")
    private String riqi5;

    /**
     * 标识
     */
    @ApiModelProperty(value = "标识")
    private String riqi6;

    /**
     * 标识
     */
    @ApiModelProperty(value = "标识")
    private String riqi7;

    /**
     * 标识
     */
    @ApiModelProperty(value = "标识")
    private Integer xianzhisl;

    /**
     * 标识
     */
    @ApiModelProperty(value = "标识")
    private Integer dangqiansl;

    private String yishengbm;

    /**
     * 标识
     */
    @ApiModelProperty(value = "标识")
    private Integer xuhao;

    /**
     * 标识
     */
    @ApiModelProperty(value = "标识")
    private Integer yysl;

    private Long hospitalId;


    /**
     * 同步
     */
    private String sync;

    /**
     * 医生姓名
     */
    private String doctorName;


    public Long getSystemid() {
        return systemid;
    }

    public void setSystemid(Long systemid) {
        this.systemid = systemid;
    }


    public String getGuahaolb() {
        return guahaolb;
    }

    public void setGuahaolb(String guahaolb) {
        this.guahaolb = guahaolb;
    }


    public String getKeshibm() {
        return keshibm;
    }

    public void setKeshibm(String keshibm) {
        this.keshibm = keshibm;
    }


    public LocalDateTime getKaishisj() {
        return kaishisj;
    }

    public void setKaishisj(LocalDateTime kaishisj) {
        this.kaishisj = kaishisj;
    }


    public LocalDateTime getJieshusj() {
        return jieshusj;
    }

    public void setJieshusj(LocalDateTime jieshusj) {
        this.jieshusj = jieshusj;
    }


    public Double getGuahaofei() {
        return guahaofei;
    }

    public void setGuahaofei(Double guahaofei) {
        this.guahaofei = guahaofei;
    }


    public Double getZhenliaofei() {
        return zhenliaofei;
    }

    public void setZhenliaofei(Double zhenliaofei) {
        this.zhenliaofei = zhenliaofei;
    }


    public String getRiqi1() {
        return riqi1;
    }

    public void setRiqi1(String riqi1) {
        this.riqi1 = riqi1;
    }


    public String getRiqi2() {
        return riqi2;
    }

    public void setRiqi2(String riqi2) {
        this.riqi2 = riqi2;
    }


    public String getRiqi3() {
        return riqi3;
    }

    public void setRiqi3(String riqi3) {
        this.riqi3 = riqi3;
    }


    public String getRiqi4() {
        return riqi4;
    }

    public void setRiqi4(String riqi4) {
        this.riqi4 = riqi4;
    }


    public String getRiqi5() {
        return riqi5;
    }

    public void setRiqi5(String riqi5) {
        this.riqi5 = riqi5;
    }


    public String getRiqi6() {
        return riqi6;
    }

    public void setRiqi6(String riqi6) {
        this.riqi6 = riqi6;
    }


    public String getRiqi7() {
        return riqi7;
    }

    public void setRiqi7(String riqi7) {
        this.riqi7 = riqi7;
    }


    public Integer getXianzhisl() {
        return xianzhisl;
    }

    public void setXianzhisl(Integer xianzhisl) {
        this.xianzhisl = xianzhisl;
    }


    public Integer getDangqiansl() {
        return dangqiansl;
    }

    public void setDangqiansl(Integer dangqiansl) {
        this.dangqiansl = dangqiansl;
    }


    public String getYishengbm() {
        return yishengbm;
    }

    public void setYishengbm(String yishengbm) {
        this.yishengbm = yishengbm;
    }


    public Integer getXuhao() {
        return xuhao;
    }

    public void setXuhao(Integer xuhao) {
        this.xuhao = xuhao;
    }


    public Integer getYysl() {
        return yysl;
    }

    public void setYysl(Integer yysl) {
        this.yysl = yysl;
    }


    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OutRegPlan)) return false;
        OutRegPlan that = (OutRegPlan) o;
        return getSystemid().equals(that.getSystemid()) &&
                getGuahaolb().equals(that.getGuahaolb()) &&
                getKeshibm().equals(that.getKeshibm()) &&
                getDangqiansl().equals(that.getDangqiansl()) &&
                getYishengbm().equals(that.getYishengbm()) &&
                getHospitalId().equals(that.getHospitalId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSystemid(), getGuahaolb(), getKeshibm(), getDangqiansl(), getYishengbm(), getHospitalId());
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }
}
