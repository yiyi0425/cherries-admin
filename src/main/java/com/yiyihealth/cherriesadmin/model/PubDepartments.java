package com.yiyihealth.cherriesadmin.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 执行科室科室(PubDepartments)实体类
 *
 * @author chen
 * @since 2020-10-10 17:14:56
 */

@ApiModel(value = "执行科室科室对象(PubDepartments)")
public class PubDepartments implements Serializable {
    private static final long serialVersionUID = -82435664198954020L;

    private Long id;

    /**
     * 科室名称
     */
    @ApiModelProperty(value = "科室名称")
    private String depName;

    /**
     * 科室别名
     */
    @ApiModelProperty(value = "科室别名")
    private String depAlias;

    /**
     * 父类科室代码
     */
    @ApiModelProperty(value = "父类科室代码")
    private Long depFatherId;

    /**
     * 末级判别
     */
    @ApiModelProperty(value = "末级判别")
    private String depUpstage;

    /**
     * 级次
     */
    @ApiModelProperty(value = "级次")
    private String depLevel;

    /**
     * 拼音码
     */
    @ApiModelProperty(value = "拼音码")
    private String chinaSpell;

    /**
     * 五笔码
     */
    @ApiModelProperty(value = "五笔码")
    private String fiveStroke;

    /**
     * 科室类别
     */
    @ApiModelProperty(value = "科室类别")
    private String depCategory;

    /**
     * 作废判别(1. 停用，0使用)
     */
    @ApiModelProperty(value = "作废判别(1. 停用，0使用)")
    private String status;

    /**
     * 挂号判别
     */
    @ApiModelProperty(value = "挂号判别")
    private String depRegistration;

    /**
     * 门诊判别
     */
    @ApiModelProperty(value = "门诊判别")
    private String depClinic;

    /**
     * 标准编码
     */
    @ApiModelProperty(value = "标准编码")
    private String depCode;

    /**
     * 同步
     */
    private String sync;
    /**
     * 院内科室编码
     */
    @ApiModelProperty(value = "院内科室编码")
    private String depInternalCode;

    /**
     * 科室信息描述(预约平台)
     */
    @ApiModelProperty(value = "科室信息描述(预约平台)")
    private String description;
    /**
     * 更新状态
     */
    @ApiModelProperty(value = "更新状态")
    private String updateStatus;
    /**
     * 就诊机构 医院 pub_hospital
     */
    @ApiModelProperty(value = "就诊机构 医院 pub_hospital")
    private Long hospitalId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime timeStamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }


    public String getDepAlias() {
        return depAlias;
    }

    public void setDepAlias(String depAlias) {
        this.depAlias = depAlias;
    }


    public Long getDepFatherId() {
        return depFatherId;
    }

    public void setDepFatherId(Long depFatherId) {
        this.depFatherId = depFatherId;
    }


    public String getDepUpstage() {
        return depUpstage;
    }

    public void setDepUpstage(String depUpstage) {
        this.depUpstage = depUpstage;
    }


    public String getDepLevel() {
        return depLevel;
    }

    public void setDepLevel(String depLevel) {
        this.depLevel = depLevel;
    }


    public String getChinaSpell() {
        return chinaSpell;
    }

    public void setChinaSpell(String chinaSpell) {
        this.chinaSpell = chinaSpell;
    }


    public String getFiveStroke() {
        return fiveStroke;
    }

    public void setFiveStroke(String fiveStroke) {
        this.fiveStroke = fiveStroke;
    }


    public String getDepCategory() {
        return depCategory;
    }

    public void setDepCategory(String depCategory) {
        this.depCategory = depCategory;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getDepRegistration() {
        return depRegistration;
    }

    public void setDepRegistration(String depRegistration) {
        this.depRegistration = depRegistration;
    }


    public String getDepClinic() {
        return depClinic;
    }

    public void setDepClinic(String depClinic) {
        this.depClinic = depClinic;
    }


    public String getDepCode() {
        return depCode;
    }

    public void setDepCode(String depCode) {
        this.depCode = depCode;
    }


    public String getDepInternalCode() {
        return depInternalCode;
    }

    public void setDepInternalCode(String depInternalCode) {
        this.depInternalCode = depInternalCode;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(String updateStatus) {
        this.updateStatus = updateStatus;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }
}
