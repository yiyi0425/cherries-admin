package com.yiyihealth.cherriesadmin.model;

import java.io.Serializable;


/**
 * 诊疗项目套餐表(PubBindInfo)实体类
 *
 * @author chen
 * @since 2020-10-25 10:37:26
 */
public class PubBindInfo implements Serializable {
    private static final long serialVersionUID = -17485972887008992L;

    private Long id;

    /**
     * 挂号类型编码
     */
    private String binCode;

    /**
     * 挂号类型名称
     */
    private String binName;

    /**
     * 挂号类型类型
     */
    private Integer binType;

    /**
     * 拼音码
     */
    private String chinaSpell;

    /**
     * 五笔码
     */
    private String fiveStroke;

    /**
     * 作废判别
     */
    private String status;

    /**
     * 所属机构
     */
    private Long hospitalId;

    /**
     * 诊疗费
     */
    private Double zlf;

    private Double fee;

    /**
     * 挂号费
     */
    private Double ghf;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBinCode() {
        return binCode;
    }

    public void setBinCode(String binCode) {
        this.binCode = binCode;
    }

    public String getBinName() {
        return binName;
    }

    public void setBinName(String binName) {
        this.binName = binName;
    }

    public Integer getBinType() {
        return binType;
    }

    public void setBinType(Integer binType) {
        this.binType = binType;
    }

    public String getChinaSpell() {
        return chinaSpell;
    }

    public void setChinaSpell(String chinaSpell) {
        this.chinaSpell = chinaSpell;
    }

    public String getFiveStroke() {
        return fiveStroke;
    }

    public void setFiveStroke(String fiveStroke) {
        this.fiveStroke = fiveStroke;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Double getZlf() {
        return zlf;
    }

    public void setZlf(Double zlf) {
        this.zlf = zlf;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Double getGhf() {
        return ghf;
    }

    public void setGhf(Double ghf) {
        this.ghf = ghf;
    }

}