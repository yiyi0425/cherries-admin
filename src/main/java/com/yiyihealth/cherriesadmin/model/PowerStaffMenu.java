package com.yiyihealth.cherriesadmin.model;


import java.io.Serializable;

/**
 * (PowerStaffMenu)实体类
 *
 * @author chen
 * @since 2020-09-25 17:58:45
 */

public class PowerStaffMenu implements Serializable {
    private static final long serialVersionUID = 702281147247235309L;

    private Long id;

    private Long sffId;

    private Long menuId;

    private Integer menuLevel;

    private Long hospitalId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getSffId() {
        return sffId;
    }

    public void setSffId(Long sffId) {
        this.sffId = sffId;
    }


    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }


    public Integer getMenuLevel() {
        return menuLevel;
    }

    public void setMenuLevel(Integer menuLevel) {
        this.menuLevel = menuLevel;
    }


    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }


}
