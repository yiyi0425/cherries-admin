package com.yiyihealth.cherriesadmin.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;


/**
 * 排版模板(OrdSchedulingTemplate)实体类
 *
 * @author chen
 * @since 2020-09-30 00:09:41
 */
public class OrdSchedulingTemplate implements Serializable {
    private static final long serialVersionUID = -35385751335418707L;

    private Long id;

    /**
     * 就诊机构 医院 pub_hospital
     */
    private Long hospitalId;

    /**
     * 所属科室 pub_departments
     */
    private Long depId;

    /**
     * 科室名称
     */
    private String depName;

    /**
     * 挂号类型项目号
     */
    private Long binId;

    /**
     * 挂号类型项名称
     */
    private String binName;

    /**
     * 医生唯一号 pub_staff_info
     */
    private Long doctorId;

    /**
     * 医生姓名
     */
    private String doctorName;

    /**
     * 星期几 依次为1 2 3 4 5 6 7
     */
    private Integer week;

    /**
     * 生成未来几周的排班 默认两周
     */
    private Integer comingWeeks;

    /**
     * 操作人员ID
     */
    private Long sffId;

    /**
     * 号源总数
     */
    private Integer amount;

    /**
     * 排班时段 早中晚 1 2 3
     */
    private Integer timeState;

    /**
     * 门诊开始时间
     */
    @JsonFormat(pattern = "HH:mm")
    @DateTimeFormat(pattern = "HH:mm")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    private LocalTime startTime;

    /**
     * 门诊结束时间
     */
    @JsonFormat(pattern = "HH:mm")
    @DateTimeFormat(pattern = "HH:mm")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    private LocalTime endTime;

    /**
     * 生效日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate effectiveDate;

    /**
     * 截止日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dueDate;

    /**
     * 开放第三方起始号源
     */
    private Integer appointmentAmount;

    /**
     * 开放第三方预约数量
     */
    private Integer appointmentBegin;

    /**
     * 递增数 默认0
     */
    private Integer increase;

    /**
     * 就诊地址
     */
    private String clinicAddress;

    /**
     * 1注销 0使用，2删除
     */
    private String status;

    /**
     * 平均就诊时间
     */
    private Integer averageVisitTime;

    /**
     * 原始排班模板id
     */
    private String originalSchedule;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime timeStamp;


    private String fortnightly;
    private String registrationFee;
    private String tip;

    /**
     * 预约开放渠道 0全部 1自有 2 12580
     */
    @ApiModelProperty(value = "预约开放渠道")
    private String doctorPracticeScope;

    /**
     * 同步
     */
    private String sync;

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Long getDepId() {
        return depId;
    }

    public void setDepId(Long depId) {
        this.depId = depId;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public Long getBinId() {
        return binId;
    }

    public void setBinId(Long binId) {
        this.binId = binId;
    }

    public String getBinName() {
        return binName;
    }

    public void setBinName(String binName) {
        this.binName = binName;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public Long getSffId() {
        return sffId;
    }

    public void setSffId(Long sffId) {
        this.sffId = sffId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getTimeState() {
        return timeState;
    }

    public void setTimeState(Integer timeState) {
        this.timeState = timeState;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getAppointmentAmount() {
        return appointmentAmount;
    }

    public void setAppointmentAmount(Integer appointmentAmount) {
        this.appointmentAmount = appointmentAmount;
    }

    public Integer getAppointmentBegin() {
        return appointmentBegin;
    }

    public void setAppointmentBegin(Integer appointmentBegin) {
        this.appointmentBegin = appointmentBegin;
    }

    public Integer getIncrease() {
        return increase;
    }

    public void setIncrease(Integer increase) {
        this.increase = increase;
    }

    public String getClinicAddress() {
        return clinicAddress;
    }

    public void setClinicAddress(String clinicAddress) {
        this.clinicAddress = clinicAddress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getAverageVisitTime() {
        return averageVisitTime;
    }

    public void setAverageVisitTime(Integer averageVisitTime) {
        this.averageVisitTime = averageVisitTime;
    }

    public String getOriginalSchedule() {
        return originalSchedule;
    }
    public void setOriginalSchedule(String originalSchedule) {
        this.originalSchedule = originalSchedule;
    }

    public Integer getComingWeeks() {
        return comingWeeks;
    }

    public void setComingWeeks(Integer comingWeeks) {
        this.comingWeeks = comingWeeks;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getFortnightly() {
        return fortnightly;
    }

    public void setFortnightly(String fortnightly) {
        this.fortnightly = fortnightly;
    }

    public String getRegistrationFee() {
        return registrationFee;
    }

    public void setRegistrationFee(String registrationFee) {
        this.registrationFee = registrationFee;
    }

    public String getDoctorPracticeScope() {
        return doctorPracticeScope;
    }

    public void setDoctorPracticeScope(String doctorPracticeScope) {
        this.doctorPracticeScope = doctorPracticeScope;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrdSchedulingTemplate)) return false;
        OrdSchedulingTemplate that = (OrdSchedulingTemplate) o;
        return getHospitalId().equals(that.getHospitalId()) &&
                getDepId().equals(that.getDepId()) &&
                getBinId().equals(that.getBinId()) &&
                getDoctorId().equals(that.getDoctorId()) &&
                getWeek().equals(that.getWeek()) &&
                getTimeState().equals(that.getTimeState()) &&
                getOriginalSchedule().equals(that.getOriginalSchedule());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHospitalId(), getDepId(), getBinId(),getDoctorId(),  getWeek(), getTimeState(), getOriginalSchedule());
    }
}
