package com.yiyihealth.cherriesadmin.model;


import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * (PubSufferer)实体类
 *
 * @author chen
 * @since 2020-10-22 18:00:01
 */

public class PubSufferer implements Serializable {
    private static final long serialVersionUID = -75592282138662994L;

    private Long id;

    private Long patId;

    private String patIdentityNum;

    private String patName;

    private String patPhone;

    private Integer patSex;

    /**
     * 病人证号
     */
    @ApiModelProperty(value = "病人证号")
    private String patCardNum;

    /**
     * 就诊机构 医院 pub_hospital
     */
    @ApiModelProperty(value = "就诊机构 医院 pub_hospital")
    private Long hospitalId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getPatId() {
        return patId;
    }

    public void setPatId(Long patId) {
        this.patId = patId;
    }


    public String getPatIdentityNum() {
        return patIdentityNum;
    }

    public void setPatIdentityNum(String patIdentityNum) {
        this.patIdentityNum = patIdentityNum;
    }


    public String getPatName() {
        return patName;
    }

    public void setPatName(String patName) {
        this.patName = patName;
    }


    public String getPatPhone() {
        return patPhone;
    }

    public void setPatPhone(String patPhone) {
        this.patPhone = patPhone;
    }


    public Integer getPatSex() {
        return patSex;
    }

    public void setPatSex(Integer patSex) {
        this.patSex = patSex;
    }


    public String getPatCardNum() {
        return patCardNum;
    }

    public void setPatCardNum(String patCardNum) {
        this.patCardNum = patCardNum;
    }


    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PubSufferer sufferer = (PubSufferer) o;
        return patIdentityNum.equals(sufferer.patIdentityNum) &&
                patName.equals(sufferer.patName) &&
                patPhone.equals(sufferer.patPhone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(patIdentityNum, patName, patPhone);
    }
}
