package com.yiyihealth.cherriesadmin.model;

import java.io.Serializable;

public class ZoneDataCenter  implements Serializable {
    //上周总数
    private Long beforeWeekCount;
    private Long beforeWeekAppCount;
    private Long currentWeekCount;
    private Long currentWeekAppCount;

    public Long getBeforeWeekCount() {
        return beforeWeekCount;
    }

    public void setBeforeWeekCount(Long beforeWeekCount) {
        this.beforeWeekCount = beforeWeekCount;
    }

    public Long getCurrentWeekCount() {
        return currentWeekCount;
    }

    public void setCurrentWeekCount(Long currentWeekCount) {
        this.currentWeekCount = currentWeekCount;
    }

    public Long getBeforeWeekAppCount() {
        return beforeWeekAppCount;
    }

    public void setBeforeWeekAppCount(Long beforeWeekAppCount) {
        this.beforeWeekAppCount = beforeWeekAppCount;
    }

    public Long getCurrentWeekAppCount() {
        return currentWeekAppCount;
    }

    public void setCurrentWeekAppCount(Long currentWeekAppCount) {
        this.currentWeekAppCount = currentWeekAppCount;
    }
}
