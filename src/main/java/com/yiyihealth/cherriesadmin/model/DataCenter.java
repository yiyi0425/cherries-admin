package com.yiyihealth.cherriesadmin.model;

import java.io.Serializable;

public class DataCenter  implements Serializable {
    private Long province;
    private Long office;
    private Long phone;
    private Long web;

    private Long total;
    private Long appTotal;

    private ZoneDataCenter ftZoneDataCenter;
    private ZoneDataCenter fqZoneDataCenter;
    private ZoneDataCenter hpZoneDataCenter;


    public Long getProvince() {
        return province;
    }

    public void setProvince(Long province) {
        this.province = province;
    }

    public Long getOffice() {
        return office;
    }

    public void setOffice(Long office) {
        this.office = office;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public Long getWeb() {
        return web;
    }

    public void setWeb(Long web) {
        this.web = web;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getAppTotal() {
        return appTotal;
    }

    public void setAppTotal(Long appTotal) {
        this.appTotal = appTotal;
    }

    public ZoneDataCenter getFtZoneDataCenter() {
        return ftZoneDataCenter;
    }

    public void setFtZoneDataCenter(ZoneDataCenter ftZoneDataCenter) {
        this.ftZoneDataCenter = ftZoneDataCenter;
    }

    public ZoneDataCenter getFqZoneDataCenter() {
        return fqZoneDataCenter;
    }

    public void setFqZoneDataCenter(ZoneDataCenter fqZoneDataCenter) {
        this.fqZoneDataCenter = fqZoneDataCenter;
    }

    public ZoneDataCenter getHpZoneDataCenter() {
        return hpZoneDataCenter;
    }

    public void setHpZoneDataCenter(ZoneDataCenter hpZoneDataCenter) {
        this.hpZoneDataCenter = hpZoneDataCenter;
    }
}
