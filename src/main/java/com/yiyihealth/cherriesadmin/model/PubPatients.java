package com.yiyihealth.cherriesadmin.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 患者信息表(PubPatients)实体类
 *
 * @author chen
 * @since 2020-09-25 17:58:46
 */

@ApiModel(value = "患者信息表对象(PubPatients)")
public class PubPatients implements Serializable {
    private static final long serialVersionUID = -17652067228328721L;

    private Long id;

    /**
     * 病人证号
     */
    @ApiModelProperty(value = "病人证号")
    private String patCardNum;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String patName;

    /**
     * 性别(1男0女)
     */
    @ApiModelProperty(value = "性别(1男0女)")
    private Integer patSex;

    /**
     * 出生年月
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @ApiModelProperty(value = "出生年月")
    private LocalDateTime patBirthday;

    /**
     * 费用类别
     */
    @ApiModelProperty(value = "费用类别")
    private Long feeId;

    private String feeName;

    /**
     * 身份证号
     */
    @ApiModelProperty(value = "身份证号")
    private String patIdentityNum;

    /**
     * 证件类型
     */
    @ApiModelProperty(value = "证件类型")
    private String patCertifiType;

    /**
     * 证件号码
     */
    @ApiModelProperty(value = "证件号码")
    private String patCertifiNum;

    /**
     * 家庭住址
     */
    @ApiModelProperty(value = "家庭住址")
    private String patFamAddress;

    /**
     * 家庭邮编
     */
    @ApiModelProperty(value = "家庭邮编")
    private String patPostcode;

    /**
     * 联系电话
     */
    @ApiModelProperty(value = "联系电话")
    private String patPhone;

    /**
     * 就职单位名称
     */
    @ApiModelProperty(value = "就职单位名称")
    private String patWorkUnit;

    /**
     * 职业
     */
    @ApiModelProperty(value = "职业")
    private String patOperation;

    /**
     * 国籍
     */
    @ApiModelProperty(value = "国籍")
    private String patCountry;

    /**
     * 民族
     */
    @ApiModelProperty(value = "民族")
    private String patNationality;

    /**
     * 婚姻状况
     */
    @ApiModelProperty(value = "婚姻状况")
    private String patMatrimony;

    /**
     * 建档日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @ApiModelProperty(value = "建档日期")
    private LocalDateTime patRecordDate;

    /**
     * 数据来源
     */
    @ApiModelProperty(value = "数据来源")
    private String dataSource;

    /**
     * 黑名单1，白名单0
     */
    @ApiModelProperty(value = "VIP1，普通0")
    private String patMemGrade;

    /**
     * 黑名单原因
     */
    @ApiModelProperty(value = "黑名单原因")
    private String blacklistReasons;

    /**
     * 分数 默认是100
     */
    @ApiModelProperty(value = "分数 默认是100")
    private Double grade;

    private String password;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime timeStamp;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getPatCardNum() {
        return patCardNum;
    }

    public void setPatCardNum(String patCardNum) {
        this.patCardNum = patCardNum;
    }


    public String getPatName() {
        return patName;
    }

    public void setPatName(String patName) {
        this.patName = patName;
    }


    public Integer getPatSex() {
        return patSex;
    }

    public void setPatSex(Integer patSex) {
        this.patSex = patSex;
    }


    public LocalDateTime getPatBirthday() {
        return patBirthday;
    }

    public void setPatBirthday(LocalDateTime patBirthday) {
        this.patBirthday = patBirthday;
    }


    public Long getFeeId() {
        return feeId;
    }

    public void setFeeId(Long feeId) {
        this.feeId = feeId;
    }


    public String getFeeName() {
        return feeName;
    }

    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }


    public String getPatIdentityNum() {
        return patIdentityNum;
    }

    public void setPatIdentityNum(String patIdentityNum) {
        this.patIdentityNum = patIdentityNum;
    }


    public String getPatCertifiType() {
        return patCertifiType;
    }

    public void setPatCertifiType(String patCertifiType) {
        this.patCertifiType = patCertifiType;
    }


    public String getPatCertifiNum() {
        return patCertifiNum;
    }

    public void setPatCertifiNum(String patCertifiNum) {
        this.patCertifiNum = patCertifiNum;
    }


    public String getPatFamAddress() {
        return patFamAddress;
    }

    public void setPatFamAddress(String patFamAddress) {
        this.patFamAddress = patFamAddress;
    }


    public String getPatPostcode() {
        return patPostcode;
    }

    public void setPatPostcode(String patPostcode) {
        this.patPostcode = patPostcode;
    }


    public String getPatPhone() {
        return patPhone;
    }

    public void setPatPhone(String patPhone) {
        this.patPhone = patPhone;
    }


    public String getPatWorkUnit() {
        return patWorkUnit;
    }

    public void setPatWorkUnit(String patWorkUnit) {
        this.patWorkUnit = patWorkUnit;
    }


    public String getPatOperation() {
        return patOperation;
    }

    public void setPatOperation(String patOperation) {
        this.patOperation = patOperation;
    }


    public String getPatCountry() {
        return patCountry;
    }

    public void setPatCountry(String patCountry) {
        this.patCountry = patCountry;
    }


    public String getPatNationality() {
        return patNationality;
    }

    public void setPatNationality(String patNationality) {
        this.patNationality = patNationality;
    }


    public String getPatMatrimony() {
        return patMatrimony;
    }

    public void setPatMatrimony(String patMatrimony) {
        this.patMatrimony = patMatrimony;
    }


    public LocalDateTime getPatRecordDate() {
        return patRecordDate;
    }

    public void setPatRecordDate(LocalDateTime patRecordDate) {
        this.patRecordDate = patRecordDate;
    }


    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }


    public String getPatMemGrade() {
        return patMemGrade;
    }

    public void setPatMemGrade(String patMemGrade) {
        this.patMemGrade = patMemGrade;
    }


    public String getBlacklistReasons() {
        return blacklistReasons;
    }

    public void setBlacklistReasons(String blacklistReasons) {
        this.blacklistReasons = blacklistReasons;
    }


    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }
}
