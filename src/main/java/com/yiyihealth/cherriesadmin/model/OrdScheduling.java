package com.yiyihealth.cherriesadmin.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

/**
 * 日常排版(OrdScheduling)实体类
 *
 * @author chen
 * @since 2020-09-25 17:58:44
 */

@ApiModel(value = "日常排版对象(OrdScheduling)")
public class OrdScheduling implements Serializable {
    private static final long serialVersionUID = -23209186780627645L;

    @Override
    public String toString() {
        return "OrdScheduling{" +
                "id=" + id +
                ", hospitalId=" + hospitalId +
                ", depId=" + depId +
                ", depName='" + depName + '\'' +
                ", binId=" + binId +
                ", binName='" + binName + '\'' +
                ", doctorId=" + doctorId +
                ", doctorName='" + doctorName + '\'' +
                ", week=" + week +
                ", registrationFee='" + registrationFee + '\'' +
                ", timeState=" + timeState +
                ", schedulingTime=" + schedulingTime +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", appointmentAmount=" + appointmentAmount +
                ", appointmentBegin=" + appointmentBegin +
                ", increase=" + increase +
                ", amount=" + amount +
                ", amountReduce=" + amountReduce +
                ", amountCancel=" + amountCancel +
                ", amountAdd=" + amountAdd +
                ", amountUse=" + amountUse +
                ", amountSurplus=" + amountSurplus +
                ", amountCurrent=" + amountCurrent +
                ", clinicAddress='" + clinicAddress + '\'' +
                ", state='" + state + '\'' +
                ", status='" + status + '\'' +
                ", stopServiceReason='" + stopServiceReason + '\'' +
                ", averageVisitTime=" + averageVisitTime +
                ", templateId=" + templateId +
                ", timeStamp=" + timeStamp +
                ", doctorPracticeScope='" + doctorPracticeScope + '\'' +
                ", originalSchedule='" + originalSchedule + '\'' +
                ", tip='" + tip + '\'' +
                '}';
    }

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID")
    private Long id;

    /**
     * 就诊机构 医院 pub_hospital
     */
    @ApiModelProperty(value = "就诊机构 医院 pub_hospital")
    private Long hospitalId;

    /**
     * 所属科室 pub_departments
     */
    @ApiModelProperty(value = "所属科室 pub_departments")
    private Long depId;

    /**
     * 科室名称
     */
    @ApiModelProperty(value = "科室名称")
    private String depName;

    /**
     * 挂号类型编号
     */
    @ApiModelProperty(value = "挂号类型编号")
    private Long binId;

    /**
     * 挂号类型名称
     */
    @ApiModelProperty(value = "挂号类型名称")
    private String binName;

    /**
     * 医生唯一号 pub_staff_info
     */
    @ApiModelProperty(value = "医生唯一号 pub_staff_info")
    private Long doctorId;

    /**
     * 医生姓名
     */
    @ApiModelProperty(value = "医生姓名")
    private String doctorName;

    /**
     * 星期几 周日1 依次为 2 3 4 5 6 7
     */
    @ApiModelProperty(value = "星期几 周日1 依次为 2 3 4 5 6 7")
    private Integer week;
    private String registrationFee;
    /**
     * 排班时段 早中晚 1 2 3
     */
    @ApiModelProperty(value = "排班时段 早中晚 1 2 3")
    private Integer timeState;

    /**
     * 排班时间（安排医生在那天看病）
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @ApiModelProperty(value = "排班时间（安排医生在那天看病）")
    private LocalDate schedulingTime;

    /**
     * 门诊开始时间
     */
    @JsonFormat(pattern = "HH:mm:ss")
    @DateTimeFormat(pattern = "HH:mm:ss")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    @ApiModelProperty(value = "门诊开始时间")
    private LocalTime startTime;

    /**
     * 门诊结束时间
     */
    @JsonFormat(pattern = "HH:mm:ss")
    @DateTimeFormat(pattern = "HH:mm:ss")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    @ApiModelProperty(value = "门诊结束时间")
    private LocalTime endTime;

    /**
     * 开放第三方起始号源
     */
    @ApiModelProperty(value = "开放第三方起始号源")
    private Integer appointmentAmount;

    /**
     * 开放第三方预约数量
     */
    @ApiModelProperty(value = "开放第三方预约数量")
    private Integer appointmentBegin;

    /**
     * 递增数 默认0
     */
    @ApiModelProperty(value = "递增数 默认0")
    private Integer increase;

    /**
     * 号源总数
     */
    @ApiModelProperty(value = "号源总数")
    private Integer amount;

    /**
     * 退号数量
     */
    @ApiModelProperty(value = "退号数量")
    private Integer amountReduce;

    /**
     * 爽约数量
     */
    @ApiModelProperty(value = "爽约数量")
    private Integer amountCancel;

    /**
     * 加号数量
     */
    @ApiModelProperty(value = "加号数量")
    private Integer amountAdd;

    /**
     * 已挂号数量
     */
    @ApiModelProperty(value = "已挂号数量")
    private Integer amountUse;

    /**
     * 剩余号源
     */
    @ApiModelProperty(value = "剩余号源")
    private Integer amountSurplus;

    /**
     * 当前号
     */
    @ApiModelProperty(value = "当前号")
    private Integer amountCurrent;

    /**
     * 就诊地址
     */
    @ApiModelProperty(value = "就诊地址")
    private String clinicAddress;

    /**
     * 排班状态  0-正常 1-停诊 2结束
     */
    @ApiModelProperty(value = "排班状态  0-正常 1-停诊 2结束")
    private String state;

    /**
     * 1注销0正常
     */
    @ApiModelProperty(value = "1注销0正常")
    private String status;

    /**
     * 停诊原因
     */
    @ApiModelProperty(value = "停诊原因")
    private String stopServiceReason;

    /**
     * 平均就诊时间
     */
    @ApiModelProperty(value = "平均就诊时间")
    private Integer averageVisitTime;

    /**
     * 模板ID
     */
    @ApiModelProperty(value = "模板ID")
    private Long templateId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime timeStamp;


    /**
     * 预约开放渠道 0全部 1自有 2 vip
     */
    @ApiModelProperty(value = "预约开放渠道")
    private String doctorPracticeScope;

    private String originalSchedule;

    public String getDoctorPracticeScope() {
        return doctorPracticeScope;
    }

    public void setDoctorPracticeScope(String doctorPracticeScope) {
        this.doctorPracticeScope = doctorPracticeScope;
    }

    private String tip;

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }


    public Long getDepId() {
        return depId;
    }

    public void setDepId(Long depId) {
        this.depId = depId;
    }


    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }


    public Long getBinId() {
        return binId;
    }

    public void setBinId(Long binId) {
        this.binId = binId;
    }


    public String getBinName() {
        return binName;
    }

    public void setBinName(String binName) {
        this.binName = binName;
    }


    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }


    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }


    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }


    public Integer getTimeState() {
        return timeState;
    }

    public void setTimeState(Integer timeState) {
        this.timeState = timeState;
    }


    public LocalDate getSchedulingTime() {
        return schedulingTime;
    }

    public void setSchedulingTime(LocalDate schedulingTime) {
        this.schedulingTime = schedulingTime;
    }


    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }


    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }


    public Integer getAppointmentAmount() {
        return appointmentAmount;
    }

    public void setAppointmentAmount(Integer appointmentAmount) {
        this.appointmentAmount = appointmentAmount;
    }


    public Integer getAppointmentBegin() {
        return appointmentBegin;
    }

    public void setAppointmentBegin(Integer appointmentBegin) {
        this.appointmentBegin = appointmentBegin;
    }


    public Integer getIncrease() {
        return increase;
    }

    public void setIncrease(Integer increase) {
        this.increase = increase;
    }


    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }


    public Integer getAmountReduce() {
        return amountReduce;
    }

    public void setAmountReduce(Integer amountReduce) {
        this.amountReduce = amountReduce;
    }


    public Integer getAmountCancel() {
        return amountCancel;
    }

    public void setAmountCancel(Integer amountCancel) {
        this.amountCancel = amountCancel;
    }


    public Integer getAmountAdd() {
        return amountAdd;
    }

    public void setAmountAdd(Integer amountAdd) {
        this.amountAdd = amountAdd;
    }


    public Integer getAmountUse() {
        return amountUse;
    }

    public void setAmountUse(Integer amountUse) {
        this.amountUse = amountUse;
    }


    public Integer getAmountSurplus() {
        return amountSurplus;
    }

    public void setAmountSurplus(Integer amountSurplus) {
        this.amountSurplus = amountSurplus;
    }


    public Integer getAmountCurrent() {
        return amountCurrent;
    }

    public void setAmountCurrent(Integer amountCurrent) {
        this.amountCurrent = amountCurrent;
    }


    public String getClinicAddress() {
        return clinicAddress;
    }

    public void setClinicAddress(String clinicAddress) {
        this.clinicAddress = clinicAddress;
    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getStopServiceReason() {
        return stopServiceReason;
    }

    public void setStopServiceReason(String stopServiceReason) {
        this.stopServiceReason = stopServiceReason;
    }


    public Integer getAverageVisitTime() {
        return averageVisitTime;
    }

    public void setAverageVisitTime(Integer averageVisitTime) {
        this.averageVisitTime = averageVisitTime;
    }


    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public String getRegistrationFee() {
        return registrationFee;
    }

    public void setRegistrationFee(String registrationFee) {
        this.registrationFee = registrationFee;
    }

    public String getOriginalSchedule() {
        return originalSchedule;
    }

    public void setOriginalSchedule(String originalSchedule) {
        this.originalSchedule = originalSchedule;
    }
}
