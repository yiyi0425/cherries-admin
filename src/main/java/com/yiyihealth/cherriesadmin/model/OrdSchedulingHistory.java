package com.yiyihealth.cherriesadmin.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;


/**
 * 日常排版(OrdSchedulingHistory)实体类
 *
 * @author chen
 * @since 2020-11-28 11:50:47
 */
public class OrdSchedulingHistory implements Serializable {
    private static final long serialVersionUID = -49506057045915006L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 就诊机构 医院 pub_hospital
     */
    private Long hospitalId;

    /**
     * 所属科室 pub_departments
     */
    private Long depId;

    /**
     * 科室名称
     */
    private String depName;

    /**
     * 挂号类型编号
     */
    private Long binId;

    /**
     * 挂号类型名称
     */
    private String binName;

    /**
     * 医生唯一号 pub_staff_info
     */
    private Long doctorId;

    /**
     * 医生姓名
     */
    private String doctorName;

    /**
     * 星期几 周日1 依次为 2 3 4 5 6 7
     */
    private Integer week;

    /**
     * 排班时段 早中晚 1 2 3
     */
    private Integer timeState;

    /**
     * 排班时间（安排医生在那天看病）
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate schedulingTime;

    /**
     * 门诊开始时间
     */
    @JsonFormat(pattern = "HH:mm:ss")
    @DateTimeFormat(pattern = "HH:mm:ss")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    private LocalTime startTime;

    /**
     * 门诊结束时间
     */
    @JsonFormat(pattern = "HH:mm:ss")
    @DateTimeFormat(pattern = "HH:mm:ss")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    private LocalTime endTime;

    /**
     * 开放第三方起始号源
     */
    private Integer appointmentAmount;

    /**
     * 开放第三方预约数量
     */
    private Integer appointmentBegin;

    /**
     * 递增数 默认0
     */
    private Integer increase;

    /**
     * 号源总数
     */
    private Integer amount;

    /**
     * 退号数量
     */
    private Integer amountReduce;

    /**
     * 爽约数量
     */
    private Integer amountCancel;

    /**
     * 加号数量
     */
    private Integer amountAdd;

    /**
     * 已挂号数量
     */
    private Integer amountUse;

    /**
     * 剩余号源
     */
    private Integer amountSurplus;

    /**
     * 当前号
     */
    private Integer amountCurrent;

    /**
     * 就诊地址
     */
    private String clinicAddress;

    /**
     * 排班状态  0-正常 1-停诊 2结束
     */
    private String state;

    /**
     * 1注销0正常
     */
    private String status;

    /**
     * 停诊原因
     */
    private String stopServiceReason;

    /**
     * 平均就诊时间
     */
    private Integer averageVisitTime;

    /**
     * 模板ID
     */
    private Long templateId;

    /**
     * 只上传挂号费
     */
    private String registrationFee;

    private String tip;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Long getDepId() {
        return depId;
    }

    public void setDepId(Long depId) {
        this.depId = depId;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public Long getBinId() {
        return binId;
    }

    public void setBinId(Long binId) {
        this.binId = binId;
    }

    public String getBinName() {
        return binName;
    }

    public void setBinName(String binName) {
        this.binName = binName;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public Integer getTimeState() {
        return timeState;
    }

    public void setTimeState(Integer timeState) {
        this.timeState = timeState;
    }

    public LocalDate getSchedulingTime() {
        return schedulingTime;
    }

    public void setSchedulingTime(LocalDate schedulingTime) {
        this.schedulingTime = schedulingTime;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Integer getAppointmentAmount() {
        return appointmentAmount;
    }

    public void setAppointmentAmount(Integer appointmentAmount) {
        this.appointmentAmount = appointmentAmount;
    }

    public Integer getAppointmentBegin() {
        return appointmentBegin;
    }

    public void setAppointmentBegin(Integer appointmentBegin) {
        this.appointmentBegin = appointmentBegin;
    }

    public Integer getIncrease() {
        return increase;
    }

    public void setIncrease(Integer increase) {
        this.increase = increase;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getAmountReduce() {
        return amountReduce;
    }

    public void setAmountReduce(Integer amountReduce) {
        this.amountReduce = amountReduce;
    }

    public Integer getAmountCancel() {
        return amountCancel;
    }

    public void setAmountCancel(Integer amountCancel) {
        this.amountCancel = amountCancel;
    }

    public Integer getAmountAdd() {
        return amountAdd;
    }

    public void setAmountAdd(Integer amountAdd) {
        this.amountAdd = amountAdd;
    }

    public Integer getAmountUse() {
        return amountUse;
    }

    public void setAmountUse(Integer amountUse) {
        this.amountUse = amountUse;
    }

    public Integer getAmountSurplus() {
        return amountSurplus;
    }

    public void setAmountSurplus(Integer amountSurplus) {
        this.amountSurplus = amountSurplus;
    }

    public Integer getAmountCurrent() {
        return amountCurrent;
    }

    public void setAmountCurrent(Integer amountCurrent) {
        this.amountCurrent = amountCurrent;
    }

    public String getClinicAddress() {
        return clinicAddress;
    }

    public void setClinicAddress(String clinicAddress) {
        this.clinicAddress = clinicAddress;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStopServiceReason() {
        return stopServiceReason;
    }

    public void setStopServiceReason(String stopServiceReason) {
        this.stopServiceReason = stopServiceReason;
    }

    public Integer getAverageVisitTime() {
        return averageVisitTime;
    }

    public void setAverageVisitTime(Integer averageVisitTime) {
        this.averageVisitTime = averageVisitTime;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public String getRegistrationFee() {
        return registrationFee;
    }

    public void setRegistrationFee(String registrationFee) {
        this.registrationFee = registrationFee;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

}