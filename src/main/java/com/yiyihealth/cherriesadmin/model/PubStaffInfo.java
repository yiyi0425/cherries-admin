package com.yiyihealth.cherriesadmin.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 职工信息表(PubStaffInfo)实体类
 *
 * @author chen
 * @since 2020-09-25 17:58:46
 */

@ApiModel(value = "职工信息表对象(PubStaffInfo)")
public class PubStaffInfo implements Serializable {
    private static final long serialVersionUID = 957590320950150069L;

    private Long id;

    /**
     * 职工姓名
     */
    @ApiModelProperty(value = "职工姓名")
    private String sffName;

    /**
     * 职工性别
     */
    @ApiModelProperty(value = "职工性别")
    private String sffSex;

    /**
     * 证件号码
     */
    @ApiModelProperty(value = "证件号码")
    private String sffCardInfo;

    /**
     * 证件类型
     */
    @ApiModelProperty(value = "证件类型")
    private String sffCardType;

    /**
     * 拼音码
     */
    @ApiModelProperty(value = "拼音码")
    private String chinaSpell;

    /**
     * 五笔码
     */
    @ApiModelProperty(value = "五笔码")
    private String fiveStroke;

    /**
     * 出生年月
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @ApiModelProperty(value = "出生年月")
    private LocalDateTime sffBirthday;

    /**
     * 职工工号
     */
    @ApiModelProperty(value = "职工工号")
    private String sffLoginNum;

    /**
     * 默认输入法(WB：五笔，PY:拼音)
     */
    @ApiModelProperty(value = "默认输入法(WB：五笔，PY:拼音)")
    private String sffInputCode;

    /**
     * 登录密码
     */
    @ApiModelProperty(value = "登录密码")
    private String password;

    private Long hospitalId;

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getSffName() {
        return sffName;
    }

    public void setSffName(String sffName) {
        this.sffName = sffName;
    }


    public String getSffSex() {
        return sffSex;
    }

    public void setSffSex(String sffSex) {
        this.sffSex = sffSex;
    }


    public String getSffCardInfo() {
        return sffCardInfo;
    }

    public void setSffCardInfo(String sffCardInfo) {
        this.sffCardInfo = sffCardInfo;
    }


    public String getSffCardType() {
        return sffCardType;
    }

    public void setSffCardType(String sffCardType) {
        this.sffCardType = sffCardType;
    }


    public String getChinaSpell() {
        return chinaSpell;
    }

    public void setChinaSpell(String chinaSpell) {
        this.chinaSpell = chinaSpell;
    }


    public String getFiveStroke() {
        return fiveStroke;
    }

    public void setFiveStroke(String fiveStroke) {
        this.fiveStroke = fiveStroke;
    }


    public LocalDateTime getSffBirthday() {
        return sffBirthday;
    }

    public void setSffBirthday(LocalDateTime sffBirthday) {
        this.sffBirthday = sffBirthday;
    }


    public String getSffLoginNum() {
        return sffLoginNum;
    }

    public void setSffLoginNum(String sffLoginNum) {
        this.sffLoginNum = sffLoginNum;
    }


    public String getSffInputCode() {
        return sffInputCode;
    }

    public void setSffInputCode(String sffInputCode) {
        this.sffInputCode = sffInputCode;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }


}
