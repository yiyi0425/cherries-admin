package com.yiyihealth.cherriesadmin.model;

import com.alibaba.excel.annotation.ExcelProperty;

import java.time.LocalDate;

public class statis {
    @ExcelProperty(value = "时间")
    private LocalDate date;
    @ExcelProperty(value = "预约量")
    private Integer num;
    @ExcelProperty(value = "医院")
    private Long HisID;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Long getHisID() {
        return HisID;
    }

    public void setHisID(Long hisID) {
        HisID = hisID;
    }
}
