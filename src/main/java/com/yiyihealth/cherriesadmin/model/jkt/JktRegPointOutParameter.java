package com.yiyihealth.cherriesadmin.model.jkt;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * com.yiyihealth.cherriesadmin.model.jkd
 *
 * @description: TODO
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2021/5/20 23:05
 * @Version 1.0
 **/
@Data
@Component
@SuppressWarnings("serial")
public class JktRegPointOutParameter implements Serializable {
    private String orderId;    /*平台的交易编号	Y	字符串	平台唯一挂号交易编号*/
    @Value("${jthealth.hospId}")
    private String hospId;	/*医院编号	Y	字符串	医疗机构唯一标识*/
    private String deptId;    /*科室编号	N	字符串*/
    private String doctorId;    /*医生编号	N	字符串	医生在医院的唯一标识，普通门诊该字段为科室编号。*/
    private String scheduleId;    /*排班编号	N	字符串*/
    private String pointId;    /*号源编号	N	字符串*/
    private String regId;    /*预约挂号编号	Y	字符串	医院唯一*/
    private String regPassword;    /*取号密码	Y	字符串	医院生成 号源有效期内不重复*/
}
