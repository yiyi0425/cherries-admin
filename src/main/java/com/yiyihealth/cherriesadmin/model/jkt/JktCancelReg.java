package com.yiyihealth.cherriesadmin.model.jkt;/**
 * @ClassName JktCancelReg
 * @Description TODO
 * @Author chen
 * @email chen18668070425@163.com
 * @Date 2021/5/25 19:17
 * @Version 1.0
 **/

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 *@ClassName JktCancelReg
 *@Description TODO
 *@Author chen
 *@email chen18668070425@163.com
 *@Date 2021/5/25 19:17
 *@Version 1.0
 **/
@Data
@Component
@SuppressWarnings("serial")
public class JktCancelReg  implements Serializable {
    private Long orderId;  /*平台的交易编号	Y	字符串	平台唯一挂号交易编号*/
    @Value("${jthealth.hospId}")
    private String hospId;	/*医院编号	Y	字符串	医疗机构唯一标识*/
    private Long regId;  /*预约挂号编号	Y	字符串	医院唯一*/
    private String regPassword;    /*取号密码	Y	字符串	医院生成 号源有效期内不重复*/
    private String papersId;  /*证件类别代码	N	字符串*/
    private String papersNo;  /*证件号码	N	字符串*/
    private String phoneNo;  /*联系手机号	N	字符串*/
    private String userName;  /*姓名	N	字符串*/
}
