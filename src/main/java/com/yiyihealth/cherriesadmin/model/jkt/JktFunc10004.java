package com.yiyihealth.cherriesadmin.model.jkt;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * com.yiyihealth.cherriesadmin.model.jkd
 *
 * @description: TODO 每天凌晨1:00-4:00上传科室和专家医生7天内的最新排班情况。若已经上传的排班无变化可以不用重复上传。
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2021/5/20 22:41
 * @Version 1.0
 **/
@Data
@Component
@SuppressWarnings("serial")
public class JktFunc10004 implements Serializable {
    @Value("${jthealth.hospId}")
    private String hospId;    /*医院编号	Y	字符串*/
    @Value("${jthealth.hospName}")
    private String hospName;    /*医院名称	Y	字符串*/
    private String deptId;    /*科室编号	Y	字符串*/
    private String deptName;    /*科室名称	Y	字符串*/
    private String doctorId;    /*医生编号	Y	字符串	医生在医院的唯一标识，普通门诊该字段为科室编号。*/
    private String doctorName;    /*医生名称	Y	字符串	普通门诊该字段为科室名称。*/
    private String scheduleId;    /*排班编号	Y	字符串	排班在医院的唯一标识*/

    private String scheduleTime;    /*排班班次	Y	字符串*/
    private String scheduleDate;    /*排班日期	Y	字符串	yyyyMMdd*/
    private String regTotal;    /*号源总数	Y	数值*/
    private String regNum;    /*剩余号源数	Y	数值*/
    private String regFee;    /*挂号费用	Y	字符串	单位 元*/
    @Value("${targetType:1}")
    private String targetType;    /*挂号目标类型	Y	字符串	0普通号，1专家号*/
    @Value("${sort:1}")
    private String sort;    /*排序	N	数值	从1开始，从小到大*/
    private String scheduleState;    /*排班状态	Y	字符串	见字典*/
    @Value("${labFlag:1}")
    private String labFlag;    /*检验科标识	N	字符串	1-普通 2-检验科。默认1*/
}
