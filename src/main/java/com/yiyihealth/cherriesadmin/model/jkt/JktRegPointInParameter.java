package com.yiyihealth.cherriesadmin.model.jkt;

import cn.hutool.json.JSON;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * com.yiyihealth.cherriesadmin.model.jkd
 *
 * @description: TODO
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2021/5/20 23:05
 * @Version 1.0
 **/
@Data
@Component
@SuppressWarnings("serial")
public class JktRegPointInParameter implements Serializable {
    private String logTraceId ;
    private String bizContent ;

    public String getLogTraceId() {
        return logTraceId;
    }

    public void setLogTraceId(String logTraceId) {
        this.logTraceId = logTraceId;
    }

    public String getBizContent() {
        return bizContent;
    }

    public void setBizContent(String bizContent) {
        this.bizContent = bizContent;
    }
}
