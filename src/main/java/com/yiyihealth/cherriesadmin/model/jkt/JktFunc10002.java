package com.yiyihealth.cherriesadmin.model.jkt;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * com.yiyihealth.cherriesadmin.model.jkd
 *
 * @description: TODO功能描述：科室信息列表上传。
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2021/5/20 22:30
 * @Version 1.0
 **/

@Data
@Component
@SuppressWarnings("serial")
public class JktFunc10002 implements Serializable {
    @Value("${jthealth.hospId}")
    private String hospId;    /*医院编号	Y	字符串*/
    @Value("${jthealth.hospName}")
    private String hospName;    /*医院名称	Y	字符串*/
    private String deptTypeCode;    /*科室分类编号	Y	字符串	见字典*/
    private String deptTypeName;    /*科室分类名称	Y	字符串*/
    private String deptId;    /*科室编号	Y	字符串*/
    private String deptName;    /*科室名称	Y	字符串*/
    private String deptDesc;    //*描述	N	字符串*/
    @Value("${sort:1}")
    private String sort;    /*排序	N	数值	从1开始，从小到大*/
    @Value("${maxAge:0}")
    private String maxAge;    /*最大限制年龄	Y	数值	科室接收患者最大年龄，0为不限制*/
    @Value("${minAge:0}")
    private String minAge;    /*最大限制年龄	Y	数值	科室接收患者最小年龄，0为不限制*/
    private String deptState;    /*科室状态	Y	字符串	0-有效1-无效*/
    private String deptTypeBelong;    /*所属科室名称	Y	字符串*/
    @Value("${labFlag:1}")
    private String labFlag;    /*检验科标识	N	字符串	1-普通 2-检验科。默认1*/
}
