package com.yiyihealth.cherriesadmin.model.jkt;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * com.yiyihealth.cherriesadmin.model.jkd
 *
 * @description: TODO 列表上传停诊通知。
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2021/5/20 22:53
 * @Version 1.0
 **/
@Data
@Component
@SuppressWarnings("serial")
public class JktFunc10006  implements Serializable {
    @Value("${jthealth.hospId}")
    private String hospId;	/*医院编号	Y	字符串*/
    @Value("${jthealth.hospName}")
    private String hospName;	/*医院名称	Y	字符串*/
    private String deptId;	/*科室编号	Y	字符串*/
    private String deptName;	/*科室名称	Y	字符串*/
    private String doctorId;	/*医生编号	Y	字符串	医生在医院的唯一标识，普通门诊该字段为科室编号。*/
    private String doctorName;	/*医生名称	Y	字符串	普通门诊该字段为科室名称。*/
    private String scheduleId;	/*排班编号	Y	字符串*/
    private String scheduleTime;	/*排班班次	Y	字符串	见字典*/
    private String scheduleDate;	/*排班日期	Y	字符串	yyyyMMdd*/
    private String reason;	/*停诊原因	N	字符串*/
}
