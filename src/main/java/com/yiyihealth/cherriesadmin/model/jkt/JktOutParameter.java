package com.yiyihealth.cherriesadmin.model.jkt;

import cn.hutool.json.JSON;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * com.yiyihealth.cherriesadmin.model.jkd
 *
 * @description: TODO
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2021/5/21 2:08
 * @Version 1.0
 **/
@Data
@Component
@SuppressWarnings("serial")
public class JktOutParameter implements Serializable {
    private Integer success;    /*成功标识	Y	数值	1 成功 0失败*/
    private String respCode;    /*描述码	N	字符串*/
    private String respDesc;    /*描述	N	字符串*/
    private String value;    /*扩展对象	N	对象	详见隔行后的字段*/

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespDesc() {
        return respDesc;
    }

    public void setRespDesc(String respDesc) {
        this.respDesc = respDesc;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
