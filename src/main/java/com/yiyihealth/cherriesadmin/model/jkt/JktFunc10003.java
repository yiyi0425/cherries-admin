package com.yiyihealth.cherriesadmin.model.jkt;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * com.yiyihealth.cherriesadmin.model.jkd
 *
 * @description: TODO 接收专家医生列表。
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2021/5/20 22:38
 * @Version 1.0
 **/
@Data
@Component
@SuppressWarnings("serial")
public class JktFunc10003 implements Serializable {
    @Value("${jthealth.hospId}")
    private String hospId;    /*医院编号	Y	字符串*/
    @Value("${jthealth.hospName}")
    private String hospName;    /*医院名称	Y	字符串*/
    private String deptId;    /*科室编号	Y	字符串*/
    private String deptName;    /*科室名称	Y	字符串*/
    private String doctorId;    /*医生编号	Y	字符串*/
    private String doctorName;    /*医生名称	Y	字符串*/
    private String levelCode;    /*医生职称编号	N	字符串	专业技术职务代码*/
    private String doctorDesc;    /*介绍	N	字符串*/
    private String headerUrl;    /*头像链接	N	字符串*/
    private String doctorRemark;    /*擅长	N	字符串*/
    @Value("${sort:1}")
    private String sort;    /*排序	N	数值	从1开始，从小到大*/
    private String doctorState;    /*医生状态	Y	字符串	0-有效1-无效*/
}
