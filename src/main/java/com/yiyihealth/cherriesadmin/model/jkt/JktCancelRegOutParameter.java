package com.yiyihealth.cherriesadmin.model.jkt;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * com.yiyihealth.cherriesadmin.model.jkd
 *
 * @description: TODO
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2021/5/20 23:04
 * @Version 1.0
 **/
@Data
@Component
@SuppressWarnings("serial")
public class JktCancelRegOutParameter implements Serializable {
    private String success;    /*成功标识	Y	数值	1 成功 0失败*/
    private String respCode;    /*描述码	N	字符串*/
    private String respDesc;    /*描述	N	字符串*/
    private String value;    /*扩展对象	N	对象*/
}
