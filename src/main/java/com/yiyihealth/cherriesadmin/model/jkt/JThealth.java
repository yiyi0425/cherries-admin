package com.yiyihealth.cherriesadmin.model.jkt;/**
 * @ClassName Jthealth
 * @Description TODO
 * @Author chen
 * @email chen18668070425@163.com
 * @Date 2021/5/21 9:59
 * @Version 1.0
 **/

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *@ClassName Jthealth
 *@Description TODO
 *@Author chen
 *@email chen18668070425@163.com
 *@Date 2021/5/21 9:59
 *@Version 1.0
 **/

@Data
@Component
public class JThealth {
    @Value("${jthealth.hospId}")
    private String hospId;
    @Value("${jthealth.hospName}")
    private String hospName;
    @Value("${jthealth.hospImgUrl}")
    private String hospImgUrl;
    @Value("${jthealth.merchantId}")
    private String merchantId;
    @Value("${jthealth.publicKey}")
    private String publicKey;
    @Value("${jthealth.privateKey}")
    private String privateKey;
    @Value("${jthealth.serviceAddress}")
    private String serviceAddress;
    @Value("${jthealth.jktServiceAddress}")
    private String jktServiceAddress;
}
