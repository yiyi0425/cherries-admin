package com.yiyihealth.cherriesadmin.model.jkt;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * com.yiyihealth.cherriesadmin.model.jkd
 *
 * @description: TODO 主动实时上传号源最新变化状态情况。
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2021/5/20 22:54
 * @Version 1.0
 **/
@Data
@Component
@SuppressWarnings("serial")
public class JktFunc10009  implements Serializable {
    @Value("${jthealth.hospId}")
    private String hospId;	/*医院编号	Y	字符串*/
    @Value("${jthealth.hospName}")
    private String hospName;	/*医院名称	Y	字符串*/
    private Long deptId;	/*科室编号	Y	字符串*/
    private String deptName;	/*科室名称	Y	字符串*/
    private Long doctorId;	/*医生编号	Y	字符串	医生在医院的唯一标识，普通门诊该字段为科室编号。*/
    private String doctorName;	/*医生名称	Y	字符串	普通门诊该字段为科室名称。*/
    private Long scheduleId;	/*排班编号	Y	字符串	排班在医院的唯一标识*/
    private Long pointId;	/*号源编号	Y	字符串*/
    private String pointNo;	/*序号	Y	数值	上下午班各自从1开始*/
    private String scheduleTime;	/*排班班次	Y	字符串	见字典*/
    private String regStatus;	/*号源状态	Y	字符串	见号源状态*/
    private Long orderId;	/*平台的交易编号	N	字符串	平台唯一挂号交易ID。如果本平台未预约则此字段为空*/
}
