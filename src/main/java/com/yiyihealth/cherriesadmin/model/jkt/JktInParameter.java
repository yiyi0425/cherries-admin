package com.yiyihealth.cherriesadmin.model.jkt;

import cn.hutool.json.JSON;
import org.springframework.stereotype.Component;

import java.io.Serializable;
@Component
public class JktInParameter implements Serializable {

    private String merchantId ;

    private String logTraceId ;

    private String interfaceMethod ;

    private String sign ;

    private JSON bizContent ;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getLogTraceId() {
        return logTraceId;
    }

    public void setLogTraceId(String logTraceId) {
        this.logTraceId = logTraceId;
    }

    public String getInterfaceMethod() {
        return interfaceMethod;
    }

    public void setInterfaceMethod(String interfaceMethod) {
        this.interfaceMethod = interfaceMethod;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public JSON getBizContent() {
        return bizContent;
    }

    public void setBizContent(JSON bizContent) {
        this.bizContent = bizContent;
    }

}
