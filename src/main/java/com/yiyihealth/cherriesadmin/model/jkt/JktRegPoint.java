package com.yiyihealth.cherriesadmin.model.jkt;/**
 * @ClassName JktRegPoint
 * @Description TODO
 * @Author chen
 * @email chen18668070425@163.com
 * @Date 2021/5/24 23:26
 * @Version 1.0
 **/

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 *@ClassName JktRegPoint
 *@Description TODO
 *@Author chen
 *@email chen18668070425@163.com
 *@Date 2021/5/24 23:26
 *@Version 1.0
 **/
@Data
@Component
@SuppressWarnings("serial")
public class JktRegPoint {
    private Long orderId;    /*平台的交易编号	Y	字符串	平台唯一挂号交易编号*/
    private String hospId;	/*医院编号	Y	字符串	医疗机构唯一标识*/
    private Long deptId;    /*科室编号	Y	字符串*/
    private String deptName;    /*科室名称	Y	字符串*/
    private Long doctorId;    /*医生编号	Y	字符串	医生在医院的唯一标识，普通门诊该字段为科室编号。*/
    private String doctorName;    /*医生名称	Y	字符串	普通门诊该字段为科室名称。*/
    private Long scheduleId;    /*排班编号	N	字符串*/
    private Long pointId;    /*号源编号	Y	字符串	院内唯一*/
    private String pointNo;    /*序号	Y	数值*/
    private String scheduleTime;    /*排班班次	Y	字符串	见字典*/
    private String scheduleDate;    /*号源日期	Y	字符串	yyyyMMdd*/
    private String targetType;    /*挂号目标类型	Y	字符串	0普通号，1专家号*/
    private String papersId;    /*证件类别代码	Y	字符串*/
    private String papersNo;    /*证件号码	Y	字符串*/
    private String phoneNo;    /*联系手机号	Y	字符串*/
    private String patientName;    /*就诊人姓名	Y	字符串*/
    private String medCardCode;    /*就诊卡代码	N	字符串	1:市民卡  2:健康卡  9:其他*/
    private String medCardNo;    /*就诊卡号	N	字符串*/
    private String labFlag;    /*检验科标识	N	字符串	1-普通 2-检验科。默认1*/

}
