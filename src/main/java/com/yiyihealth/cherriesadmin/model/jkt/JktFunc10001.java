package com.yiyihealth.cherriesadmin.model.jkt;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * com.yiyihealth.cherriesadmin.model.jkd
 *
 * @description: TODO接收上传的医院信息。
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2021/5/20 22:30
 * @Version 1.0
 **/

@Data
@Component
@SuppressWarnings("serial")
public class JktFunc10001  implements Serializable {
    @Value("${jthealth.hospId}")
    private String hospId;	/*医院编号	Y	字符串	医疗机构唯一标识*/
    @Value("${jthealth.hospName}")
    private String hospName;	/*医院名称	Y	字符串*/
    @Value("${locationCode:330103}")
    private String locationCode;	/*区域编码	Y	字符串	见字典*/
    @Value("${hospLevelCode:00}")
    private String hospLevelCode;	/*等级编码	Y	字符串	见字典*/
    @Value("${jthealth.hospImgUrl}")
    private String hospImgUrl;	/*医院标志图外网地址	N	字符串*/
    @Value("${hospType:6}")
    private String hospType;	/*隶属级别编码	Y	字符串	见字典*/
    private String telephone;	/*联系电话	N	字符串*/
    @Value("${hospState:0}")
    private String hospState;/*医院状态	Y	字符串	0-正常预约1-暂停预约*/
    @Value("${jthealth.introduction}")
    private String introduction;	/*医院简介	N	字符串*/
    @Value("${jthealth.serviceAddress}")
    private String serviceAddress;	/*服务地址	Y	字符串	线上服务地址，用于挂号，取消时调用*/
    @Value("${jthealth.publicKey}")
    private String publicKey;	/*医院方面加密公钥	Y	字符串	用于调用院方接口时对参数加密*/
}
