package com.yiyihealth.cherriesadmin.model.jkt;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * com.yiyihealth.cherriesadmin.model.jkd
 *
 * @description: TODO 根预约就诊t+1日凌晨2:00-4:00前结束
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2021/5/20 22:53
 * @Version 1.0
 **/
@Data
@Component
@SuppressWarnings("serial")
public class JktFunc10008 implements Serializable {
    @Value("${jthealth.hospId}")
    private String hospId;    /*医院编号	Y	字符串*/
    @Value("${jthealth.hospName}")
    private String hospName;    /*医院名称	Y	字符串*/
    private String orderId;    /*平台的交易编号	Y	字符串	平台唯一挂号交易ID*/
    private String regId;    /*挂号编号	Y	字符串	医院唯一*/
    private String regPassword;    /*取号密码	Y	字符串	医院生成 最起码7天内不重复*/
    private String medDate;    /*就诊日期	Y	字符串	yyyyMMdd*/
    private String medStatus;    /*就诊状态	Y	字符串	见字典*/
}
