package com.yiyihealth.cherriesadmin.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * 号源明细信息表(OrdSourceDetail)实体类
 *
 * @author chen
 * @since 2020-09-25 17:58:45
 */

@ApiModel(value = "号源明细信息表对象(OrdSourceDetail)")
public class OrdSourceDetail implements Serializable {
    private static final long serialVersionUID = -38910674856928220L;

    private Long id;

    /**
     * 排班ID
     */
    @ApiModelProperty(value = "排班ID")
    private Long schedulingId;

    /**
     * 排班日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @ApiModelProperty(value = "排班日期")
    private LocalDate schedulingDate;

    /**
     * 号源明细类型 0院内号源和1第三方号源(平台、官网)
     */
    @ApiModelProperty(value = "号源明细类型 0院内号源和1第三方号源(平台、官网)")
    private String type;

    /**
     * 就诊时间点
     */
    @JsonFormat(pattern = "HH:mm")
    @DateTimeFormat(pattern = "HH:mm")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    @ApiModelProperty(value = "就诊时间点")
    private LocalTime visitTime;

    /**
     * 就诊时间段 早中晚 1 2 3
     */
    @ApiModelProperty(value = "就诊时间段 早中晚 1 2 3")
    private Integer timeState;

    /**
     * 号源序号
     */
    @ApiModelProperty(value = "号源序号")
    private String serialNumber;

    /**
     * 科室ID
     */
    @ApiModelProperty(value = "科室ID")
    private Long depId;

    /**
     * 科室名称
     */
    @ApiModelProperty(value = "科室名称")
    private String depName;

    /**
     * 医生ID
     */
    @ApiModelProperty(value = "医生ID")
    private Long doctorId;

    /**
     * 医生姓名
     */
    @ApiModelProperty(value = "医生姓名")
    private String doctorName;

    /**
     * 挂号类型
     */
    @ApiModelProperty(value = "挂号类型")
    private Long binId;

    /**
     * 类型名称
     */
    @ApiModelProperty(value = "类型名称")
    private String binName;

    /**
     * 号源生效时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @ApiModelProperty(value = "号源生效时间")
    private LocalDateTime effectiveTime;

    /**
     * 使用状态(1窗口挂号 2平台预约取号 3平台爽约 4院内预约取号 5院内爽约 6退号)
     */
    @ApiModelProperty(value = "使用状态(1窗口挂号 2平台预约取号 3平台爽约 4院内预约取号 5院内爽约 6退号 7医生停诊)")
    private String state;

    /**
     * 号源状态(0未使用  已使用1)
     */
    @ApiModelProperty(value = "号源状态(0未使用  已使用1)")
    private String status;

    /**
     * 就诊机构 医院 pub_hospital
     */
    @ApiModelProperty(value = "就诊机构 医院 pub_hospital")
    private Long hospitalId;

    private Long sufferId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getSchedulingId() {
        return schedulingId;
    }

    public void setSchedulingId(Long schedulingId) {
        this.schedulingId = schedulingId;
    }


    public LocalDate getSchedulingDate() {
        return schedulingDate;
    }

    public void setSchedulingDate(LocalDate schedulingDate) {
        this.schedulingDate = schedulingDate;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public LocalTime getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(LocalTime visitTime) {
        this.visitTime = visitTime;
    }


    public Integer getTimeState() {
        return timeState;
    }

    public void setTimeState(Integer timeState) {
        this.timeState = timeState;
    }


    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }


    public Long getDepId() {
        return depId;
    }

    public void setDepId(Long depId) {
        this.depId = depId;
    }


    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }


    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }


    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }


    public Long getBinId() {
        return binId;
    }

    public void setBinId(Long binId) {
        this.binId = binId;
    }


    public String getBinName() {
        return binName;
    }

    public void setBinName(String binName) {
        this.binName = binName;
    }


    public LocalDateTime getEffectiveTime() {
        return effectiveTime;
    }

    public void setEffectiveTime(LocalDateTime effectiveTime) {
        this.effectiveTime = effectiveTime;
    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Long getSufferId() {
        return sufferId;
    }

    public void setSufferId(Long sufferId) {
        this.sufferId = sufferId;
    }
}
