package com.yiyihealth.cherriesadmin.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.interceptor.CacheErrorHandler;

/**
 * com.yiyihealth.cherriesadmin.config
 *
 * @description: TODO
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2020/9/3 23:43
 * @Version 1.0
 **/
public class CallbackCacheErrorHandler  implements CacheErrorHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(CallbackCacheErrorHandler.class);

    @Override
    public void handleCacheGetError(RuntimeException exception, Cache cache, Object key) {
        LOGGER.error("cache get error, cacheName:{}, key:{}, msg:", cache.getName(), key, exception);
    }

    @Override
    public void handleCachePutError(RuntimeException exception, Cache cache, Object key, Object value) {
        LOGGER.error("cache put error, cacheName:{}, key:{}, msg:", cache.getName(), key, exception);

    }

    @Override
    public void handleCacheEvictError(RuntimeException exception, Cache cache, Object key) {
        LOGGER.error("cache evict error, cacheName:{}, key:{}, msg:", cache.getName(), key, exception);

    }

    @Override
    public void handleCacheClearError(RuntimeException exception, Cache cache) {
        LOGGER.error("cache clear error, cacheName:{}, msg:", cache.getName(), exception);
    }
}
