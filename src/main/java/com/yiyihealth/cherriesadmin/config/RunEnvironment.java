package com.yiyihealth.cherriesadmin.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@ConfigurationProperties(prefix = "spring.profiles")
@Data
@Component
public class RunEnvironment {
    private String active;
}
