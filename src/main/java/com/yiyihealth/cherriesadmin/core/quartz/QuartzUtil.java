package com.yiyihealth.cherriesadmin.core.quartz;

import cn.hutool.core.util.RandomUtil;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import static org.quartz.DateBuilder.futureDate;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * @author 10560
 */
public class QuartzUtil {
    public static  Scheduler scheduler;
    public static Integer redisCacheTaska(String appointmentId) throws SchedulerException {

        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        JobDetail jobDetail = JobBuilder.newJob(SchedulerQuartzJob.class).withIdentity(appointmentId, "group2").build();
        //给自定义任务传值
        jobDetail.getJobDataMap().put("appointmentId", appointmentId);

        System.out.println("任务执行："+RandomUtil.randomInt(0,1));
        int interval = RandomUtil.randomInt(0,1);
        SimpleTrigger trigger = (SimpleTrigger)newTrigger()
                .withIdentity(appointmentId, "group2")
                .startAt(futureDate(interval, DateBuilder.IntervalUnit.MINUTE))
                .forJob(appointmentId, "group2")
                .build();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
        return interval;
    }
    public static Integer redisCacheTask(String schedulingIdKey ,String sourceDetailIdKey ,String sourceDetailId,String appointmentId) throws SchedulerException {

        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        JobDetail jobDetail = JobBuilder.newJob(SchedulerQuartzJob.class).withIdentity(sourceDetailId, "group2").build();
        //给自定义任务传值
        jobDetail.getJobDataMap().put("schedulingIdKey", schedulingIdKey);
        jobDetail.getJobDataMap().put("sourceDetailIdKey", sourceDetailIdKey);
        jobDetail.getJobDataMap().put("sourceDetailId", sourceDetailId);
        jobDetail.getJobDataMap().put("appointmentId", appointmentId);

        System.out.println("任务执行："+RandomUtil.randomInt(0,15));
        int interval = RandomUtil.randomInt(0,15);
        SimpleTrigger trigger = (SimpleTrigger)newTrigger()
                .withIdentity(sourceDetailId, "group2")
                .startAt(futureDate(interval, DateBuilder.IntervalUnit.MINUTE))
                .forJob(sourceDetailId, "group2")
                .build();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
        return interval;
    }
}
