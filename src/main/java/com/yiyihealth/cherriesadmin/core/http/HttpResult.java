package com.yiyihealth.cherriesadmin.core.http;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * HTTP结果封装
 * @author Louis
 * @date Jan 12, 2019
 */
@ApiModel(value = "Http请求结果返回结果对象")
public class HttpResult {

	@ApiModelProperty(value = "code",required = true)
	private int code = 200;
	@ApiModelProperty(value = "msg",required = true)
	private String msg;
	@ApiModelProperty(value = "data",required = true)
	private Object data;

	public static HttpResult failure() {
		return failure(HttpStatus.SC_INTERNAL_SERVER_ERROR, "未知异常，请联系管理员");
	}

	public static HttpResult failure(String msg) {
		return failure(HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
	}

	public static HttpResult failure(int code, String msg) {
		HttpResult r = new HttpResult();
		r.setCode(code);
		r.setMsg(msg);
		return r;
	}

	public static HttpResult success(String msg) {
		HttpResult r = new HttpResult();
		r.setMsg(msg);
		return r;
	}

	public static HttpResult success(Object data) {
		HttpResult r = new HttpResult();
		r.setData(data);
		return r;
	}

	public static HttpResult success() {
		return new HttpResult();
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
