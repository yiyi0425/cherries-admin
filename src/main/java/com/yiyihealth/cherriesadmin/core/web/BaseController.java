
package com.yiyihealth.cherriesadmin.core.web;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yiyihealth.cherriesadmin.core.http.HttpResult;
import com.yiyihealth.cherriesadmin.core.page.PageRequest;
import com.yiyihealth.cherriesadmin.core.page.PageUtils;
import com.yiyihealth.cherriesadmin.core.page.Param;
import com.yiyihealth.cherriesadmin.core.service.BaseService;
import com.yiyihealth.cherriesadmin.model.PubSufferer;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
* @Title: BaseController.java
* @Package com.easyhrip.core.web
* @Description: springmvc Controller基类,通过泛型实现基本的增删查改、ajax返回列表数据以及简单的属性查询
*
* 例：
* List page     : GET /user/
* Create page   : GET /user/create
* Create action : POST /user/edit
* Update page   : GET /user/update/{id}
* Update action : POST /user/edit
* Delete action : GET /user/delete/{id}
* Delete action : POST /user/delete
*
* @author sany
* @date 2015年4月23日 下午10:46:04
* @version V1.0
 */
@Component
public abstract class BaseController<T, E, ID extends Serializable> {

	@Autowired
	private BaseService<T, E, ID> baseService;

	protected void setBaseService(BaseService<T, E, ID> baseService) {
		this.baseService = baseService;
	}

	protected void initBinder(WebDataBinder binder) {
	}

	@ApiOperation(value = "主键查询数据记录")
	@ApiResponse(code = 400,message = "参数没有填好",response = String.class)
	@ApiImplicitParam(name = "id",value = "主键id",required = true)
	@GetMapping("/select_by_primary_key")
    @ResponseBody
    public HttpResult get( ID id){
        return HttpResult.success(baseService.selectByPrimaryKey(id));
    }

	/**
	 * 新增
	 * @param record
	 * @return
	 */
	@ApiOperation(value = "主键查询数据记录",notes = "新增之后返回HttpResult对象")
	@ApiResponse(code = 400,message = "请填写正确参数",response = String.class)
	@PostMapping("/create")
    @ResponseBody
    public HttpResult create(@RequestBody  T record) throws Exception {
        return HttpResult.success(baseService.insert(record));
    }

	/**
	 * 按主键id删除
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "根据主键删除数据记录",notes = "根据主键id删除单个记录")
	@ApiResponse(code = 400,message = "参数没有填好",response = String.class)
	@ApiImplicitParam(name = "id",value = "主键id",required = true)
	@PostMapping("/delete_by_primary_key")
    @ResponseBody
	public HttpResult deleteByPrimaryKey(ID id) throws Exception {
        return HttpResult.success(baseService.deleteByPrimaryKey(id));
    }


	/**
	 * 按主键更新记录信息
	 * @param record
	 * @return
	 */
	@ApiOperation(value = "主键选择更新数据记录",notes = "更新之后返回更新后数据对象")
	@ApiResponse(code = 400,message = "请填写正确参数",response = String.class)
	@PostMapping("/update_by_primary_key_selective")
    @ResponseBody
	public HttpResult updateByPrimaryKeySelective(@RequestBody  T record) {
        return HttpResult.success(baseService.updateByPrimaryKeySelective(record));
    }

	/**
	 * 按主键更新信息
	 * @param record
	 * @return
	 */
	@ApiOperation(value = "主键更新数据记录",notes = "更新之后返回更新后数据对象")
	@ApiResponse(code = 400,message = "请填写正确参数",response = String.class)
	@PostMapping("/update_by_primary_key")
    @ResponseBody
	public HttpResult updateByPrimaryKey(@RequestBody T record) {
        return HttpResult.success(baseService.updateByPrimaryKey(record));
    }

	/**
	 * 列表查询
	 * @return
	 */
	@ApiOperation(value = "列表查询数据信息",notes = "返回对应条件的数据列表")
	@PostMapping("/select_list")
    @ResponseBody
    public HttpResult selectList(@RequestBody Map<String, Object> map) {
        return HttpResult.success(baseService.selectList(map));
    }

	/**
	 * 列表查询（含扩展）
	 * @return
	 */
	@ApiOperation(value = "列表扩展查询数据信息",notes = "返回对应条件的数据列表")
	@PostMapping("/select_list_wrapper")
    @ResponseBody
    public HttpResult selectListWrapper(@RequestBody Map<String, Object> map) {
        return HttpResult.success(baseService.selectListWrapper(map));
    }

	/**
	 * 按主键id查询扩展记录
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "主键查询扩展数据记录")
	@ApiResponse(code = 400,message = "参数没有填好",response = String.class)
	@ApiImplicitParam(name = "id",value = "主键id",required = true)
	@GetMapping("/select_wrapper_by_primary_key")
    @ResponseBody
    public HttpResult selectWrapperByPrimaryKey(ID id){
        return HttpResult.success(baseService.selectWrapperByPrimaryKey(id));
    }


	/**
	 * 分页查询
	 * @return
	 */
	@ApiOperation(value = "分页列表查询数据信息",notes = "返回对应条件的分页数据列表")
	@PostMapping("/select_list_Page")
	@ResponseBody
	public HttpResult selectListPage(@RequestBody PageRequest pageRequest) {
		int pageNum = pageRequest.getPageNum();
		int pageSize = pageRequest.getPageSize();

		Map<String, Object> map = new HashMap<>();
		for (Param param:pageRequest.getParams()){
			if (!StrUtil.isBlank(param.getValue())){
				map.put(param.getName(),param.getValue());
			}
		}
		PageInfo<T> pageInfo = baseService.selectListPage(map,pageNum,pageSize);
		return HttpResult.success(PageUtils.getPageResult(pageRequest,pageInfo));
	}

	/**
	 * 分页查询
	 * @return
	 */
	@ApiOperation(value = "分页列表查询数据信息",notes = "返回对应条件的分页数据列表")
	@PostMapping("/select_list_wrapper_Page")
	@ResponseBody
	public HttpResult selectListWrapperPage(@RequestBody PageRequest pageRequest) throws Exception {
		int pageNum = pageRequest.getPageNum();
		int pageSize = pageRequest.getPageSize();
		Map<String, Object> map = new HashMap<>();
		for (Param param:pageRequest.getParams()){
			if (!StrUtil.isBlank(param.getValue())){
				map.put(param.getName(),param.getValue());
			}
		}
		PageInfo<T> pageInfo = baseService.selectListPage(map,pageNum,pageSize);
		return HttpResult.success(PageUtils.getPageResult(pageRequest,pageInfo));
	}

}
