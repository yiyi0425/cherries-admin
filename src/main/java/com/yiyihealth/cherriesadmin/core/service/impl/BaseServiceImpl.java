package com.yiyihealth.cherriesadmin.core.service.impl;


import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yiyihealth.cherriesadmin.core.mapper.EntityMapper;
import com.yiyihealth.cherriesadmin.core.service.BaseService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * com.yicloud.yicloudadmin.core.service.impl
 *
 * @description: TODO
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2020/8/11 0:50
 * @Version 1.0
 **/
@Transactional
@Component
@CacheConfig(cacheNames = "baseService")
abstract public class BaseServiceImpl<T, E, ID extends Serializable> implements BaseService<T, E, ID> {

    EntityMapper<T, E, ID> mapper;

    private Snowflake snowflake = IdUtil.getSnowflake(1, 1);

    public void setEntityMapper(EntityMapper<T, E, ID> mapper) {
        this.mapper = mapper;
    }


    @Override
    public T selectByPrimaryKey(ID id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public T insert(T record) throws Exception {
        mapper.insert(record);
        return record;
    }

    @Override
    public int deleteByPrimaryKey(ID id) throws Exception {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public T updateByPrimaryKeySelective(T record)  {
        int ret = mapper.updateByPrimaryKeySelective(record);
        return record;
    }

    @Override
    public T updateByPrimaryKey(T record)  {
        int ret = mapper.updateByPrimaryKey(record);
        return record;
    }


    @Override
    public List<T> selectList(Map<String, Object> map) {
        return  mapper.selectList(map);
    }


    /**
     *
     * @Title: selectWrapperByPrimaryKey
     * @Description: 用户扩展信息
     * @param id
     * @return
     * UserWrapper
     * @throws
     */
    @Override
    public E selectWrapperByPrimaryKey(ID id) {
        return mapper.selectWrapperByPrimaryKey(id);
    }

    /**
     * @Description: 查询用户扩展信息列表（不分页）
     * @param map
     * @return
     */

    @Override
    public List<E> selectListWrapper(Map<String, Object> map) {
        return mapper.selectListWrapper(map);
    }

    @Override
    public PageInfo<T> selectListPage(Map<String, Object> map, int pageNum, int pageSize ) {
        PageHelper.startPage(pageNum, pageSize);
        List<T> list = mapper.selectList(map);
        return new PageInfo<>(list);
    }

    @Override
    public PageInfo<E> selectListWrapperPage(Map<String, Object> map,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<E> list = mapper.selectListWrapper(map);
        return new PageInfo<E>(list);
    }
}
