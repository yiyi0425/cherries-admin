package com.yiyihealth.cherriesadmin.core.service;

import com.github.pagehelper.PageInfo;
import com.yiyihealth.cherriesadmin.core.page.PageRequest;
import com.yiyihealth.cherriesadmin.core.page.PageResult;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * com.yicloud.yicloudadmin.core.service
 *
 * @description: TODO
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2020/8/11 0:50
 * @Version 1.0
 **/
@Component
public interface BaseService<T, E, ID extends Serializable>  {
    T selectByPrimaryKey(ID id);

    T insert(T record) throws Exception;

    int deleteByPrimaryKey(ID id) throws Exception;

    T updateByPrimaryKeySelective(T record) ;

    T updateByPrimaryKey(T record) ;

    List<T> selectList(Map<String, Object> map);


    /**
     *
     * @Title: selectWrapperByPrimaryKey
     * @Description: 用户扩展信息
     * @param id
     * @return
     * E
     * @throws
     */
    E selectWrapperByPrimaryKey(ID id);


    /**
     * @Description: 查询用户扩展信息列表（不分页）
     * @param map
     * @return
     */
    List<E> selectListWrapper(Map<String, Object> map);
    /**
     * 分页查询
     * 这里统一封装了分页请求和结果，避免直接引入具体框架的分页对象, 如MyBatis或JPA的分页对象
     * 从而避免因为替换ORM框架而导致服务层、控制层的分页接口也需要变动的情况，替换ORM框架也不会
     * 影响服务层以上的分页接口，起到了解耦的作用
     */
    PageInfo<E> selectListWrapperPage(Map<String, Object> map, int pageNum, int pageSize);
    /**
     * 分页查询
     * 这里统一封装了分页请求和结果，避免直接引入具体框架的分页对象, 如MyBatis或JPA的分页对象
     * 从而避免因为替换ORM框架而导致服务层、控制层的分页接口也需要变动的情况，替换ORM框架也不会
     * 影响服务层以上的分页接口，起到了解耦的作用
     */
    PageInfo<T> selectListPage(Map<String, Object> map, int pageNum, int pageSize);

}
