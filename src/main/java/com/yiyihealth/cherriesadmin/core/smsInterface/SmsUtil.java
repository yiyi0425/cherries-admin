package com.yiyihealth.cherriesadmin.core.smsInterface;


import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class SmsUtil {
    public static void SendSms(String phone, String content, String TemplateId) {
        try {
            CloseableHttpClient client = null;
            CloseableHttpResponse response = null;
            try {
                List<BasicNameValuePair> formparams = new ArrayList<>();
                formparams.add(new BasicNameValuePair("Account","15924121133"));
                //登录后台 首页显示
                formparams.add(new BasicNameValuePair("Pwd","97b534edebd5b838d86280c1e"));
                formparams.add(new BasicNameValuePair("Content",content));
                formparams.add(new BasicNameValuePair("Mobile",phone));
                //模板短信
                formparams.add(new BasicNameValuePair("TemplateId",TemplateId));
                //用户定义扩展码
                formparams.add(new BasicNameValuePair("ExtNo","123"));
                //登录后台 添加签名获取id
                formparams.add(new BasicNameValuePair("SignId","32286"));
                HttpPost httpPost = new HttpPost("http://api.feige.ee/SmsService/Send");
                httpPost.setEntity(new UrlEncodedFormEntity(formparams,"UTF-8"));
                client = HttpClients.createDefault();
                response = client.execute(httpPost);
                HttpEntity entity = (HttpEntity) response.getEntity();
                String result = EntityUtils.toString((org.apache.http.HttpEntity) entity);
                System.out.println(result);
            } finally {
                if (response != null) {
                    response.close();
                }
                if (client != null) {
                    client.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * @Title: smsCode
     * @Description: TODO( 产生验证码)
     * @param @return    设定文件
     * @return String    返回类型
     * @throws
     */
    public static String smsCode(){
        String random = new Random().nextInt(1000000)+"";
        if(random.length()!=4){
            return smsCode();
        }else{
            return random;
        }
    }

}
