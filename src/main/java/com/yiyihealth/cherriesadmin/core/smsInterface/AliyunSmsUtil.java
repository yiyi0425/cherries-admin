package com.yiyihealth.cherriesadmin.core.smsInterface;

import cn.hutool.core.lang.UUID;
import cn.hutool.json.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

import java.util.Random;

/**
 * @author 10560
 */
public class AliyunSmsUtil {
    // 产品名称:云通信短信API产品,开发者无需替换
    static final String product = "Dysmsapi";
    // 产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";

    // TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
    static final String accessKeyId = "LTAI4GFt3aDxFUvSCRm64Zvs";
    static final String accessKeySecret = "CBWoXa59n88RZxStHibFMFDUPHbPbr";
    public static String sendSms(String Phone, String TemplateCode, String TemplateParam){
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(domain);
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", Phone);
        request.putQueryParameter("SignName", "孙泰和");
        request.putQueryParameter("TemplateCode", TemplateCode);
        request.putQueryParameter("TemplateParam", TemplateParam);
        request.putQueryParameter("OutId", String.valueOf(UUID.fastUUID()));
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return "";
    }
    /**
     * @Title: smsCode
     * @Description: TODO( 产生验证码)
     * @param @return    设定文件
     * @return String    返回类型
     * @throws
     */
    public static String smsCode(){
        String random = new Random().nextInt(1000000)+"";
        if(random.length()!=4){
            return smsCode();
        }else{
            return random;
        }
    }
    public static void main(String[] args) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("code",smsCode());
//        System.out.println(sendSms("18668070425","SMS_201135076",jsonObject.toString()));

        //SMS_200702281 预约提醒---您好${mtname},这里是品言健康，您已成功预约${menzhen}门诊。预约号：${num}。地址：${address}。电话：${phone}。;
        JSONObject appointmentReminder = new JSONObject();
        appointmentReminder.set("mtname","陈煜诚");
        appointmentReminder.set("menzhen","1234");
        appointmentReminder.set("num",smsCode());
        appointmentReminder.set("address","浙江省杭州市西湖区古墩路");
        appointmentReminder.set("phone","18668070425");
        System.out.println(appointmentReminder.toString());
//        System.out.println(sendSms("18668070425","SMS_200702281",appointmentReminder.toString()));

        //SMS_200702289 取消预约提醒---您好${patName},这里是品言健康，您预约我院${depName}门诊。预约号：${serialNumber}预约记录取消。咨询电话：${phone}。
        JSONObject cancelAppointmentReminder = new JSONObject();
        cancelAppointmentReminder.set("patName","陈煜诚");
        cancelAppointmentReminder.set("depName","中医内科");
        cancelAppointmentReminder.set("serialNumber",smsCode());
        cancelAppointmentReminder.set("phone","18668070425");
        System.out.println(cancelAppointmentReminder.toString());
        System.out.println(sendSms("18668070425","SMS_200702289",cancelAppointmentReminder.toString()));
    }
}
