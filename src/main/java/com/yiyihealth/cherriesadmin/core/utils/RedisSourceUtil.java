package com.yiyihealth.cherriesadmin.core.utils;

import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

/**
 * @author 10560
 */
public class RedisSourceUtil {

    public static synchronized void removeSourece(String schedulingKey, String sourceDetailKey, Long sourceDetailId) throws Exception {
        RedisTemplate<Object, Object> redisCacheTemplate = new RedisTemplate<>();
        if (redisCacheTemplate.opsForSet().isMember(sourceDetailKey,sourceDetailId)){
            redisCacheTemplate.opsForValue().decrement(schedulingKey,1);
            redisCacheTemplate.opsForSet().remove(sourceDetailKey,sourceDetailId);
        }else {
            throw new Exception("此号已被预约，请重新选择！");
        }
    }

}
