package com.yiyihealth.cherriesadmin.core.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.LinkedHashMap;

/**
 * AES加密处理
 * @author jthealth-NZH
 * @Date 2021/04/22 8:47
 * @Describe
 * @Version 1.0
 */
@Slf4j
public class AESUtils {

	private static final String AESTYPE = "AES/ECB/PKCS5Padding";

	private final static String CHARSET = "UTF-8";

	public static final String PASSWORD_KEY = "Vw+NoBQ/6v1XB+C6";

	//入参为对象,可使用此方法进行加密
	public static String aesEncrypt(Object o, String use_key){
		JSONObject orderedJson = convertOrderedJson(o);
		return aesEncrypt(orderedJson.toJSONString(),use_key);
	}

	//入参为对象,可使用此方法进行解密
	public static String aesDecrypt(Object o, String use_key) throws Exception {
		return aesDecrypt(o.toString(),use_key);
	}

	//密码加密
	public static String aesEncrypt4Password(String plainText) {
		return aesEncrypt(plainText,PASSWORD_KEY);
	}

	//加密
	public static String aesEncrypt(String plainText, String use_key) {
		byte[] encrypt = null;
		try {
			Key key = generateKey(use_key);
			Cipher cipher = Cipher.getInstance(AESTYPE);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			encrypt = cipher.doFinal(plainText.getBytes(CHARSET));
		} catch (Exception e) {
			log.error("encrypt error!", e);
			return null;
		}
		return new String(Base64.encodeBase64(encrypt));
	}

	// 解密
	public static String aesDecrypt(String encryptData, String useKey) throws Exception {
		log.debug("AES解密入参encryptData:{}\n,useKey:{},长度:{}",encryptData,useKey,useKey.length());
		byte[] decrypt = null;
		try {
			Key key = generateKey(useKey);
			Cipher cipher = Cipher.getInstance(AESTYPE);
			cipher.init(Cipher.DECRYPT_MODE, key);
			decrypt = cipher.doFinal(Base64.decodeBase64(encryptData.getBytes(CHARSET)));
		}catch (Exception e){
			log.error("decrypt error!", e);
			throw e;
		}
		return new String(decrypt,"utf-8").trim();
	}

	public static JSONObject convertOrderedJson(Object o){
		LinkedHashMap<String, String> jsonMap
				= JSON.parseObject(JSONObject.toJSONString(o),LinkedHashMap.class, Feature.OrderedField);
		JSONObject jsonObject = new JSONObject(true);
		jsonObject.putAll(jsonMap);
		return jsonObject;
	}

	private static Key generateKey(String key) throws Exception {
		try {
			SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(CHARSET), "AES");
			return keySpec;
		} catch (Exception e) {
			log.error("generateKey", e);
			throw e;
		}
	}

	public static void main(String[] args) {
		String encStr = aesEncrypt("今晚打老虎——ABCDE_ABCDE#/'12345",PASSWORD_KEY);

		try {
			System.out.println(aesDecrypt(encStr,PASSWORD_KEY));
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}