package com.yiyihealth.cherriesadmin.core.page;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * 分页返回结果
 * @author Louis
 * @date Jan 12, 2019
 */
@ApiModel(value = "PageResult分页请求结果对象")
public class PageResult {
	/**
	 * 当前页码
	 */
	@ApiModelProperty(value = "当前页码")
	private int pageNum;
	/**
	 * 每页数量
	 */
	@ApiModelProperty(value = "每页数量")
	private int pageSize;
	/**
	 * 记录总数
	 */
	@ApiModelProperty(value = "记录总数")
	private long totalSize;
	/**
	 * 页码总数
	 */
	@ApiModelProperty(value = "页码总数")
	private int totalPage;
	/**
	 * 分页数据
	 */
	@ApiModelProperty(value = "分页数据")
	private List<?> content;
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public long getTotalSize() {
		return totalSize;
	}
	public void setTotalSize(long totalSize) {
		this.totalSize = totalSize;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public List<?> getContent() {
		return content;
	}
	public void setContent(List<?> content) {
		this.content = content;
	}
}
