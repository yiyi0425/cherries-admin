package com.yiyihealth.cherriesadmin.core.page;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 分页参数
 * @author Louis
 * @date Jan 19, 2019
 */
@ApiModel(value = "Param入参对象")
public class Param {
	@ApiModelProperty(value = "入参key")
	private String name;
	@ApiModelProperty(value = "入参value")
	private String value;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Param{" +
				"name='" + name + '\'' +
				", value='" + value + '\'' +
				'}';
	}
}
