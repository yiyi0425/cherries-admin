package com.yiyihealth.cherriesadmin.core.mapper;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * com.yicloud.yicloudadmin.core
 *
 * @description: TODO
 * @author: chen
 * @email: 1056065518@qq.com
 * @date: 2020/8/11 0:48
 * @Version 1.0
 **/
@Component
public interface EntityMapper<T, E, ID extends Serializable> {
    T selectByPrimaryKey(ID id);

    List<T> selectList(Map<String, Object> map);

    int deleteByPrimaryKey(ID id);

    void deleteList(Map<String, Object> map);

    int insert(T record);

    int updateByPrimaryKeySelective(T record);

    int updateByPrimaryKey(T record);

    /**
     *
     * @Title: selectWrapperByPrimaryKey
     * @Description: 用户扩展信息
     * @param id
     * @return
     * E
     * @throws
     */
    E selectWrapperByPrimaryKey(ID id);

    /**
     *
     * @Title: selectListWrapperPage
     * @Description: 查询用户扩展信息列表（分页）
     * @param map
     * @return
     * List<UserWrapper>
     * @throws
     */
    List<E> selectListWrapper(Map<String, Object> map);
}
