package com.yiyihealth.cherriesadmin.core.session;

import cn.hutool.system.SystemUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SpringSessionConfig implements WebMvcConfigurer {
    /**
     * 得到的访问路径为当前项目端口路径 + /upload/image/
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        /*
         * 说明：增加虚拟路径(经过本人测试：在此处配置的虚拟路径，用springboot内置的tomcat时有效，
         * 用外部的tomcat也有效;所以用到外部的tomcat时不需在tomcat/config下的相应文件配置虚拟路径了,阿里云linux也没问题)
         */
        if (SystemUtil.getOsInfo().isLinux()){
            registry.addResourceHandler("static/**").addResourceLocations("file:/home/img/");
        }else {
            registry.addResourceHandler("static/**").addResourceLocations("file:/D:/test/");
        }
        WebMvcConfigurer.super.addResourceHandlers(registry);
    }

}
