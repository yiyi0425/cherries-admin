package com.yiyihealth.cherriesadmin.core.annotation;

import cn.hutool.core.util.ReflectUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author 陈煜诚
 * @ClassName: BeanAspect
 * @Package com.easyhrip.starry.provider.common.aspect
 * @Description: 数据字典code to name convert
 * @date 2019年8月17日 下午2:31:39
 */
@Order(3)
@Aspect
@Component
@Slf4j
public class BeanAspect {
    private String HospitalId;
    /**
     * @throws
     * @Title: selectListWrapper
     * @Description: 扩展信息列表查询进行切面
     */
    @Pointcut("execution(public * com.yiyihealth.cherriesadmin.mapper.*.select*List(..))  ")
    public void setBeanList() {
    }

    @Pointcut("execution(public * com.yiyihealth.cherriesadmin.mapper.*.select*ByPrimaryKey(..)) ")
    public void setBean() {
    }

    /**
     * 环绕操作
     *
     * @param pjp 切入点
     * @return 原方法返回值
     * @throws Throwable 异常信息
     */
    @Around("setBeanList()")
    public Object setBeanAroundList(ProceedingJoinPoint pjp) throws Throwable {
        Object object = pjp.proceed();
        return object;

    }
    @Around("setBean()")
    public Object setBeanAround(ProceedingJoinPoint pjp) throws Throwable {
        Object object = pjp.proceed();
        return object;

    }

    @SuppressWarnings("unchecked")
    @AfterReturning(pointcut = "setBeanList()", returning = "objs")
    public void setBeanAfterReturningList(JoinPoint point, Object objs) {
        if (objs == null) {
            return;
        }
        List<Object> lists = (List<Object>) objs;
        if (lists.size() == 0) {
            return;
        }
        DictionaryReflectAnnotation reflectFields = lists.get(0).getClass().getAnnotation(DictionaryReflectAnnotation.class);
        if (reflectFields == null) {
            //未添加注解
            return;
        }

        for (Object obj : lists) {
            if (!("".equals(HospitalId))) {
                Method method = ReflectUtil.getMethod(obj.getClass(),"set"+reflectFields.value(),new Class[]{String.class});
                ReflectUtil.invoke(obj, method, HospitalId);
            }
        }
    }

    @SuppressWarnings("unchecked")
    @AfterReturning(pointcut = "setBean()", returning = "obj")
    public void setBeanAfterReturning(JoinPoint point, Object obj) {
        if (obj == null) {
            return;
        }

        DictionaryReflectAnnotation reflectFields = obj.getClass().getAnnotation(DictionaryReflectAnnotation.class);
        if (reflectFields == null) {
            return;
        }

        Method method = ReflectUtil.getMethod(obj.getClass(),"set"+reflectFields.value(),new Class[]{String.class});
        ReflectUtil.invoke(obj, method, HospitalId);
    }

}
