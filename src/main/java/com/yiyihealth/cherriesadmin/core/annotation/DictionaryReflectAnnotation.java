package com.yiyihealth.cherriesadmin.core.annotation;

import java.lang.annotation.*;

/**
 * @author 10560
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface DictionaryReflectAnnotation {
	String value() default "";
}
