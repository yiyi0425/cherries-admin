
package com.yiyihealth.cherriesadmin.core.webservice;

import org.springframework.core.io.support.PropertiesLoaderUtils;

import javax.xml.namespace.QName;
import javax.xml.ws.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.Set;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 *
 */
@WebServiceClient(name = "MainServiceImplPortTypeService", targetNamespace = "http://impl.ws.tynet.com", wsdlLocation = "${WSDL_URL}")
public class MainServiceImplPortTypeService
    extends Service
{
    /**
     * 不指定url默认地址
     */
    private static String WSDLURL = "http://192.168.2.204:8080/gh_ws_client/service?wsdl";
    private final static URL MAINSERVICEIMPLPORTTYPESERVICE_WSDL_LOCATION;
    private final static WebServiceException MAINSERVICEIMPLPORTTYPESERVICE_EXCEPTION;
    private final static QName MAINSERVICEIMPLPORTTYPESERVICE_QNAME = new QName("http://impl.ws.tynet.com", "MainServiceImplPortTypeService");

    static {
        try {
            Properties properties = PropertiesLoaderUtils.loadAllProperties("config.properties");
            //遍历取值
            Set<Object> objects = properties.keySet();
            for (Object object : objects) {
                //取到参数赋值给静态变量
                if ("REGISTER_WSDL_URL".equals(object.toString())) {
                    System.out.println(new String(properties.getProperty((String) object).getBytes(StandardCharsets.ISO_8859_1), "gbk"));
                    WSDLURL = new String(properties.getProperty((String) object).getBytes(StandardCharsets.ISO_8859_1), "gbk");
                }
            }
        } catch (IOException ex){
            ex.printStackTrace();
        }

        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL(WSDLURL);
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        MAINSERVICEIMPLPORTTYPESERVICE_WSDL_LOCATION = url;
        MAINSERVICEIMPLPORTTYPESERVICE_EXCEPTION = e;
    }

    public MainServiceImplPortTypeService() {
        super(__getWsdlLocation(), MAINSERVICEIMPLPORTTYPESERVICE_QNAME);
    }

    public MainServiceImplPortTypeService(WebServiceFeature... features) {
        super(__getWsdlLocation(), MAINSERVICEIMPLPORTTYPESERVICE_QNAME, features);
    }

    public MainServiceImplPortTypeService(URL wsdlLocation) {
        super(wsdlLocation, MAINSERVICEIMPLPORTTYPESERVICE_QNAME);
    }

    public MainServiceImplPortTypeService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, MAINSERVICEIMPLPORTTYPESERVICE_QNAME, features);
    }

    public MainServiceImplPortTypeService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public MainServiceImplPortTypeService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return
     *     returns MainServiceImplPortType
     */
    @WebEndpoint(name = "MainServiceImplPortTypePort")
    public MainServiceImplPortType getMainServiceImplPortTypePort() {
        return super.getPort(new QName("http://impl.ws.tynet.com", "MainServiceImplPortTypePort"), MainServiceImplPortType.class);
    }

    /**
     *
     * @param features
     *     A list of {@link WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns MainServiceImplPortType
     */
    @WebEndpoint(name = "MainServiceImplPortTypePort")
    public MainServiceImplPortType getMainServiceImplPortTypePort(WebServiceFeature... features) {
        return super.getPort(new QName("http://impl.ws.tynet.com", "MainServiceImplPortTypePort"), MainServiceImplPortType.class, features);
    }

    private static URL __getWsdlLocation() {
        if (MAINSERVICEIMPLPORTTYPESERVICE_EXCEPTION!= null) {
            throw MAINSERVICEIMPLPORTTYPESERVICE_EXCEPTION;
        }
        return MAINSERVICEIMPLPORTTYPESERVICE_WSDL_LOCATION;
    }

}
