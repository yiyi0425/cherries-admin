/**
 * Created by Kiva on 17/2/6.
 */
(function ($) {

    $.fn.extend({
        grayExpandTable:function(options,callback) {
            var $input=this,
                id=new Date().getTime();
            $input.options =$.extend({},$.fn.grayExpandTable.defaults,options);
            callback = callback || function () {};
            $("body").append('<div class="table-radius absolute-table d-n" id="'+id+'" style="border-color:#666;box-shadow: 4px 5px 12px #666"></div>');
            $input.off("input.create propertychange.create").on("input.create propertychange.create",function () {
                if($input.val().trim().length>0){
                    setTimeout(function () {
                        options.param[options.name]=$input.val();
                        tableCreate($("#"+id),$input,callback);
                    },300);
                    $input.off("input.create propertychange.create");
                }

            });
            return this;
        }
    });

	/**
		column:[
            {
                colspan:1,
                text:"",
                title:"收费项目",
                cellClick:function ($td) {}
            }
		]
	*/
    $.fn.grayExpandTable.defaults={
        attribute : {"data-item-id":"id"},//放着要存在tr标签中的数据 key表示的是a标签中的属性  value表示的是json数据中的key
        param : {},//url请求参数
        url:"data-expand.json",
        method:"get",
        paging:{
            pageNum:"pageNum",//表示这是第几页，后面的value表示的是服务器需要的key
            pageSize:"pageSize",//表示每页显示，后面的value表示的是服务器需要的key
        },
        column:[],
        rowClick: function($tr,rowData){}//单击行的事件
    };

    function tableCreate($expand_table, $input, fn) {
        var options=$input.options,
            rowData;
        $expand_table.html("");
        if(!options.param[options.paging.pageSize]){
            options.param[options.paging.pageSize]=10;
        }
        initTable($input);
        $(window).resize(function () {
            changeTableWidth();
            changeTableLocation();
        });

        $(document).on("click",function () {
            $expand_table.hide();
        });
        
        
        //$(document).on("keydown.gray-expand-table");
        $(document).on("keydown.gray-expand-table",function (e) {
            if($expand_table.css("display")!=="none"){
                var codeMap={"37":"37","38":"38","39":"39","40":"40","13":"13"};
                if(codeMap[e.keyCode]){
                    addTableKeyboardEvent(e.keyCode);
                    return false;
                }
            }
        });

        $expand_table.on("click","td",function (e) {
           trClick($(e.target).parents("tr:first"));
            e.stopPropagation();
        });


        function initTable($input) {
			$expand_table.append('<table class="table table-fixed table-hover text-center">');
			$expand_table.append('</table>');
			$expand_table.append('<div class="page" style="height:18px;"><span>&lt;</span><span class="page-focus">1</span><span>2</span><span>3</span><span class="page-n-focus">...</span><span>10</span><span>&gt;</span></div>');
			
			initTableThead($expand_table.find("table:first"));

            var $tbody=$expand_table.find("tbody:first").length>0?$expand_table.find("tbody:first"):$expand_table.append("<tbody></tbody>").find("tbody:first"),
                $page=$expand_table.find("div.page:first");
            addInputEvent($tbody,$page);
            addPageNumEvent($tbody,$page);
            changeTableWidth();
            changeTableLocation();
            if ($input.val().trim().length >= 2) {
                loadTable($tbody,$page);
                $tbody.find("tr:first").addClass("select-tr");
                $expand_table.show();
            }
        }



		//生成表头
		function initTableThead(table) {
			$(table).append("<thead><tr style='background-color: #666;color: #fff;'>");
			var tr = $(table).find("tr:first");
			$(options.column).each(function(index, column){
				if (column.html == undefined || column.html == "") {
					if (column.hidden) {
						$(tr).append('<td style="display:none;" colspan="' + column.colspan + '">' + column.title + '</td>');
					} else {
						$(tr).append('<td colspan="' + column.colspan + '">' + column.title + '</td>');
					}
				} else {
					if (column.hidden) {
						$(tr).append('<td style="display:none;" colspan="' + column.colspan + '">' + column.html + '</td>');
					} else {
						$(tr).append('<td colspan="' + column.colspan + '">' + column.html + '</td>');
					}
				}
			});
			$(table).append("</tr></thead>");
			$(table).append("<tbody></tbody>");
		}

        //给绑定表格的Input增加事件
        function addInputEvent($tbody,$page) {
            var timer=null;
            $input.off("input propertychange").on("input propertychange",function () {
                var val=$input.val().trim();
                options.param[options.name]=val;
                clearTimeout(timer);
                if(val.length <= 1){
                    $expand_table.hide();
                    return;
                }
                timer=setTimeout(function () {
                    loadTable($tbody,$page);
                    changeTableLocation();
                    $expand_table.show();
                    $tbody.find("tr:first").addClass("select-tr");
                },300);
            });
            // $input.on("focus",function (e) {
            //     var val=$input.val().trim();
            //     if(val.length>=2){
            //         $expand_table.show();
            //         $tbody.find("tr:first").addClass("select-tr");
            //     }
            //     e.stopPropagation();
            // });
        }

        //根据套餐加载套餐明细的方法
        function loadTable($tbody,$page) {
            $.ajax({
                url:options.url,
                dataType:"json",
                type:options.method,
                async:false,
                data: options.param,
                success:function (data) {
                    if(data.ret==1)
                    {
                        layer.msg(data.msg);
                    }
                    else
                    {
                        var html="";
                        rowData=data.rows||data.data;
                        if(!rowData) return;
                        $.each(rowData,function (i,value) {
                            html+='<tr ';
                            for(var _attr in options.attribute){
                                html+=_attr+'='+value[options.attribute[_attr]]+" ";
                            }
                            html+=">";
                            var _data=value;
                            $.each(options.column,function (i,value) {
                                if (value.hidden) {
                                    html+='<td style="display:none;" colspan='+value.colspan+'>';
                                } else {
                                    let fs = 'font-size: 14px;font-weight: 700;'
                                    let sty = 'white-space:nowrap;overflow:hidden;text-overflow:ellipsis;'
                                    html+=`<td colspan='${value.colspan}' style="${i != 1 ? sty : ''}${i == 1 ? fs : ''}" title="${_data[value["text"]]}">`;
                                }
                                if(_data[value["text"]] != undefined && _data[value["text"]]){
                                    html+=_data[value["text"]];
                                }
                                if (options.column[i]["html"] != undefined) {
                                    html+=options.column[i]["html"];
                                }
                                html+='</td>';
                            });
                            html+='</tr>';
                        });
                        $tbody.html(html);
                        setPageTotal(data["total"]);
                        addPageNum($page);
                        addTrHoverEvent();
                        fn();
                    }
                },
                error:function () {
                    layer.msg("请求出现例外:" + jqXHR.statusText);
                }
            });
        }

        //根据数据添加分页html代码
        function addPageNum($page) {
            var html='<span>&lt;</span>';
            var pageNum=parseInt(options.param[options.paging.pageNum]);
            var pageTotal=parseInt(options.paging["pageTotal"]);
            if(pageTotal<=5){
                for(var i=1;i<=pageTotal;i++){
                    html+='<span>'+i+'</span>';
                }
            }else{
                if(pageNum-1<=2){
                    for(var i=1;i<=4;i++){
                        html+='<span>'+i+'</span>';
                    }
                    html+='<span>...</span><span>'+pageTotal+'</span>';
                }else if(pageTotal-pageNum<=2){
                    html+='<span>1</span><span>...</span><span>'+(pageTotal-3)+'</span><span>'+(pageTotal-2)+'</span><span>'+(pageTotal-1)+'</span><span>'+pageTotal+'</span>';
                }else{
                    html+='<span>1</span><span>...</span><span>'+(pageNum-1)+'</span><span>'+pageNum+'</span><span>'+(pageNum+1)+'</span><span>...</span><span>'+pageTotal+'</span>';
                }
            }
            html+='<span>&gt;</span>';
            $page.html(html);
            $page.children("span").each(function () {
                var $span=$(this),
                    text=$span.text();
                if(parseInt(text)===pageNum){
                    $span.addClass("page-focus");
                } else if(text==="..."){
                    $span.addClass("page-n-focus");
                }
            });
        }

        //给点击页数的按钮添加事件
        function addPageNumEvent($tbody,$page) {
            options.param[options.paging.pageNum]=1;
            var arrow={
                "<":-1,
                ">":1
            };
            $page.on("click","span",function (e) {
                e.stopPropagation();
                var pageNum=parseInt(options.param[options.paging.pageNum]),
                    pageTotal=parseInt(options.paging["pageTotal"]),
                    $target=$(e.target),
                    number=parseInt($target.text()),
                    pageNumNext=pageNum+parseInt(arrow[$target.text()]);
                if(number===number && pageNum!==number){
                    options.param[options.paging.pageNum]=number;
                    loadTable($tbody,$page);
                }else if(number!==number && pageNumNext>=1 && pageNumNext<=pageTotal){
                    options.param[options.paging.pageNum]=pageNumNext;
                    loadTable($tbody,$page);
                }
            });
        }

        //expand表格的宽度和高度
        function changeTableWidth() {
            var $table=$input.parents("table:first");
            options.width?$expand_table.outerWidth(options.width):$expand_table.outerWidth($table.outerWidth());
            options.height?$expand_table.css("maxHeight",options.height+"px"):null;
        }

        //expand表格的定位
        function changeTableLocation() {
            // 可视区高度 / 可视区宽度
            let clientH = document.documentElement.clientHeight
            let clientW = document.documentElement.clientWidth
            // input宽 /
            let inputW = $input.outerWidth()
            let inputTop = $input.offset().top
            let inputLeft = $input.offset().left
            let positionObj = {
                "position": "absolute",
                "z-index": 9999,
            }
            if ((inputLeft + inputW / 2) <= (clientW / 2)) {
                // 在左半屏
                if (inputTop <= (clientH / 2)) {
                    // 在上半屏幕
                    positionObj = {
                        ...positionObj,
                        top: inputTop,
                        left: inputLeft + inputW + 5,
                        bottom: 'auto'
                    }
                }else{
                    // 在下半屏幕
                    positionObj = {
                        ...positionObj,
                        top: "auto",
                        bottom: clientH - inputTop,
                        left: inputLeft + inputW + 5
                    }
                }
            }else {
                // 在右半屏
                if (inputTop <= (clientH / 2)) {
                    // 在上半屏幕
                    positionObj = {
                        ...positionObj,
                        top: inputTop,
                        right: clientW - inputLeft + 5,
                        bottom: 'auto'
                    }
                }else{
                    // 在下半屏幕
                    positionObj = {
                        ...positionObj,
                        top: 'auto',
                        bottom: clientH - inputTop,
                        right: clientW - inputLeft + 5,
                    }
                }
            }
            $expand_table.css(positionObj);
        }

        // function changeTableLocation() {
            //屏幕高度1040 检索框最大高度300 160表示位移向右移动药品名称的宽度
            // var screenHeight  = window.document.body.offsetHeight;
            // var screenHeight  = document.documentElement.clientHeight;
            // var top=$input.offset().top+$input.innerHeight()-parseFloat($expand_table.css("marginTop")),
            //     left=$input.offset().left-parseFloat($expand_table.css("marginLeft"));
            // console.log(options)
            // console.log(left+parseInt(options.width))
            // if(left+parseInt(options.width)>$(window).width()){
            //     left=$input.offset().left+$input.width()-options.width-10;
            // }
            // if((screenHeight / 2 - top) >= 0){
            //     if(parseFloat($expand_table.css("height")) > 48){
            //         top = top
            //     }else {
            //         top = top - parseFloat($expand_table.css("height"))
            //     }
            //     //区别诊断-输入框的宽度
            //     if(parseFloat($input.css("width"))> 260){
            //         left=$input.offset().left-parseFloat($expand_table.css("marginLeft"))+ parseFloat($input.css("width"));
            //     }else{
            //         left=$input.offset().left-parseFloat($expand_table.css("marginLeft"))+ 160;
            //     }
            // }else if((screenHeight / 2 - top) < 0){
            //     //top=top-检索框实际高度parseFloat($expand_table.css("height")) 48||300
            //     if(parseFloat($expand_table.css("height")) > 48){
            //         top = top- parseFloat($expand_table.css("height"))
            //     }else {
            //         top = top- 300
            //     }
            //     //区别诊断-输入框的宽度
            //     if(parseFloat($input.css("width")) > 260){
            //         left=$input.offset().left-parseFloat($expand_table.css("marginLeft"))+ parseFloat($input.css("width"));
            //     }else{
            //         left=$input.offset().left-parseFloat($expand_table.css("marginLeft"))+ 160;
            //     }
            // }
            // $expand_table.css({
            //     "position": "absolute",
            //     "top":top,
            //     "left":left,
            //     "z-index": 9999
            // });
        // }

        //添加键盘事件
        function addTableKeyboardEvent(keyCode) {
            var $tbody=$expand_table.find("tBody"),
                $trs=$tbody.find("tr"),
                $selectTr=$tbody.find(".select-tr"),
                $page=$expand_table.find("div.page:first"),
                keyMap={
                    "37":function () {
                        $page.find("span:first").click();
                    },
                    "38":function () {
                        var $prevTr=$selectTr.prev();
                        if($prevTr.length>=1){
                            $selectTr.removeClass("select-tr");
                            $prevTr.addClass("select-tr");
                            if($prevTr.position().top-$prevTr.innerHeight()<0){
                                $expand_table.scrollTop($expand_table.scrollTop()-$prevTr.innerHeight());
                            }
                        }else{
                            $selectTr.removeClass("select-tr");
                            $trs.eq($trs.length-1).addClass('select-tr');
                            $expand_table.scrollTop($expand_table.outerHeight());
                        }
                    },
                    "39":function () {
                        $page.find("span:last").click();
                    },
                    "40":function () {
                        var $nextTr=$selectTr.next();
                        if($nextTr.length>=1){
                            $selectTr.removeClass("select-tr");
                            $nextTr.addClass("select-tr");
                            if($nextTr.position().top+$nextTr.innerHeight()*2>$expand_table.height()){
                                $expand_table.scrollTop($expand_table.scrollTop()+$nextTr.innerHeight());
                            }
                        }else{
                            $selectTr.removeClass("select-tr");
                            $trs.eq(0).addClass('select-tr');
                            $expand_table.scrollTop(0);
                        }
                    },
                    "13":function () {
                        if($selectTr.length>0){
                            trClick($selectTr);
                        }
                    }
                };
            keyMap[keyCode]();

        }
        
        function addTrHoverEvent() {
            var $trs=$expand_table.find("tr");
            $trs.on("mouseenter",function () {
                $(this).addClass("select-tr").siblings().removeClass("select-tr");
            });
        }

        function trClick($tr) {
            var index=$tr.parent().children().index($tr);
            options.rowClick($tr,rowData[index],$input);
            $expand_table.hide();
        }

        function setPageTotal(total) {
            var pageSize=parseInt(options.param[options.paging.pageSize]);
            total=parseInt(total);
            if(total%pageSize>0){
                options.paging["pageTotal"]=parseInt(total/pageSize)+1;
            }else{
                options.paging["pageTotal"]=total/pageSize;
            }
        }
    }
}(jQuery));