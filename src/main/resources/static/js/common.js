$(function () {
    resize();
    $(window).resize(function () {
        resize();
    });
    var myTime = new Date().getTime();
    $('head').find("link").each(function () {
        var l = $(this).attr("href");
        var nl = l + "?t=" + myTime;
        $(this).attr("href", nl);
    });
    $('head').find("script").each(function () {
        var s = $(this).attr("src");
        var ns = s + "?t=" + myTime;
        if (s == "" || s == null) {
            $(this).removeAttr("src");
        } else {
            $(this).attr("src", ns);
        }
    });
    $(".j-cms_leftMenu ul li").hover(function () {
        $(this).addClass("liHover").siblings().removeClass("liHover");
    });
    $(".j-cms_hasChildren,.j-cms_hasGrandson").click(function () {
        var open = $(this).find("i").first().hasClass("openChild");
        $(this).find("ul").first().slideToggle().toggleClass("fn-bort");
        $(this).find("i").first().toggleClass("openChild");
    });
    $(document).click(function () {
        $(".j-cms_leftMenu").animate({"left": "-14rem"});
    });
    $(".j-cms_main").height($(window).height() - 65);
    $(".j-cms_TabBottom").height($(window).height() - 100);
    $(".j-cms_TabBottom").find("iframe").css({"height": $(window).height() - 90});
    $(".j-cms_leftMenu ul li").click(function (event) {
        event.stopPropagation();
        var hasChild = $(this).find("ul").length;
        if (hasChild != 0) {
            event.stopPropagation();
        } else {
            var title = $(this).text();
            var iframeAddress = $(this).attr("data-href");
            $(".j-cms_leftMenu").animate({"left": "-14rem"});
            $(".j-cms_tabTop ul").append('<li role="presentation" class="fn-cp"><a>' + title + '</a><i></i></li>');
            $(".j-cms_TabBottom").append('<div><iframe width="100%" height="100%" scrolling="auto" frameborder="0" src="' + iframeAddress + '"></iframe></div>');
            $(".j-cms_TabBottom>div").hide();
            $(".j-cms_TabBottom>div:last-child").show();
            $(".j-cms_tabTop ul li").removeClass("active");
            $(".j-cms_tabTop ul li:last-child").addClass("active");
            $(".j-cms_TabBottom").find("iframe").css({"height": $(window).height() - 90});
            $(".j-cms_tabTop ul li").click(function () {
                var index = $(this).index();
                $(this).addClass("active").siblings().removeClass("active");
                $(".j-cms_TabBottom>div").eq(index).show().siblings().hide();
            });
            $(".j-cms_tabTop ul li i").unbind('click').click(function (event) {
                event.stopPropagation();
                var index = $(this).parents("li").index();
                $(this).parents("li").remove();
                $(".j-cms_TabBottom>div").eq(index).remove();
                $(".j-cms_tabTop ul li").removeClass("active");
                $(".j-cms_tabTop ul li:last-child").addClass("active");
                $(".j-cms_TabBottom>div").hide();
                $(".j-cms_TabBottom>div:last-child").show();
            });
        }
    });
// $(".j-cms_tabTop ul li").click(function(){
// 	var index = $(this).index();
// 	$(this).addClass("active").siblings().removeClass("active");
// 	$(".j-cms_TabBottom>div").eq(index).show().siblings().hide();
// });
    $(".j-cms_menuIcon").click(function (event) {
        event.stopPropagation();
        $(".j-cms_leftMenu").animate({"left": 0});
    });
    $(".j-links").click(function (event) {
        var links = $(this).attr("data-href");
        window.location.href = links;
    });
    $("table").each(function () {
        var trLen = $(this).find("tr").length;
        var tdLen = $(this).find("tr").find("td").length;
        var len = tdLen / trLen;
        $(this).find("tr").find("td").css({"width": 100 / len + "%"});
    });
    $(".j-cms_selectArea select").change(function () {
        var txt = $(this).find("option:checked").text();
        $(this).parents(".j-cms_selectArea").find(".j-showTxt").text(txt);
    });
    $('.j-selectAll').click(function () {
        var checked = $(this).is(':checked');
        if (checked) {
            $(this).parents("table").find("input:checkbox").attr("checked", "checked");
        } else {
            $(this).parents("table").find("input:checkbox").removeAttr("checked");
        }
    });

    $("#txtBeginDate").calendar({
        controlId: "divDate",                                 // 弹出的日期控件ID，默认: $(this).attr("id") + "Calendar"
        speed: 200,                                           // 三种预定速度之一的字符串("slow", "normal", or "fast")或表示动画时长的毫秒数值(如：1000),默认：200
        complement: true,                                     // 是否显示日期或年空白处的前后月的补充,默认：true
        readonly: true,                                       // 目标对象是否设为只读，默认：true
        upperLimit: new Date(),                               // 日期上限，默认：NaN(不限制)
        lowerLimit: new Date("2011/01/01"),                   // 日期下限，默认：NaN(不限制)
        callback: function () {                               // 点击选择日期后的回调函数
        }
    });

    $(".dropdown-menu li").click(function () {
        var txt = $(this).text();
        // alert(txt);
        $(this).parents(".btn-group").find(".btn i").text(txt);
    });

})

function resize() {
    var winH = $(window).height();
    var winW = $(window).width();
    $(".j-cms_container").css({"height": winH, "width": winW});
    $(".j-cms_leftMenu").css({"height": winH});
    var fontSize = winW / 32;
    if (fontSize > 15) {
        $("html").css({'font-size': '15px'});
    } else {
        $("html").css({'font-size': fontSize + 'px'});
    }
}
