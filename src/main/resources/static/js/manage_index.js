/**
 * Created by Kiva on 16/10/11.
 */


var IE8=(navigator.appName==="Microsoft Internet Explorer" && (navigator.appVersion.split(";")[1]+"").replace(/[ ]/g,"")==="MSIE8.0")?true:false;
var menuTab={
    $menu:$("#menu").parent(),
    menuUp:true,
    menuHWhole:$("#menu").parent().outerHeight(),
    start:function () {
        $(document).ready(function () {
            /*菜单点击的方法*/
            $("#menu > li").click(function () {
                menuTab.menuFocus(this,"menu-focus");
            });

            $(".secondary-menu > li").click(function () {
                menuTab.menuFocus(this,"secondary-menu-focus");
            });

            $(".third-menu > li").click(function () {
                menuTab.menuFocus(this,"third-menu-focus");
            });

            $("#menu li").click(function () {
                menuTab.menuClick(this);
                return false;
            });
            $("#top-nav").click(function () {
                menuTab.mySlideToggle();
            });
            menuTab.menuHeight();
            menuTab.contentH();
            $("#menu").find("li:first").click().children("ul").children("li:first").click();
            menuTab.tabLength();
        });
    },
    resize:function () {
        $(window).resize(function () {
            menuTab.menuHeight();
            menuTab.contentH();
            menuTab.tabLength();
        });
    },
    /*取消事件冒泡的方法*/
    cancelBubbling:function (e) {
        e = window.event || e;
        if (e.stopPropagation) {
            e.stopPropagation();
        } else {
            e.cancelBubble = true;
        }
    },
    notTag:function (href,li) {
        if($(".nav").children("li").attr("class")===undefined){
            $.each($("#menu").children("li"),function () {
                var $oneLi=$(this);
                var $toUl=$oneLi.children("ul");
                var $oneA=$oneLi.find("a:first");
                var id=($oneA.attr("href")+"").replace("#","");
                if($toUl.attr("role")!==undefined){
                    $.each($toUl.children("li"),function () {
                        var $toLi=$(this);
                        var $thirdUl=$toLi.children("ul");
                        var $toA=$toLi.find("a:first");
                        if($thirdUl.attr("role")!==undefined){
                            $.each($thirdUl.children("li"),function () {
                                var $thirdLi=$(this);
                                var $thirdA=$thirdLi.find("a:first");
                                $thirdA.removeClass("third-menu-focus");
                                $thirdA.find("div:first").addClass("display-none");
                            });
                        }
                        $thirdUl.slideUp();
                        $toA.removeClass("secondary-menu-focus");
                        $toA.find("div:first").addClass("display-none");
                    });
                }
                $toUl.slideUp();
                $oneA.removeClass("menu-focus");
               // $oneA.find("img:first").attr('src',"images/icon_"+id+"_grey.png");
                $oneA.find("div:first").addClass("display-none");
            });
        }
    },
    /*tag标签的关闭*/
    tagClose:function (button,e) {
        menuTab.cancelBubbling(e);
        var li=$(button).parent();
        var id=(li.children("a").attr("href")+"").replace("#","");
        var href=li.children("a").attr("href");
        $("#"+id).remove();
        if(li.hasClass("active")){
            li.next().length?li.next().click():li.prev().click();
        }
        li.remove();
        menuTab.notTag(href,li);
        menuTab.tabLength();
        Param.clear(id);
    },
    addTag:function (id,text,src) {
        if($("#"+id).attr("role")===undefined){
            var ul=$("#iFrame-content ul:first");
            var html="<li role='presentation' onclick='menuTab.tagClick(this)'> <a href='#"+id+"' role='tab' data-toggle='tab'>"+text+"</a> <button type='button' class='close alpha1 close-position no-border tabBtn-close' onclick='menuTab.tagClose(this,event)'><span aria-hidden='true' class='tab-close alpha1'>&times;</span><span class='sr-only'>Close</span></button></li>";
            ul.append(html);
            menuTab.addIFrame(id,src);
            $("#iFrame-content ul:first li:last a:first").click();
        }else {
            $("#content").children(".activy").removeClass("activity");
            $("#"+id).addClass("activity");
            var tab=$("#iFrame-content ul:first").children("li").find("a[href=#"+id+"]");
            var ul=$("#iFrame-content ul:first");
            tab.click();
        }
        ul.find(".tab-close").mouseenter(function(){
        	$(this).css("background-color","#e5e5e5");
        });
        ul.find(".tab-close").mouseleave(function(){
        	$(this).css("background-color","#fff");
        });
    },
    tagClick:function (tag) {
        var href=$(tag).children("a:first").attr("href");
        $.each($("#menu").find("a"),function () {
            if($(this).attr("href")===href){
                var aOneP=$(this).parent();
                var aToP=aOneP.parent();
                var aThreeP=aToP.parent();
                var aFourP=aThreeP.parent();
                if((aToP.attr("class")+"").indexOf("third-menu")>-1){
                    if(aFourP.css("display")==="none"){
                        aFourP.parent().click();
                    }
                    if(aToP.css("display")==="none"){
                        aThreeP.click();
                    }
                }else if((aToP.attr("class")+"").indexOf("secondary-menu")>-1){
                    if(aToP.css("display")==="none"){
                        aThreeP.click();
                    }
                }
                aOneP.click();
                return false;
            }
        });
        menuTab.tabLength();
    },
    menuFocus:function (li,clazz) {
        $(li).find("a:first").addClass(clazz);
        var lis=$(li).siblings();
        var ulOne= lis.find("ul:first");
        var ulTo;
        var ulThree;
        var aOne=lis.children("a");
        var aTo;
        var aThree;
        if(clazz==="menu-focus"){
            ulTo=ulOne.find("li ul:first");
            ulThree=ulTo.find("li ul:first");
            aTo=ulOne.find("li").children("a");
            aThree=ulTo.find("li").children("a");
            aThree.removeClass("third-menu-focus");
            aTo.removeClass("secondary-menu-focus");
            aOne.removeClass("menu-focus");
            aThree.find("div:first").addClass("display-none");
            aTo.find("div:first").addClass("display-none");
            aOne.find("div:first").addClass("display-none");
            ulThree.slideUp();
            ulTo.slideUp();
            ulOne.slideUp();
        }else if(clazz==="secondary-menu-focus"){
            ulTo=ulOne.find("li ul:first");
            aTo=ulOne.find("li").children("a");
            aOne.removeClass("secondary-menu-focus");
            aTo.removeClass("third-menu-focus");
            aOne.find("div:first").addClass("display-none");
            aTo.find("div:first").addClass("display-none");
            ulTo.slideUp();
            ulOne.slideUp();
        }else{
            aOne.removeClass("third-menu-focus");
            aOne.find("div:first").addClass("display-none");
            ulOne.slideUp();
        }
        menuTab.lowerMenu(li);
    },
    /*显示下级菜单的方法*/
    lowerMenu:function (li) {
        var ul=$(li).children("ul");
        $(li).find("a:first").find("div:first").removeClass("display-none");
        if(ul.attr("role")){
            menuTab.arrowRotate(ul);
            ul.slideToggle();
            return true;
        }
        return false;
    },

    /*改变一级菜单的图标*/
//     menuIcon:function (li) {
//         $.each($(li).parent().children("li"),function () {
//             var imgName=($(this).find("a:first").attr("href")+"").replace("#","");
//             $(this).find("img:first").attr("src","images/icon_"+imgName+"_grey.png");
//         });
//         var $a=$(li).children("a:first");
//         var imgName=($a.attr("href")+"").replace("#","");
//         $a.find("img:first").attr("src","images/icon_"+imgName+".png");
//     },
    menuClick:function (li) {
        //menuTab.menuIcon(li);
        var ul=$("#iFrame-content ul:first");
        var a=$(li).find("a:first");
        var id=(a.attr("href")+"").replace("#","");
        var src=a.attr("src");
        var text=a.text();
        var flag=true;
        if($(li).find("li:first").attr("role")!=="presentation"){
            $.each($("#iFrame-content ul:first li"),function () {
                if($(this).find("a:first").attr("href")===("#"+id)){
                    $("#content").children(".active").removeClass("activity");
                    $("#"+id).addClass("activity");
                    $(this).find("a:first").tab("show");
                    flag=false;
                    return false;
                }
            });
            if(flag){
                menuTab.addTag(id,text,src);
            }
        }
        menuTab.menuHeight();
        menuTab.contentH();
        menuTab.tabLength();
    },
    /*拥有下级菜单的菜单箭头旋转动画*/
    arrowRotate:function (ul) {
        var $arrow=$(ul).parent().find(".arrow-position:first");
        if(IE8){
            if($(ul).css("display")==="none"){
                $arrow.addClass("arrow-position-down");
            }else{
                $arrow.removeClass("arrow-position-down");
            }
        }else {
            if($(ul).css("display")==="none"){
                $(ul).parent().find(".arrow-position:first").removeClass("arrow-position-down  arrow-rotateRecover");
                $(ul).parent().find(".arrow-position:first").addClass("arrow-rotate");

            }else{
                $(ul).parent().find(".arrow-position:first").removeClass("arrow-rotate");
                $(ul).parent().find(".arrow-position:first").addClass("arrow-position-down  arrow-rotateRecover");
            }
        }
    },
    setTimeHide:function () {
        menuTab.$menu.hide();
        menuTab.$menu.parent().hide();
        $("#iFrame-content").addClass("width-100");
    },
    mySlideToggle:function () {
        var $menuP=menuTab.$menu.parent();
        var marginLeft=$("#menu").css("width");
        if(menuTab.menuUp){
            //$menu.stop().animate({"width":"0"},500);
            $menuP.stop().animate({"marginLeft":"-"+marginLeft},500);
            menuTab.menuUp=false;
            setTimeout(menuTab.setTimeHide,500);
        }else{
            $("#iFrame-content").removeClass("width-100");
            menuTab.$menu.parent().show();
            menuTab.$menu.show();
            //$menu.stop().animate({"width":"100%"},500);
            $menuP.stop().animate({"marginLeft":"0"},500);
            menuTab.menuUp=true;
        }
        setTimeout(function () {
            menuTab.tabLength();
        },500);
    },
    menuHeight:function () {
        var height=$("body").height()-$(".top").outerHeight();
        $("#menu").parent().outerHeight(height);
        menuTab.menuScroll(height);
    },
    contentH:function () {
        var height=$("body").outerHeight()-$("#iFrame-content").find("div:first").outerHeight()-$(".top").innerHeight()-$(".top").css("margin-bottom").replace("px","");
        $("#content").outerHeight(height);
    },
    reLoad:function (src) {
        var href=$("#iFrame-content").find("ul:first").find("li[class=active]").children("a").attr("href");
        var id=href.replace("#","");
        var iFrame=$("#"+id);
        iFrame.html("");
        iFrame.load(src);
    },
    addIFrame:function (id,src) {
        var html="<div role='tabpanel' class='activity tab-pane fade in' id='"+id+"'>";
        $("#content").append(html);
        $("#"+id).load(src,function () {
            menuTab. menuHeight();
        });
    },
    getJqueryContent:function () {
        return $("#content").children(".activity");
    },
    getContentId:function () {
        if(menuTab.getJqueryContent().size()==1){
            return menuTab.getJqueryContent().attr("id");
        }
        return "";
    },
    menuScroll:function (height) {
        setTimeout(function () {
            var $menu=$("#menu");
            var $parent=$("#menu").parent();
            var menuHeight=$menu.height();
            var $bar=$parent.find(".scrollBar");
            $bar.height(height/2);
            var barHeight=height/2;
            if(menuHeight>height){
                $bar.removeClass("display-none");
                $parent.on("mousewheel",function (event, delta, deltaX, deltaY) {
                    menuHeight=$menu.height();
                    barHeight=height/2;
                    var move=20*deltaY;
                    var menuTop=$menu.position().top;
                    var topAddMoveTemp=menuTop+move;
                    var times=Math.abs((menuHeight-height)/(height-barHeight));
                    //判断  上移还是下移  大于0为向上移动
                    if(deltaY>0){
                        log4j.info("上移");
                        if($menu.position().top<0){
                            //判断菜单是否已经移动到最顶端  是的话直接将top置为0
                            if(topAddMoveTemp>=0){
                                topAddMoveTemp=0;
                            }
                            $menu.stop().animate({"top":topAddMoveTemp+"px"},"fast");
                            $bar.stop().animate({"top":-topAddMoveTemp/times+"px"},"fast");
                        }
                    }else{
                        if(menuHeight>height-menuTop){
                            log4j.info("下移");
                            //判断菜单是否已经移动到最底端  是的话直接将top置为菜单的高度减去菜单上层div的高度
                            if(height-topAddMoveTemp>=menuHeight){
                                topAddMoveTemp=height-menuHeight;
                            }
                            $menu.stop().animate({"top":topAddMoveTemp+"px"},"fast");
                            $bar.stop().animate({"top":-topAddMoveTemp/times+"px"},"fast");
                        }
                    }
                });
            }else{
                $parent.find(".scrollBar").addClass("display-none");
                $parent.off("mousewheel");
                $menu.stop().animate({"top":"0px"},"fast");
                $bar.stop().animate({"top":"0px"},"fast");
            }
            if(height-$menu.position().top>menuHeight){
                var topAddMoveTemp=height-menuHeight;
                if(topAddMoveTemp>0) topAddMoveTemp=0;
                $menu.stop().animate({"top":topAddMoveTemp+"px"},"fast");
                $bar.stop().animate({"top":-topAddMoveTemp/+"px"},"fast");
            }
        },350);
    },
    getWidth:function (node) {
      var width=0;
        $(node).each(function () {
           width+=$(this).outerWidth(true);
        });
        return width;
    },
    tabLength:function () {
        var $div=$("#iFrame-content").children("div").eq(1);
        var $ul=$div.children("ul:first");
        var $activeLi=$ul.children("li[class=active]");
        var divWidth=menuTab.getWidth($div);
        var liW=menuTab.getWidth($activeLi);
        var liPreAllW=menuTab.getWidth($activeLi.prevAll());
        var liNextAllW=menuTab.getWidth($activeLi.nextAll());
        $ul.width(divWidth+liPreAllW+liNextAllW+liW);
        if($ul.width()<divWidth) $ul.width(divWidth);
        menuTab.tabMove($ul,$activeLi,divWidth,liW,liPreAllW,liNextAllW);
    },
    tabMove:function ($ul,$activeLi,divWidth,liW,liPreAllW,liNextAllW) {
        var ulW=$ul.width()-divWidth;
        var liPreW=menuTab.getWidth($activeLi.prev());
        var liNextW=menuTab.getWidth($activeLi.next());
        var oldLeft=$ul.position().left;
        var left=0;
        var tempLength=0;
        var $tempLi=$ul.children("li:first");
        //总长度小于divWidth
        if(ulW<divWidth){
            left=0;
        }else if((-oldLeft)>=liPreAllW){
            //log4j.info("标签在可见区域前");
            left=liPreAllW-liPreW;
        }else if(liPreAllW+liW+liNextW>=(divWidth-oldLeft)){
            //标签在可见区域后
            while(liW+liNextW+liPreAllW>tempLength+divWidth){
                tempLength+=menuTab.getWidth($tempLi);
                $tempLi=$tempLi.next();
                if($tempLi.next().length===0) break;
            }
            left=tempLength;
        }else if(liPreAllW>-oldLeft && liPreAllW+liW<divWidth-oldLeft){
            if(liPreAllW+liW+liNextW>=(divWidth-oldLeft)){
               // log4j.info("标签在可见区域内  后一格不完整");
                while(tempLength<-oldLeft){
                    tempLength+=menuTab.getWidth($tempLi);
                    $tempLi=$tempLi.next();
                }
                left=tempLength+menuTab.getWidth($tempLi);
                if(liPreAllW+liW+liNextW>left+divWidth){
                    left+=menuTab.getWidth($tempLi.next());
                }
            }else{
              //log4j.info("标签在可见区域内  后一格完整还多");
                if($activeLi.next().length===0){
                    while (tempLength+divWidth<liPreAllW+liW){
                        tempLength+=menuTab.getWidth($tempLi);
                        $tempLi=$tempLi.next();
                        if($tempLi.length===0) break;
                    }
                    left=tempLength;
                }else if($activeLi.next().next().length===0){
                    while (tempLength+divWidth<liPreAllW+liW+liNextW){
                        tempLength+=menuTab.getWidth($tempLi);
                        $tempLi=$tempLi.next();
                        if($tempLi.length===0) break;
                    }
                    left=tempLength;
                }else {
                    left=-oldLeft;
                }

            }
        }
        if(left<0) left=0;
        $ul.animate({"left":"-"+left+"px"},"fast");
    },
    tabButtonClick:function (direction) {
        var $div=$("#iFrame-content").children("div").eq(1);
        var $ul=$div.children("ul:first");
        var $lis=$ul.children("li");
        var $activeLi=null;
        var divWidth=menuTab.getWidth($div);
        var ulW=$ul.width()-divWidth;
        var oldLeft=$ul.position().left;
        var left=0;
        var arr=[0,1,2];
        var $tempLi=$ul.children("li:first");
        $lis.each(function () {
            if($(this).hasClass("active")){
                $activeLi=$(this);
                return false;
            }
        });
        while (left<-oldLeft){
            left+=menuTab.getWidth($tempLi);
            $tempLi=$tempLi.next();
        }
        if(direction==="左"){
            $.each(arr,function () {
                if($tempLi.prev().length===0) return false;
                $tempLi=$tempLi.prev();
                left-=menuTab.getWidth($tempLi);
            });
        }else {
            $.each(arr,function () {
                if(ulW<=(left+divWidth)) return false;
                left+=menuTab.getWidth($tempLi);
                $tempLi=$tempLi.next();
            });
        }
        if(left<0) left=0;
        $ul.animate({"left":"-"+left+"px"},"fast");
    }
};

menuTab.start();
menuTab.resize();


