var Element;
Element = {
    /**
     * 给元素创建下拉框
     *
     * @param elements
     *            参数为[元素参数对象集合]
     * @param isIncludeSearch
     *            是否支持查询(0-不支持 1-支持)
     * @param isIncludeNull
     *            是否添加空白option(1-添加 其他值不添加)
     * @param isSearchCondition
     *            是否为查询条件，查询条件name和id都需要加前缀search_,(1-查询条件 其他非查询条件)
     */
    createDictElements: function (elements, async) {
        // 未定义元素 直接返回
        if (elements.length == 0) {
            return;
        }
        async = async == true ? true : false;
        var keies = [];
        $(elements).each(function (index, element) {
            element.label && keies.push(element.label);
        });
        keies = uniqueArray(keies);
        ajaxNormalPost(
            "/pub/dictionary/load_dicts",
            {
                "dictKeies": keies
            },
            function (data) {
                if (data.length == 0) {
                    return;
                }
                $(elements).each(function (eindex, element) {
                    $(data)
                        .each(
                            function (dindex,
                                      dictClass) {
                                if (dictClass.dicCode == element.label) { // 匹配字典信息
                                    if (element.type == Constants.colType.radio) {
                                        Element
                                            .createRadio(
                                                dictClass,
                                                element); // 单选框
                                    } else if (element.type == Constants.colType.checkbox) {
                                        Element
                                            .createCheckbox(
                                                dictClass,
                                                element); // 复选框
                                    } else if (element.type == Constants.colType.multiselect) {
                                        Element
                                            .createMultiselect(
                                                dictClass,
                                                element);
                                    } else {
                                        Element
                                            .createSelect(
                                                dictClass,
                                                element); // 下拉框
                                    }
                                }
                            })
                });
            }, async)
    },

    createRadio: function (dictClass, element) {
        var defClass = "col-width-80 radio";
        if (element.radioClassName) {
            defClass = element.radioClassName;
        }
        if (!element.position) {
            return; // 未标识下拉显示位置
        }
        radioHtml = "";
        // radio前面的文字
        if (element.prevText) {
            checkboxHtml = "<label>" + element.prevText + "</label>";
        }
        var field = element.field;
        if (field) {
            $(dictClass.dicts).each(
                function (dindex, dict) {
                    radioHtml += "<div class=\"" + defClass
                        + "\"><label><input type=\"radio\" name=\""
                        + field + "\" value=\"" + dict.dicCode + "\">"
                        + dict.dicName + "</label></div>";
                });
        }
        $("#" + element.position).append(radioHtml);
    },

    createCheckbox: function (dictClass, element) {
        var defClass = "col-width-80 checkbox";
        if (element.checkboxClassName) {
            defClass = element.checkboxClassName;
        }
        if (!element.position) {
            return;
        }
        checkboxHtml = "";
        // checkBox前面文字
        if (element.prevText) {
            checkboxHtml = "<label>" + element.prevText + "</label>";
        }
        var field = element.field;
        if (field) {
            $(dictClass.dicts).each(
                function (dindex, dict) {
                    checkboxHtml += "<div class=\"" + defClass
                        + "\"><label><input type=\"checkbox\" name=\""
                        + field + "\" value=\"" + dict.dicCode + "\">"
                        + dict.dicName + "</label></div>";
                });
        }
        $("#" + element.position).append(checkboxHtml);
    },

    createMultiselect: function (dictClass, element) {
        Element.createSelect(dictClass, element, true);
    },

    createSelect: function (dictClass, element, ismultiple) {
        var defClass = "form-control";
        var multipleHtml = "";
        if (ismultiple) {
            multipleHtml = "multiple=\"multiple\"";
        }
        if (element.selectClassName) {
            defClass = element.selectClassName;
        }
        if (!element.position) {
            return; // 未标识下拉显示位置
        }
        selectHtml = "";
        var field = element.field;
        if (field) {
            if (element.isSearchCondition) {
                // 查询条件，添加前缀search_
                selectHtml = "<select class=\"" + defClass
                    + "\" name= \"search_" + field + "\" id=\"search_"
                    + field + "\"" + multipleHtml + ">";
                // selectHtml = "<select class=\""+defClass+"\" name= \"search_"
                // + field + "\">";
            } else {
                selectHtml = "<select class=\"" + defClass + "\" name= \""
                    + field + "\" id=\"" + field + "\"" + multipleHtml
                    + ">";
                // selectHtml = "<select class=\""+defClass+"\" name= \"" +
                // field + "\">";
            }
            // 添加空白option
            if (element.isIncludeNull) {
                selectHtml += "<option selected=\"selected\" value=''>-请选择-</option>";
            }
            $(dictClass.dicts).each(
                function (dindex, dict) {
                    selectHtml += "<option value=\"" + dict.dicCode + "\">"
                        + dict.dicName + "</option>";
                });
            selectHtml += "</select>";
            $("#" + element.position).append(selectHtml);

        }
    },

    linkageDiv: function (prevName, linkageValue, divId) {
        $('input[name="' + prevName + '"]').on(
            'click',
            function (event) {
                var defaultValue = event.currentTarget.defaultValue;
                if (defaultValue == linkageValue) {
                    // 文本框清空
                    $("#" + divId).find("input[type='text']").val("");
                    // 文本域清空
                    $("#" + divId).find("textarea").val("");
                    // 单选多选清除选中
                    $("#" + divId).find(
                        "input[type='radio'],input[type='checkbox']")
                        .prop("checked", false);
                    // 下拉多选全部取消
                    $("#" + divId).find("select").multiselect(
                        'clearSelection');
                    $("#" + divId).addClass('hidden');
                } else {
                    $("#" + divId).removeClass('hidden');
                }
            });
    },

    selectCheckBox: function (name, data) {
        if (data) {
            data = data + "";
            var array = data.split(",");
            for (var i = 0; i < array.length; i++) {
                $(":checkbox[name='" + name + "'][value='" + array[i] + "']")
                    .click();
            }
        }
    },

    /**
     * 用户下拉框
     */
    showUser: function (param) {
        var defClass = "form-control ";
        var selectHtml = "";
        var url = "/pub/user/select_list";
        if (param.url) {
            url = param.url;
        }
        ajaxNormalPost(url, {}, function (data) {
            defClass += param.field;
            if (data.data.length == 0) {
                return;
            }
            if (param.isSearchCondition) {
                selectHtml = "<select class=\"" + defClass
                    + "\" name=\"search_" + param.field + "\" id=\"search_"
                    + param.field + "\">";
            } else {
                selectHtml = "<select class=\"" + defClass + "\" name=\""
                    + param.field + "\" id=\"" + param.field + "\">";
            }
            // 添加空白option
            if (param.isIncludeNull) {
                selectHtml += "<option selected=\"selected\">请选择</option>";
            }
            $(data.data).each(
                function (index, user) {
                    selectHtml += "<option value=\"" + user.id + "\">"
                        + user.userName + "</option>";
                });
            selectHtml += "</select>";
            $("#" + param.position).html(selectHtml);
        });
    },

    /**
     * 科室列表
     */
    showDepartment: function (param) {
        var defClass = "form-control ";
        var selectHtml = "";
        var url = "/pub/departments/select_list_wrapper_by_buss_hospital";
        if (param.url) {
            url = param.url;
        }
        ajaxNormalPost(
            url,
            {},
            function (data) {
                log4j.info(data.data);
                defClass += param.field;
                if (data.data.length == 0) {
                    return;
                }
                if (param.isSearchCondition) {
                    selectHtml = "<select class=\"" + defClass
                        + "\" name=\"search_" + param.field
                        + "\" id=\"search_" + param.field + "\">";
                } else {
                    selectHtml = "<select class=\"" + defClass
                        + "\" name=\"" + param.field + "\" id=\""
                        + param.field + "\">";
                }
                // 添加空白option
                if (param.isIncludeNull) {
                    selectHtml += "<option selected=\"selected\" value=\"\">请选择</option>";
                }
                $(data.data).each(
                    function (index, department) {
                        selectHtml += "<option value=\"" + department.id + "\">" + department.depName+"-"+department.chinaSpell.toUpperCase() + "</option>";
                    });
                selectHtml += "</select>";
                $("#" + param.position).html(selectHtml);
            });
    },
    /**
     * 职工列表
     */
    showStaffInfo: function (param) {
        var defClass = "form-control ";
        var selectHtml = "";
        var url = "/pub/staff_info/select_list_wrapper_by_dep_and_hospital";
        if (param.url) {
            url = param.url;
        }
        ajaxNormalPost(
            url,
            {
                "depId": param.depId
            },
            function (data) {
                defClass += param.field;
                if (param.isSearchCondition) {
                    selectHtml = "<select class=\"" + defClass
                        + "\" name=\"search_" + param.field
                        + "\" id=\"search_" + param.field + "\">";
                } else {
                    selectHtml = "<select class=\"" + defClass
                        + "\" name=\"" + param.field + "\" id=\""
                        + param.field + "\">";
                }
                // 添加空白option
                if (param.isIncludeNull || data.data.length == 0) {
                    selectHtml += "<option selected=\"selected\" value=''>请选择</option>";
                }
                $(data.data).each(
                    function (index, staffInfo) {
                        selectHtml += "<option value=\"" + staffInfo.id
                            + "\">" + staffInfo.sffName+"-"+staffInfo.sffLoginNum+"-"+staffInfo.chinaSpell.toUpperCase()
                            + "</option>";
                    });
                selectHtml += "</select>";
                $("#" + param.position).html(selectHtml);
            });
    },

    /**
     * 杭州市医保二级代码
     */
    secondaryCodeList: function (param) {
        var defClass = "form-control ";
        var selectHtml = "";
        var typeName = param.label;
        var url = "/med/meddresccode/select_med_dr_sec_list";
        if (param.url) {
            url = param.url;
        }
        ajaxNormalPost(
            url,
            {"typeName": typeName},
            function (data) {
                defClass += param.field;
                if (data.data.length == 0) {
                    return;
                }
                if (param.isSearchCondition) {
                    selectHtml = "<select class=\"" + defClass
                        + "\" name=\"search_" + param.field
                        + "\" id=\"search_" + param.field + "\">";
                } else {
                    selectHtml = "<select class=\"" + defClass
                        + "\" name=\"" + param.field + "\" id=\""
                        + param.field + "\">";
                }
                // 添加空白option
                if (param.isIncludeNull) {
                    selectHtml += "<option selected=\"selected\" value=\"\">请选择</option>";
                }
                $(data.data).each(
                    function (index, department) {
                        selectHtml += "<option value=\""
                            + department.id + "\">"
                            + department.codeName + "</option>";
                    });
                selectHtml += "</select>";
                $("#" + param.position).html(selectHtml);
            });
    },
    /**
     * 杭州市医保服务项目
     */
    medOneItemList: function (param) {
        var defClass = "form-control ";
        var selectHtml = "";
        var chinaSpell = param.label;
        var url = "/SocialSecurity/MedOneItems/select_MedOneItems_List";
        if (param.url) {
            url = param.url;
        }
        ajaxNormalPost(
            url,
            {"chinaSpell": chinaSpell},
            function (data) {
                defClass += param.field;
                if (data.data.length == 0) {
                    return;
                }
                if (param.isSearchCondition) {
                    selectHtml = "<select class=\"" + defClass
                        + "\" name=\"search_" + param.field
                        + "\" id=\"search_" + param.field + "\">";
                } else {
                    selectHtml = "<select class=\"" + defClass
                        + "\" name=\"" + param.field + "\" id=\""
                        + param.field + "\">";
                }
                // 添加空白option
                if (param.isIncludeNull) {
                    selectHtml += "<option selected=\"selected\" value=\"\">请选择</option>";
                }
                $(data.data).each(
                    function (index, medoneitem) {
                        selectHtml += "<option value=\""
                            + medoneitem.id + "\">"
                            + medoneitem.itemCode + "--" + medoneitem.chinaSpell + "--" + medoneitem.itemName + "--" + medoneitem.chargeCategoryName + "--" + medoneitem.chargeLevelName + "</option>";
                    });
                selectHtml += "</select>";
                $("#" + param.position).html(selectHtml);
            });
    }
};