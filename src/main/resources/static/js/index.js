/**
 * Created by Kiva on 16/12/27.
 */
// (function () {
var $pageContainer = $(".page-container");
var $iNavBar = $(".i-navbar:first");
var menuHideAndShowFlag = true;
$(document).ready(function () {
    $(".i-navbar:first").find("li").on("click", function () {
        var $ul = $(".nav:first");
        menuClick($(this), $ul);
        return false;
    });
    $(document).click(function () {
        if ($("body").hasClass("mini-bar")) {
            menuArrowRotate($("ul[menu-expanded=true]").parent());
        }
    });
    if ($(window).outerWidth(true) <= 768) {
        $("body").addClass("mini-bar");
        menuHideAndShowFlag = false;
        menuBigToMini($pageContainer);
    }
});

$(window).resize(function () {
    $(window).outerWidth(true) <= 768 ? ($("body").addClass("mini-bar"), menuBigToMini($pageContainer), menuHideAndShow(false)) : ($("body").removeClass("mini-bar"), menuMiniToBig($pageContainer, menuHideAndShow(true)));
});

//菜单点击的方法，传入被点击的菜单
function menuClick($li, $ul) {
    menuFocus($li, $ul);
}

//使菜单隐藏然后显示出来的方法
function menuHideAndShow(flag) {
    if (flag != menuHideAndShowFlag) {
        $iNavBar.children().hide();
        setTimeout(function () {
            $iNavBar.children().stop().fadeIn(400);
            menuHideAndShowFlag = !menuHideAndShowFlag;
        }, 200);
    }
}

//菜单从大变小的方法
function menuMiniToBig($div, fn) {
    fn = fn || function () {
    };
    $div.stop().animate({"marginLeft": "220px"}, 200, "linear", function () {
        fn();
    });
}

//菜单从小变大的方法
function menuBigToMini($div, fn) {
    fn = fn || function () {
    };
    $div.stop().animate({"marginLeft": "70px"}, 200, "linear", function () {
        fn();
    });
}

//让所有菜单得到失去焦点的方法
function menuFocus($li, $ul) {
    var $a = $li.find("a:first");
    if ($a.hasClass("menuItem")) {
        $(".menuFocus").removeClass("menuFocus");
        $ul.find(".menuItem").parent().removeClass("menuFocus");
        menuBorderLeft($ul.find(".menuItem").parent());
        $ul.find(".menuItem").find("span:last").addClass("d-n");
        $li.addClass("menuFocus");
        $a.find("span:last").removeClass("d-n");
        menuBorderLeft($li);
        var $div = $(".page-content:first");
        changeIFrame($li, $div);
        var $superUl = $li.parent();
        while ($superUl.attr("class") != "nav") {
            var $superLi = $superUl.parent();
            $superLi.addClass("menuFocus");
            $superUl = $superLi.parent();
        }
    } else {
        $li.addClass("menuFocus");
    }
    menuArrowRotate($li);
}

//让拥有下级菜单的菜单按钮旋转同时展开的方法
function menuArrowRotate($li) {
    $li.siblings("li").find("ul:first").each(function () {
        var $ul = $(this);
        var $arrow = $ul.parent().find("a:first").find("span:last");
        if ($ul.attr("menu-expanded") == "true") {
            ulUp($ul, $arrow);
        }
    });
    var $ul = $li.find("ul:first");
    var $arrow = $li.find("a:first").find("span:last");
    if ($ul.attr("menu-expanded") == "true") {
        ulUp($ul, $arrow);
    } else if ($ul.attr("menu-expanded") == "false") {
        ulDown($ul, $arrow);
    }

    function ulUp($ul, $arrow) {
        $ul.slideUp();
        $ul.attr("menu-expanded", "false");
        $arrow.removeClass("arrow-rotate");
        $arrow.addClass("arrow-rotateRecover");
    }

    function ulDown($ul, $arrow) {
        $ul.slideDown();
        $ul.attr("menu-expanded", "true");
        $arrow.addClass("arrow-rotate");
        $arrow.removeClass("arrow-rotateRecover");
    }
}

//让没有下级菜单的菜单右侧加上取消边框的方法
function menuBorderLeft($li) {
    var $a = $li.find("a:first");
    $li.hasClass("menuFocus") ? $a.stop().animate({"borderLeftWidth": "4px"}, 300) : $a.stop().animate({"borderLeftWidth": "0"}, 300);
}

//改变iframe加载的网页的方法
function changeIFrame($li, $div) {
    var url = $li.find("a:first").attr("lt-url");
    var $iFrame = $div.find(".i-frame");
    $iFrame.attr("src") == url ? ($iFrame.hide(), setTimeout(function () {
        $iFrame.fadeIn();
    }, 200)) : ($iFrame.fadeOut(200, function () {
        $div.html("");
        // var html='<div class="crumb"></div>'
        // var html=' <div id="scrollText">welcome to yskj</div>'
        var html = '<iframe class="i-frame"  src="' + url + '" frameborder="0" seamless></iframe>';
        $div.append(html);
        $div.find(".i-frame").fadeIn(200);
        // createCrumb(url,$li.find('.nav-label').text());
    }));
}


