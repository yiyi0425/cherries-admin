//页面初始化
Param.initPageParam();
var storage = window.localStorage;
var HardwareInfo = storage.getItem('HardwareInfo')
var macAddress;
if (!HardwareInfo) {
    layer.msg('硬件信息获取失败')
} else {
    macAddress = JSON.parse(HardwareInfo).macAddress
}
var user = storage.getItem('user')

var hospitalId = JSON.parse(user).unitId

function GetPrinterInformation(indexCode) {
    var printerType;
    var printerIndex = null;
    ajaxNormalPost('/pub/dictionary/select_by_dic_code', {
        search_dicCode: 'DYJLX',
        search_chinaSpell: indexCode
    }, function (res) {
        if (res.data.length > 0) {
            printerType = res.data[0].id;
            if (!macAddress) {
                return layer.msg('mac地址未获取, 请刷新页面重试')
            }
            ajaxNormalPost('/pub/PrinterManagement/select_list', {
                "search_hospitalId": hospitalId,
                "search_macAddress": macAddress,
                "search_printerType": printerType
            }, function (data) {
                if (data.data.length > 0) {
                    printerIndex = data.data[0].printerName;
                } else {
                    alertHIS('打印机未设置, 请重新维护打印机, 如已经匹配打印机请刷新页面!')
                }
            });
        } else {
            alertHIS('无此类型打印机，请前去维护')
        }
    })

    return printerIndex;
}

// 挂号个人日汇总  registeredPersonalDaySummary---------------
function registeredPersonalDaySummary(data) {
    var count = 1;
    LODOP = getLodop();
    // PRINT_INITA函数与PRINT_INIT都有初始化功能，建议和要求同PRINT_INIT。
    // 如果打印页有上边距或左边距要求，或可视编辑区域大小用实际纸张大小不合适时，调用本函数。

    LODOP.PRINT_INITA(0, 0, '210mm', '297mm', "挂号个人日汇总");

    // 设定打印纸张为固定纸张或自适应内容高，并设定相关大小值或纸张名及打印方向

    LODOP.SET_PRINT_PAGESIZE(1, 0, 0, "A4");

    var strHtml = "<base href=\"http://localhost:63342/trunk/clinic/pages/registration/reportForm/\"/><script>window.alert = null;window.confirm = null;window.open = null;window.showModalDialog = null;</script><style> table,td,th {border: 1px solid black;border-style: solid;border-collapse: collapse}</style>"
        +
        "<table border='1'>\n" +
        "    <tr >\n" +
        "        <td width='10%'>科室名称</td>\n" +
        "        <td width='5%'>初诊</td>\n" +
        "        <td width='5%'>复诊</td>\n" +
        "        <td width='10%'>挂号总数</td>\n" +
        "        <td width='5%'>退号</td>\n" +
        "        <td width='5%'>作废</td>\n" +
        "        <td width='5%'>接诊</td>\n" +
        "        <td width='8%'>挂号费</td>\n" +
        "        <td width='8%'>诊查费</td>\n" +
        "        <td width='10%'>合计金额</td>\n" +
        "        <td width='8%'>省医保</td>\n" +
        "        <td width='8%'>市医保</td>\n" +
        "        <td width='10%'>实收现金</td>\n" +
        "    </tr>\n"
    var strTemp
    if (data && data.length > 0) {
        for (var i = 0; i < 2; i++) {
            (data[i].returnVisits == null) ? data[i].returnVisits = '0'
                : data[i].returnVisits;
            (data[i].totalVisits == null) ? data[i].totalVisits = '0'
                : data[i].totalVisits;
            (data[i].retiredVisits == null) ? data[i].retiredVisits = '0'
                : data[i].retiredVisits;
            (data[i].obsoleteVisits == null) ? data[i].obsoleteVisits = '0'
                : data[i].obsoleteVisits;
            (data[i].visits == null) ? data[i].visits = '0' : data[i].visits;
            (data[i].registrationFee == null) ? data[i].registrationFee = '0.00'
                : data[i].registrationFee;
            (data[i].firstVisits == null) ? data[i].firstVisits = '0'
                : data[i].firstVisits;
            (data[i].hzybFee == null) ? data[i].hzybFee = '0.00'
                : data[i].hzybFee;
            (data[i].examinationFee == null) ? data[i].examinationFee = '0.00'
                : data[i].examinationFee;
            (data[i].totalFee == null) ? data[i].totalFee = '0.00'
                : data[i].totalFee;
            (data[i].szybFee == null) ? data[i].szybFee = '0.00'
                : data[i].szybFee;
            (data[i].cashFee == null) ? data[i].cashFee = '0.00'
                : data[i].cashFee;
            count++;
            strTemp = "    <tr>\n" +
                "        <td align='right'>" + data[i].depName + "</td>\n" +
                "        <td align='right'>" + data[i].firstVisits + "</td>\n" +
                "        <td align='right'>" + data[i].returnVisits + "</td>\n"
                +
                "        <td align='right'>" + data[i].totalVisits + "</td>\n" +
                "        <td align='right'>" + data[i].retiredVisits + "</td>\n"
                +
                "        <td align='right'>" + data[i].obsoleteVisits
                + "</td>\n" +
                "        <td align='right'>" + data[i].visits + "</td>\n" +
                "        <td align='right'>" + data[i].registrationFee
                + "</td>\n" +
                "        <td align='right'>" + data[i].examinationFee
                + "</td>\n" +
                "        <td align='right'>" + data[i].totalFee + "</td>\n" +
                "        <td align='right'>" + data[i].szybFee + "</td>\n" +
                "        <td align='right'>" + data[i].hzybFee + "</td>\n" +
                "        <td align='right'>" + data[i].cashFee + "</td>\n" +
                "    </tr>\n"
            strHtml = strHtml + strTemp
        }
    }
    strHtml = strHtml + "</table>\n "

    LODOP.ADD_PRINT_TEXT(57, 76, 531, 35, Param.user.hosptalName + "挂号操作员个人日汇总报表");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 14);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);

    LODOP.ADD_PRINT_TABLE(140, 28, 686, 27 * count, strHtml);

    LODOP.ADD_PRINT_TEXT(117, 33, 47, 20, "操作员：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(117, 79, 100, 20, "操作员姓名");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(117, 264, 85, 20, "结账单单号：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(117, 347, 100, 20, "201804170102");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(117, 524, 69, 20, "打印日期：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(117, 592, 100, 20, "2018年04月17日");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);

    LODOP.ADD_PRINT_TEXT(166 + 24 * (count - 1), 29, 95, 20, "挂号区间：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(166 + 24 * (count - 1), 124, 555, 20, "150~584");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.SET_PRINT_STYLEA(0, "TextFrame", 2);
    LODOP.SET_PRINT_STYLEA(0, "SpacePatch", 1);
    LODOP.ADD_PRINT_TEXT(191 + 24 * (count - 1), 29, 95, 20, "退号区间");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(191 + 24 * (count - 1), 123, 559, 20, "120~152");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.SET_PRINT_STYLEA(0, "TextFrame", 2);
    LODOP.SET_PRINT_STYLEA(0, "SpacePatch", 1);

    // LODOP.SET_PRINTER_INDEXA(3);
    // LODOP.PRINT_DESIGN();
    // LODOP.PRINT();

}

// 挂号全院日汇总 registeredHospitalDaySummary------------
function registeredHospitalDaySummary() {
    LODOP = getLodop();
    // PRINT_INITA函数与PRINT_INIT都有初始化功能，建议和要求同PRINT_INIT。
    // 如果打印页有上边距或左边距要求，或可视编辑区域大小用实际纸张大小不合适时，调用本函数。

    LODOP.PRINT_INITA(14, 11, 800, 600, "挂号全院日汇总");

    // 设定打印纸张为固定纸张或自适应内容高，并设定相关大小值或纸张名及打印方向

    LODOP.SET_PRINT_PAGESIZE(1, 0, 0, "A4");
}

//煎药打印模版--------------
function CreatePrintTisane(hospitalName, receiveNum, rcpPosts, totalNumber, patName, remark) {
    LODOP = getLodop();
    var data = {
        hospitalName: '',
        receiveNum: '',
        rcpPosts: '',
        totalNumber: '',
        patName: '',
        remark: ''
    };
    LODOP.PRINT_INITA(14, 11, 800, 600, "煎药打印模板");
    LODOP.SET_PRINT_PAGESIZE(1, 1300, 1200, "");
    LODOP.ADD_PRINT_TEXT(99, 91, 296, 25, hospitalName);
    LODOP.ADD_PRINT_TEXT(135, 88, 253, 20, receiveNum);
    LODOP.ADD_PRINT_TEXT(178, 199, 91, 20, "中草药：" + rcpPosts + " 帖");
    LODOP.ADD_PRINT_TEXT(214, 89, 100, 20, "代煎：" + rcpPosts + " 帖");
    LODOP.ADD_PRINT_TEXT(216, 199, 100, 20, "" + totalNumber + "包");
    LODOP.ADD_PRINT_TEXT(179, 89, 100, 20, "姓名：" + patName + "");
    LODOP.ADD_PRINT_TEXT(251, 89, 142, 41, "备注：" + remark + "");

    // LODOP.SET_PRINTER_INDEXA(3);
    // // LODOP.PRINT_DESIGN();
    // LODOP.PRINT();

};

//煎药另煎打印模版-------
function CreatePrintTisanePart(data) {
    var data = data;
    LODOP = getLodop();

    // var strHtml = "<base href=\"http://localhost:63342/trunk/clinic/pages/drugStore/chinaSendChildren/\"/><script>window.alert = null;window.confirm = null;window.open = null;window.showModalDialog = null;</script><style> table,td,th {border: 1px solid black;border-style: solid;border-collapse: collapse}</style>" +
    //     "<table border='1'>\n"
    // var strTemp
    // if (data) {
    //     for (var i = 0; i < data.listRecipeDetailWrapper.length; i++) {
    //         (data.listRecipeDetailWrapper[i].drgName == null) ? data.listRecipeDetailWrapper[i].drgName = '0' : data.listRecipeDetailWrapper[i].drgName;
    //         (data.listRecipeDetailWrapper[i].redPrice == null) ? data.listRecipeDetailWrapper[i].redPrice = '0' : data.listRecipeDetailWrapper[i].redPrice;
    //         (data.listRecipeDetailWrapper[i].redBatch == null) ? data.listRecipeDetailWrapper[i].redBatch = '0' : data.listRecipeDetailWrapper[i].redBatch;
    //         strTemp = "    <tr>\n" +
    //             "        <td align='right'>" + data.patCardNum + "</td>\n" +
    //             "        <td align='right'>" + data.agentDate + "</td>\n" +
    //             "        <td align='right'>" + data.patName + "</td>\n" +
    //             "        <td align='right'>" + data.agentDate + "</td>\n" +
    //             "        <td align='right'>" + data.listRecipeDetailWrapper[i].depName + "</td>\n" +
    //             "        <td align='right'>" + data.listRecipeDetailWrapper[i].redPrice + "</td>\n" +
    //             "        <td align='right'>" + data.listRecipeDetailWrapper[i].redBatch + "</td>\n" +
    //             "        <td align='right'>发药人姓名</td>\n" +
    //             "        <td align='right'>病人签名</td>\n" +
    //             "        <td align='right'>" + data.agentDate + "</td>\n" +
    //             "        <td align='right'></td>\n" +
    //             "        <td align='right'></td>\n" +
    //             "        <td align='right'></td>\n" +
    //             "    </tr>\n"
    //         strHtml = strHtml + strTemp
    //     }
    // }
    // strHtml = strHtml + "</table>\n "

    var printHtml
    printHtml = "<style> table,td,th {border: 1px solid black;border-style: solid;border-collapse: collapse}</style><table border=\"1\">\n"
    if (data) {
        for (var i = 0; i < data.listRecipeDetailWrapper.length; i++) {

            printHtml += "    <tr>\n" +
                "        <td>" + data.listRecipeDetailWrapper[i].drgName
                + data.listRecipeDetailWrapper[i].redPrice
                + data.listRecipeDetailWrapper[i].redBatch + "</td>\n" +
                "    </tr>\n"

        }
    }
    printHtml = printHtml + "</table>\n "
    log4j.info(data.listRecipeDetailWrapper)
    log4j.info(printHtml)

    LODOP.PRINT_INITA(14, 11, 800, 600, "煎药另煎打印模板");
    LODOP.SET_PRINT_PAGESIZE(1, 1300, 1200, "");

    LODOP.ADD_PRINT_TEXT(121, 90, 162, 30, "门诊号：" + data.patCardNum + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(121, 355, 190, 30, "打印日期：" + data.agentDate + "  ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(175, 96, 91, 30, "姓名：" + data.patName + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(175, 359, 181, 30, "收费日期：" + data.agentDate + "  ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);

    LODOP.ADD_PRINT_TEXT(350, 94, 141, 30, "发药人姓名：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(350, 291, 142, 30, "病人签名：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(350, 501, 215, 30, "发药时间：" + data.agentDate + "");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(62, 89, 554, 40, Param.user.hosptalName + "另煎药品取药单");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 16);
    LODOP.ADD_PRINT_TABLE(230, 90, 686, 300, printHtml);

    // LODOP.SET_PRINTER_INDEXA(3);
    // // LODOP.PRINT_DESIGN();
    // LODOP.PRINT();
}

// 查询医疗机构编号
let medicalNumber = ''
function getMedicalNumber(feeId) {
    ajaxNormalPost('/pub/hospitalMedical/select_list', { search_hospitalId: Param.user.hospitalId}, function(data) {
        if (feeId == 2) {
            medicalNumber = data.data[0].institutionalCode
        } else if (feeId == 3) {
            medicalNumber = data.data[1].institutionalCode
        }
    })
}

//省医保申报表打印模板
function CreatePrintForm(tableHtml, index, date) {
    getMedicalNumber(3)
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var day = new Date().getDate();
    let dateSearch = date ? date.split("-")[0] + "年" + date.split("-")[1] + "月" :  year+ "年" + (month-1) + "月"
    LODOP = getLodop();
    var printHtml = `<style>table {width: 100%;border-collapse: collapse;}thead td,tbody tr:last-child td {text-align: center;font-weight: 700; height: 28px;}
        td {padding: 3px 4px;width:50%;font-size: 14px;border:1px solid #000}tbody td:nth-of-type(odd) {text-align: left;}tbody td:nth-of-type(even) {text-align: right;}</style>`
    printHtml += tableHtml
    // LODOP.PRINT_INITA(14, 11, 800, 900, "省医保申报表打印模板");
    // LODOP.SET_PRINT_PAGESIZE(1, 1300, 1200, "");
    LODOP.PRINT_INITA(0, 0, "210mm", "148mm", "省医保申报表打印模板");
    printZoom(2)  // 缩放纸张
    LODOP.ADD_PRINT_TEXT(39,229,322,40, "省级各类医疗费用申请核拨表");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 17);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.ADD_PRINT_TABLE(144,61,646,521, printHtml);

    LODOP.ADD_PRINT_TEXT(82,342,78,25, dateSearch);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
    LODOP.ADD_PRINT_TEXT(115,56,280,25,"定点医疗机构盖章：" + Param.user.hosptalName + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(115,337,192,25,"定点医疗机构编号：" + medicalNumber + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(115,575,129,25, "单位：元（下至角分）");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(428,58,169,25, "单位负责人：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(428,244,169,25, "财务负责人：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(429,411,138,25, "制表人：" + Param.user.name + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(428,568,169,25,"制表日期：" + year + "年" + month + "月" + day + "日" + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(458,56,556,56,"说明：1.本表于次月十日前报省医保中心\r\n      2.离休干部其中审批药品费用请填列《离休干部审批药品申请核拨表》");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);

    LODOP.SET_PRINTER_INDEXA(index);
    // LODOP.PRINT_DESIGN();
    LODOP.PRINT();
};

//市医保子女统筹申报表
function CreatePrintChildrenForm(tableHtml, index,date) {
    getMedicalNumber(2)
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var day = new Date().getDate();
    var data = data;
    let dateSearch = date ? date.split("-")[0] + "年" + date.split("-")[1] + "月" :  year+ "年" + (month-1) + "月"
    LODOP = getLodop();
    var printHtml = `<style>table {width: 100%;border-collapse: collapse;border:1px solid #000;text-align: center;}
        thead td,tbody tr:last-child td {text-align: center;font-weight: 700;height: 28px;} td {padding: 3px 4px;border: 1px solid #000;font-size: 14px;}</style>`
    printHtml += tableHtml

    // LODOP.PRINT_INITA(14, 11, 800, 900, "市医保子女统筹申报表打印模板");
    // LODOP.SET_PRINT_PAGESIZE(1, 2000, 3000, "");
    LODOP.PRINT_INITA(0, 0, "210mm", "148mm", "市医保子女统筹申报表打印模板");
    printZoom(2)  // 缩放纸张
    // LODOP.SET_PRINT_PAGESIZE(1, 2100, 1480, "");
    LODOP.ADD_PRINT_TEXT(35,117,554,40, "杭州市离休干部、子女统筹、保健干部医疗费申请核拨表");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.ADD_PRINT_TABLE(133,24,698,487, printHtml);
    LODOP.ADD_PRINT_TEXT(76,338,103,25, dateSearch);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
    LODOP.ADD_PRINT_TEXT(110,37,305,25,"医疗机构名称(盖章)：" + Param.user.hosptalName + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(109,527,192,25,"医疗机构编号：" + medicalNumber + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(443,32,115,25, "负责人：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(443,318,100,25, "制表人：" + Param.user.name + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(443,543,161,25,"制表日期：" + year + "年" + month + "月" + day + "日" + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);

    LODOP.SET_PRINTER_INDEXA(index);
    // LODOP.PRINT_DESIGN();
    LODOP.PRINT();
};

//市医保基本医疗申报表
function CreatePrintMedicalCityForm(tableHtml, index,date) {
    getMedicalNumber(2)
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var day = new Date().getDate();
    let dateSearch = date ? date.split("-")[0] + "年" + date.split("-")[1] + "月" :  year+ "年" + (month-1) + "月"
    var data = data;
    LODOP = getLodop();
    var printHtml = `<style>table {width: 100%;border-collapse: collapse;border:1px solid #000;text-align: center;}
        thead td,tbody tr:last-child td {text-align: center;font-weight: 700;height: 28px;} td {padding: 3px 4px;border: 1px solid #000;font-size: 14px;}</style>`
    printHtml += tableHtml

    // LODOP.PRINT_INITA(14, 11, 800, 900, "市医保基本医疗申报表打印模板");
    // LODOP.SET_PRINT_PAGESIZE(1, 2000, 3000, "");
    LODOP.PRINT_INITA(0, 0, "210mm", "148mm", "市医保基本医疗申报表打印模板");
    printZoom(2)  // 缩放纸张
    // LODOP.SET_PRINT_PAGESIZE(1, 2100, 1480, "");
    LODOP.ADD_PRINT_TEXT(41,83,554,40, "     杭州市基本医疗保险定点医疗机构医疗费申请核拨表");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.ADD_PRINT_TABLE(133,24,698,487, printHtml);
    LODOP.ADD_PRINT_TEXT(80,328,103,25, dateSearch);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
    LODOP.ADD_PRINT_TEXT(110,37,305,25,"医疗机构名称(盖章)：" + Param.user.hosptalName + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(109,527,192,25,"医疗机构编号：" + medicalNumber + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(452,32,115,25, "负责人：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(456,318,100,25, "制表人：" + Param.user.name + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(455,543,161,25,"制表日期：" + year + "年" + month + "月" + day + "日" + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);

    LODOP.SET_PRINTER_INDEXA(index);
    // LODOP.PRINT_DESIGN();
    LODOP.PRINT();

};

//门诊药房收支汇总月报表--------

function CreateSummaryOfIEForm(data) {
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var day = new Date().getDate();
    var data = data;
    LODOP = getLodop();

    var printHtml;

    printHtml = "<style> table,td,th {border: 1px solid black;border-style: solid;border-collapse: collapse} table{width:500px;}</style><table border=\"1\">\n"
    printHtml += " <tr>\n" +
        "<td colspan='5' style='height:50px;font-size:20;font-weight:bold;text-align: center;'>项目</td>\n"
        +
        "<td colspan='5' style='height:50px;font-size:20;font-weight:bold;text-align: center;'>药品</td>\n"
        +
        "<td colspan='5' style='height:50px;font-size:20;font-weight:bold;text-align: center;'>材料</td>\n"
        +
        "<td colspan='5' style='height:50px;font-size:20;font-weight:bold;text-align: center;'>合计</td>\n"
        +
        "</tr>\n";

    if (data) {
        printHtml += "<tr>\n" +
            "<td colspan='5'>" + data[0].turnoverWay + "</td>\n" +
            "<td colspan='5'>" + (data[0].tallyAmount).toFixed(2) + "</td>\n" +
            "<td colspan='5'>" + (0.00).toFixed(2) + "</td>\n" +
            "<td colspan='5'>" + (data[0].tallyAmount).toFixed(2) + "</td>\n" +
            "</tr>\n";
        printHtml += "<tr>\n" +
            "<td rowspan='6' colspan='1'>收入</td>\n";
        for (var i = 1; i < 7; i++) {
            printHtml +=
                "<td colspan='4'>" + data[i].turnoverWay + "</td>\n" +
                "<td colspan='5'>" + (data[i].tallyAmount).toFixed(2)
                + "</td>\n" +
                "<td colspan='5'>" + (0.00).toFixed(2) + "</td>\n" +
                "<td colspan='5'>" + (data[i].tallyAmount).toFixed(2)
                + "</td>\n" +
                "</tr>\n"
        }
        printHtml += "<tr>\n" +
            "<td rowspan='7' colspan='1'>支出</td>\n";
        for (var j = 7; j < 14; j++) {
            printHtml +=
                "<td colspan='4'>" + data[j].turnoverWay + "</td>\n" +
                "<td colspan='5'>" + (data[j].tallyAmount).toFixed(2)
                + "</td>\n" +
                "<td colspan='5'>" + (0.00).toFixed(2) + "</td>\n" +
                "<td colspan='5'>" + (data[j].tallyAmount).toFixed(2)
                + "</td>\n" +
                "</tr>\n"
        }
        for (var k = 14; k < 17; k++) {
            printHtml += "<tr>\n" +
                "<td colspan='5'>" + data[k].turnoverWay + "</td>\n" +
                "<td colspan='5'>" + (data[k].tallyAmount).toFixed(2)
                + "</td>\n" +
                "<td colspan='5'>" + (0.00).toFixed(2) + "</td>\n" +
                "<td colspan='5'>" + (data[k].tallyAmount).toFixed(2)
                + "</td>\n" +
                "</tr>\n";
        }

        // printHtml += "<tr>\n"+
        //     "<td colspan='5' width='20px'>" + data[0].natName + "</td>\n"+
        //     "<td colspan='5'>" + data[0].edPatNum + "</td>\n"+
        //     "<td colspan='5'>" + data[0].edPatNum + "</td>\n" +
        //     "<td colspan='5'>" + data[0].edTotalMedicalFee + "</td>\n" +
        //     "</tr>\n";
        // printHtml += "<tr>\n"+
        //     "<td rowspan='6' colspan='1'>收入</td>\n"+
        //     "<td colspan='4'>" + data[0].natName + "</td>\n"+
        //     "<td colspan='5'>" + data[0].edPatNum + "</td>\n" +
        //     "<td colspan='5'>" + data[0].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[0].edSelfPaying + "</td>\n" +
        //     "</tr>\n"+
        //     "<td colspan='4'>" + data[1].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n"+
        //     "<td colspan='4'>" + data[1].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n"+
        //     "<td colspan='4'>" + data[1].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n"+
        //     "<td colspan='4'>" + data[1].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n"+
        //     "<td colspan='4'>" + data[1].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n"+
        //     "<tr>\n"+
        //     "<td rowspan='7' colspan='1'>支出</td>\n"+
        //     "<td colspan='4'>" + data[2].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[2].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[2].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[2].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n"+
        //     "<td colspan='4'>" + data[1].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n"+
        //     "<td colspan='4'>" + data[1].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n"+
        //     "<td colspan='4'>" + data[1].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n"+
        //     "<td colspan='4'>" + data[1].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n"+
        //     "<td colspan='4'>" + data[1].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[1].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n"+
        //     "<td colspan='4'>" + data[3].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[3].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[3].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[3].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n";
        //
        // printHtml += "<tr>\n"+
        //     "<td colspan='5'>" + data[0].natName + "</td>\n"+
        //     "<td colspan='5'>" + data[0].edPatNum + "</td>\n"+
        //     "<td colspan='5'>" + data[0].edPatNum + "</td>\n" +
        //     "<td colspan='5'>" + data[0].edTotalMedicalFee + "</td>\n" +
        //     "</tr>\n"+
        //     "</tr>\n"+
        //     "<td colspan='5'>" + data[3].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[3].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[3].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[3].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n"+
        //     "<td colspan='5'>" + data[3].natName + "</td>\n" +
        //     "<td colspan='5'>" + data[3].edTotalMedicalFee + "</td>\n" +
        //     "<td colspan='5'>" + data[3].edSelfPaying + "</td>\n" +
        //     "<td colspan='5'>" + data[3].edMedicalExpenses + "</td>\n" +
        //     "</tr>\n";

    }
    printHtml = printHtml + "</table>\n "

    log4j.info(data)
    log4j.info(printHtml)

    LODOP.PRINT_INITA(14, 11, 800, 900, "药房收支汇总月报表打印模板");
    LODOP.SET_PRINT_PAGESIZE(1, 2000, 3000, "");
    LODOP.ADD_PRINT_TEXT(62, 89, 554, 40, "    " + Param.user.hosptalName + "药房收支汇总月报表");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
    LODOP.ADD_PRINT_TABLE(237, 102, 553, 487, printHtml);

    LODOP.ADD_PRINT_TEXT(118, 263, 171, 25, "统计月份：2018年3月");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);

    LODOP.ADD_PRINT_TEXT(768, 80, 226, 25,
        "制表日期：" + year + "年" + month + "月" + day + "日" + "  ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.ADD_PRINT_TEXT(768, 483, 244, 25, "制表人：" + data[0].sffName + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);

    // LODOP.SET_PRINTER_INDEXA(3);
    // // LODOP.PRINT_DESIGN();
    // LODOP.PRINT();

};

//药库出入库-分页
function CreateGoDownEntryForm(data, index,titleName, storeName) {
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var day = new Date().getDate();
    var data = data;
    LODOP = getLodop();
    var printHtml = "";
    let putName;   // 出入库文字提示
    if (data.hasOwnProperty('inDetailWrapperList')) {
        putName = '入库'
        let DetailWrapperList = data.inDetailWrapperList
        printHtml = putInStorageHtml(DetailWrapperList,printHtml)
    }else {
        putName = '出库'
        let DetailWrapperList = data.outDetailWrapperList
        printHtml = outboundHtml(DetailWrapperList,printHtml, data.turnoverCode)
    }
    printHtml = printHtml + "</table>\n "
    // if (Param.user.hospitalId == "330000") {
    //     LODOP.PRINT_INITA(0, 0, "210mm", "148mm", "药库药品验收单打印模板");
    //     LODOP.SET_PRINT_PAGESIZE(2, 1480, 2100, "");
    // } else {
    //     LODOP.PRINT_INITA(0, 0, 800, 796, "药库药品验收单打印模板");
    //     LODOP.SET_PRINT_PAGESIZE(1, 2420, 1400, "");
    // }
    LODOP.PRINT_INITA(0, 0, 800, 796, "药库药品验收单打印模板");
    LODOP.SET_PRINT_PAGESIZE(1, 2420, 1400, "");
    LODOP.ADD_PRINT_TABLE(75, 32, 732, 300, printHtml);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);

    LODOP.ADD_PRINT_TEXT(19,208,387,25, Param.user.hosptalName + storeName + titleName + '清单');
    LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
    LODOP.SET_PRINT_STYLEA(0,"LinkedItem",1);
    if (data.hasOwnProperty('inDetailWrapperList')) {
        LODOP.ADD_PRINT_TEXT(52, 38, 200, 20, "供货单位：" + (data.supName || ''));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
        LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
        LODOP.ADD_PRINT_TEXT(53,631,131,20, "验收日期：" + data.turnoverDate + " ");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
        LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
        LODOP.ADD_PRINT_TEXT(390, 467, 100, 20, "科室主管：");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
        LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
        LODOP.ADD_PRINT_TEXT(390, 257, 95, 20, "采购：");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
        LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
        LODOP.ADD_PRINT_TEXT(390, 360, 100, 20, "验收：");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
        LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
        LODOP.ADD_PRINT_TEXT(53,303,138,20, "入库单号：" + data.turnoverNum + " ");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
        LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    }else {
        LODOP.ADD_PRINT_TEXT(52, 38, 200, 20, "单位：" + (data.supName || ''));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
        LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
        LODOP.ADD_PRINT_TEXT(53,631,131,20, "日期：" + data.turnoverDate + " ");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
        LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
        LODOP.ADD_PRINT_TEXT(390, 257, 95, 20, "领用人：");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
        LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
        LODOP.ADD_PRINT_TEXT(390, 360, 100, 20, "发药人：");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
        LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    }
    LODOP.ADD_PRINT_TEXT(53,483,150,20, "编号：" + data.turnoverNum + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(390,44,100,20, "制表：" + (data.turnoverSffName == null ? '' : data.turnoverSffName) + "");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(416, 44, 150, 20,
        "制表日期：" + year + "年" + month + "月" + day + "日" + "    ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(417,210,551,20, "备注： " + data.summary);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(390,150,100,20, "财会：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT("4.23mm","184.68mm","20.48mm","8.2mm", putName);
    LODOP.SET_PRINT_STYLEA(0,"FontName","楷体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 16);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    LODOP.SET_PRINT_STYLEA(0,"TextFrame",11);

    //强制分页
    LODOP.NewPageA();

    LODOP.PRINT_DESIGN();
    // LODOP.SET_PRINTER_INDEXA(index);
    // LODOP.PRINT();

};

// 药库出入库打印模板
function putInStorageHtml(DetailWrapperList,printHtml) {
    printHtml = "<style> table{width: 100%;} table,td {border: 1px solid black;border-style:solid;border-collapse:" +
        " collapse} td {font-size: 13px;text-align: center;}</style><table border=\"1\">\n"
    printHtml += " <thead><tr>\n" +
        "<td>序号</td>\n" +
        "<td>发票号</td>\n" +
        "<td>名称</td>\n" +
        "<td>规格</td>\n" +
        "<td>单位</td>\n" +
        "<td>批号</td>\n" +
        "<td>购入数量</td>\n" +
        "<td>进价</td>\n" +
        "<td>进价总额</td>\n" +
        "<td>零售价</td>\n" +
        "<td>零售总额</td>\n" +
        "<td>进销差价</td>\n" +
        "</tr></thead>\n"
    printHtml += "<tbody>\n"
    for (var i = 0; i < DetailWrapperList.length; i++) {
        printHtml += "<tr>\n" +
            "        <td style='text-align: left'>"+ (i + 1) + "</td>\n" +
            "        <td style='text-align: center'>"+ (DetailWrapperList[i].tickNum || '')+ "</td>\n" +
            "        <td style='text-align: left'>" + DetailWrapperList[i].drgName + "</td>\n" +
            "        <td style='text-align: left'>" + DetailWrapperList[i].drgSpecification + "</td>\n" +
            "        <td style='text-align: left'>" + DetailWrapperList[i].drgPackingUnit + "</td>\n" +
            "        <td style='text-align: center'>"+ DetailWrapperList[i].drugBatchNum + "</td>\n" +
            "        <td style='text-align: right'>" + (DetailWrapperList[i].drugBigQuantity).toFixed(3) + "</td>\n" +
            "        <td style='text-align: right'>" + (DetailWrapperList[i].drugBuyPrice).toFixed(2) + "</td>\n" +
            "        <td style='text-align: right'>" + (DetailWrapperList[i].drugTotalBuyPrice).toFixed(2) + "</td>\n" +
            "        <td style='text-align: right'>" + (DetailWrapperList[i].drugPrice).toFixed(2) + "</td>\n" +
            "        <td style='text-align: right'>" + (DetailWrapperList[i].drugTotalPrice).toFixed(2) + "</td>\n" +
            "        <td style='text-align: right'>" + (DetailWrapperList[i].netPrice).toFixed(2) + "</td>\n" + "    </tr>\n";
    }
    printHtml += "<tr>\n" +
        "    <td></td>\n" +
        "<td  tdata=\"AllCount\" format=\"#\" style='text-align: center'>\n" +
        "<p align=\"center\"><b>共&nbsp&nbsp<b>#<b>&nbsp&nbsp笔</b></p>\n" +
        "</td>\n" +
        "    <td></td>\n" +
        "    <td></td>\n" +
        "    <td></td>\n" +
        "    <td></td>\n" +
        "    <td></td>\n" +
        "    <td></td>\n" +
        "<td tdata=\"AllSum\" format=\"0.00\" text-align: center'><b>###</b></td>\n" +
        "<td></td>\n" +
        "<td tdata=\"AllSum\" format=\"0.00\" text-align: center'><b>###</b></td>\n" +
        "<td tdata=\"AllSum\" format=\"0.00\" text-align: center'><b>###</b></td>\n" + " </tr>"
    return printHtml
}
function outboundHtml(DetailWrapperList,printHtml,negative) {
    negative = negative == 12 ? '-' : ''
    printHtml = "<style> table{width: 100%;} table,td {border: 1px solid black;border-style:" +
        " solid;border-collapse:collapse} td {font-size: 13px;text-align: center;}</style><table border=\"1\">\n"
    printHtml += " <thead><tr>\n" +
        "<td>序号</td>\n" +
        "<td>发票号</td>\n" +
        "<td>名称</td>\n" +
        "<td>规格</td>\n" +
        "<td>单位</td>\n" +
        "<td>产地厂家</td>\n" +
        "<td>批号</td>\n" +
        "<td>实拨数量</td>\n" +
        "<td>请领数量</td>\n" +
        "<td>进价</td>\n" +
        "<td>进价总额</td>\n" +
        "<td>零售价</td>\n" +
        "<td>零售总额</td>\n" +
        "</tr></thead>\n"
    printHtml += "<tbody>\n"
    for (var i = 0; i < DetailWrapperList.length; i++) {
        printHtml += "<tr>\n" +
            "        <td style='text-align: center'>"+ (i + 1) + "</td>\n" +
            "        <td style='text-align: right'>" + (DetailWrapperList[i].tickNum || '')+ "</td>\n" +
            "        <td style='text-align: center'>" + DetailWrapperList[i].drgName + "</td>\n" +
            "        <td style='text-align: left'>" + DetailWrapperList[i].drgSpecification + "</td>\n" +
            "        <td style='text-align: left'>" + DetailWrapperList[i].drgPackingUnit + "</td>\n" +
            "        <td style='text-align: left'>" + DetailWrapperList[i].drgRegionName + "</td>\n" +
            "        <td style='text-align: right'>" + DetailWrapperList[i].drugBatchNum + "</td>\n" +
            "        <td style='text-align: right'>" + negative + DetailWrapperList[i].grantQuantity.toFixed(3) + "</td>\n" +
            "        <td style='text-align: right'>" + negative + (DetailWrapperList[i].applyQuantity).toFixed(3) + "</td>\n" +
            "        <td style='text-align: right'>" + DetailWrapperList[i].drugBuyPrice.toFixed(2) + "</td>\n" +
            "        <td style='text-align: right'>" + negative + DetailWrapperList[i].drugBuyAmount.toFixed(2) + "</td>\n" +
            "        <td style='text-align: right'>" + DetailWrapperList[i].drugPrice.toFixed(2) + "</td>\n" +
            "        <td style='text-align: right'>" + negative + (DetailWrapperList[i].applyQuantity*DetailWrapperList[i].drugPrice).toFixed(2) + "</td>\n" +
            "    </tr>\n";
    }
    printHtml += "<tr>\n" +
        "    <td></td>\n" +
        "<td  tdata=\"AllCount\" format=\"#\" style='text-align: center'>\n"
        +"<p align=\"center\"><b>共&nbsp&nbsp<b>#<b>&nbsp&nbsp笔</b></p>\n" +
        "</td>\n" +
        "    <td></td>\n" +
        "    <td></td>\n" +
        "    <td></td>\n" +
        "    <td></td>\n" +
        "    <td></td>\n" +
        "    <td></td>\n" +
        "    <td></td>\n" +
        "    <td></td>\n" +
        "    <td tdata=\"AllSum\" format=\"0.00\" style='text-align: center'><b>###</b></td>\n" +
        "    <td></td>\n" +
        "    <td tdata=\"AllSum\" format=\"0.00\" style='text-align: center'><b>###</b></td>\n" +
        "    </tr>"
    return printHtml
}

// 药房出入库打印模板
function pharmacyInTemplate(data,printHtml) {
    printHtml = "<style>td {font-size: 13px;text-align: center} table {width: 100%} table,td,th {border: 1px solid black;border-style: solid;border-collapse: collapse}</style><table border=\"1\">\n"
    printHtml += " <thead><tr>\n" +
        "<td colspan='2'>序号</td>\n" +
        "<td colspan='10'>名称</td>\n" +
        "<td colspan='5'>规格</td>\n" +
        "<td colspan='6'>单位</td>\n" +
        "<td colspan='10'>产地厂家</td>\n" +
        "<td colspan='10'>生产批号</td>\n" +
        "<td colspan='10'>实拨数量</td>\n" +
        "<td colspan='10'>请领数量</td>\n" +
        "<td colspan='10'>零售价</td>\n" +
        "<td colspan='10'>零售金额</td>\n" +
        "</tr></thead>\n"
    printHtml += "<tbody>\n"
    for (var i = 0; i < data.inDetailWrapperList.length; i++) {
        printHtml += "<tr>\n" +
            "        <td colspan='2' style='text-align: left'>"
            + (i + 1) + "</td>\n" +
            "        <td colspan='10' style='text-align: left'>"
            + data.inDetailWrapperList[i].drgName + "</td>\n" +
            "        <td colspan='5' style='text-align: left'>"
            + data.inDetailWrapperList[i].drgSpecification + "</td>\n" +
            "        <td colspan='6' style='text-align: left'>"
            + data.inDetailWrapperList[i].drgPackingUnit + "</td>\n" +
            "        <td colspan='10' style='text-align: right'>"
            + data.inDetailWrapperList[i].drgRegionName + "</td>\n" +
            "        <td colspan='10' style='text-align: right'>"
            + (data.inDetailWrapperList[i].drugBatchNum || '') + "</td>\n" +
            "        <td colspan='10' style='text-align: right'>"
            + (data.inDetailWrapperList[i].drugBigQuantity).toFixed(3)
            + "</td>\n" +
            "        <td colspan='10' style='text-align: right'>"
            + (data.inDetailWrapperList[i].drugBigQuantity).toFixed(3)
            + "</td>\n" +
            "        <td colspan='10' style='text-align: right'>"
            + (data.inDetailWrapperList[i].drugPrice || 0).toFixed(2) + "</td>\n"
            +
            "        <td colspan='10' style='text-align: right'>"
            + (data.inDetailWrapperList[i].drugTotalPrice || 0).toFixed(2)
            + "</td>\n" +
            "    </tr>\n";
    }
    printHtml += "</tbody>\n"
    printHtml += "<tr><td colspan='2'></td>\n" +
        "<td colspan='10' tdata=\"AllCount\" format=\"#\" style='text-align: left'>\n"+
        "<p align=\"center\"><b>共&nbsp&nbsp<b>#<b>&nbsp&nbsp笔</b></p>\n" +
        "</td>\n" +
        "<td colspan='15'></td>\n" +
        "<td colspan='6'></td>\n" +
        "<td colspan='10'></td>\n" +
        "<td colspan='10'></td>\n" +
        "<td colspan='10'></td>\n" +
        "<td colspan='10' tdata=\"AllSum\" format=\"0.00\" style='text-align: right'><font>###</font></td>\n" +
        "<td colspan='10' tdata=\"AllSum\" format=\"0.00\" style='text-align: right'><font>###</font></td>\n" +
        " </tr>\n"
    printHtml = printHtml + "</table>\n "
    return printHtml
}
function pharmacyOutTemplate(data,printHtml) {
    printHtml = "<style>td {font-size: 13px;text-align: center} table {width: 100%} table,td,th {border: 1px solid black;border-style: solid;border-collapse: collapse}</style><table border=\"1\">\n"
    printHtml += " <thead><tr>\n" +
        "<td colspan='2'>序号</td>\n" +
        "<td colspan='10'>名称</td>\n" +
        "<td colspan='5'>规格</td>\n" +
        "<td colspan='6'>单位</td>\n" +
        "<td colspan='10'>产地厂家</td>\n" +
        "<td colspan='10'>生产产地</td>\n" +
        "<td colspan='10'>批号</td>\n" +
        "<td colspan='10'>零售价</td>\n" +
        "<td colspan='10'>库存数量</td>\n" +
        "<td colspan='10'>出库数量</td>\n" +
        "</tr></thead>\n"
    printHtml += "<tbody>\n"
    for (var i = 0; i < data.outDetailWrapperList.length; i++) {
        printHtml += "<tr>\n" +
            "        <td colspan='2' style='text-align: left'>"
            + (i + 1) + "</td>\n" +
            "        <td colspan='10' style='text-align: left'>"
            + data.outDetailWrapperList[i].drgName + "</td>\n" +
            "        <td colspan='5' style='text-align: left'>"
            + data.outDetailWrapperList[i].drgSpecification + "</td>\n" +
            "        <td colspan='6' style='text-align: left'>"
            + data.outDetailWrapperList[i].drgPackingUnit + "</td>\n" +
            "        <td colspan='10' style='text-align: right'>"
            + data.outDetailWrapperList[i].drgRegionName + "</td>\n" +
            "        <td colspan='10' style='text-align: right'>"
            + data.outDetailWrapperList[i].drgFactoryName + "</td>\n" +
            "        <td colspan='10' style='text-align: right'>"
            + (data.outDetailWrapperList[i].drugBatchNum || '') + "</td>\n" +
            "        <td colspan='10' style='text-align: right'>"
            + (data.outDetailWrapperList[i].drugPrice || 0).toFixed(2) + "</td>\n" +
            "        <td colspan='10' style='text-align: right'>"
            + ((data.outDetailWrapperList[i].stringtotalQuantity || 0)).toFixed(3)+ "</td>\n" +
            "        <td colspan='10' style='text-align: right'>"
            + (data.outDetailWrapperList[i].applyQuantity).toFixed(3)+ "</td>\n" +
            "    </tr>\n";
    }
    printHtml += "</tbody>\n"
    printHtml += "<tr><td colspan='2'></td>\n" +
        "<td colspan='10' tdata=\"AllCount\" format=\"#\" style='text-align: left'>\n"+
        "<p align=\"center\"><b>共&nbsp&nbsp<b>#<b>&nbsp&nbsp笔</b></p>\n" +
        "</td>\n" +
        "<td colspan='15'></td>\n" +
        "<td colspan='6'></td>\n" +
        "<td colspan='10'></td>\n" +
        "<td colspan='10'></td>\n" +
        "<td colspan='10'  tdata=\"AllSum\" format=\"0.00\" style='text-align: right'><font>###</font></td>\n" +
        "<td colspan='10'></td>\n" +
        "<td colspan='10' tdata=\"AllSum\" format=\"0.00\" style='text-align: right'><font>###</font></td>\n" +
        " </tr>\n"
    printHtml = printHtml + "</table>\n "
    return printHtml
}


//药房出入库-分页
function CreateLeadForm(data, index, title, storeName) {
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var day = new Date().getDate();
    var data = data;
    LODOP = getLodop();
    let printHtml
    let putName;   // 出入库文字提示
    if (data.hasOwnProperty('inDetailWrapperList')) {
        putName = '入库'
        printHtml = pharmacyInTemplate(data,printHtml)
    }else {
        putName = '出库'
        printHtml = pharmacyOutTemplate(data,printHtml)
    }
    // if (Param.user.hospitalId == "330004") {
    //     LODOP.PRINT_INITA(0, 0, 800, 796, "药库药品验收单打印模板");
    //     LODOP.SET_PRINT_PAGESIZE(1, 2420, 1400, "");
    // } else {
    //     LODOP.PRINT_INITA(0, 0, "210mm", "148mm", "药房药品领用清单打印模板");
    //     LODOP.SET_PRINT_PAGESIZE(2, 1480, 2100, "");
    // }
    LODOP.PRINT_INITA(0, 0, 800, 796, "药房药品领用清单打印模板");
    LODOP.SET_PRINT_PAGESIZE(1, 2420, 1400, "");


    LODOP.ADD_PRINT_TABLE(86, 32, "93%", 318, printHtml);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
    title = title.indexOf('药房') > -1 ? title.split('药房')[1] : title
    LODOP.ADD_PRINT_TEXT(26,195,413,25, Param.user.hosptalName + storeName + title +"清单");
    LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
    LODOP.SET_PRINT_STYLEA(0,"LinkedItem",1);
    LODOP.ADD_PRINT_TEXT(66,342,183,20,"单位：" + (data.supName || ''));
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(64,600,147,20, "日期：" + data.turnoverDate + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(66, 45, 125, 19, "编号：" + data.turnoverCode + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(66,162,137,19,"入库单号：" + data.turnoverNum);
    LODOP.SET_PRINT_STYLEA(0,"FontSize",8);
    LODOP.ADD_PRINT_TEXT(418, 42, 100, 20, "领用人：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(419, 153, 95, 20, "发药人：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(419, 257, 100, 20, "会计：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(420, 360, 127, 20, "制表：" + Param.user.name + "");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(441,42,729,20, "备注： " + data.summary);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT("6.35mm","184.68mm","20.48mm","8.2mm", putName);
    LODOP.SET_PRINT_STYLEA(0,"FontName","楷体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 16);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    LODOP.SET_PRINT_STYLEA(0,"TextFrame",11);

    LODOP.PRINT_DESIGN();
    // LODOP.SET_PRINTER_INDEXA(index);
    // LODOP.PRINT();

}

//西药库盘点-分页
function CreateInventoryList(dataPrint, index) {
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var day = new Date().getDate();
    var data = dataPrint;
    LODOP = getLodop();
    var printHtml;
    printHtml = "<style> table {width: 100%;}table,td {border: 1px solid black;border-style:solid;border-collapse:collapse}</style><table border=\"1\">\n"
    printHtml += " <thead><tr>\n" +
        "<td colspan='2' style='text-align: left'>序号</td>\n" +
        "<td colspan='10' style='text-align: left'>药品名称</td>\n"
        +
        "<td colspan='12' style='text-align: left'>药品规格</td>\n"
        +
        "<td colspan='10' style='text-align: left'>生产厂商</td>\n"
        +
        "<td colspan='5' style='text-align: left'>库存</td>\n" +
        "<td colspan='8' style='text-align: left'>盘点数量</td>\n" +
        "<td colspan='5' style='text-align: left'>盈亏</td>\n" +
        "<td colspan='2' style='text-align: left'>单位</td>\n" +
        "<td colspan='5' style='text-align: left'>零售价</td>\n" +
        "<td colspan='10' style='text-align: left'>批号</td>\n" +
        "<td colspan='12' style='text-align: left'>有效日期</td>\n"
        +
        "</tr></thead>\n"
    printHtml += "<tbody>\n"
    if (data) {
        for (var i = 0; i < data.inventoryDetails.length; i++) {
            printHtml += "    <tr>\n" +
                "        <td colspan='2'>" + (i + 1)
                + "</td>\n" +
                "        <td colspan='10'>"
                + data.inventoryDetails[i].drgName + "</td>\n" +
                "        <td colspan='12' >"
                + data.inventoryDetails[i].bigDrgSpecification + "</td>\n" +
                "        <td colspan='10' >"
                + data.inventoryDetails[i].drgRegionName + "</td>\n" +
                "        <td colspan='5' >"
                + data.inventoryDetails[i].bigNum + "</td>\n" +
                "        <td colspan='8' >"
                + data.inventoryDetails[i].bigInventNum + "</td>\n" +
                "        <td colspan='5' >"
                + (data.inventoryDetails[i].profitLoss == null ? ''
                    : data.inventoryDetails[i].profitLoss) + "</td>\n" +
                "        <td colspan='2' >"
                + data.inventoryDetails[i].bigDrgPackingUnit + "</td>\n" +
                "        <td colspan='5' >"
                + data.inventoryDetails[i].drgBigPrice + "</td>\n" +
                "        <td colspan='10' >"
                + data.inventoryDetails[i].drugBatchNum + "</td>\n" +
                "        <td colspan='12' >"
                + data.inventoryDetails[i].drugValidity + "</td>\n" +
                "    </tr>\n";
        }
    }
    printHtml += "<tbody>\n"
    printHtml += "<tfoot>\n" +
        "  <tr>\n" +
        "    <td colspan='2'></td>\n" +
        "<td colspan='10' tdata=\"subCount\" format=\"#\" style='text-align: left'>\n"
        +
        "<p align=\"center\"><b>共&nbsp&nbsp<b>#<b>&nbsp&nbsp笔</b></p>\n" +
        "</td>\n" +
        "<td colspan='12'></td>\n" +
        "<td colspan='10'></td>\n" +
        "    <td colspan='5'></td>\n" +
        "    <td colspan='8' tdata=\"subSum\" format=\"0.00\" style='text-align: right'><font>###</font></td>\n"
        +
        "<td colspan='5'></td>\n" +
        "<td colspan='2'></td>\n" +
        "<td colspan='5' tdata=\"subSum\" format=\"0.00\" style='text-align: right'><font>###</font></td>\n"
        +
        "<td colspan='10'></td>\n" +
        "<td colspan='12'></td>\n" +
        " </tr>\n" +
        "  </tfoot>\n"
    printHtml = printHtml + "</table>\n "
    log4j.info(data)
    log4j.info(printHtml)

    if (Param.user.hospitalId == "330000") {
        LODOP.PRINT_INITA(0, 0, "210mm", "148mm", "盘点打印模板");
        LODOP.SET_PRINT_PAGESIZE(2, 1480, 2100, "");
    } else {
        LODOP.PRINT_INITA(0, 0, 800, 900, "盘点打印模板");
        LODOP.SET_PRINT_PAGESIZE(1, 2420, 1400, "");
    }

    LODOP.ADD_PRINT_TABLE(106, 23, 700, 322, printHtml);

    LODOP.ADD_PRINT_HTM(32, 88, 414, 25, Param.user.hosptalName + "药库药品盘点单");
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.ADD_PRINT_TEXT(70, 26, 208, 20, "购货单位：" + data.storeName + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(70, 246, 177, 20, "盘点部门：" + data.depName + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(70, 441, 148, 20, "盘点日期: " + data.inventoryDate + " ");
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);

    LODOP.ADD_PRINT_TEXT(462, 26, 100, 25, "制表：" + data.inventorySffName + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(462, 191, 202, 25,
        "制表日期：" + year + "年" + month + "月" + day + "日" + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(462, 498, 80, 25, "备注： ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);

    // LODOP.PRINT_DESIGN();

    LODOP.SET_PRINTER_INDEXA(index);
    log4j.info(index)
    LODOP.PRINT();

}

//调价-分页
function CreatePriceListForm(mes, index) {
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var day = new Date().getDate();
    var data = mes.listAdjustDetailWrapper;
    var price = 0;
    LODOP = getLodop();
    var printHtml
    printHtml = "<style> table {width:100%} table,td,th {border: 1px solid black;border-style: solid;border-collapse:" +
        " collapse}</style><table border=\"1\">\n"
    printHtml += " <thead><tr>\n" +
        "<td colspan='2' style='font-size: 10px;text-align: left'>序号</td>\n" +
        "<td colspan='12' style='font-size: 10px;text-align: left'>药品名称</td>\n"
        +
        "<td colspan='12' style='font-size: 10px;text-align: left'>药品规格</td>\n"
        +
        "<td colspan='2' style='font-size: 10px;text-align: left'>单位</td>\n" +
        "<td colspan='10' style='font-size: 10px;text-align: left'>库存</td>\n" +
        "<td colspan='10' style='font-size: 10px;text-align: left'>调前进价</td>\n" +
        "<td colspan='10' style='font-size: 10px;text-align: left'>调后进价</td>\n" +
        "<td colspan='10' style='font-size: 10px;text-align: left'>调前售价</td>\n"
        +
        "<td colspan='10' style='font-size: 10px;text-align: left'>调后售价</td>\n"
        +
        "<td colspan='10' style='font-size: 10px;text-align: left'>调盈</td>\n"
        +
        "<td colspan='10' style='font-size: 10px;text-align: left'>调亏</td>\n"
        +
        "</tr></thead>\n"
    printHtml += "<tbody>\n"
    if (data) {
        for (var i = 0; i < data.length; i++) {
             let profit =0.00;
             let loss =0.00;
            if(data[i].turnoverCode==33){
                profit =0;

                profit= (data[i].drugNewPrice- data[i].drugPrimaryPrice)*data[i].drugBigQuantity.toFixed(4);
              }else if (data[i].turnoverCode==83){
                loss =0.00;
                loss= (data[i].drugPrimaryPrice - data[i].drugNewPrice)*data[i].drugBigQuantity.toFixed(4);
              }else{
                profit =0.00;
                loss =0.00;
            }
            printHtml += " <tr>\n" +
                //序号
                "        <td colspan='2' style='font-size: 10px;'>" + (i + 1)
                + "</td>\n" +
                //名称
                "        <td colspan='12' style='font-size: 10px;'>"
                + data[i].drgName + "</td>\n" +
                //规格
                "        <td colspan='12' style='font-size: 10px;'>"
                + data[i].drgSpecification + "</td>\n" +
                //单位
                "        <td colspan='2' style='font-size: 10px;'>"
                + (data[i].drgPackingUnit == '' || data[i].drgPackingUnit
                == null ? '' : data[i].drgPackingUnit) + "</td>\n" +
                //库存
                "        <td colspan='10' style='font-size: 10px;'>"
                + data[i].drugBigQuantity + "</td>\n" +
                //调前进价
                "        <td colspan='10' style='font-size: 10px;text-align: right'>"
                + (data[i].drugPrimaryBuyPrice || '') + "</td>\n" +
                //调后进价
                "        <td colspan='10' style='font-size: 10px;text-align: right'>"
                + (data[i].drugNewBuyPrice || '') + "</td>\n" +
                //调前售价
                "        <td colspan='10' style='font-size: 10px;text-align: right'>"
                + data[i].drugPrimaryPrice + "</td>\n" +
                //调后售价
                "        <td colspan='10' style='font-size: 10px;text-align: right'>"
                + data[i].drugNewPrice + "</td>\n" +
                //调盈
                "        <td colspan='10' style='font-size: 10px;text-align: right'>"
                + profit
                + "</td>\n" +
                //条亏
                "        <td colspan='10' style='font-size: 10px;text-align: right'>"
                + loss + "</td>\n" +
                "    </tr>\n";
            price += ((data[i].drugPrimaryPrice - data[i].drugNewPrice)
                * data[i].drugBigQuantity) * data[i].drugBigQuantity
        }
        printHtml += "</tbody>\n"
        printHtml += "<tfoot>\n" +
            "  <tr>\n" +
            "    <td colspan='2'></td>\n" +
            "<td colspan='12' tdata=\"subCount\" format=\"#\" style='font-size: 12px;text-align: left'>\n"
            +
            "<p align=\"center\"><b>共&nbsp&nbsp<b>#<b>&nbsp&nbsp笔</b></p>\n" +
            "</td>\n" +
            "<td colspan='12'></td>\n" +
            "<td colspan='2'></td>\n" +
            "    <td colspan='10'></td>\n" +
            "    <td colspan='10'></td>\n" +
            "<td colspan='10'></td>\n" +
            "<td colspan='10'></td>\n" +
            "<td colspan='10'></td>\n" +
            "<td colspan='10'  tdata=\"subSum\" format=\"0.00\" style='font-size: 10px;text-align: right'><font>###</font></td>\n"
            +
            "<td colspan='10' tdata=\"subSum\" format=\"0.00\" style='font-size: 10px;text-align: right'><font>###</font></td>\n"
            +
            " </tr>\n" +
            "  </tfoot>\n"
        // printHtml += "<tr>\n" +
        //     "<td colspan='2' style='font-size: 10px;'></td>" +
        //     "<td colspan='12' style='font-size: 10px;'>合计" + data.length + "笔</td>" +
        //     "<td colspan='12' style='font-size: 10px;'></td>" +
        //     "<td colspan='2' style='font-size: 10px;'></td>" +
        //     "<td colspan='10' style='font-size: 10px;'></td>" +
        //     "<td colspan='10' style='font-size: 10px;'></td>" +
        //     "<td colspan='10' style='font-size: 10px;'></td>" +
        //     "<td colspan='10' style='font-size: 10px;'></td>" +
        //     "<td colspan='10' style='font-size: 10px;'></td>" +
        //     "<td colspan='10' style='font-size: 10px;text-align: right'>"+(price).toFixed(4)+"</td>" +
        //     "</tr>\n";
    }
    printHtml = printHtml + "</table>\n "
    if (Param.user.hospitalId == "330000") {
        LODOP.PRINT_INITA(0, 0, "210mm", "148mm", "调价单打印模板");
        LODOP.SET_PRINT_PAGESIZE(2, 1480, 2100, "");
    } else {
        LODOP.PRINT_INITA(0, 0, 800, 796, "调价单打印模板");
        LODOP.SET_PRINT_PAGESIZE(1, 2420, 1400, "");
    }

    LODOP.ADD_PRINT_TABLE(100,35,722,285, printHtml);
    LODOP.ADD_PRINT_TEXT(25,217,405,30, Param.user.hosptalName + "调价清单");
    LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
    LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.SET_PRINT_STYLEA(0,"LinkedItem",1);
    LODOP.ADD_PRINT_TEXT(75,36,208,20, "编号：" + mes.turnoverNum + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    // LODOP.ADD_PRINT_TEXT(75,638,121,20, "编号：" + mes.turnoverNum + " ");
    // LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    // LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    // LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);

    LODOP.ADD_PRINT_TEXT(444, 37, 100, 20, "制表：" + Param.user.name);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(75,270,166,20,"制表日期：" + year + "年" + month + "月" + day + "日" + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(75,463,166,20,"调价日期：" + mes.adjDate + " ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(444,157,80,20, "备注： ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);

    // LODOP.PRINT_DESIGN();
    LODOP.SET_PRINTER_INDEXA(index);
    LODOP.PRINT();

}

// 获取诊断,主诉,电子签名
function additionalData(visId) {
    let ZD,ZS,loginNum;
    // 查询病人症段
    ajaxNormalPost("/cli/diagnose_info/select_list", {'search_visId' : visId }, item => {
        ZD = item.data
    })
    // 查询主诉
    ajaxNormalPost("/cli/visited_info/select_wrapper_by_primary_key", {'id' : visId }, i => {
        ZS = i.data
    })
    // 获取电子签名
    ajaxNormalPost("/pub/staff_info/select_by_primary_key", {'id' : ZS.sffId }, j => {
        loginNum = j.data.sffLoginNum
    })
    let zdmx = '';
    if (ZD) {
        ZD.forEach(item => {
            zdmx += item.diaName + ' ' || ''
        })
    }
    return {ZS,loginNum,zdmx}
}
/**
 * 项目打印
 * @param {Object} data 数据
 * @param {Object} peopleInfo 就诊病人信息
 * @param {String} index 打印机名称
 * @param {Number} visId 就诊病人id
 * */
function projectPackage(data, peopleInfo, index, visId) {
    let {ZS,loginNum,zdmx} = additionalData(visId)
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var day = new Date().getDate();
    let numIndex = index
    let purpose;   // 检查目的
    LODOP = getLodop();
    Object.keys(data).forEach(j => {
        var project
        if (j == 'XM') project = "收 费 治 疗 单"
        else if (j == 'JC') project = "超声检查申请单"
        else if (j == 'JY') project = "检验项目清单"
        var patient = peopleInfo.patientsWrapper;
        data[j].forEach((jItem, jIndex) => {
            if (data[j][jIndex].length == 0) return
            let newArr = []
            if (data[j][jIndex] && data[j][jIndex].length > 18 ) {
                newArr.push(data[j][jIndex].slice(0,18))
                newArr.push(data[j][jIndex].slice(18))
            } else if (!data[j][jIndex]) {
                return
            } else {
                newArr.push(data[j][jIndex])
            }
            newArr.forEach((item,index) => {
                var printHtml = "<style> table thead tr{font-weight: 700} table,td,th {border: 0px solid #61964e;border-style:" +
                    " solid;border-collapse: collapse;} td{height: 22px;} .level1 td { height: 16px; padding-left: 10px; font-size: 12px;} </style>" +
                    "<table border=\"1\"><thead><tr><td width='280mm'>项目名称</td><td width='70mm'>数量</td><td width='70mm'>单位</td><td width='70mm'>金额(元)</td></tr></thead><tbody>";
                var tableHtml = "";
                let totalMoney = 0;
                for (var i = 0; i < item.length; i++) {
                    if (i == 0) purpose = item[i].warningTips
                    tableHtml += `<tr class="${item[i].mainFlag == 1 ? '' : 'level1'}"><td width='300mm'>${item[i].itemName}</td><td width='100mm'>${item[i].exdQuantity}</td><td width='100mm'>${item[i].itemUnit}</td><td width='100mm'>${(+item[i].exdSumPrice).toFixed(2)}</td></tr>`;
                    if (item[i].mainFlag == 1) {
                        totalMoney += (+item[i].exdSumPrice)
                    }
                }
                ZS.visSymptom = ZS.visSymptom.length > 110 ? ZS.visSymptom.slice(0, 109) + '。。。' : ZS.visSymptom
                let hpiHeight = 17*Math.floor(ZS.visSymptom.length/28) || 0
                let addressHeight = (patient.patFamAddress || '').length > 18 ? 20 * Math.floor(patient.patFamAddress.length / 18) : 0
                let purposeMultiple = purpose ? (15*Math.floor(purpose.length/26) || 0) : 0
                printHtml = printHtml + tableHtml + "</tbody></table>";
                LODOP.PRINT_INITA(0, 0, "148mm", "210mm", "处方笺打印模板"+(+new Date()));
                LODOP.SET_PRINT_PAGESIZE(1, 1480, 2100, "CreateCustomPage");
                LODOP.SET_PRINT_MODE("RESELECT_PRINTER", true);
                LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
                LODOP.ADD_PRINT_TEXT("7.94mm","18.79mm","89.91mm","7.41mm", Param.user.hosptalName);
                LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
                LODOP.SET_PRINT_STYLEA(0, "FontSize", 16);
                LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
                LODOP.ADD_PRINT_TEXT(57,96,287,28,project);
                LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
                LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
                LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
                LODOP.ADD_PRINT_TEXT("32.01mm","1.85mm","37.57mm","5.29mm", "姓 名：" + patient.patName);
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT("26.46mm","1.59mm","38.1mm","5.29mm", "门诊号：" + patient.patCardNum);
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT("32.01mm","40.48mm","24.08mm","5.29mm", "性别：" + (patient.patSex == 1 ? '男' : '女'));
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT("32.01mm","66.68mm","23.55mm","5.29mm", "年龄：" + patient.patAge);
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT("32.01mm","93.13mm","39.16mm","5.29mm","类别：" + patient.feeName);
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT("26.46mm","92.87mm","39.42mm","5.29mm", "日期：" + year + "-" + month + "-" + day);
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT(142,352,147,20, "电话：" + patient.patPhone);
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT("37.57mm","1.85mm","91.28mm",20+addressHeight, "地 址：" + (patient.patFamAddress || ''));
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT(163+addressHeight,5,66,22, "脉 案：");
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT(163+addressHeight,62,439,22+hpiHeight+addressHeight, ZS.visSymptom);
                LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
                LODOP.SET_PRINT_STYLEA(0,"LineSpacing",-3);
                LODOP.ADD_PRINT_TEXT(184+hpiHeight+addressHeight,7,66,22, "诊 断：");
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT(184+hpiHeight+addressHeight,63,438,22, zdmx);
                LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
                LODOP.ADD_PRINT_TEXT(205+hpiHeight+addressHeight,8,100,22,"检查目的：");
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT(205+hpiHeight+addressHeight,87,415,22+purposeMultiple,purpose || '');
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.SET_PRINT_STYLEA(0,"LineSpacing",-3);
                LODOP.ADD_PRINT_TABLE(230+hpiHeight+purposeMultiple+addressHeight,7,494,422-hpiHeight-purposeMultiple-addressHeight, printHtml);
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT(664,11,156,25, "申请日期："+`${year}-${month}-${day}`);
                LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
                LODOP.ADD_PRINT_TEXT(643,12,56,25,"合计");
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.ADD_PRINT_TEXT(642,417,84,25, (+totalMoney).toFixed(2));
                LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
                LODOP.ADD_PRINT_TEXT(663,311,140,25,"申请医生：" + (ZS.turnSffName || ZS.sffName));
                LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
                LODOP.ADD_PRINT_TEXT(663,448,59,25, ZS.turnSffName ? ZS.sffName : '');
                LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
                LODOP.ADD_PRINT_TEXT(737,421,80,25, `第${index+1}页/共${newArr.length}页`);
                LODOP.ADD_PRINT_TEXT(685,11,407,74,"注意事项: \r\n  1. 检查肝,胆,脾,胰需要禁食水8小时以上\r\n  2. 子宫附件经阴道检查需排空膀胱\r\n  3. 经腹部检查,子宫附件,前列腺,膀胱需充盈膀胱");
                LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
                LODOP.SET_PRINT_STYLEA(0,"LineSpacing",-5);
                LODOP.ADD_PRINT_IMAGE(707,416,80,26,`<img src='${Param.host_url}/clinic/images/dzqm/${loginNum}.bmp'>`);
                LODOP.SET_PRINT_STYLEA(0,"Stretch",2)
                LODOP.ADD_PRINT_LINE(252+hpiHeight+purposeMultiple+addressHeight,8,252+hpiHeight+purposeMultiple+addressHeight,504,0,1);
                LODOP.ADD_PRINT_LINE(229+hpiHeight+purposeMultiple+addressHeight,8,229+hpiHeight+purposeMultiple+addressHeight,504,0,1);
                LODOP.ADD_PRINT_LINE(661,6,662,502,0,1);
                LODOP.ADD_PRINT_BARCODE(32,410,100,40,"128Auto",patient.patCardNum);
                // LODOP.PRINT_DESIGN();
                LODOP.SET_PRINTER_INDEXA(numIndex);
                LODOP.PRINT();
            })
        })
    })
}

/**
 * 中药处方表格
 * @param recipeDetailWrapper
 * @param rcpType  判断是否为 5颗粒剂
 * @param usage    判断是否为 2膏方(4列)
 * @returns
 */
function chTemplate(recipeDetailWrapper, rcpType, usage) {
    let allGrams = 0;         // 单帖克数
    let drgPoison = 0;        // 普通0, 毒品1
    let drgSpiritGrade = 0;   // 非精神药0
    let col = usage == 2 ? 4 : 3   // 列数
    let tableHtml = "";
    let printHtml = `<style> table,td {border: 0px solid black;border-style: solid;border-collapse:collapse;${usage == 2? 'font-size: 13px;line-height: 14px;' : ''}}</style><table border="1">`
    if (recipeDetailWrapper.some(item => item.treId)) {
        let xdf = recipeDetailWrapper.filter( item =>  item.treId )  // 所有的组方和协定方
        let xdfArr = [];  // 组方协定方的treID
        xdf.forEach(item => xdfArr.push(item.treId))
        xdfArr = Array.from(new Set(xdfArr))
        xdfArr.forEach(id => {
            let xdfId;   // 协定方的id
            let xdfName;
            ajaxNormalPost('/cli/treaty_recipe/select_by_primary_key', {id}, function (res) {
                if (res.ret == 0) {
                    if (res.data.treStatus == 0) {
                        xdfId = id
                        xdfName = {
                            drgName: res.data.treName,
                            redOnceDose: '',
                            drgPackingUnit: '',
                            modName: '煎服'
                        }
                    }
                } else {
                    layer.msg(res.msg)
                }
            })
            if (xdfId) recipeDetailWrapper = recipeDetailWrapper.filter(item => item.treId != xdfId)
            if (xdfName) recipeDetailWrapper.unshift(xdfName)
        })
    }
    for (var k = 0; k < Math.ceil(recipeDetailWrapper.length / col); k++) {
        tableHtml = tableHtml + "<tr>\n";
        let max = k === Math.ceil(recipeDetailWrapper.length / col) - 1 ? recipeDetailWrapper.length - k * col : col
        for (var j = 0; j < max; j++) {
            let reDeWra = recipeDetailWrapper[col * k + j]
             if(reDeWra.drgPoison != '0') drgPoison = reDeWra.drgPoison
             if(reDeWra.drgSpiritGrade != '0') drgSpiritGrade = reDeWra.drgSpiritGrade
            if (reDeWra.drgName.indexOf('/') > 0) {
                reDeWra.drgName = reDeWra.drgName.slice(0,reDeWra.drgName.indexOf('/',reDeWra.drgName.indexOf('/') + 1))
            }
            let unit = (reDeWra.modName && reDeWra.modName != '煎服' && rcpType != 5) ? "/" + reDeWra.modName : ''
            let dose = reDeWra.redOnceDose + reDeWra.drgPackingUnit
            tableHtml = tableHtml + "  <td width='400mm' style='padding-bottom:8px;'>" + reDeWra.drgName.split("[")[0] + " " + dose + unit + "</td>";
            if (reDeWra.drgPackingUnit == "g") allGrams += +reDeWra.redOnceDose
        }
        tableHtml = tableHtml + "</tr>\n";
    }
    printHtml += tableHtml + "</table>";
    return { printHtml, allGrams, drgPoison, drgSpiritGrade }
}
/**
 * 中药处方打印
 * @param {Object} data 数据
 * @param {String} index 打印机名称
 * @param {Number} hospitalId 医院编号
 * @param {Number} number 是否为补打
 * @param {Boolean} out 是否外配处方
 * @param {Boolean} twoPiece 打印两张处方 (优先判断)
 * */
function chMedicine(data, index, hospitalId = 330000, number = 0,out = false, twoPiece = false) {
    LODOP = getLodop();
    var patient = data.patientsWrapper;
    for (let i = 0; i < data.recipeInfoWrapperList.length; i++) {
        let recipeInfo = data.recipeInfoWrapperList[i];
        let usage = recipeInfo.rcpSpecialUsage     // 判断是否为膏方 2膏方
        let getMedCard = recipeInfo.receiveNum ? recipeInfo.receiveNum.split('_')[1] : ''
        let recipeDetailWrapper = recipeInfo.listRecipeDetailWrapper;
        let rcpType = recipeInfo.rcpType   // 判断是否为颗粒处方
        let { printHtml, allGrams, drgPoison, drgSpiritGrade } = chTemplate(recipeDetailWrapper, rcpType, usage)
        let { hospitalId, hosptalAddress, hosptalRegionalCode, hosptalPhone } = Param.user
        let XJStick = hospitalId == 330000 ? "   总贴 " + allGrams * recipeInfo.rcpPosts + "g" +"   单贴 " + allGrams  +"g" : ''
        // 临时处理
        let CZ = hospitalId == 330005 ? "电话:0571-86075949    传真:0571-86072747    预约咨询电话:18868713300" : ''
        let DX = hospitalId == 330003 ? "电话:" + (hosptalPhone || '') : ''
        let strAddress = "共 " + recipeDetailWrapper.length + " 味药 " + "    服 " + recipeInfo.rcpPosts + " 剂     金额 " + recipeInfo.recFee + " 元"
            + XJStick +"\n"
            + "注意:请勿遗失,处方当日有效。因特殊情况,该处方有效为__天(签名:)\n"
            + "地址:" + (hosptalAddress || '') + "     邮编:" + (hosptalRegionalCode || '') + "\n"
            + DX + CZ

        let diagnoseInfo = "";
        if (recipeInfo.diagnoseInfoList) {
            for (var j = 0; j < recipeInfo.diagnoseInfoList.length; j++) {
                diagnoseInfo = diagnoseInfo + recipeInfo.diagnoseInfoList[j].diaName + ";";
            }
        }
        let visSymptomNew = data.visPresentHis || data.visitedInfoWrapper.visPresentHis
        visSymptomNew = visSymptomNew.length > 110 ? visSymptomNew.slice(0,110) + '.....' : visSymptomNew
        let addressHeight = (patient.patFamAddress || '').length > 18 ? 20 * Math.floor(patient.patFamAddress.length / 18) : 0
        let hpiHeight = 16*Math.floor(visSymptomNew.length/30) || 0    // 现病史高度
        let diaHeight = 16*Math.floor(diagnoseInfo.length/30) || 0        // 诊断高度
        LODOP.PRINT_INITA(0, 0, "148mm", "210mm", "处方笺打印模板");
        usage == 2 ? printZoom(1, 'height') : LODOP.SET_PRINT_PAGESIZE(1, 1480, 2100, "CreateCustomPage");
        LODOP.SET_PRINT_MODE("RESELECT_PRINTER", true);
        LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
        LODOP.ADD_PRINT_TEXT("9mm","21.43mm","89.11mm","7.41mm", Param.user.hosptalName);
        LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 17);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        LODOP.ADD_PRINT_TEXT(61,104,287,28,"门诊中药处方笺");
        LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
        LODOP.SET_PRINT_STYLEA(0,"FontSize",16);
        LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
        // LODOP.ADD_PRINT_BARCODE("0mm","127.79mm","22.75mm","21.96mm", "QRCode", "http://weixin.qq.com/r/C3VOSrTEyny2KfApbyCP");
        LODOP.ADD_PRINT_TEXT("35.45mm","1.59mm","36.78mm","6.61mm", "姓 名：" + patient.patName);
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT("29.37mm","1.32mm","47mm","5.56mm", "门诊号：" + patient.patCardNum);
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.SET_PRINT_STYLEA(0,"Bold",1);
        LODOP.ADD_PRINT_TEXT("35.45mm","39.95mm","21.43mm","6.61mm", "性别：" + (patient.patSex == 1 ? '男' : '女'));
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT("35.45mm","64.82mm","23.02mm","6.61mm", "年龄：" + patient.patAge);
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT("35.72mm","94.46mm","41.8mm","6.61mm", "类别：" + patient.feeName);
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT("29.63mm","94.19mm","41.28mm","6.61mm", "日期：" + recipeInfo.rcpDate);
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT(158,357,159,25, "电话：" + patient.patPhone);
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT("41.8mm","1.32mm","18.79mm","6.61mm", "地 址：");
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT(159,60,290,20+addressHeight, patient.patFamAddress);
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT(183+addressHeight,5,80,23, "现病史：");
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT(183+addressHeight,60,457,24+hpiHeight, visSymptomNew);
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.SET_PRINT_STYLEA(0,"LineSpacing",-2);
        LODOP.ADD_PRINT_TEXT(208+hpiHeight+addressHeight,5,67,24, "诊 断：");
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT(208+hpiHeight+addressHeight,60,457,24+diaHeight, diagnoseInfo);
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.SET_PRINT_STYLEA(0,"LineSpacing",-2);
        LODOP.ADD_PRINT_TEXT(232+hpiHeight+diaHeight+addressHeight,5,68,25, "处 方：");
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT(232+hpiHeight+diaHeight+addressHeight,168,120,25, "用 法：" + (rcpType == 5 ? '冲服' : '口服'));
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TABLE(264+hpiHeight+diaHeight+addressHeight,27,484,377-hpiHeight-diaHeight-addressHeight, printHtml);
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT("173.34mm","2.65mm","14.55mm","5.29mm", "配药：");
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT("173.34mm","32.81mm","17.2mm","5.29mm", "复核：");
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT("173.34mm","64.82mm","16.14mm","5.29mm", "发药：");
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT(655,362,120,20,"医生：" + (recipeInfo.turnSffName || recipeInfo.sffName));
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT(655,472,60,20,recipeInfo.turnSffName ? recipeInfo.sffName : '');
        LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        LODOP.ADD_PRINT_TEXT("178.81mm","2.38mm","138.38mm","21.09mm", strAddress);
        LODOP.SET_PRINT_STYLEA(0,"FontSize",10);
        if (recipeInfo.recDoctorAsks) { // 医嘱
            LODOP.ADD_PRINT_TEXT(633,8,516,20,"医嘱:" + ((recipeInfo.recDoctorAsks && recipeInfo.recDoctorAsks.slice(0,31)) || ''));
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
        }
        if (hospitalId == 330003 && data.loginNum) {   // 医生签字
            LODOP.ADD_PRINT_IMAGE(687,407,116,43,`<img src='${Param.host_url}/clinic/images/dzqm/${data.loginNum}.bmp'>`);
            LODOP.SET_PRINT_STYLEA(0,"Stretch",2)
        }else if (hospitalId == 330005 && data.loginSffName) {
            LODOP.ADD_PRINT_IMAGE(687,407,116,43,`<img src='${Param.host_url}/clinic/images/dzqm/${data.loginSffName}.bmp'>`);
            LODOP.SET_PRINT_STYLEA(0,"Stretch",2)
        }
        if (out) { // 外配处方
            LODOP.ADD_PRINT_ELLIPSE(29,418,89,29,0,1);
            LODOP.ADD_PRINT_TEXT(35,415,110,26," 外配处方 ");
            LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
            LODOP.SET_PRINT_STYLEA(0,"FontSize",13);
        }
        if (getMedCard) { // 领药证
            LODOP.ADD_PRINT_TEXT(86,199,320,26,"领药证号: " + getMedCard);
            LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
            LODOP.SET_PRINT_STYLEA(0,"Alignment",3);
            LODOP.SET_PRINT_STYLEA(0,"Bold",1);
        }
        if (drgPoison != '0' && drgPoison) {
            LODOP.ADD_PRINT_TEXT(61,455,78,26," 毒品类 ");
            LODOP.SET_PRINT_STYLEA(0,"FontSize",13);
            LODOP.SET_PRINT_STYLEA(0,"TextFrame",11);
        }
        if (drgSpiritGrade != '0' && drgSpiritGrade) {
            LODOP.ADD_PRINT_TEXT(61,381,74,26," 精神类 ");
            LODOP.SET_PRINT_STYLEA(0,"FontSize",13);
            LODOP.SET_PRINT_STYLEA(0,"TextFrame",11);
        }
        let { condense } = data.recipeInfoWrapperList[i]
        if (recipeInfo.rcpBoilSign === '1' && !out) {  // 煎药标识
            LODOP.ADD_PRINT_TEXT("9.26mm","2.65mm","20.48mm","8.2mm", '煎药');
            LODOP.SET_PRINT_STYLEA(0,"FontName","楷体");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 20);
            LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
            LODOP.ADD_PRINT_LINE(32,13,32,88,0,1);
            LODOP.ADD_PRINT_LINE(62,88,32,88,0,1);
            LODOP.ADD_PRINT_LINE(62,13,62,88,0,1);
            LODOP.ADD_PRINT_LINE(62,13,32,13,0,1);
            if (condense && condense != 1) {   // 浓缩标识
                let amount;
                switch (condense) {
                    case 2: amount = '150'; break;
                    case 3: amount = '120'; break;
                    case 4: amount = '100'; break;
                    case 5: amount = '80'; break;
                    case 6: amount = '60'; break;
                    default: amount = '200'; break;
                }
                LODOP.ADD_PRINT_TEXT(64,5,101,26,"浓缩:"+ amount +"ml");
                LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
            }
        }
        if(recipeInfo.rcpBoilSign === '0' && !out && hospitalId != "330004" && hospitalId != "330005" && twoPiece !== ''){
            twoPiece=true
        }
        // LODOP.PRINT_DESIGN();
        LODOP.SET_PRINTER_INDEXA(index);
        if (twoPiece) {
            LODOP.SET_PRINT_COPIES(2)   // 优先判断
        } else if (hospitalId != "330004" && hospitalId != "330005" && !number && recipeInfo.rcpBoilSign != '1' && condense != 1 && !twoPiece) {
            LODOP.SET_PRINT_COPIES(2)  // 1. 不是仙居, 不是城站  2.不是补打  3. 不煎药  4. 非浓缩200
        }
        LODOP.PRINT();
    }
}

/**
 * 西药处方打印
 * @param {Object} data 数据
 * @param {String} index 打印机名称
 * @param {Number} hospitalId 医院编号
 * @param {Boolean} out 是否外配处方
 * */
function westMedicine(data, index, hospitalId, out= false) {
    if (!hospitalId) hospitalId == '330000'
    LODOP = getLodop();
    var patient = data.patientsWrapper;
    for (var a = 0; a < data.recipeInfoWrapperList.length; a++) {
        let drgPoison = 0;        // 普通0, 毒品1
        let drgSpiritGrade = 0;   // 非精神药0
        var recipeInfo = data.recipeInfoWrapperList[a];
        var recipeDetailWrapper = recipeInfo.listRecipeDetailWrapper;
        var newRecipeDetailWrapper = [];
        for (var i = 0; i < recipeDetailWrapper.length; i+=5) {
            newRecipeDetailWrapper.push(recipeDetailWrapper.slice(i,i+5))
        }
        for (var j = 0; j < newRecipeDetailWrapper.length; j++) {
            let nRDW = newRecipeDetailWrapper[j]
            var printHtml = "<style> tr{height: 30px;}</style><table width='100%'>\n";
            var totalMoney = 0;
            for (var x = 0; x < nRDW.length; x++) {
                if(nRDW[x].drgPoison != '0') drgPoison = nRDW[x].drgPoison
                if(nRDW[x].drgSpiritGrade != '0') drgSpiritGrade = nRDW[x].drgSpiritGrade
                printHtml += `<tr>
                                <td colspan="2">${x + 1}.</td>
                                <td colspan="6">${nRDW[x].drgName} / ${nRDW[x].drgSpecification}</td>                        
                                <td colspan="1" style="text-align: right">${nRDW[x].redQuantity} ${nRDW[x].drgPackingUnit}</td>
                              </tr>
                              <tr>
                                <td colspan="2"></td>
                                <td colspan="7">
                                    用法: 
                                    <span style="margin:0 30px">${nRDW[x].redOnceDose} ${nRDW[x].redDoseUnit}</span>
                                    <span>${nRDW[x].freName}</span>
                                    <span style="margin-left: 60px">${nRDW[x].modName}</span>
                                </td>
                              </tr>`
                totalMoney += nRDW[x].redSumFee
            }
            printHtml += "</table>";

            var strAddress = "注意：请勿遗失,处方当日有效。因特殊情况,该处方有效为__天(签名:)\n"
                + "邮编：" + Param.user.hosptalRegionalCode.trim() + "           电话：" + (Param.user.hosptalPhone || '') + '\n'
                + "地址：" + Param.user.hosptalAddress + '\n'
                + "                               第" + (j+1) + "页/共" + newRecipeDetailWrapper.length + "页 "

            var diagnoseInfo = "";
            if (recipeInfo.diagnoseInfoList) {
                for (var z = 0; z < recipeInfo.diagnoseInfoList.length; z++) {
                    diagnoseInfo = diagnoseInfo + recipeInfo.diagnoseInfoList[z].diaName + ";";
                }
            }
            let addressHeight = (patient.patFamAddress || '').length > 18 ? 20 * Math.floor(patient.patFamAddress.length / 18) : 0
            let diaHeight = diagnoseInfo.length > 29 ? 20 * Math.floor(diagnoseInfo.length / 29) : 0
            LODOP.PRINT_INITA(0, 0, "148mm", "210mm", "处方笺打印模板");
            LODOP.SET_PRINT_PAGESIZE(1, 1480, 2100, "CreateCustomPage");
            LODOP.SET_PRINT_MODE("RESELECT_PRINTER", true);
            LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
            LODOP.ADD_PRINT_TEXT("10.05mm","22.75mm","87.79mm","7.41mm", Param.user.hosptalName);
            LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 16);
            LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
            LODOP.ADD_PRINT_TEXT(65,105,287,28,"西药处方笺");
            LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
            LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
            LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
            // LODOP.ADD_PRINT_BARCODE("0mm","127.79mm","22.75mm","21.96mm", "QRCode", "http://weixin.qq.com/r/C3VOSrTEyny2KfApbyCP");
            LODOP.ADD_PRINT_TEXT("35.72mm","1.85mm","34.4mm","5.29mm", "姓 名：" + patient.patName);
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT("29.37mm","1.59mm","41.01mm","5.29mm", "门诊号：" + patient.patCardNum);
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT("35.72mm","41.8mm","22.81mm","5.29mm", "性别：" + (patient.patSex == 1 ? '男' : '女'));
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT("35.72mm","68.26mm","24.13mm","5.29mm", "年龄：" + patient.patAge);
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT("35.72mm","95.25mm","42.33mm","5.29mm", "类别：" + patient.feeName);
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT("29.63mm","94.99mm","42.33mm","5.29mm", "日期：" + recipeInfo.rcpDate);
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT(157,360,160,20, "电话：" + (patient.patPhone || ''));
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT(158,7,63,20, "地 址：");
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT(159,60,290,20+addressHeight, patient.patFamAddress);
            LODOP.SET_PRINT_STYLEA(0,"LineSpacing",-4);
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT(182+addressHeight,7,63,20, "诊 断：");
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT(183+addressHeight,59,438,20+diaHeight, diagnoseInfo);
            LODOP.SET_PRINT_STYLEA(0,"LineSpacing",-3);
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT(206+addressHeight+diaHeight,7,63,20, "处 方：");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
            LODOP.ADD_PRINT_TABLE(234+addressHeight+diaHeight,19,470,348, printHtml);
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT("172mm","2.12mm","23.28mm","5.29mm", "配药药师：");
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT("172mm","43.92mm","22.49mm","5.29mm", "复核药师：");
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT(650,340,115,20,"医生：" + (recipeInfo.turnSffName || recipeInfo.sffName));
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT(650,457,60,20,recipeInfo.turnSffName ? recipeInfo.sffName : '');
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            LODOP.ADD_PRINT_TEXT("177.8mm","2.12mm","135.47mm","20.03mm", strAddress);
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
            if (hospitalId == 330003 && data.loginNum) {   // 医生签字
                LODOP.ADD_PRINT_IMAGE(676,417,80,26,`<img src='${Param.host_url}/clinic/images/dzqm/${data.loginNum}.bmp'>`);
                LODOP.SET_PRINT_STYLEA(0,"Stretch",2)
            }else if (hospitalId == 330005 && data.loginSffName) {
                LODOP.ADD_PRINT_IMAGE(676,417,80,26,`<img src='${Param.host_url}/clinic/images/dzqm/${data.loginSffName}.bmp'>`);
                LODOP.SET_PRINT_STYLEA(0,"Stretch",2)
            }
            LODOP.ADD_PRINT_TEXT(627,8,516,20,"医嘱:" + ((recipeInfo.recDoctorAsks && recipeInfo.recDoctorAsks.slice(0,31)) || ''));
            LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
            if (out) { // 外配处方
                LODOP.ADD_PRINT_ELLIPSE(29,418,89,29,0,1);
                LODOP.ADD_PRINT_TEXT(35,415,110,26," 外配处方 ");
                LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
                LODOP.SET_PRINT_STYLEA(0,"FontSize",13);
            }
            if (drgPoison != '0' && drgPoison) {
                LODOP.ADD_PRINT_TEXT(61,455,78,26," 毒品类 ");
                LODOP.SET_PRINT_STYLEA(0,"FontSize",13);
                LODOP.SET_PRINT_STYLEA(0,"TextFrame",11);
            }
            if (drgSpiritGrade != '0' && drgSpiritGrade) {
                LODOP.ADD_PRINT_TEXT(61,381,74,26," 精神类 ");
                LODOP.SET_PRINT_STYLEA(0,"FontSize",13);
                LODOP.SET_PRINT_STYLEA(0,"TextFrame",11);
            }
            // LODOP.PRINT_DESIGN();
            LODOP.SET_PRINTER_INDEXA(index);
            LODOP.PRINT();
        }

    }
}

//发票打印
function invoiceInformation(res, index) {
    Param.initPageParam();
    horizontalInvoiceInformation(res, index)
}

//横版发票打印
function horizontalInvoiceInformation(res, index) {
    var data = {};
    data.specialDiseaseSign = res.data.patientsWrapper.specialDiseaseSign
    data.patName = res.data.patName;//姓名
    data.billDate = res.data.billDate;//日期
    data.sffName = res.data.sffName;//收款人
    data.patCardNum = res.data.patCardNum;//门诊号
    data.medicareCard = res.data.patientsWrapper.medicareCard == null ? '' : res.data.patientsWrapper.medicareCard;//医保卡号
    //医保信息

    data.settleMentInformation = res.data.billSelfFee == null ? '' : res.data.billSelfFee;
    data.medicalInsuranceInformation = res.data.medicalInsuranceInformation == null ? '' : res.data.medicalInsuranceInformation;

    //显示实收 优惠
    data.billTotalFee = res.data.billTotalFee;
    data.billCash = res.data.billCash;
    data.billFavorFee = res.data.billFavorFee;
    data.totalSettleAmount = res.data.totalSettleAmount;

    for (let i = 0; i <= 15; i++) {
        data['ticketFee' + i] = res.data.ticketInfoWrapperList[i].ticketFee == null ? '' : res.data.ticketInfoWrapperList[i].ticketFee;
    }

    var table = '<style>table {font-size: 12px;width: 100%;}tr {height: 10px;line-height: 10px;}td {white-space: nowrap;text-overflow: ellipsis;}</style><table style="padding: 0;margin: 0">'
    var item = '';
    var flag = true;
    for (var i = 0; i < res.data.chargesProjectList.length; i++) {
        if (flag) {
            item = res.data.chargesProjectList[i];
            var typeNDU;
            var qU;
            if (item.type == '2') {
                typeNDU = item.typeName + "(" + (item.redOnceDose || '') + item.drgPackingUnit + ")"
                qU = item.quantity + item.drgPackingUnit
            } else {
                typeNDU = item.typeName
                qU = item.quantity
            }
            table += '<tr>'+
                '<td>(' + (i + 1)+ ')  ' + item.drgName + ' </td>' +
                '<td>' + typeNDU + '</td>' +
                '<td>' + qU + '</td>' +
                '<td>' + item.sumPrice+ '</td>' +
                '<td style="text-align:center;">'+ (Math.round(item.redSelfScale * 100) + '%') + '</td>' +
                '</tr>'
            if (i > 16) {
                table += '<tr><td colspan="5">收费发票纸张有限，其余明细未打印</td></tr>'
                flag = false;
                break;
            }
        }
    }

    data.table = table;
    createFeePrint(data, index, res);
}


//发票重打
function invoiceReplay(res, index, patch) {
    let {patientsWrapper:{specialDiseaseSign,medicareCard},patName,billDate,sffName,patCardNum,billSelfFee:settleMentInformation,medicalInsuranceInformation,billTotalFee,billCash,billFavorFee,totalSettleAmount} = res.data
    // patName:姓名, billDate:日期, sffName:收款人, patCardNum:门诊号, medicareCard:医保卡号, settleMentInformation:合计信息, billTotalFee, billCash, billFavorFee存储总金额 优惠 实收现金
    let data = {specialDiseaseSign,patName,billDate,sffName,patCardNum,medicareCard,settleMentInformation,medicalInsuranceInformation,billTotalFee,billCash,billFavorFee,totalSettleAmount}

    for (let i = 0; i <= 15; i++) {
        data['ticketFee' + i] = res.data.ticketInfoWrapperList[i].ticketFee == null ? '' : res.data.ticketInfoWrapperList[i].ticketFee;
    }
    // 获取明细项table
    let { chargesProjectList } = res.data
    let westArr = chargesProjectList.filter(item => item.typeName.includes('西药'))
    let otherArr = chargesProjectList.filter(item => !item.typeName.includes('西药'))
    chargesProjectList = [...westArr, ...otherArr]
    let table = detailTable(chargesProjectList, patch)

    data.table = table;
    invoiceInformationPrint(data, index, res, patch);
}

// 获取明细项table
function detailTable(data, patch) {
    let tableHtmlArr = []
    // if (patch) {
    //     let pageObj = { chData: [], westData: [], cureData: [], otherData: [] }
    //     data.forEach(item => {
    //         if (item.typeName === '草药') pageObj.chData.push(item)
    //         else if (item.typeName === '西药') pageObj.westData.push(item)
    //         else if (item.typeName.includes('治疗')) pageObj.cureData.push(item) // 治疗费
    //         else pageObj.otherData.push(item)     // 检验检查
    //     })
    //     Object.keys(pageObj).forEach(item => {
    //         if (pageObj[item].length === 0) return delete pageObj[item]
    //         tableHtmlArr.push(handleTableHtml(pageObj[item]))
    //     })
    // } else {
        tableHtmlArr.push(handleTableHtml(data))
    // }
    return tableHtmlArr
}

function handleTableHtml(data) {
    let fs = +localStorage.getItem('fpfs')
    let lineHeight = +localStorage.getItem('lh')
    let table = `<style>table {width: 100%;font-size: ${fs || 9}px;}tr {line-height: ${lineHeight || 8}px;}td {white-space: nowrap;text-overflow: ellipsis;}</style><table style="padding: 0;margin: 0">`
    for (let i = 0; i < data.length; i++) {
        // 名称, 类别, 数量, 金额, 自理比例
        let { drgName, medicalGrade, quantity, sumPrice, redSelfScale, typeName} = data[i];
        drgName = drgName.length > 10 ? drgName.slice(0, 10) + '...' : drgName
        drgName = drgName == '一个部位' ? 'B超(一个部位)' : drgName
        table += `<tr>
                    <td style="width:40%">(${i + 1}) ${drgName}</td>
                    <td style="width:13%">${medicalGrade || ''}</td>
                    <td style="width:13%">${typeName === '协定方' ? 1 : quantity}</td>
                    <td style="width:16%">${sumPrice || ''}</td>
                    <td style="width:18%;text-align:center;">${Math.round(redSelfScale * 100)} %</td>
                 </tr>`
        if (i > 19) {
            table += '<tr><td colspan="5">收费发票纸张有限，其余明细未打印</td></tr>'
            break
        }
    }
    return table
}

//挂号日报报表
function regCheckoutForm(res, index) {
    var data = {};
    data.chgNum = res.data.chgNum;//结算单号
    data.sffName = res.data.sffName;//人员签字
    data.selfPayAmount = res.data.selfPayAmount;//zifei
    data.money = res.data.money;//实收小计
    data.readyMoney = res.data.readyMoney;//现金
    data.favorable = res.data.favorable;//优惠
    data.subtotal = res.data.subtotal;//合计
    data.people = res.data.people;//人
    data.departmentsWrappers = res.data.departmentsWrappers
    data.billClassifyWrappers = res.data.billClassifyWrappers
    //
    regPrint(data, index)
}

/**
 * 收费个人/汇总日(月)报报表
 * @param data 数据
 * @param index 打印机序号
 * @param billType 标题 （挂号 ：收费）
 * @param title 报表类型
 */
function chargeCheckoutForm(data, index, billType, title) {
    //按发票项目统计
    var printItem = " <thead style='background: #DADADA'><tr>\n" +
        "<td>开单项目</td>\n" +
        "<td>总金额</td>\n" +
        "<td>自费金额</td>\n" +
        "<td>医保金额</td>\n" +
        "<td>优惠金额</td>\n" +
        "</tr></thead>\n"
    printItem += "<tbody>\n"
    if (data) {
        for (var i = 0; i < data.ticInfoWrapperList.length; i++) {
            printItem += "<tr>\n" +
                "   <td>"+ data.ticInfoWrapperList[i].ticketName + "</td>" +
                "   <td>"+ data.ticInfoWrapperList[i].total + "</td>" +
                "   <td>"+ data.ticInfoWrapperList[i].payFee + "</td>" +
                "   <td>"+ data.ticInfoWrapperList[i].billingAmount+ "</td>" +
                "   <td>"+ data.ticInfoWrapperList[i].favorFee+ "</td>" +
                "</tr>";
        }
        printItem += "</tbody>";
    }
    //按患者类型统计
    var printPaient=" <thead style='background: #DADADA'><tr>\n" +
        "<td>费用类型</td>" +
        "<td>总金额</td>" +
        "<td>自费金额</td>" +
        "<td>医保金额</td>" +
        "<td>优惠金额</td>" +
        "<td hidden>人次</td>" +
        "</tr></thead>"
    printPaient += "<tbody>"
    if (data) {
        for (var i = 0; i < data.feeInfoWrapperList.length; i++) {
            printPaient += "<tr>\n" +
                "<td>" + data.feeInfoWrapperList[i].feeName + "</td>\n" +
                "<td>" + data.feeInfoWrapperList[i].total + "</td>\n" +
                "<td>" + data.feeInfoWrapperList[i].cash+ "</td>\n" +
                "<td>" + (data.feeInfoWrapperList[i].billingAmount || 0 )+ "</td>\n" +
                "<td>" + data.feeInfoWrapperList[i].favorFee+ "</td>\n" +
                "<td hidden>" +  (data.feeInfoWrapperList[i].settlements || 0) + "</td>\n" +
                "</tr>\n";
        }
        printPaient += "</tbody>\n"
    }
    //结账单信息
    var printChargeMessage = " <thead style='background: #DADADA'><tr>\n" +
        "<td>项目</td>\n" +
        "<td>金额</td>\n" +
        "<td>项目</td>\n" +
        "<td>金额</td>\n" +
        "</tr></thead>\n"
    printChargeMessage += "<tbody><tr>" +
        "   <td>总金额</td><td>"+ (data.total || 0)+ "</td>" +
        "   <td>支付宝</td><td>"+ (data.alipay || 0) + "</td>" +
        "</tr>" +
        "<tr>" +
        "   <td>自费金额</td><td>"+ (data.cashReceived || 0)+ "</td>" +
        "   <td>微信</td><td>"+ (data.weChatPay || 0) + "</td>" +
        "</tr><tr>" +
        "   <td>医保金额</td><td>"+(data.billingAmount || 0)+"</td>" +
        "   <td>银行卡</td><td>"+(data.unionPay || 0)+"</td>" +
        "</tr><tr>" +
        "   <td>市医保金额</td><td>"+(data.cityBillingAmount || 0)+"</td>" +
        "   <td>微医支付</td><td>"+(data.wyPay || 0)+"</td>" +
        "</tr><tr>" +
        "   <td>省医保金额</td><td>"+(data.proBillingAmount || 0)+"</td>" +
        "   <td>微脉支付</td><td>"+(data.wmPay || 0)+"</td>" +
        "</tr><tr>" +
        "   <td>作废票据</td><td></td>" +
        "   <td>现金</td><td>"+(data.cashReceived-data.wmPay-data.wyPay ||0).toFixed(2)+ "</td>" +
        "</tr><tr>" +
        "   <td>有效票据</td><td></td>" +
        "   <td>优惠</td>td>"+ (data.favorFee || 0)+ "</td>" +
        "</tr><tr>" +
        "   <td>处方数</td><td></td><td></td><td></td>" +
        "</tr><tr>" +
        "   <td>实收人次</td><td>"+ data.settlements + "</td>" +
        "   <td>实收小计</td><td>"+(data.cashReceived || 0) + "</td>" +
        "</tr></tbody>"
    chargePrint(data, [printItem, printPaient, printChargeMessage], index, billType, title)
}

/**
 * 中药库,西药库，西药房收支汇总月报表
 * @param table1, table2, table3 的html
 * @param storeId 判断来源 (中/西)(药库/药房)
 * @param index 打印机序号
 * @param dateArr 日期数组
 * @param tbLength 表格行数
 */
function monthlyTotalTable(table1, table2, table3, storeId,index,dateArr,tbLength) {
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var titleYear = dateArr[0];
    var titleMonth = dateArr[2] ? dateArr[1] + '-' + dateArr[2] :dateArr[1];
    var day = new Date().getDate();
    LODOP = getLodop();
    var drugType = getStoreName(storeId)
    let tableH = tbLength*18
    LODOP.PRINT_INITA(10, 10, 800, 850, "套打挂号日报报表小票的模板");
    printZoom()  // 缩放纸张
    LODOP.ADD_PRINT_TEXT(17,122,298,32, Param.user.hosptalName);
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 16);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXT(44,125,294,25, drugType + "收支汇总月报表");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXT(68,126,293,24, "统计月份：" + titleYear + "年" + titleMonth + "月（所有类别）");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_HTM(91, 32, 482, tableH, table2);
    LODOP.ADD_PRINT_HTM(91, 190, 482, tableH, table1);
    LODOP.ADD_PRINT_HTM(91, 348, 482, tableH, table3);
    LODOP.ADD_PRINT_TEXT(tableH+100, 282, 100, 20, "制表人: " + Param.user.name);
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");
    LODOP.ADD_PRINT_TEXT(tableH+100, 424,89,20, year + "年" + month + "月" + day + "日");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");
    LODOP.ADD_PRINT_TEXT(tableH+100, 32, 191, 20, "注：购入及差价包含挂账金额");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");
    // LODOP.PRINT_DESIGN();
    LODOP.SET_PRINTER_INDEXA(index)
    LODOP.PRINT();
}

/**
 * 打印缩放 A5 A4
 * @param num 打印方向 1竖  2横
 * @param option 缩放类型 根据宽/高
 */
function printZoom(num = 1, option= 'width' ) {
    if (Param.user.hospitalId != 330004) {
        LODOP.SET_PRINT_PAGESIZE(num, 2100, 2960, "");
        LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","Auto-Width");
    } else {
        LODOP.SET_PRINT_PAGESIZE(num, 1480, 2100, "");
    }
}

/**
 * 中药房收支汇总月报表
 * @param table 表格html
 * @param index 打印机序号
 * @param dateArr 日期数组
 * @param reportTitle 表格标题
 */
function ZhMonthlyTotalTable(table,index,dateArr,reportTitle) {
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var titleYear = dateArr[0];
    var titleMonth = dateArr[2] ? dateArr[1] + '-' + dateArr[2] :dateArr[1];
    var day = new Date().getDate();
    LODOP = getLodop();
    LODOP.PRINT_INITA(0, 0, "148mm", "210mm", "套打挂号日报报表小票的模板");
    printZoom()  // 缩放纸张
    LODOP.ADD_PRINT_TEXT(6,118,298,32, Param.user.hosptalName);
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 16);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXT(33,176,182,25, reportTitle);
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_TEXT(56,131,277,24, "统计月份：" + titleYear + "年" + titleMonth + "月（所有类别）");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
    LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
    LODOP.ADD_PRINT_HTM(78, 32, 482, 583, table);
    LODOP.ADD_PRINT_TEXT(523,413,100,20, "制表人: " + Param.user.name);
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");
    LODOP.ADD_PRINT_TEXT(524,49,155,20, "制表日期：" + year + "年" + month + "月" + day + "日");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");

    // LODOP.PRINT_DESIGN();
    LODOP.SET_PRINTER_INDEXA(index)
    LODOP.PRINT();
}

/** 仙居职工医保报表
 * @param table
 * @param date
 * @param type 职工 1 居民 2
 * @param index 打印机序号
 */
function workerHealthReport(table,date,type,index) {
    let typeName = type == 1 ? "仙居县城镇职工定点医药机构门诊医疗费用申报表" : "仙居县城乡居保定点医疗机构门诊医疗费用申报表"
    LODOP = getLodop();
    LODOP.PRINT_INITA(10,10,2100,2960,"仙居职工医保报表");
    LODOP.SET_PRINT_PAGESIZE(2,2100,2960,"");
    LODOP.ADD_PRINT_TEXT(10,241,600,32,typeName);
    LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
    LODOP.SET_PRINT_STYLEA(0,"FontSize",16);
    LODOP.ADD_PRINT_TEXT(61,224,287,25,"定点医疗机构: "+ Param.user.hosptalName);
    LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
    LODOP.SET_PRINT_STYLEA(0,"FontSize",10);
    LODOP.ADD_PRINT_TEXT(59,39,150,24,"机构编码: "  + 80);
    LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
    LODOP.SET_PRINT_STYLEA(0,"FontSize",10);
    LODOP.ADD_PRINT_HTM(81,20,1050,583,table);
    LODOP.ADD_PRINT_TEXT(61,572,172,25,"申报年月: " + date);
    LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
    LODOP.SET_PRINT_STYLEA(0,"FontSize",10);
    LODOP.ADD_PRINT_TEXT(61,853,166,25,"单位:元(下至角分)");
    LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
    LODOP.SET_PRINT_STYLEA(0,"FontSize",10);

    // LODOP.PRINT_DESIGN();
    LODOP.SET_PRINTER_INDEXA(index)
    LODOP.PRINT();
}


