// 2020.01.01    元旦
// 2020.01.25    春节
// 2020.02.08    元宵
// 2020.03.08    妇女
// 2020.04.04    清明
// 2020.05.01    劳动
// 2020.06.25    端午
// 2020.10.01    中秋
// 2020.10.25    重阳

function changeBackground() {
    let currentTime = new Date().getTime()

    if (currentTime > +new Date('2020.01.01 00:00:00') && currentTime < +new Date('2020.01.01 24:00:00')) {
        setBg("yd", "#d91f39")
    } else if (currentTime > +new Date('2020.01.25 00:00:00') && currentTime < +new Date('2020.01.25 24:00:00')) {
        setBg("cj")
    } else if (currentTime > +new Date('2020.02.08 00:00:00') && currentTime < +new Date('2020.02.08 24:00:00')) {
        setBg("yx")
    } else if (currentTime > +new Date('2020.03.08 00:00:00') && currentTime < +new Date('2020.03.08 24:00:00')) {
        setBg("fn")
    } else if (currentTime > +new Date('2020.04.04 00:00:00') && currentTime < +new Date('2020.04.04 24:00:00')) {
        setBg("qm")
    } else if (currentTime > +new Date('2020.05.01 00:00:00') && currentTime < +new Date('2020.05.01 24:00:00')) {
        setBg("ld")
    } else if (currentTime > +new Date('2020.06.25 00:00:00') && currentTime < +new Date('2020.06.25 24:00:00')) {
        setBg("dw")
    } else if (currentTime > +new Date('2020.10.01 00:00:00') && currentTime < +new Date('2020.10.01 24:00:00')) {
        setBg("zq")
    } else if (currentTime > +new Date('2020.10.25 00:00:00') && currentTime < +new Date('2020.10.25 24:00:00')) {
        setBg("cy")
    } else {
        setBg("background")
    }
}

function setBg(txt, color = '#00a0e9') {
    $("body").css('background', `url(./images/login/${txt}.jpg) no-repeat center`)
    $("body").css('background-size', 'cover')
    $("#confirm").css("background-color", color)
    $("#confirm").css("border", `1px solid ${ color }`)
}




