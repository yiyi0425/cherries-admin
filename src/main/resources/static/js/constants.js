﻿var Constants = {
	/**
	 * 键盘向上PageUp键值
	 */
	PgUp : "38",
	/**
	 * 键盘向下PageDown键值
	 */
	PgDn : "40",
	/**
	 * 键盘回车键值
	 */
	PgEnter : "13",
	/**
	 * 键盘退出Esc键值
	 */
	PgEsc : "27",
	/**
	 * 通用值1
	 */
	Common_String_1  : "1",
	/**
	 * 通用0
	 */
	Common_String_0  : "0",
	/**
	 * 新增
	 */
	Method_Type_New : "new",
	/**
	 * 编辑
	 */
	Method_Type_Edit : "edit",
	/**
	 * 问诊内容页面展现方式
	 */
	colType : {"text": "1", "select" : "2", "checkbox" : "3", "radio" : "4","multiselect" : "5","checkboxlisttext":"6"}
};