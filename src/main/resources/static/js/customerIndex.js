/**
 * Created by Kiva on 16/10/11.
 */


var customerMenuTab={
    menuUp:true,
    start:function () {
        /*菜单点击的方法*/
        $("#content").delegate(".menu > li","click",function () {
            customerMenuTab.menuFocus(this,"menu-focus");
        });
        $("#content").delegate(".secondary-menu > li","click",function () {
            customerMenuTab.menuFocus(this,"secondary-menu-focus");
        });

        $("#content").delegate(".third-menu > li","click",function () {
            customerMenuTab.menuFocus(this,"third-menu-focus");
        });
        $("#content").delegate(".menu li","click",function () {
            customerMenuTab.menuClick(this);
            return false;
        });

        $("#content").find(".secondary-menu > li").click(function () {
            customerMenuTab.menuFocus(this,"secondary-menu-focus");
        });

        $("#content").find(".third-menu > li").click(function () {
            customerMenuTab.menuFocus(this,"third-menu-focus");
        });

        $("#content").find(".menu li").click(function () {
            customerMenuTab.menuClick(this);
            return false;
        });
    },
    resize:function () {
        $(window).resize(function () {
            customerMenuTab.menuHeight();
            customerMenuTab.contentH();
            customerMenuTab.tabLength();
        });
    },
    /*取消事件冒泡的方法*/
    cancelBubbling:function (e) {
        e = window.event || e;
        if (e.stopPropagation) {
            e.stopPropagation();
        } else {
            e.cancelBubble = true;
        }
    },
    notTag:function (href,li) {
        if($("#content").children(".active").find(".nav").children("li").attr("class")===undefined){
            $.each($("#content").children(".active").find(".menu").children("li"),function () {
                var $oneLi=$(this);
                var $toUl=$oneLi.children("ul");
                var $oneA=$oneLi.find("a:first");
                var id=($oneA.attr("href")+"").replace("#","");
                if($toUl.attr("role")!==undefined){
                    $.each($toUl.children("li"),function () {
                        var $toLi=$(this);
                        var $thirdUl=$toLi.children("ul");
                        var $toA=$toLi.find("a:first");
                        if($thirdUl.attr("role")!==undefined){
                            $.each($thirdUl.children("li"),function () {
                                var $thirdLi=$(this);
                                var $thirdA=$thirdLi.find("a:first");
                                $thirdA.removeClass("third-menu-focus");
                                $thirdA.find("div:first").addClass("display-none");
                            });
                        }
                        $thirdUl.slideUp();
                        $toA.removeClass("secondary-menu-focus");
                        $toA.find("div:first").addClass("display-none");
                    });
                }
                $toUl.slideUp();
                $oneA.removeClass("menu-focus");
                $oneA.find("img:first").attr('src',"images/icon_"+id+"_grey.png");
                $oneA.find("div:first").addClass("display-none");
            });
        }
    },
    /*tag标签的关闭*/
    tagClose:function (button,e) {
        customerMenuTab.cancelBubbling(e);
        var li=$(button).parent();
        var id=(li.children("a").attr("href")+"").replace("#","");
        var href=li.children("a").attr("href");
        $("#content").children(".active").find("#"+id).remove();
        if(li.hasClass("active")){
            li.next().length?li.next().click():li.prev().click();
        }
        li.remove();
        customerMenuTab.notTag(href,li);
        customerMenuTab.tabLength();
        Param.clear(id);
    },
    addTag:function (id,text,src) {
        if($("#content").children(".active").find("#"+id).attr("role")===undefined){
            var ul=$("#content").children(".active").find(".iFrame-content").find("ul:first");
            var html="<li role='presentation' onclick='customerMenuTab.tagClick(this)'> <a href='#"+id+"' role='tab' data-toggle='tab'>"+text+"</a> <button type='button' class='close close-position no-border' onclick='customerMenuTab.tagClose(this,event)'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button></li>";
            ul.append(html);
            customerMenuTab.addIFrame(id,src);
            $("#content").children(".active").find(".iFrame-content ul:first li:last a:first").tab("show");
        }else {
            var tab=$("#content").children(".active").find(".iFrame-content ul:first").children("li").find("a[href=#"+id+"]");
            tab.click();
        }
        customerMenuTab.menuHeight();
    },
    tagClick:function (tag) {
        var href=$(tag).children("a:first").attr("href");
        $.each($("#content").children(".active").find(".menu").find("a"),function () {
            if($(this).attr("href")===href){
                var aOneP=$(this).parent();
                var aToP=aOneP.parent();
                var aThreeP=aToP.parent();
                var aFourP=aThreeP.parent();
                if((aToP.attr("class")+"").indexOf("third-menu")>-1){
                    if(aFourP.css("display")==="none"){
                        aFourP.parent().click();
                    }
                    if(aToP.css("display")==="none"){
                        aThreeP.click();
                    }
                }else if((aToP.attr("class")+"").indexOf("secondary-menu")>-1){
                    if(aToP.css("display")==="none"){
                        aThreeP.click();
                    }
                }
                aOneP.click();
                return false;
            }
        });
        customerMenuTab.tabLength();
    },
    menuFocus:function (li,clazz) {
        $(li).find("a:first").addClass(clazz);
        var lis=$(li).siblings();
        var ulOne= lis.find("ul:first");
        var ulTo;
        var ulThree;
        var aOne=lis.children("a");
        var aTo;
        var aThree;
        if(clazz==="menu-focus"){
            ulTo=ulOne.find("li ul:first");
            ulThree=ulTo.find("li ul:first");
            aTo=ulOne.find("li").children("a");
            aThree=ulTo.find("li").children("a");
            aThree.removeClass("third-menu-focus");
            aTo.removeClass("secondary-menu-focus");
            aOne.removeClass("menu-focus");
            aThree.find("div:first").addClass("display-none");
            aTo.find("div:first").addClass("display-none");
            aOne.find("div:first").addClass("display-none");
            ulThree.slideUp();
            ulTo.slideUp();
            ulOne.slideUp();
        }else if(clazz==="secondary-menu-focus"){
            ulTo=ulOne.find("li ul:first");
            aTo=ulOne.find("li").children("a");
            aOne.removeClass("secondary-menu-focus");
            aTo.removeClass("third-menu-focus");
            aOne.find("div:first").addClass("display-none");
            aTo.find("div:first").addClass("display-none");
            ulTo.slideUp();
            ulOne.slideUp();
        }else{
            aOne.removeClass("third-menu-focus");
            aOne.find("div:first").addClass("display-none");
            ulOne.slideUp();
        }
        customerMenuTab.lowerMenu(li);
    },
    /*显示下级菜单的方法*/
    lowerMenu:function (li) {
        var ul=$(li).children("ul");
        $(li).find("a:first").find("div:first").removeClass("display-none");
        if(ul.attr("role")){
            customerMenuTab.arrowRotate(ul);
            ul.slideToggle();
            return true;
        }
        return false;
    },

    /*改变一级菜单的图标*/
    menuIcon:function (li) {
        $.each($(li).parent().children("li"),function () {
            var imgName=($(this).find("a:first").attr("href")+"").replace("#","");
            $(this).find("img:first").attr("src","images/icon_"+imgName+"_grey.png");
        });
        var $a=$(li).children("a:first");
        var imgName=($a.attr("href")+"").replace("#","");
        $a.find("img:first").attr("src","images/icon_"+imgName+".png");
    },
    menuClick:function (li) {
        customerMenuTab.menuIcon(li);
        var a=$(li).find("a:first");
        var id=(a.attr("href")+"").replace("#","");
        var src=a.attr("src");
        var text=a.text();
        var flag=true;
        if($(li).find("li:first").attr("role")!=="presentation"){
            $.each($("#content").children(".active").find(".iFrame-content ul:first li"),function () {
                if($(this).find("a:first").attr("href")===("#"+id)){
                    $(this).find("a:first").tab("show");
                    flag=false;
                    return false;
                }
            });
            if(flag){
                customerMenuTab.addTag(id,text,src);
            }
        }
        customerMenuTab.contentH();
        customerMenuTab.menuHeight();
        customerMenuTab.tabLength();
    },
    /*拥有下级菜单的菜单箭头旋转动画*/
    arrowRotate:function (ul) {
        var $arrow=$(ul).parent().find(".arrow-position:first");
        if(IE8){
            if($(ul).css("display")==="none"){
                $arrow.addClass("arrow-position-down");
            }else{
                $arrow.removeClass("arrow-position-down");
            }
        }else {
            if($(ul).css("display")==="none"){
                $(ul).parent().find(".arrow-position:first").removeClass("arrow-position-down  arrow-rotateRecover");
                $(ul).parent().find(".arrow-position:first").addClass("arrow-rotate");

            }else{
                $(ul).parent().find(".arrow-position:first").removeClass("arrow-rotate");
                $(ul).parent().find(".arrow-position:first").addClass("arrow-position-down  arrow-rotateRecover");
            }
        }
    },
    setTimeHide:function () {
        $("#content").children(".active").find(".customerIndexMenu").parent().hide();
        $("#content").children(".active").find(".customerIndexMenu").parent().parent().hide();
        $("#content").children(".active").find(".iFrame-content").addClass("width-100");
    },
    mySlideToggle:function () {
        var $menuP=$("#content").children(".active").find(".customerIndexMenu").parent().parent();
        var marginLeft=$("#menu").css("width");
        if(customerMenuTab.menuUp){
            $menuP.stop().animate({"marginLeft":"-"+marginLeft},500);
            customerMenuTab.menuUp=false;
            setTimeout(customerMenuTab.setTimeHide,500);
        }else{
            $("#content").children(".active").find(".iFrame-content").removeClass("width-100");
            $("#content").children(".active").find(".customerIndexMenu").parent().parent().show();
            $("#content").children(".active").find(".customerIndexMenu").parent().show();
            $menuP.stop().animate({"marginLeft":"0"},500);
            customerMenuTab.menuUp=true;
        }
        setTimeout(function () {
            customerMenuTab.tabLength();
        },500);
    },
    menuHeight:function () {
        var $active=$("#content").children(".active");
        var $iFrameContent=$("#content").children(".active").find(".iFrame-content");
        var height;
        if($iFrameContent.height()>$("body").height()){
            height=$iFrameContent.height()-$("body").children("div:first").height()-$("#iFrame-content").children("div:first").height()-$active.children("div:first").children("div:first").height();
        }else{
            height=$("body").height()-$("body").children("div:first").height()-$("#iFrame-content").children("div:first").height()-$active.children("div:first").children("div:first").height();
        }
        var $menuP=$active.find(".menu").parent();
        $menuP.parent().parent().css("height",height);
        $menuP.parent().css("height",height);
        $menuP.css("height",height);
        customerMenuTab.menuScroll(height);
        customerMenuTab.contentH(height);
    },
    contentH:function (height) {
        // var $body=$("body");
        // var height=$body.height()-$body.children(".active").find("div").eq(1).height()-$body.children(".active").find(".iFrame-content").find("div:first").height();
        // var $content=$body.children(".active").find(".content");
        // $content.height(height);
        var $active=$("#content").children(".active");
        height=height-$("#iFrame-content").children("div:first").height();
        $active.find(".content").outerHeight(height);
    },
    reLoad:function (src) {
        var href=$("#content").children(".active").find(".iFrame-content").children("ul").find("li[class=active]").children("a").attr("href");
        var id=href.replace("#","");
        var iFrame=$("#"+id);
        iFrame.html("");
        iFrame.load(src);
    },
    addIFrame:function (id,src) {
        var html="<div role='tabpanel' class='tab-pane fade in' id='"+id+"' style='height: 100%;'>";
        $("#content").children(".active").find(".content").append(html);
        $("#"+id).load(src);
        // var html="<div role='tabpanel' class='tab-pane fade in' id='"+id+"'><div class='embed-responsive embed-responsive-4by3'><iframe class='embed-responsive-item' src='"+src+"'></iframe></div></div>";
        // $("#content").append(html);
    },
    getJqueryContent:function () {
        return $("#content").children(".active").find(".content").children(".active");
    },
    getContentId:function () {
        if(customerMenuTab.getJqueryContent().size()==1){
            return customerMenuTab.getJqueryContent().attr("id");
        }
        return "";
    },
    menuScroll:function (height) {
        var $menu=$("#content").children(".active").find(".menu");
        var $menuP=$menu.parent();
        setTimeout(function () {
            if($menu.height()>height){
                $menuP.css("overflow-y","scroll");
                $menu.find("li").css("overflow","hidden");
            }else{
                $menuP.css("overflow","hidden");
                $menuP.css("overflow-y","hidden");
            }
        },300);
    },
    getWidth:function (node) {
      var width=0;
        $(node).each(function () {
           width+=$(this).outerWidth(true);
        });
        return width;
    },
    tabLength:function () {
        var $div=$("#content").children(".active").find(".iFrame-content").children("div").eq(1);
        var $ul=$div.children("ul:first");
        var $activeLi=$ul.children("li[class=active]");
        var divWidth=customerMenuTab.getWidth($div);
        var liW=customerMenuTab.getWidth($activeLi);
        var liPreAllW=customerMenuTab.getWidth($activeLi.prevAll());
        var liNextAllW=customerMenuTab.getWidth($activeLi.nextAll());
        $ul.width(divWidth+liPreAllW+liNextAllW+liW);
        if($ul.width()<divWidth) $ul.width(divWidth);
        if($("#content").children(".active").find(".iFrame-content").find("div").eq(1).children("ul").children("li").length>0)
        customerMenuTab.tabMove($ul,$activeLi,divWidth,liW,liPreAllW,liNextAllW);
    },
    tabMove:function ($ul,$activeLi,divWidth,liW,liPreAllW,liNextAllW) {
        var ulW=$ul.width()-divWidth;
        var liPreW=customerMenuTab.getWidth($activeLi.prev());
        var liNextW=customerMenuTab.getWidth($activeLi.next());
        var oldLeft=$ul.position().left;
        var left=0;
        var tempLength=0;
        var $tempLi=$ul.children("li:first");
        //总长度小于divWidth
        if(ulW<divWidth){
            left=0;
        }else if((-oldLeft)>=liPreAllW){
            //log4j.info("标签在可见区域前");
            left=liPreAllW-liPreW;
        }else if(liPreAllW+liW+liNextW>=(divWidth-oldLeft)){
            //标签在可见区域后
            while(liW+liNextW+liPreAllW>tempLength+divWidth){
                tempLength+=customerMenuTab.getWidth($tempLi);
                $tempLi=$tempLi.next();
                if($tempLi.next().length===0) break;
            }
            left=tempLength;
        }else if(liPreAllW>-oldLeft && liPreAllW+liW<divWidth-oldLeft){
            if(liPreAllW+liW+liNextW>=(divWidth-oldLeft)){
               // log4j.info("标签在可见区域内  后一格不完整");
                while(tempLength<-oldLeft){
                    tempLength+=customerMenuTab.getWidth($tempLi);
                    $tempLi=$tempLi.next();
                }
                left=tempLength+customerMenuTab.getWidth($tempLi);
                if(liPreAllW+liW+liNextW>left+divWidth){
                    left+=customerMenuTab.getWidth($tempLi.next());
                }
            }else{
              //log4j.info("标签在可见区域内  后一格完整还多");
                if($activeLi.next().length===0){
                    while (tempLength+divWidth<liPreAllW+liW){
                        tempLength+=customerMenuTab.getWidth($tempLi);
                        $tempLi=$tempLi.next();
                        if($tempLi.length===0) break;
                    }
                    left=tempLength;
                }else if($activeLi.next().next().length===0){
                    while (tempLength+divWidth<liPreAllW+liW+liNextW){
                        tempLength+=customerMenuTab.getWidth($tempLi);
                        $tempLi=$tempLi.next();
                        if($tempLi.length===0) break;
                    }
                    left=tempLength;
                }else {
                    left=-oldLeft;
                }

            }
        }
        if(left<0) left=0;
        $ul.animate({"left":"-"+left+"px"},"fast");
    },
    tabButtonClick:function (direction) {
        var $div=$("#content").children(".active").find(".iFrame-content").children("div").eq(1);
        var $ul=$div.children("ul:first");
        var $lis=$ul.children("li");
        var $activeLi=null;
        var divWidth=customerMenuTab.getWidth($div);
        var ulW=$ul.width()-divWidth;
        var oldLeft=$ul.position().left;
        var left=0;
        var arr=[0,1,2];
        var $tempLi=$ul.children("li:first");
        $lis.each(function () {
            if($(this).hasClass("active")){
                $activeLi=$(this);
                return false;
            }
        });
        while (left<-oldLeft){
            left+=customerMenuTab.getWidth($tempLi);
            $tempLi=$tempLi.next();
        }
        if(direction==="左"){
            $.each(arr,function () {
                if($tempLi.prev().length===0) return false;
                $tempLi=$tempLi.prev();
                left-=customerMenuTab.getWidth($tempLi);
            });
        }else {
            $.each(arr,function () {
                if(ulW<=(left+divWidth)) return false;
                left+=customerMenuTab.getWidth($tempLi);
                $tempLi=$tempLi.next();
            });
        }
        if(left<0) left=0;
        $ul.animate({"left":"-"+left+"px"},"fast");
    }
};

customerMenuTab.start();
customerMenuTab.resize();

