/**
 * Created by Administrator on 2017/7/24.
 */
(function () {
    Param.initPageParam();
    let li = "";

    if (Param.user.type == '0' || Param.user.type == 0 || Param.user.type == 2 || Param.user.type == '2') {
        li = `<li class="layui-nav-item">
                          <a href="javascript:;" style="margin-right: 50px;" class="if-item" if-url="pages/hospital/hospital.html">医院管理</a>`
        li += '</li>'
        li += `<li class="layui-nav-item">
                          <a href="javascript:;" style="margin-right: 10px;" class="if-item" if-url="pages/pub/departments/department.html">科室管理</a>`
        li += '</li>'
        li += `<li class="layui-nav-item">
                          <a href="javascript:;" style="margin-right: 10px;" class="if-item" if-url="pages/doctor/doctor.html">医生管理</a>`
        li += '</li>'
        li += `<li class="layui-nav-item">
                           <a href="#"><i class="iconfont" ></i>排班管理</a>
                          <dl class="layui-nav-child" style="background: #000;">`
        li += `<dd><a href="javascript:;" class="if-item" style="margin-left: 20px;" if-url="pages/schedulingTemplate/outRegPlan.html">排班模板</a></dd>`
        // li += `<dd><a href="javascript:;" class="if-item" style="margin-left: 20px;" if-url="pages/schedulingTemplate/schedulingTemplate.html">排班模板</a></dd>`
        li += `<dd><a href="javascript:;" class="if-item"  style="margin-left: 20px;" if-url="pages/scheduling/scheduling.html">排班临时调整</a></dd>`
        li += '</dl></li>'
        li += `<li class="layui-nav-item">
                           <a href="#"><i class="iconfont" ></i>订单管理</a>
                          <dl class="layui-nav-child" style="background: #000;">`
        li += `<dd><a href="javascript:;" class="if-item" style="margin-left: 20px;" if-url="pages/appointment/appointment.html">预约记录</a></dd>`


        li += '</dl></li>'
        if (Param.user.type == 2 || Param.user.type == '2'){
        li += `<li class="layui-nav-item">
                           <a href="#"><i class="iconfont" ></i>用户管理</a>
                          <dl class="layui-nav-child" style="background: #000;">`
            li += `<dd><a href="javascript:;" class="if-item" style="margin-left: 20px;" if-url="pages/patients/patients.html">用户管理</a></dd>`
            li += `<dd><a href="javascript:;" class="if-item" style="margin-left: 20px;" if-url="pages/suffer/list.html">就诊人管理</a></dd>`
            // li += `<dd><a href="javascript:;" class="if-item" style="margin-left: 20px;" if-url="pages/suffer/list.html">12580就诊人管理</a></dd>`
        }
        li += '</dl></li>'

        li += `<li class="layui-nav-item">
                           <a href="#"><i class="iconfont" ></i>系统管理</a>
                          <dl class="layui-nav-child" style="background: #000;">`
        li += `<dd><a href="javascript:;" class="if-item" style="margin-left: 20px;" if-url="pages/bindInfo/bindInfo.html">挂号类型</a></dd>`
        // li += `<dd><a href="javascript:;" class="if-item" style="margin-left: 20px;" if-url="pages/pub/sysparameter/list.html">短信参数管理</a></dd>`
        // li += `<dd><a href="javascript:;" class="if-item" style="margin-left: 20px;" if-url="pages/dictionary/dictionary.html">公共字典维护</a></dd>`
        li += `<dd><a href="javascript:;" class="if-item" style="margin-left: 20px;" if-url="pages/staffInfo/staffInfo.html">账户管理</a></dd>`
        li += '<dd> <a href="javascript:;" class="if-item" style="margin-left: 20px;"  if-url="pages/Statis/statis.html">统计报表</a></dd>'
/*        li += '<dd> <a href="javascript:;" class="if-item" style="margin-left: 20px;"  if-url="pages/Statis/Statis2.html">科室统计报表</a></dd>'
        li += '<dd> <a href="javascript:;" class="if-item" style="margin-left: 20px;"  if-url="pages/Statis/statis3.html">医生统计报表</a></dd>'*/
        li += '<dd> <a href="javascript:;" class="if-item" style="margin-left: 20px;"  if-url="pages/Statis/dataview/views/index.html">分析图</a></dd>'
    } else if (Param.user.type == '1' || Param.user.type == 1) {
        li += `<li class="layui-nav-item">
                          <a href="javascript:;" style="margin-right: 10px;" class="if-item" if-url="pages/appointment/appointment.html">预约记录</a>`
        li += '</li>'
        // li += `<li class="layui-nav-item">
        //                   <a href="javascript:;" style="margin-right: 10px;" class="if-item" if-url="pages/patients/patients.html">账户管理</a>`
        // li += '</li>'
        // li += `<li class="layui-nav-item">
        //                   <a href="javascript:;" style="margin-right: 10px;" class="if-item" if-url="pages/scheduling/scheduling.html">排班查询</a>`
        // li += '</li>'
    }else if (Param.user.type == '3' || Param.user.type == 3) {
        li += `<li class="layui-nav-item">
                          <a href="javascript:;" style="margin-right: 10px;" class="if-item" if-url="pages/doctor/doctor.html">医生管理</a>`
        li += '</li>'
        li += `<li class="layui-nav-item">
                           <a href="#"><i class="iconfont" ></i>排班管理</a>
                          <dl class="layui-nav-child" style="background: #000;">`
        li += `<dd><a href="javascript:;" class="if-item" style="margin-left: 20px;" if-url="pages/schedulingTemplate/outRegPlan.html">排班模板</a></dd>`
        // li += `<dd><a href="javascript:;" class="if-item" style="margin-left: 20px;" if-url="pages/schedulingTemplate/schedulingTemplate.html">排班模板</a></dd>`
        li += `<dd><a href="javascript:;" class="if-item"  style="margin-left: 20px;" if-url="pages/scheduling/scheduling.html">排班临时调整</a></dd>`
        li += '</dl></li>'
        li += `<li class="layui-nav-item">
                          <a href="javascript:;" style="margin-right: 10px;" class="if-item" if-url="pages/appointment/appointment3.html">预约记录</a>`
        li += '</li>'
        // li += `<li class="layui-nav-item">
        //                   <a href="javascript:;" style="margin-right: 10px;" class="if-item" if-url="pages/patients/patients.html">账户管理</a>`
        // li += '</li>'
        // li += `<li class="layui-nav-item">
        //                   <a href="javascript:;" style="margin-right: 10px;" class="if-item" if-url="pages/scheduling/scheduling.html">排班查询</a>`
        // li += '</li>'
    }
    console.log(Param.user)

    li += '</dl></li>'


    $('.layui-nav').html(li)

    //改变iframe加载的网页的方法
    $(".if-item").on("click", function () {
        let url = $(this).attr("if-url")
        let $div = $(".page-content:first")
        let $iFrame = $div.find(".i-frame");
        $iFrame.attr("src") == url ? ($iFrame.hide(), setTimeout(function () {
            $iFrame.fadeIn();
        }, 200)) : ($iFrame.fadeOut(200, function () {
            $div.html("");
            var html = '<iframe class="i-frame"  src="' + url + '" frameborder="0" seamless></iframe>';
            $div.append(html);
            $div.find(".i-frame").fadeIn(200);
        }));
    })

    // ajaxNormalPost('/power/staff/menu/selectListWrapperForMenu',{},function(res){
    //     var li = '';
    //     if(res.data){
    //         var menu = res.data;
    //         // console.log(menu)
    //         for(var i=0;i<menu.length;i++){
    //             li+='<li><a href="#"><i class="iconfont">'+menu[i].ico+'</i><span class="nav-label">'+menu[i].name+'</span>'
    //                 +'<span class="arrow iconfont icon-jiantou3"></span>' +
    //                 '</a><ul class="nav nav-second d-n" menu-expanded="false">'
    //             for(var j=0;j<menu[i].wrappers.length;j++){
    //                 var param ='';
    //                 if(menu[i].wrappers[j].url.indexOf('?') >0){
    //                     param = '&level='+menu[i].wrappers[j].menuLevel;
    //                 }else{
    //                     param = '?level='+menu[i].wrappers[j].menuLevel;
    //                 }
    //                 li+='<li><a class="menuItem" href="javascript:void(0)"  lt-url=" '+menu[i].wrappers[j].url+param+' ">'
    //                     + '<span class="nav-label">'+menu[i].wrappers[j].name+'</span>'+
    //                     '<span class="arrow iconfont d-n">&#xe636;</span>' +
    //                     '</a></li>'
    //             }
    //             li += '</ul></li>'
    //         }
    //     }
    //     $('.nav').html(li)
    // })

    // ajaxNormalPost('/menu/menus/selectByMenuName', {}, function (res) {
    //     var li = '';
    //     //预约挂号登记
    //     if (res.data) {
    //         $('.portrait-name').text(res.data[0].sffName);
    //         var powerList = res.data[0].spcName;
    //         if (powerList.indexOf("预约挂号登记") != -1) {
    //             li += "<li><a href='#'><i class='iconfont'>&#xe675;</i><span class='nav-label'>预约挂号登记</span>"
    //                 +
    //                 "<span class='arrow iconfont icon-jiantou3'></span>" +
    //                 "</a><ul class='nav nav-second d-n' menu-expanded='false'>"
    //             if (powerList.indexOf("预约挂号登记") != -1) {
    //                 li += "<li><a class='menuItem' href='javascript:void(0)' lt-url='pages/ord/schedulingModel/list.html'>"
    //                     +
    //                     " <span class='nav-label'>排班模版</span>" +
    //                     "<span class='arrow iconfont d-n'>&#xe636;</span>" +
    //                     "</a></li>"
    //             }
    //             if (powerList.indexOf("预约挂号登记") != -1) {
    //
    //                 li += ' <li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/ord/schedulingManage/list.html">'
    //                     +
    //                     '<span class="nav-label">排班管理</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("预约挂号登记") != -1) {
    //
    //                 li += ' <li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/ord/schedulingClose/list.html">'
    //                     +
    //                     '<span class="nav-label">停诊管理</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("历史排班") != -1) {
    //
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/ord/schedulingHistory/list.html"> <span class="nav-label">历史排班</span>'
    //                     +
    //                     '<span class="arrow iconfont d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("预约挂号登记") != -1) {
    //                 li += "<li><a class='menuItem' href='javascript:void(0)' lt-url='pages/ord/registration/list.html'>"
    //                     +
    //                     " <span class='nav-label'>预约登记</span>" +
    //                     "<span class='arrow iconfont d-n'>&#xe636;</span>" +
    //                     "</a></li>"
    //             }
    //             if (powerList.indexOf("预约挂号登记") != -1) {
    //                 li += "<li><a class='menuItem' href='javascript:void(0)' lt-url='pages/ord/registration/list.html'>"
    //                     +
    //                     " <span class='nav-label'>挂号登记</span>" +
    //                     "<span class='arrow iconfont d-n'>&#xe636;</span>" +
    //                     "</a></li>"
    //             }
    //             if (powerList.indexOf("预约挂号登记") != -1) {
    //
    //                 li += ' <li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/ord/doorOfficeReg/list.html">'
    //                     +
    //                     '<span class="nav-label">门办预约</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("预约挂号登记") != -1) {
    //
    //                 li += ' <li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/ord/regForm/index.html">'
    //                     +
    //                     '<span class="nav-label">预约报表</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             // if (powerList.indexOf("登记") != -1) {
    //             //     li += "<li><a class='menuItem' href='javascript:void(0)' lt-url='test.html'>"
    //             //         +
    //             //         " <span class='nav-label'>医保测试相关</span>" +
    //             //         "<span class='arrow iconfont d-n'>&#xe636;</span>" +
    //             //         "</a></li>"
    //             // }
    //             li += '</ul></li>'
    //         }
    //
    //         //门诊挂号管理
    //         if (powerList.indexOf("门诊挂号管理") != -1) {
    //             li += "<li><a href='#'><i class='iconfont'>&#xe675;</i><span class='nav-label'>门诊挂号管理</span>"
    //                 +
    //                 "<span class='arrow iconfont icon-jiantou3'></span>" +
    //                 "</a><ul class='nav nav-second d-n' menu-expanded='false'>"
    //             // if (powerList.indexOf("排班模板") != -1) {
    //             //     li += "<li><a class='menuItem' href='javascript:void(0)' lt-url='pages/registration/schedulingModel/list.html'>"
    //             //         +
    //             //         " <span class='nav-label'>排班模板</span>" +
    //             //         "<span class='arrow iconfont d-n'>&#xe636;</span>" +
    //             //         "</a></li>"
    //             // }
    //             // if (powerList.indexOf("排班管理") != -1) {
    //             //
    //             //     li += ' <li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/registration/schedulingManage/list.html">'
    //             //         +
    //             //         '<span class="nav-label">排班管理</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             // }
    //             // if (powerList.indexOf("历史排班") != -1) {
    //             //
    //             //     li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/registration/list.html"> <span class="nav-label">历史排班</span>'
    //             //         +
    //             //         '<span class="arrow iconfont d-n">&#xe636;</span></a></li>'
    //             // }
    //             if (powerList.indexOf("门诊挂号") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/registration/reg/list.html">'
    //                     +
    //                     '<span class="nav-label">门诊挂号</span><span class="arrow iconfont d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("挂号记录") != -1) {
    //
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/registration/reg/regList.html">'
    //                     +
    //                     '<span class="nav-label">挂号记录</span><span class="arrow iconfont d-n">&#xe636;</span></a></li>'
    //             }
    //             /*  if (powerList.indexOf("挂号日汇总") != -1) {
    //
    //                   li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/registration/reportForm/personalDaySummary.html">' +
    //                       '<span class="nav-label">挂号日汇总</span><span class="arrow iconfont icon-xiazai6 d-n"></span></a></li>'
    //               }*/
    //             if (powerList.indexOf("挂号记录") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/registration/reginfo_maintenance/reginfo.html">'
    //                     +
    //                     '<span class="nav-label">信息维护</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("门诊挂号") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/registration/reportForm/reg_report.html">'
    //                     +
    //                     '<span class="nav-label">统计报表</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             li += '</ul></li>'
    //         }
    //
    //         //票据管理
    //         if (powerList.indexOf("票据管理") != -1) {
    //             li += '<li><a href="#"><i class="iconfont">&#xe605;</i><span class="nav-label">票据管理</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6"></span></a>' +
    //                 '<ul class="nav nav-second d-n" menu-expanded="false">'
    //
    //             if (powerList.indexOf("发票领用") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/charges/billreceive/list.html">'
    //                     +
    //                     '<span class="nav-label">发票领用</span><span class="arrow iconfont d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("发票号设置") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/charges/charges_list/billUse.html">'
    //                     +
    //                     '<span class="nav-label">发票号设置</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             li += '</ul></li>'
    //
    //         }
    //         //门诊收费管理
    //         if (powerList.indexOf("门诊收费管理") != -1) {
    //             li += "<li><a href='#'><i class='iconfont icon-menzhenshoufei'></i><span class='nav-label'>门诊收费管理</span>"
    //                 +
    //                 "<span class='arrow iconfont icon-xiazai6'></span>" +
    //                 "</a><ul class='nav nav-second d-n' menu-expanded='false'>"
    //             // if (powerList.indexOf("门诊收费") != -1) {
    //             //     li += "<li><a class='menuItem' href='javascript:void(0)' lt-url='pages/regFee/feeList.html'>"
    //             //         +
    //             //         " <span class='nav-label'>门诊收费</span>" +
    //             //         "<span class='arrow iconfont  d-n'>&#xe636;</span>" +
    //             //         "</a></li>"
    //             // }
    //             if (powerList.indexOf("门诊收费") != -1) {
    //                 li += "<li><a class='menuItem' href='javascript:void(0)' lt-url='pages/charges/charges_list/list.html'>"
    //                     +
    //                     " <span class='nav-label'>门诊收费</span>" +
    //                     "<span class='arrow iconfont  d-n'>&#xe636;</span>" +
    //                     "</a></li>"
    //             }
    //             if (powerList.indexOf("快速门诊") != -1) {
    //                 li += ' <li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/fastReg/fastReg.html">'
    //                     +
    //                     '<span class="nav-label">方便门诊</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             li += '</ul></li>'
    //         }
    //
    //         //诊间医生
    //         if (powerList.indexOf("诊间医生") != -1) {
    //             li += "<li><a href='#'><i class='iconfont'>&#xe6ee;</i><span class='nav-label'>诊间医生</span>"
    //                 +
    //                 "<span class='arrow iconfont icon-xiazai6'></span>" +
    //                 "</a><ul class='nav nav-second d-n' menu-expanded='false'>"
    //             if (powerList.indexOf("门诊医生") != -1) {
    //                 li += "<li><a class='menuItem' href='javascript:void(0)' lt-url='pages/clinicDoctor/patientsList.html'>"
    //                     +
    //                     " <span class='nav-label'>门诊医生</span>" +
    //                     "<span class='arrow iconfont  d-n'>&#xe636;</span>" +
    //                     "</a></li>"
    //             }
    //             // if (powerList.indexOf("门诊医生") != -1) {
    //             //     li += "<li><a class='menuItem' href='javascript:void(0)' lt-url='pages/clinic/clinic_doctor.html'>"
    //             //         +
    //             //         " <span class='nav-label'>门诊医生(旧)</span>" +
    //             //         "<span class='arrow iconfont  d-n'>&#xe636;</span>" +
    //             //         "</a></li>"
    //             // }
    //             if (powerList.indexOf("历史就诊") != -1) {
    //
    //                 li += ' <li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/clinicDoctor/checkOld.html">'
    //                     +
    //                     '<span class="nav-label">历史就诊信息</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             li += '</ul></li>'
    //         }
    //
    //         //药品管理
    //         if (powerList.indexOf("药品管理") != -1) {
    //             li += '<li><a href="#"><i class="iconfont">&#xe60e;</i><span class="nav-label">药品管理</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6"></span></a> <ul class="nav nav-second d-n" menu-expanded="false">'
    //             if (powerList.indexOf("药品信息管理") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drug/yaopingxinxigl/main.html">'
    //                     +
    //                     '<span class="nav-label">药品信息管理</span> <span class="arrow iconfont d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("药品信息管理") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drug/yaopingxinxigl/mainNew.html">'
    //                     +
    //                     '<span class="nav-label">药品信息匹配</span> <span class="arrow iconfont d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("西药规格转换") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drug/specifications/change.html?storeId=5001">'
    //                     +
    //                     '<span class="nav-label">西药规格转换</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("中药规格转换") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/specifications/change.html?storeId=5002">'
    //                     +
    //                     '<span class="nav-label">药品规格转换</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("西药协定处方") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/cli/treatyrecipe/list.html">'
    //                     +
    //                     '<span class="nav-label">西药协定处方</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("西药协定处方") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/cli/treatyrecipe/listMakeUp.html">'
    //                     +
    //                     '<span class="nav-label">西药组方</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("中药协定处方") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/cli/treatyrecipe/chlist.html">'
    //                     +
    //                     '<span class="nav-label">中药协定处方</span><span class="arrow iconfont d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("中药协定处方") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/cli/treatyrecipe/chlistMakeUp.html">'
    //                     +
    //                     '<span class="nav-label">中药组方</span><span class="arrow iconfont d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("中药协定处方") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drug/chinesePatent/list.html">'
    //                     +
    //                     '<span class="nav-label">中草药限量管理</span><span class="arrow iconfont d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("药品频次管理") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drug/frequency/list.html">'
    //                     +
    //                     '<span class="nav-label">药品频次管理</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //
    //             }
    //             if (powerList.indexOf("药品厂商管理") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drug/manufacturer/list.html">'
    //                     +
    //                     '<span class="nav-label">药品厂商管理</span><span class="arrow iconfont d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("供应商管理") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drug/supplier/list.html">'
    //                     +
    //                     '<span class="nav-label">药品供应商管理</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("药品剂型管理") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drug/preparation/list.html">'
    //                     +
    //
    //                     '<span class="nav-label">药品剂型管理</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("药品单位管理") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drug/unit/list.html">'
    //                     +
    //                     '<span class="nav-label">药品单位管理</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //
    //             }
    //             if (powerList.indexOf("给药方式管理") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drug/usagemode/list.html">'
    //                     +
    //                     ' <span class="nav-label">给药方式管理</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             li += '</ul></li>'
    //
    //         }
    //
    //         //药库设置
    //         if (powerList.indexOf("药库设置") != -1) {
    //             li += '<li><a href="#"><i class="iconfont">&#xe628;</i><span class="nav-label">药库设置</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6"></span></a><ul class="nav nav-second d-n" menu-expanded="false">'
    //             if (powerList.indexOf("库房管理") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/drugStoreSetting/storeManage/manage.html">'
    //                     +
    //                     '<span class="nav-label">库房管理</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("记账分类") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drugStoreSetting/accountClassify/classify.html">'
    //                     +
    //                     '<span class="nav-label">记账分类</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("出入库方式") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drugStoreSetting/passWays/ways.html">'
    //                     +
    //                     '<span class="nav-label">出入库方式</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("药库存储范围") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/drugStoreSetting/storageRange/range.html">'
    //                     +
    //                     '<span class="nav-label">药库存储范围</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             li += '</ul></li>'
    //         }
    //
    //         if (powerList.indexOf("患者就诊记录") != -1) {
    //             li += '<li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/clinic/patient_list.html">'
    //                 +
    //                 '<i class="iconfont">&#xe614;</i><span class="nav-label">患者就诊记录</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6 d-n"></span></a></li>'
    //         }
    //         if (powerList.indexOf("西药房管理") != -1) {
    //             li += '<li><a href="#"><i class="iconfont">&#xe905;</i><span class="nav-label">西药房管理</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6"></span></a><ul class="nav nav-second d-n" menu-expanded="false">'
    //             if (powerList.indexOf("西药入库") != -1) {
    //
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/in_bills/list.html?storeId=5001">'
    //                     +
    //                     '<span class="nav-label">西药入库</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("西药出库") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/out_bills/list.html?storeId=5001">'
    //                     +
    //                     '<span class="nav-label">药品申领</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("西药出库") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/out_bills/list.html?storeId=5001">'
    //                     +
    //                     '<span class="nav-label">西药出库</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("西药发药") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"  lt-url="pages/dispensing/dispensing_sign.html?storeId=5001">'
    //                     +
    //                     '<span class="nav-label">西药发药</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("西药总库存查看") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/stocks/stocks.html?storeId=5001">'
    //                     +
    //                     '<span class="nav-label">西药总库存查看</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("西药批次库存查看") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/pharmacy/stocks/stocksBatch.html?storeId=5001">'
    //                     +
    //                     '<span class="nav-label">西药批次库存查看</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '<span class="arrow icon icon-fangxiangyou01 d-n"></span></a></li>'
    //             }
    //             if (powerList.indexOf("西药月末结转记录查看") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/book_table/list.html?storeId=5001">'
    //                     +
    //                     '<span class="nav-label">西药月末结转记录查看</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("西药月末结转报表") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/pharmacy/account_book/list.html">'
    //                     +
    //                     '<span class="nav-label">西药月末结转报表</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("西药发放记录") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/dispensingDetail/list.html?storeId=5001">'
    //                     +
    //                     '<span class="nav-label">西药发放记录</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("西药规格转换") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/pharmacy/specifications/change.html?storeId=5001">'
    //                     +
    //                     '<span class="nav-label">西药规格转换</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("西药批次药品调价") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/priceModify/list2.html?storeId=5001">'
    //                     +
    //                     '<span class="nav-label">西药批次药品调价</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //
    //             }
    //             if (powerList.indexOf("西药批次药品调价") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/inventory/list.html?storeId=5001">'
    //                     +
    //                     '<span class="nav-label">西药房盘点</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             li += '</ul></li>'
    //         }
    //         if (powerList.indexOf("中药房管理") != -1) {
    //             li += '<li><a href="#"><i class="iconfont">&#xe691;</i><span class="nav-label">中药房管理</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6"></span> </a><ul class="nav nav-second d-n" menu-expanded="false">'
    //             if (powerList.indexOf("中药入库") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/pharmacy/in_bills/list.html?storeId=5002">'
    //                     +
    //                     '<span class="nav-label">药房入库</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("中药出库") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/out_bills/list.html?storeId=5002">'
    //                     +
    //                     '<span class="nav-label">药房出库</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("中药发药") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/dispensing/dispensing_sign.html?storeId=5002">'
    //                     +
    //                     '<span class="nav-label">中药发药</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("中药发药") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/dispenseCheck/outAuditing.html?storeId=5002">'
    //                     +
    //                     '<span class="nav-label">代煎审核</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("中药总库存查看") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/stocks/stocks.html?storeId=5002">'
    //                     +
    //                     '<span class="nav-label">中药总库存查看</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("中药批次库存查看") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/stocks/stocksBatch.html?storeId=5002">'
    //                     +
    //                     ' <span class="nav-label">中药批次库存查看</span><span class="arrow iconfont d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("中药月末结转记录查看") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/pharmacy/book_table/list.html?storeId=5002">'
    //                     +
    //                     '<span class="nav-label">中药月末结转记录查看</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("中药月末结转报表") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/pharmacy/account_book/list.html">'
    //                     +
    //                     '<span class="nav-label">中药月末结转报表</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("中药发放记录") != -1) {
    //                 li += '<li>  <a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/dispensingDetail/list.html?storeId=5002">'
    //                     +
    //                     '<span class="nav-label">中药发放记录</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("中药规格转换") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/specifications/change.html?storeId=5002">'
    //                     +
    //                     '<span class="nav-label">药品规格转换</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("中药批次药品调价") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/priceModify/list2.html?storeId=5002">'
    //                     +
    //                     '<span class="nav-label">中药批次药品调价</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("中药批次药品调价") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/pharmacy/inventory/list.html?storeId=5002">'
    //                     +
    //                     '<span class="nav-label">中药房盘点</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             li += '</ul></li>'
    //         }
    //         if (powerList.indexOf("西药库管理") != -1) {
    //             li += '<li><a href="#"><i class="iconfont">&#xe640;</i><span class="nav-label">西药库管理</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6"></span></a> <ul class="nav nav-second d-n" menu-expanded="false">'
    //             if (powerList.indexOf("西药入库") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/store/in_bills/list.html?storeId=5003">'
    //                     +
    //                     '<span class="nav-label">西药入库</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("西药出库") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/store/out_bills/list.html?storeId=5003">'
    //                     +
    //                     '<span class="nav-label">西药出库</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("西药总库存查看") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/store/stocks/stocks.html?storeId=5003">'
    //                     +
    //                     '<span class="nav-label">西药总库存查看</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("西药批次库存查看") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/store/stocks/stocksBatch.html?storeId=5003">'
    //                     +
    //                     '<span class="nav-label">西药批次库存查看</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '<span class="arrow icon icon-fangxiangyou01 d-n"></span></a></li>'
    //             }
    //             if (powerList.indexOf("西药批次药品调价") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/store/priceModify/list2.html?storeId=5003">'
    //                     +
    //                     '<span class="nav-label">西药批次药品调价</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //
    //             if (powerList.indexOf("西药月末结转记录查看") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/store/book_table/list.html?storeId=5003">'
    //                     +
    //                     '<span class="nav-label">西药月末结转记录查看</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("西药月末结转报表") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/store/account_book/list.html">'
    //                     +
    //                     '<span class="nav-label">西药月末结转报表</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("西药月末结转报表") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/store/inventory/list.html?storeId=5003">'
    //                     +
    //                     '<span class="nav-label">西药库盘点</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("药房收支汇总月报表") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/financialManage/summaryOfIncomeAndExpenditureForm.html?storeId=5003">'
    //                     +
    //                     '<span class="nav-label">西药库实时收支情况</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             li += '</ul></li>'
    //         }
    //         if (powerList.indexOf("中药库管理") != -1) {
    //             li += '<li><a href="#"><i class="iconfont">&#xe691;</i><span class="nav-label">中药库管理</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6"></span> </a><ul class="nav nav-second d-n" menu-expanded="false">'
    //             if (powerList.indexOf("中药入库") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/store/in_bills/list.html?storeId=5004">'
    //                     +
    //                     '<span class="nav-label">中药入库</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("中药出库") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)"lt-url="pages/store/out_bills/list.html?storeId=5004">'
    //                     +
    //                     '<span class="nav-label">中药出库</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("中药总库存查看") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/store/stocks/stocks.html?storeId=5004">'
    //                     +
    //                     '<span class="nav-label">中药总库存查看</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("中药批次库存查看") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/store/stocks/stocksBatch.html?storeId=5004">'
    //                     +
    //                     ' <span class="nav-label">中药批次库存查看</span><span class="arrow iconfont d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("中药月末结转记录查看") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/store/book_table/list.html?storeId=5004">'
    //                     +
    //                     '<span class="nav-label">中药月末结转记录查看</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("中药批次药品调价") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)"lt-url="pages/store/priceModify/list2.html?storeId=5004">'
    //                     +
    //                     '<span class="nav-label">中药批次药品调价</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("中药批次药品调价") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/store/inventory/list.html?storeId=5004">'
    //                     +
    //                     '<span class="nav-label">中药库盘点</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //             }
    //             if (powerList.indexOf("药房收支汇总月报表") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/financialManage/summaryOfIncomeAndExpenditureForm.html?storeId=5004">'
    //                     +
    //                     '<span class="nav-label">中药库实时收支情况</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             li += '</ul></li>'
    //         }
    //
    //         if (powerList.indexOf("健康管理") != -1) {
    //             li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="forwardChronic.html">'
    //                 +
    //                 '<i class="iconfont">&#xe60b;</i><span class="nav-label">健康管理</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6 d-n"></span></a></li>'
    //         }
    //
    //         if (powerList.indexOf("医保信息管理") != -1) {
    //             li += '<li><a href="#"><i class="iconfont">&#xe640;</i><span class="nav-label">医保信息管理</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6"></span></a> <ul class="nav nav-second d-n" menu-expanded="false">'
    //             // if (powerList.indexOf("市医保对账") != -1) {
    //             //     li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/healthInsurance/cityReconcile.html">'
    //             //         +
    //             //         '<span class="nav-label">市医保对账(旧)</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //             //         +
    //             //         '</a></li>'
    //             // }
    //             // if (powerList.indexOf("省医保对账") != -1) {
    //             //     li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/healthInsurance/ZJSReconcile.html">'
    //             //         +
    //             //         '<span class="nav-label">省医保对账(旧)</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //             //         +
    //             //         '</a></li>'
    //             // }
    //             if (powerList.indexOf("市医保对账") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/healthInsurance/cityReconcile_new.html">'
    //                     +
    //                     '<span class="nav-label">市医保对账</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("省医保对账") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/healthInsurance/ZJSReconcile_new.html">'
    //                     +
    //                     '<span class="nav-label">省医保对账</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("省医保对账") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/healthInsurance/ZJSRabnormalHandle.html">'
    //                     +
    //                     '<span class="nav-label">省医保异常交易处理</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("市医保对账") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/healthInsurance/city/list.html">'
    //                     +
    //                     '<span class="nav-label">市医保异常交易处理</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("康复备案") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/healthInsurance/records.html">'
    //                     +
    //                     '<span class="nav-label">康复备案</span> <span class="arrow iconfont d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("市医保外配处方") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/healthInsurance/recipeOuter/list.html">'
    //                     +
    //                     '<span class="nav-label">市医保外配处方</span> <span class="arrow iconfont d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("市医保基础字典") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/healthInsurance/dict_download/list.html">'
    //                     +
    //                     '<span class="nav-label">市医保基础字典</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("省医保基础字典") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/healthInsurance/dict_download/list2.html">'
    //                     +
    //                     '<span class="nav-label">省医保基础字典</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //
    //             li += '</ul></li>'
    //         }
    //
    //         if (powerList.indexOf("库存监管") != -1) {
    //             li += '<li><a href="#"><i class="iconfont">&#xe638;</i><span class="nav-label">库存监管</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6"></span></a><ul class="nav nav-second d-n" menu-expanded="false">'
    //             if (powerList.indexOf("库存上传") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/repertory/trade/listTest.html">'
    //                     +
    //                     '<span class="nav-label">库存上传</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("药品查询") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/repertory/trade/list.html">'
    //                     +
    //                     '<span class="nav-label">药品查询</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             li += '</ul></li>'
    //         }
    //         //基础数据维护
    //         if (powerList.indexOf("基础数据维护") != -1) {
    //             li += '<li><a href="#"><i class="iconfont">&#xe638;</i><span class="nav-label">基础数据维护</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6"></span></a><ul class="nav nav-second d-n" menu-expanded="false">'
    //             if (powerList.indexOf("患者信息管理") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/pub/patients/list.html">'
    //                     +
    //                     '<span class="nav-label">患者信息管理</span><span class="arrow iconfont  d-n">&#xe636;</span></a></li>'
    //
    //             }
    //             if (powerList.indexOf("职工信息管理") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/pub/staff_info/list.html">'
    //                     +
    //                     '<span class="nav-label">职工信息管理</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("科室信息维护") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/pub/departments/list.html">'
    //                     +
    //                     '<span class="nav-label">科室信息维护</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("疾病信息维护") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/pub/Disease/list.html">'
    //                     +
    //                     '<span class="nav-label">疾病信息维护</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //
    //             if (powerList.indexOf("收费项目维护") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/pub/items/list.html">'
    //                     +
    //                     '<span class="nav-label">收费项目维护</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("收费科目维护") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/pub/subjects/list.html">'
    //                     +
    //                     '<span class="nav-label">收费科目维护</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("收费科目维护") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/pub/relyItem/list.html">'
    //                     +
    //                     '<span class="nav-label">项目依赖维护</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //
    //             if (powerList.indexOf("打印机管理") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/pub/printer/printerList.html">'
    //                     +
    //                     '<span class="nav-label">打印机管理</span><span class="arrow iconfont d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("参数信息管理") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/pub/sysparameter/list.html">'
    //                     +
    //                     '<span class="nav-label">参数信息管理</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("公共字典维护") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/pub/dictionary/list_new.html">'
    //                     +
    //                     '<span class="nav-label">公共字典维护</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             li += '</ul></li>'
    //
    //         }
    //
    //         //开单项目维护
    //         if (powerList.indexOf("开单项目维护") != -1) {
    //             li += '<li><a href="#"><i class="iconfont">&#xe60c;</i><span class="nav-label">开单项目维护</span>'
    //                 +
    //                 '<span class="arrow iconfont icon-xiazai6"></span></a>' +
    //                 '<ul class="nav nav-second d-n" menu-expanded="false">'
    //
    //             if (powerList.indexOf("检查项目") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/cli/bill_pacs/pacs.html">'
    //                     +
    //                     '<span class="nav-label">检查项目</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("检验项目") != -1) {
    //                 li += '<li> <a class="menuItem" href="javascript:void(0)" lt-url="pages/cli/bill_lis/lis.html">'
    //                     +
    //                     '<span class="nav-label">检验项目</span> <span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             li += '</ul></li>'
    //
    //         }
    //
    //         //权限管理
    //         if (powerList.indexOf("权限管理") != -1) {
    //             li += '<li> <a href="#"><i class="iconfont">&#xe616;</i>' +
    //                 '<span class="nav-label">权限管理</span><span class="arrow iconfont icon-xiazai6"></span>'
    //                 +
    //                 '</a><ul class="nav nav-second d-n" menu-expanded="false">'
    //             if (powerList.indexOf("角色权限") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/power/list.html">'
    //                     +
    //                     '<span class="nav-label">角色权限</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //
    //             if (powerList.indexOf("角色权限") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/power/roleManage.html">'
    //                     +
    //                     '<span class="nav-label">角色权限(旧)</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             if (powerList.indexOf("用户权限") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/power/userManage.html">'
    //                     +
    //                     '<span class="nav-label">用户权限</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //             }
    //             li += '</ul></li>'
    //         }
    //         //财务报表
    //         if (powerList.indexOf("财务报表") != -1) {
    //             li += '<li> <a href="#"><i class="iconfont">&#xe6bb;</i>' +
    //                 '<span class="nav-label">财务报表</span><span class="arrow iconfont icon-xiazai6"></span>'
    //                 +
    //                 '</a><ul class="nav nav-second d-n" menu-expanded="false">'
    //
    //             if (powerList.indexOf("省医保申报表") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/financialManage/provincialDeclarationForm.html">'
    //                     +
    //                     '<span class="nav-label">省医保申报表</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("市医保子女统筹申报表") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/financialManage/childrenMedicalInsuranceForm.html">'
    //                     +
    //                     '<span class="nav-label">市医保子女统筹申报表</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("市医保基本医疗申报表") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/financialManage/cityMedicalInsuranceForm.html">'
    //                     +
    //                     '<span class="nav-label">市医保基本医疗申报表</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("药房收支汇总月报表") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/financialManage/summaryOfIncomeAndExpenditureForm.html">'
    //                     +
    //                     '<span class="nav-label">药房收支汇总月报表</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("药房收支汇总月报表") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/financialManage/regCheckout.html">'
    //                     +
    //                     '<span class="nav-label">挂号结账单</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             li += '</ul></li>'
    //         }
    //         //财务报表
    //         if (powerList.indexOf("数据分析") != -1) {
    //             li += '<li> <a href="#"><i class="iconfont">&#xe6bb;</i>' +
    //                 '<span class="nav-label">数据分析</span><span class="arrow iconfont icon-xiazai6"></span>'
    //                 +
    //                 '</a><ul class="nav nav-second d-n" menu-expanded="false">'
    //
    //             if (powerList.indexOf("患者地域分布分析") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/financialManage/provincialDeclarationForm.html">'
    //                     +
    //                     '<span class="nav-label">患者地域分布分析</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("患者年龄分布分析") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/financialManage/childrenMedicalInsuranceForm.html">'
    //                     +
    //                     '<span class="nav-label">患者年龄分布分析</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             if (powerList.indexOf("药品进销价格趋势分析") != -1) {
    //                 li += '<li><a class="menuItem" href="javascript:void(0)" lt-url="pages/financialManage/cityMedicalInsuranceForm.html">'
    //                     +
    //                     '<span class="nav-label">药品进销价格趋势分析</span><span class="arrow iconfont  d-n">&#xe636;</span>'
    //                     +
    //                     '</a></li>'
    //
    //             }
    //             li += '</ul></li>'
    //         }
    //
    //     }
    //
    //     $('.nav').html(li)
    // })
})(jQuery)

