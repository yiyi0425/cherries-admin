/**
 * 键盘上下键、回车事件
 */
function keydownAction(event, target, index) {
    $target = $(target);
    if (event.keyCode == Constants.PgUp) {
        if (index != 0) {
            index--;
        }
        $(target).find("p").eq(index).mouseover();
    } else if (event.keyCode == Constants.PgDn) {
        if (index < $target.find("p").length - 1) {
            index++;
        }
        $target.find("p").eq(index).mouseover();
    } else if (event.keyCode == Constants.PgEnter) {
        $target.find("p[class=new_hover]").click();
    }
    return index;
}

/**
 * ajax统一表单Post提交方式
 */
function ajaxFormPost(postUrl, redirectUrl, id) {
    $.ajax({
        async: false,
        type: "POST",
        dataType: "json",
        url: concatToken(Param.host_url + postUrl),
        data: $('#' + id).serialize(),
        success: function (data, textStatus) {
            if (data.ret == 0) {
                layer.msg("操作成功");
                if (redirectUrl) Page.gotoURL(redirectUrl);
            }
            if (data.ret == 1) {
                layer.msg(data.msg)
            }
        },
        error: function (jqXHR) {
            layer.msg(postUrl + "请求出现例外:" + jqXHR.statusText);
        }
    });
}

/**
 * ajax统一Post方式提交，回调callback
 */
function ajaxNormalPost(postUrl, postData, callback, async = false) {
    $.ajax({
        async: async,
        type: "POST",
        dataType: "json",
        url: concatToken(Param.host_url + postUrl),
        data: postData,
        success: function (data, textStatus) {
            callback(data);
        },
        error: function (jqXHR) {
            layer.msg(postUrl + "请求出现例外:" + jqXHR.statusText);
            return false;
        }
    });
}

/**
 * ajax统一Post方式提交
 * @return <Promise>
 */
function ajaxPromise(postUrl, postData, async = false) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: async,
            type: "POST",
            dataType: "json",
            url: concatToken(Param.host_url + postUrl),
            data: postData,
            success: function (data) {
                if (data.ret === 0) {
                    resolve(data);
                } else {
                    layer.msg('出现异常, ' + data.msg);
                    resolve(data);
                }
            },
            error: function (e) {
                layer.msg("请求出现例外:" + jqXHR.statusText);
                reject(e)
            }
        });
    })
}

/**
 * ajax统一Post方式提交数据为JSON格式，回调callback
 */
function ajaxNormalPostJson(postUrl, postData, callback, async = false) {
    $.ajax({
        async: async,
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        url: concatToken(Param.host_url + postUrl),
        data: postData,
        success: function (data, textStatus) {
            callback(data);
        },
        error: function (jqXHR) {
            layer.msg(postUrl + "请求出现例外:" + jqXHR.statusText);
            return false;
        }
    });
}

$.prototype.serializeObject = function () {
    var a, o, h, i, e;
    a = this.serializeArray();
    o = {};
    h = o.hasOwnProperty;
    for (i = 0; i < a.length; i++) {
        e = a[i];
        if (!h.call(o, e.name)) {
            o[e.name] = e.value;
        }
    }
    return o;
};

function uniqueQueryStr() {
    var sid = "&sid=" + new Date().getTime() + "&sid2=" + Math.round(Math.random() * 1000);
    return sid;
}

/**
 * 为每个访问链接添加token参数
 *
 * @param href
 * @returns
 */
function concatToken(href) {

    if (href.indexOf("?") == -1) {
        if (Param.uriParams.token) {
            href += "?appId=" + Param.uriParams.appId + "&token=" + Param.uriParams.token + uniqueQueryStr();
        } else {
            href += "?appId=" + Param.uriParams.appId + "&token=" + Param.user.token + uniqueQueryStr();
        }

    } else {
        href += "&appId=" + Param.uriParams.appId + "&token=" + Param.uriParams.token + uniqueQueryStr();
    }
    return href;
}

/**
 * 健康管理
 * @param href
 * @returns
 */
function concatUserToken(href) {

    if (href.indexOf("?") == -1) {
        href += "?appId=" + Param.uriParams.appId + "&token=" + Param.uriParams.token + "&name=" + Param.uriParams.name;
    } else {
        href += "&appId=" + Param.uriParams.appId + "&token=" + Param.uriParams.token + "&name=" + Param.uriParams.name;
    }
    return href;
}

//获取选中行
function getRows(table) {
    selects = table.bootstrapTable('getSelections');
    if (selects.length < 1) {
        layer.msg("请选择数据");
        return null;
    }
    return selects;
}

//根据字段名称返回对应的选中行的数据集合
function getRowFieldValues(table, field) {
    var selects = getRows(table);
    var returnAarry = [];
    if (selects) {
        for (var i = 0; i < selects.length; i++) {
            returnAarry.push(selects[i][field]);
        }
    }
    return returnAarry;
}

//取出数组重复元素
function uniqueArray(data) {
    data = data || [];
    var a = {};
    for (var i = 0; i < data.length; i++) {
        var v = data[i];
        if (typeof (a[v]) == 'undefined') {
            a[v] = 1;
        }
    }
    ;
    data.length = 0;
    for (var i in a) {
        data[data.length] = i;
    }
    return data;
}

//js计算
function fixMath(m, n, op) {
    var a = (m + " ");
    var b = (n + " ");
    var x = 1;
    var y = 1;
    var c = 1;
    if (a.indexOf(".") > 0) {
        x = Math.pow(10, a.length - a.indexOf(".") - 1);
    }
    if (b.indexOf(".") > 0) {
        y = Math.pow(10, b.length - b.indexOf(".") - 1);
    }
    switch (op) {
        case   '+':
        case   '-':
            c = Math.max(x, y);
            m = Math.round(m * c);
            n = Math.round(n * c);
            break;
        case   '*':
            c = x * y
            m = Math.round(m * x);
            n = Math.round(n * y);
            break;
        case   '/':
            c = Math.max(x, y);
            m = Math.round(m * c);
            n = Math.round(n * c);
            c = 1;
            break;
    }
    var exp = m + op + n;
    exp = exp.replace("--", "+");
    return eval("( " + exp + ")/ " + c);
}

//门诊号自动填充
function autoFill(inputId, hospitalId) {
    if (hospitalId != '330004') {
        pCN = '00000000' + $("#" + inputId).val();
        if (pCN == '00000000') {
            $("#" + inputId).val('')
        } else {
            $('#' + inputId).val(pCN.substr(-8, 8))
        }
    } else {
        pCN = '00000000' + $("#" + inputId).val();
        if (pCN == '00000000') {
            $("#" + inputId).val('')
        } else {
            $('#' + inputId).val("2" + pCN.substr(-8, 8))
        }
    }
}

function autoFillNew(val) {
    let pCN = '00000000' + val
    return pCN.substr(-8, 8)
}

//权限判断
// function judgePower(powerWord) {
//     ajaxNormalPost('/menu/menus/selectByMenuName', {}, function (res) {
//         if (res.data) {
//              powerList = res.data[0].spcName;
//             if (powerList.indexOf(powerWord) == -1) {
//                 layer.msg('您没有该权限', {time: 2000}, function () {
//                     $('.i-frame', window.parent.document).attr('src', '')
//                 })
//             } else {
//                 return
//             }
//         }
//     })
// }
//创建下拉列表选项
function createOldOption(url, obj, words, valueKey, viewKey, selectId) {
    var option = '<option value>请选择</option>'
    ajaxNormalPost(url, obj, function (res) {
        if (res) {
            var obj = eval(/*`res.${words}`*/'res[words]');
            for (var i = 0; i < obj.length; i++) {
                option += "<option value=" + obj[i][valueKey] +
                    ">" + obj[i][viewKey] +
                    "</option>"

                /*`<option value="${item[valueKey]}">${item[viewKey]}</option>`*/
            }
            $('#' + selectId).html(option)

        }
    })
}

//返回当天日期
function todayDate() {
    time = new Date();
    year = time.getFullYear();
    month = time.getMonth() + 1;
    if (month < 10) {
        month = '0' + month
    }
    day = time.getDate()
    if (day < 10) {
        day = '0' + day
    }
    return year + "-" + month + "-" + day
    /* `${year}-${month}-${day}`*/
}

//日期加减返回日期
function getDate(date, days) {
    if (days == undefined || days == '') {
        days = 0;
    }
    var oldDate = date.slice(0, 4) + "-" + date.slice(4, 6) + "-" + date.slice(6)
    var dateNew = new Date(oldDate);
    dateNew.setDate(dateNew.getDate() + days);
    var month = dateNew.getMonth() + 1;
    var day = dateNew.getDate();
    var mm = "'" + month + "'";
    var dd = "'" + day + "'";

    //单位数前面加0
    if (mm.length == 3) {
        month = "0" + month;
    }
    if (dd.length == 3) {
        day = "0" + day;
    }

    var time = dateNew.getFullYear() + "-" + month + "-" + day
    return time.split('-').join("")
}


//处理键盘事件 禁止后退键（Backspace）密码或单行、多行文本框除外
function banBackSpace(e) {
    var ev = e || window.event;//获取event对象
    var obj = ev.target || ev.srcElement;//获取事件源

    var t = obj.type || obj.getAttribute('type');//获取事件源类型

//获取作为判断条件的事件类型
    var vReadOnly = obj.getAttribute('readonly');
    var vEnabled = obj.getAttribute('enabled');
//处理null值情况
    vReadOnly = (vReadOnly == null) ? false : vReadOnly;
    vEnabled = (vEnabled == null) ? true : vEnabled;

//当敲Backspace键时，事件源类型为密码或单行、多行文本的，
//并且readonly属性为true或enabled属性为false的，则退格键失效
    var flag1 = (ev.keyCode == 8 && (t == "password" || t == "text" || t == "textarea")
        && (vReadOnly == true || vEnabled != true)) ? true : false;

//当敲Backspace键时，事件源类型非密码或单行、多行文本的，则退格键失效
    var flag2 = (ev.keyCode == 8 && t != "password" && t != "text" && t != "textarea")
        ? true : false;

//判断
    if (flag2) {
        return false;
    }
    if (flag1) {
        return false;
    }
}

/**
 * (东新馆) 挂号小票打印模板
 * @params {Object} data 数据
 * @params {String} timeStateNew 上午 / 下午 / 夜门诊
 * @params {Number} printerIndex 打印机序号
 * */
function CreatePrintPageDongXin(data, timeStateNew, printerIndex) {
    LODOP = getLodop();
    LODOP.PRINT_INITA(0, 0, "120mm", "93mm", "挂号小票");
    LODOP.SET_PRINT_PAGESIZE(1, 1200, 930, "GHXP");
    LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
    LODOP.ADD_PRINT_TEXT(44, 45, 94, 19, data.patCardNum);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(64, 45, 121, 19, data.patName);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(83, 45, 124, 21, data.schedulingDate.join('-') || data.registerDate.substring(0, 10));
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(143, -1, 178, 25, data.dicName);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(103, 45, 60, 19, data.regPrice.toFixed(2));  //挂号费
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(122, 44, 59, 20, data.treatPrice.toFixed(2));  //诊疗费
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(163, -1, 144, 30, data.doctorName + " " + timeStateNew + " " + data.num);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(184, -1, 139, 25, "诊室 " + (data.roomAddress || ''));
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(185, 85, 79, 30, data.specialDiseaseSign == '2' ? '规定病种' : '');
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
    LODOP.ADD_PRINT_TEXT(206, 88, 64, 30, data.appWay ? "预约号" : '');
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    if (data.medStr1) {
        LODOP.ADD_PRINT_TEXT(43, 193, 142, 20, data.patCardNum);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(60, 193, 136, 20, data.patName);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(76, 193, 145, 20, data.schedulingDate.join('-') || data.registerDate.substring(0, 10));
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(123, 193, 129, 19, data.dicName);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(92, 193, 78, 24, data.regPrice.toFixed(2));
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(108, 193, 96, 20, data.treatPrice.toFixed(2));
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(139, 193, 163, 18, data.doctorName + " " + timeStateNew + " " + data.num);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(69, 271, 24, 102, "当\r\n天\r\n有\r\n效\r\n\r\n过\r\n期\r\n作\r\n废");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "LineSpacing", -3);
        LODOP.ADD_PRINT_TEXT(154, 141, 145, 19, "现金支付：" + data.medStr5.slice(4));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(169, 141, 139, 19, "基金支付：" + data.medStr1.slice(4));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(184, 186, 109, 44, "本年账户：" + data.medStr2.slice(4) + "\r\n历年账户：" + data.medStr3.slice(4) + "\r\n统筹基金：" + data.medStr4.slice(4));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "LineSpacing", -3);
    } else {
        LODOP.ADD_PRINT_TEXT(44, 193, 94, 19, data.patCardNum);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(64, 193, 121, 19, data.patName);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(83, 193, 124, 21, data.schedulingDate.join('-') || data.registerDate.substring(0, 10));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(143, 148, 170, 25, data.dicName);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(103, 193, 60, 19, data.regPrice.toFixed(2));  //挂号费
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(122, 193, 59, 20, data.treatPrice.toFixed(2));  //诊疗费
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(163, 148, 144, 25, data.doctorName + " " + timeStateNew + " " + data.num);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(191, 163, 112, 25, "当天有效 过期作废");
    }

    // LODOP.PRINT_DESIGN();
    LODOP.SET_PRINTER_INDEXA(printerIndex);
    LODOP.PRINT();
}

//挂号小票打印模板 visitingFloor
function CreatePrintPage(strPatCardNum, strPatName, strRegisterDate, strDicName, strTotalFee, strCashFee, strDoctorName, strTimeState, strRegNum, medStr, printerIndex) {
    LODOP = getLodop();

    //     LODOP.PRINT_INIT("30");
    let hospitalId = Param.user.hospitalId
    if (hospitalId == '330004' || hospitalId == '330000') {
        LODOP.PRINT_INITA(0, 0, "150mm", "101.6mm", "挂号小票");
        LODOP.SET_PRINT_PAGESIZE(1, 1500, 1016, "");
        LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
        LODOP.ADD_PRINT_TEXT(105, 137, 129, 19, '门诊号：' + strPatCardNum + '');
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(106, 292, 142, 20, '门诊号：' + strPatCardNum + '');
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(124, 137, 121, 19, '姓名：' + (strPatName).substring(0, 6) + '');
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(126, 292, 136, 20, '姓名：' + (strPatName).substring(0, 6) + '');
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(142, 137, 154, 21, '日期：' + (strRegisterDate).substring(0, 10) + '');
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(142, 290, 145, 20, '日期：' + (strRegisterDate).substring(0, 10) + '');
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(161, 137, 122, 20, '科别：' + strDicName + '');
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(163, 290, 151, 19, '科别：' + strDicName + '');
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(182, 137, 90, 19, '金额：' + strTotalFee + '');
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(181, 291, 78, 24, '金额：' + strTotalFee + '');
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(201, 137, 110, 20, '实付现金：' + strCashFee + '');
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(201, 291, 96, 20, '实付现金：' + strCashFee + '');
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(222, 137, 148, 20, strDoctorName + "(" + strTimeState + ")(" + strRegNum + ")");
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(223, 290, 153, 18, strDoctorName + "(" + strTimeState + ")(" + strRegNum + ")");
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    } else {
        // 无文字套打版
        LODOP.PRINT_INITA(0, 0, "119.99mm", "101.6mm", "挂号小票");
        LODOP.SET_PRINT_PAGESIZE(1, 1200, 1016, "");
        LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
        LODOP.ADD_PRINT_TEXT(108, 183, 94, 19, strPatCardNum);
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(107, 331, 142, 20, strPatCardNum);
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(126, 183, 121, 19, (strPatName).substring(0, 6));
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(126, 331, 136, 20, (strPatName).substring(0, 6));
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(143, 183, 124, 21, (strRegisterDate).substring(0, 10));
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(143, 331, 145, 20, (strRegisterDate).substring(0, 10));
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(161, 183, 92, 20, strDicName);
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(161, 329, 151, 19, strDicName);
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(178, 183, 60, 19, strTotalFee);
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(178, 331, 78, 24, strTotalFee);
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(196, 200, 59, 20, strCashFee);
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(197, 351, 96, 20, strCashFee);
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(220, 130, 159, 40, strDoctorName + strTimeState + " " + strRegNum);
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(219, 285, 163, 43, strDoctorName + strTimeState + " " + strRegNum);
        LODOP.SET_PRINT_STYLEA(0, "FontName", "黑体");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    }
    // LODOP.PRINT_DESIGN();

    LODOP.SET_PRINTER_INDEXA(printerIndex);
    LODOP.PRINT();
}

//挂号小票-维护模板 visitingFloor
function CreatePrintPageSetUp(strPatCardNum, strPatName, strRegisterDate, strDicName, strTotalFee, strCashFee, strDoctorName, strTimeState, strRegNum, medStr, index) {
    LODOP = getLodop();
    // LODOP.PRINT_INIT(30,20,357,380,"套打挂号小票的模板");
    // LODOP.SET_PRINT_PAGESIZE(1,1030,1020,"");
    // LODOP.PRINT_INIT("30");
    // LODOP.SET_PRINT_PAGESIZE(1,1030,1017,"");
    // LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE",1);
    // LODOP.ADD_PRINT_TEXT(105,98,76,19,strPatCardNum);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(107,236,94,20,strPatCardNum);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(124,98,63,19,strPatName);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(127,236,93,20,strPatName);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(142,70,143,21,strRegDate);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(143,234,172,20,strRegDate);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(161,95,122,20,strDicName);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(164,234,151,19,strDicName);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(205,92,90,24,strRegTotalFee);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(202,234,78,20,strRegTotalFee);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(227,90,85,25,strRegCashFee);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(222,234,96,25,strRegCashFee);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(184,94,121,20,strDoctorName+"("+strTimeState+")("+strRegNum+")");
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(184,233,160,18,strDoctorName+"("+strTimeState+")("+strRegNum+")");
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(249,152,132,20,"当天有效过期作废");
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);
    // LODOP.ADD_PRINT_TEXT(271,84,285,65,medStr);
    // LODOP.SET_PRINT_STYLEA(0,"Bold",1);


    LODOP.PRINT_INIT("30");
    LODOP.SET_PRINT_PAGESIZE(1, 1030, 1017, "");
    LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
    LODOP.ADD_PRINT_TEXT(105, 49, 129, 19, '门诊号：' + strPatCardNum + '');
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(107, 242, 142, 20, '门诊号：' + strPatCardNum + '');
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(124, 49, 121, 19, '姓名：' + strPatName + '');
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(127, 242, 136, 20, '姓名：' + strPatName + '');
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(142, 49, 154, 21, '日期：' + (strRegisterDate).substring(0, 10) + '');
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(143, 241, 196, 20, '日期：' + (strRegisterDate).substring(0, 10) + '');
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(161, 49, 122, 20, '科别：' + strDicName + '');
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(164, 240, 151, 19, '科别：' + strDicName + '');
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(205, 51, 90, 24, '金额：' + strTotalFee + '');
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(202, 240, 78, 24, '金额：' + strTotalFee + '');
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(227, 51, 110, 25, '实付现金：' + strCashFee + '');
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(222, 240, 96, 25, '实付现金：' + strCashFee + '');
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(184, 50, 121, 20, strDoctorName + "(" + strTimeState + ")(" + strRegNum + ")");
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(184, 239, 160, 18, strDoctorName + "(" + strTimeState + ")(" + strRegNum + ")");
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(249, 145, 132, 20, "当天有效 过期作废");
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);

    LODOP.PRINT_SETUP();


}

// 获取省医保或市医保或自费的医保信息模板
function provinceCityTemplate(data) {
    let fs = +localStorage.getItem('ybjs')
    let provinceCityHtml = `<style>*{margin: 0;padding:0;} p{font-size: ${fs || 12}px;}</style>`
    if (data.data.feeId == 2) {
        let {xjzf, lnzfzfbf, dnzhzf, dnzhye, lnzhzf, lnzhye, jjzf, bcqfbz, qfbzlj} = data.data.invoiceInfoWrapper
        provinceCityHtml += `
            <div>
                <p>${xjzf}</p>
                <p>${lnzfzfbf}</p>
                <p><span style="width: 160px;display: inline-block">${dnzhzf}</span><span>${dnzhye}</span></p>
                <p><span style="width: 160px;display: inline-block">${lnzhzf}</span><span>${lnzhye}</span></p>
                <p>${jjzf}； ${bcqfbz}； ${qfbzlj && (qfbzlj.slice(0, 7) + ((+qfbzlj.slice(7)) + (+bcqfbz.slice(7))).toFixed(2))}</p>
            </div>`
    } else if (data.data.feeId == 3) {
        let {bcqfbz, xjzf, zffje, zlje, zfje, dbyy, cybzfbzfy, dnzhzf, dnzhye, lnzhzf, lnzhye, jjzf, dbbxjjzf, gwybz} = data.data.invoiceInfoWrapper
        provinceCityHtml += `
            <div>
                <p>${xjzf}； ${zffje}； ${zlje}； ${zfje}； ${dbyy}</p>
                <p>${cybzfbzfy}</p>
                <p><span style="width: 180px;display: inline-block">${dnzhzf}</span><span>${dnzhye}</span></p>
                <p><span style="width: 180px;display: inline-block">${lnzhzf}</span><span>${lnzhye}</span></p>
                <p>${jjzf}； ${dbbxjjzf}； ${gwybz}</p>
                <p>${bcqfbz}</p>
            </div>`
    } else {
        let {billTotalFee, billCashFee, billFavorFee} = data.data
        provinceCityHtml += `
            <p>(自费) 实收金额:${billCashFee}；  现金支付:${billTotalFee}； 优惠金额:${billFavorFee}</p>   
        `
    }
    return provinceCityHtml
}

/**
 * 收费发票打印 / 补打
 * @params {Object} data 开单数据
 * @params {Number} index 打印机序号
 * @params {Object} res 就诊人医保金额信息
 * @params {Boolean} patch 区分是否是补打
 * */
function invoiceInformationPrint(data, index, res, patch = true) {
    let provinceCityHtml = Param.user.hospitalId != 330004 ? provinceCityTemplate(res) : ''
    LODOP = getLodop();
    invoiceInformationPrint.data = {
        'patName': '',//姓名
        'billDate': '',//日
        'sum': '',//合计
        'loginNum': '',//收款人
        'table': '',//文本
        'patCardNum': '',//门诊号
        'medicareCard': '',//医保卡号
        'settleMentInformation': '',//医保信息
        'ticketFee0': '',
        'ticketFee1': '',
        'ticketFee2': '',
        'ticketFee3': '',
        'ticketFee4': '',
        'ticketFee5': '',
        'ticketFee6': '',
        'ticketFee7': '',
        'ticketFee8': '',
        'ticketFee9': '',
        'ticketFee10': '',
        'ticketFee11': '',
        'ticketFee12': '',
        'ticketFee13': '',
        'ticketFee14': '',
        'ticketFee15': '',
        'billCash': '',
        'billTotalFee': '',
        'billFavorFee': '',
    }
    var data = $.extend({}, invoiceInformationPrint.data, data);
    data.table.forEach((item, num) => {
        LODOP.PRINT_INITA(10, 1, 791, 470, data.patName + "(" + data.patCardNum + ")收费发票补打");
        LODOP.SET_PRINTER_INDEXA(index);
        LODOP.SET_PRINT_PAGESIZE(1, 2100, 1270, "CreateCustomPage");
        LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);

        LODOP.ADD_PRINT_TEXT(65, 177, 137, 17, res.data.natureName);
        LODOP.ADD_PRINT_TEXT(84, 99, 83, 20, data.medicareCard);
        LODOP.ADD_PRINT_TEXT(83, 229, 90, 17, data.patCardNum);
        LODOP.ADD_PRINT_TEXT(100, 71, 119, 32, data.patName);
        LODOP.ADD_PRINT_TEXT(370, 92, 85, 20, data.sffName);
        LODOP.ADD_PRINT_TABLE(74, 322, 397, 270, item);
        LODOP.ADD_PRINT_TEXT(346, 97, 224, 17, data.totalSettleAmount);
        LODOP.ADD_PRINT_TEXT(99, 173, 83, 17, data.billDate.slice(0, 10).split('-').join(' '));
        LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);

        LODOP.ADD_PRINT_TEXT(154, 99, 55, 17, data.ticketFee0);
        LODOP.ADD_PRINT_TEXT(177, 99, 55, 17, data.ticketFee1);
        LODOP.ADD_PRINT_TEXT(201, 99, 55, 17, data.ticketFee2);
        LODOP.ADD_PRINT_TEXT(225, 99, 55, 17, data.ticketFee3);
        LODOP.ADD_PRINT_TEXT(251, 98, 55, 17, data.ticketFee4);
        LODOP.ADD_PRINT_TEXT(274, 99, 55, 17, data.ticketFee5);
        LODOP.ADD_PRINT_TEXT(297, 98, 55, 17, data.billFavorFee == "0" ? '' : data.billFavorFee);
        LODOP.ADD_PRINT_TEXT(322, 99, 55, 17, data.ticketFee7);
        LODOP.ADD_PRINT_TEXT(154, 227, 50, 17, data.ticketFee8);
        LODOP.ADD_PRINT_TEXT(177, 227, 50, 17, data.ticketFee9);
        LODOP.ADD_PRINT_TEXT(200, 226, 50, 17, data.ticketFee10);
        LODOP.ADD_PRINT_TEXT(224, 227, 50, 17, data.ticketFee11);
        LODOP.ADD_PRINT_TEXT(250, 227, 50, 17, data.ticketFee12);
        LODOP.ADD_PRINT_TEXT(276, 227, 50, 17, data.ticketFee13);
        LODOP.ADD_PRINT_TEXT(299, 227, 50, 17, data.ticketFee14);
        LODOP.ADD_PRINT_TEXT(322, 227, 50, 17, data.ticketFee15);
        LODOP.ADD_PRINT_TEXT(83, 177, 53, 17, "门诊号:");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
        LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
        if (res.data.medChargesInfo && res.data.medChargesInfo.prescribedDisease != 0) {
            LODOP.ADD_PRINT_TEXT(346, 445, 100, 17, "规定病种");
        }
        if (data.billFavorFee != '0') LODOP.ADD_PRINT_TEXT(299, 36, 58, 17, "优惠金额");
        // if (patch) LODOP.ADD_PRINT_TEXT(39,650,90,17,`第${num + 1}页, 共${data.table.length}页`);
        if (Param.user.hospitalId == 330004) {
            LODOP.ADD_PRINT_TEXT(364, 342, 392, 90, data.medicalInsuranceInformation || '');
        } else {
            // if ((patch && num + 1 === data.table.length) || !patch) {
            LODOP.ADD_PRINT_HTM(356, 332, 386, 85, provinceCityHtml);
            // }
        }
        LODOP.ADD_PRINT_TEXT(390, 35, 59, 18, "实收金额");
        LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
        LODOP.ADD_PRINT_TEXT(390, 93, 86, 17, (+data.settleMentInformation).toFixed(2));

        if (patch) {
            LODOP.ADD_PRINT_SHAPE(2, 131, 33, 250, 235, 0, 1, "#FF8040");
            LODOP.ADD_PRINT_TEXT(133, 36, 58, 16, "  项目");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.ADD_PRINT_TEXT(155, 37, 58, 17, "西药费");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(178, 37, 58, 17, "中药费");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(202, 37, 58, 17, "中成药");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(227, 38, 58, 17, "挂号费");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(252, 37, 58, 17, "诊查费");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(276, 37, 58, 17, "检查费");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(322, 34, 58, 17, "");
            LODOP.ADD_PRINT_TEXT(346, 35, 58, 17, "合计");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(133, 98, 60, 16, "   金额");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.ADD_PRINT_TEXT(228, 164, 56, 17, "手术费");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(203, 164, 56, 17, "");
            LODOP.ADD_PRINT_TEXT(178, 164, 56, 17, "治疗费");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(155, 166, 56, 17, "化验费");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(133, 229, 49, 16, "  金额");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.ADD_PRINT_TEXT(133, 166, 56, 16, "  项目");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.ADD_PRINT_TEXT(321, 164, 56, 17, "其他");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(300, 166, 56, 17, "材料费");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(276, 166, 56, 17, "输氧费");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(252, 165, 56, 17, "输血费");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);

            LODOP.ADD_PRINT_SHAPE(0, 150, 33, 250, 1, 0, 1, "#FF8040");
            LODOP.ADD_PRINT_SHAPE(0, 198, 33, 250, 1, 0, 1, "#FF8040");
            LODOP.ADD_PRINT_SHAPE(0, 273, 33, 250, 1, 0, 1, "#FF8040");
            LODOP.ADD_PRINT_SHAPE(0, 222, 33, 250, 1, 0, 1, "#FF8040");
            LODOP.ADD_PRINT_SHAPE(0, 174, 33, 250, 1, 0, 1, "#FF8040");
            LODOP.ADD_PRINT_SHAPE(0, 247, 33, 250, 1, 0, 1, "#FF8040");
            LODOP.ADD_PRINT_SHAPE(0, 130, 96, 1, 235, 0, 1, "#FF8040");
            LODOP.ADD_PRINT_SHAPE(0, 295, 32, 250, 1, 0, 1, "#FF8040");
            LODOP.ADD_PRINT_SHAPE(0, 318, 33, 250, 1, 0, 1, "#FF8040");
            LODOP.ADD_PRINT_SHAPE(0, 341, 33, 250, 1, 0, 1, "#FF8040");
            LODOP.ADD_PRINT_SHAPE(0, 130, 226, 1, 212, 0, 1, "#FF8040");
            LODOP.ADD_PRINT_SHAPE(0, 130, 160, 1, 212, 0, 1, "#804000");
            LODOP.ADD_PRINT_SHAPE(3, 15, 104, 115, 75, 0, 4, "#FF8080");
            LODOP.ADD_PRINT_TEXT(4, 85, 156, 23, "浙江省医疗服务");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(27, 106, 111, 23, "(门诊)发票");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
            LODOP.ADD_PRINT_TEXT(50, 98, 135, 29, "发        票       联");
            LODOP.ADD_PRINT_TEXT(82, 35, 65, 17, "医保卡号:");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.ADD_PRINT_TEXT(100, 35, 39, 17, "姓名:");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.ADD_PRINT_TEXT(371, 35, 53, 17, "收款人:");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.ADD_PRINT_TEXT(370, 195, 90, 18, "收款单位盖章");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.ADD_PRINT_TEXT(399, 211, 70, 18, "(手开无效)");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.ADD_PRINT_TEXT(3, 427, 256, 26, "浙江省医疗服务(门诊)");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
            LODOP.ADD_PRINT_TEXT(30, 476, 169, 26, "收费项目明细单");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
            LODOP.ADD_PRINT_TEXT(63, 387, 63, 14, "项目/规格");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#804000");
            LODOP.ADD_PRINT_TEXT(61, 485, 32, 14, "类别");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#804000");
            LODOP.ADD_PRINT_TEXT(61, 543, 31, 14, "数量");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#804000");
            LODOP.ADD_PRINT_TEXT(61, 579, 32, 14, "金额");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#804000");
            LODOP.ADD_PRINT_TEXT(62, 653, 74, 14, "自理自费(%)");
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#804000");
            LODOP.ADD_PRINT_SHAPE(0, 59, 393, 308, 1, 0, 2, "#804000");
            LODOP.ADD_PRINT_SHAPE(0, 342, 342, 384, 1, 0, 2, "#804000");
            LODOP.ADD_PRINT_TEXT(346, 342, 100, 17, "医保结算信息:");
            LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8000");
        }

        // LODOP.PRINT_DESIGN();
        LODOP.SET_PRINTER_INDEXA(index);
        LODOP.PRINT();
    })
}

/**
 * 费用发票维护
 * @deprecated 停用
 * */
function invoiceInformationPrintSetUp(data, index) {
    LODOP = getLodop();
    invoiceInformationPrintSetUp.data = {
        'patName': '',//姓名
        'billDate': '',//日
        'sum': '',//合计
        'loginNum': '',//收款人
        'table': '',//文本
        'patCardNum': '',//门诊号
        'medicareCard': '',//医保卡号
        'settleMentInformation': '',//医保信息
        'ticketFee0': '',
        'ticketFee1': '',
        'ticketFee2': '',
        'ticketFee3': '',
        'ticketFee4': '',
        'ticketFee5': '',
        'ticketFee6': '',
        'ticketFee7': '',
        'ticketFee8': '',
        'ticketFee9': '',
        'ticketFee10': '',
        'ticketFee11': '',
        'ticketFee12': '',
        'ticketFee13': '',
        'ticketFee14': '',
        'ticketFee15': '',
    }
    var data = $.extend({}, invoiceInformationPrintSetUp.data, data);

    LODOP.PRINT_INITA(10, 1, 791, 470, "浙江省医疗服务(门诊)收费发票样式");
    LODOP.SET_PRINTER_INDEXA(index);
    LODOP.SET_PRINT_PAGESIZE(1, 2100, 1270, "CreateCustomPage");

    LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
    LODOP.ADD_PRINT_TEXT(183, 15, 17, 231, "浙\r\n国\r\n税\r\n印\r\n(\r\n杭\r\n...(省略)");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 7);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "Angle", -90);
    LODOP.ADD_PRINT_TEXT(209, -2, 17, 127, "浙\r\n江\r\n圣\r\n地\r\n票\r\n证\r\n...(省略)");
    LODOP.SET_PRINT_STYLEA(0, "FontName", "楷体");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 7);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8000");
    LODOP.SET_PRINT_STYLEA(0, "Angle", -90);
    LODOP.ADD_PRINT_TEXT(155, 99, 55, 17, data.ticketFee0);
    LODOP.ADD_PRINT_TEXT(179, 100, 55, 17, data.ticketFee1);
    LODOP.ADD_PRINT_TEXT(203, 99, 55, 17, data.ticketFee2);
    LODOP.ADD_PRINT_TEXT(228, 99, 55, 17, data.ticketFee3);
    LODOP.ADD_PRINT_TEXT(253, 98, 55, 17, data.ticketFee4);
    LODOP.ADD_PRINT_TEXT(276, 100, 55, 17, data.ticketFee5);
    LODOP.ADD_PRINT_TEXT(298, 99, 55, 17, data.ticketFee6);
    LODOP.ADD_PRINT_TEXT(322, 100, 55, 17, data.ticketFee7);
    LODOP.ADD_PRINT_TEXT(154, 230, 50, 17, data.ticketFee8);
    LODOP.ADD_PRINT_TEXT(178, 227, 50, 17, data.ticketFee9);
    LODOP.ADD_PRINT_TEXT(201, 228, 50, 17, data.ticketFee10);
    LODOP.ADD_PRINT_TEXT(229, 229, 50, 17, data.ticketFee11);
    LODOP.ADD_PRINT_TEXT(253, 228, 50, 17, data.ticketFee12);
    LODOP.ADD_PRINT_TEXT(276, 230, 50, 17, data.ticketFee13);
    LODOP.ADD_PRINT_TEXT(299, 230, 50, 17, data.ticketFee14);
    LODOP.ADD_PRINT_TEXT(322, 229, 50, 17, data.ticketFee15);
    LODOP.ADD_PRINT_SHAPE(2, 131, 33, 250, 235, 0, 1, "#FF8040");
    LODOP.ADD_PRINT_TEXT(133, 36, 58, 16, "  项目");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_TEXT(155, 37, 58, 17, "西药费");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(178, 37, 58, 17, "中药费");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(202, 37, 58, 17, "中成药");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(227, 38, 58, 17, "挂号费");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(252, 37, 58, 17, "诊查费");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(276, 37, 58, 17, "检查费");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(299, 36, 58, 17, "");
    LODOP.ADD_PRINT_TEXT(322, 34, 58, 17, "");
    LODOP.ADD_PRINT_TEXT(346, 35, 58, 17, "合计");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(133, 98, 60, 16, "   金额");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_TEXT(228, 164, 56, 17, "手术费");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(203, 164, 56, 17, "");
    LODOP.ADD_PRINT_TEXT(178, 164, 56, 17, "治疗费");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(155, 166, 56, 17, "化验费");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(133, 229, 49, 16, "  金额");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_TEXT(133, 166, 56, 16, "  项目");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_TEXT(321, 164, 56, 17, "其他");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(300, 166, 56, 17, "材料费");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(276, 166, 56, 17, "输氧费");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(252, 165, 56, 17, "输血费");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(346, 97, 188, 17, data.sum);
    LODOP.ADD_PRINT_TEXT(128, 288, 19, 244, "第\r\n一\r\n联\r\n发\r\n票\r\n联\r\n...(省略)");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_SHAPE(0, 150, 33, 250, 1, 0, 1, "#FF8040");
    LODOP.ADD_PRINT_SHAPE(0, 198, 33, 250, 1, 0, 1, "#FF8040");
    LODOP.ADD_PRINT_SHAPE(0, 273, 33, 250, 1, 0, 1, "#FF8040");
    LODOP.ADD_PRINT_SHAPE(0, 222, 33, 250, 1, 0, 1, "#FF8040");
    LODOP.ADD_PRINT_SHAPE(0, 174, 33, 250, 1, 0, 1, "#FF8040");
    LODOP.ADD_PRINT_SHAPE(0, 247, 33, 250, 1, 0, 1, "#FF8040");
    LODOP.ADD_PRINT_SHAPE(0, 130, 96, 1, 235, 0, 1, "#FF8040");
    LODOP.ADD_PRINT_SHAPE(0, 295, 32, 250, 1, 0, 1, "#FF8040");
    LODOP.ADD_PRINT_SHAPE(0, 318, 33, 250, 1, 0, 1, "#FF8040");
    LODOP.ADD_PRINT_SHAPE(0, 341, 33, 250, 1, 0, 1, "#FF8040");
    LODOP.ADD_PRINT_SHAPE(0, 130, 226, 1, 212, 0, 1, "#FF8040");
    LODOP.ADD_PRINT_SHAPE(0, 130, 160, 1, 212, 0, 1, "#804000");
    LODOP.ADD_PRINT_SHAPE(3, 15, 104, 115, 75, 0, 4, "#FF8080");
    LODOP.ADD_PRINT_TEXT(4, 85, 156, 23, "浙江省医疗服务");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(27, 106, 111, 23, "(门诊)发票");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    LODOP.ADD_PRINT_TEXT(50, 98, 135, 29, "发        票       联");
    LODOP.ADD_PRINT_TEXT(89, 35, 65, 17, "医保卡号:");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_TEXT(108, 35, 39, 17, "姓名:");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_TEXT(371, 35, 53, 17, "收款人:");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_TEXT(370, 195, 90, 18, "收款单位盖章");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_TEXT(399, 211, 70, 18, "(手开无效)");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_TEXT(3, 427, 256, 26, "浙江省医疗服务(门诊)");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_TEXT(30, 476, 169, 26, "收费项目明细单");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_TEXT(63, 387, 63, 14, "项目/规格");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#804000");
    LODOP.ADD_PRINT_TEXT(61, 485, 32, 14, "类别");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#804000");
    LODOP.ADD_PRINT_TEXT(61, 543, 31, 14, "数量");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#804000");
    LODOP.ADD_PRINT_TEXT(61, 579, 32, 14, "金额");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#804000");
    LODOP.ADD_PRINT_TEXT(62, 653, 74, 14, "自理自费(%)");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#804000");
    LODOP.ADD_PRINT_SHAPE(0, 59, 393, 308, 1, 0, 2, "#804000");
    LODOP.ADD_PRINT_SHAPE(0, 342, 342, 384, 1, 0, 2, "#804000");
    LODOP.ADD_PRINT_TEXT(346, 342, 100, 17, "医保结算信息:");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8000");
    LODOP.ADD_PRINT_TABLE(74, 342, 387, 250, data.table);
    LODOP.ADD_PRINT_TEXT(346, 342, 392, 90, data.settleMentInformation == null ? '' : "         " + data.settleMentInformation);
    LODOP.ADD_PRINT_TEXT(92, 164, 53, 17, "门诊号:");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.ADD_PRINT_TEXT(109, 179, 110, 17, data.billDate);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);
    // LODOP.ADD_PRINT_TEXT(371,92,83,20,data.sffName);
    LODOP.SET_PRINT_STYLEA(0, "AlignJustify", 1);


    LODOP.ADD_PRINT_TEXT(89, 86, 83, 20, data.medicareCard);

    LODOP.ADD_PRINT_TEXT(92, 220, 90, 17, data.patCardNum);
    LODOP.ADD_PRINT_TEXT(110, 85, 56, 20, data.patName);
    LODOP.ADD_PRINT_TEXT(371, 94, 100, 20, data.loginNum);

    LODOP.PRINT_SETUP();

}

//出入库发票维护
function CreateGoDownEntryFormSetUp(data, index) {
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var day = new Date().getDate();
    var data = data;
    LODOP = getLodop();
    var printHtml
    printHtml = "<style> table,td,th {border: 1px solid black;width='100%';border-style: solid;border-collapse: collapse}</style><table border=\"1\">\n"
    printHtml += " <thead><tr>\n" +
        "<td style='width:4%;font-size: 12px;text-align: center'>序号</td>\n" +
        "<td style='width:6%;font-size: 12px;text-align: center'>发票号</td>\n" +
        "<td style='width:13%;font-size: 12px;text-align: center'>名称</td>\n" +
        "<td style='width:13%;font-size: 12px;text-align: center'>规格</td>\n" +
        "<td style='width:4%;font-size: 12px;text-align: center'>单位</td>\n" +
        "<td style='width:10%;font-size: 12px;text-align: center'>购入数量</td>\n" +
        "<td style='width:10%;font-size: 12px;text-align: center'>购入单价</td>\n" +
        "<td style='width:10%;font-size: 12px;text-align: center'>购入金额</td>\n" +
        "<td style='width:10%;font-size: 12px;text-align: center'>零售单价</td>\n" +
        "<td style='width:10%;font-size: 12px;text-align: center'>零售金额</td>\n" +
        "<td style='width:10%;font-size: 12px;text-align: center'>进销差价</td>\n" +
        "</tr></thead>\n"
    printHtml += "<tbody>\n"
    if (data) {
        for (var i = 0; i < data.inDetailWrapperList.length; i++) {

            printHtml += "<tr>\n" +
                "        <td style='width:4%;font-size: 10px;text-align: left'>"
                + (i + 1) + "</td>\n" +
                "        <td style='width:6%;font-size: 10px;text-align: center'>"
                + (i + 1) + "</td>\n" +
                "        <td style='width:13%;font-size: 10px;text-align: left'>"
                + data.inDetailWrapperList[i].drgName + "</td>\n" +
                "        <td style='width:13%;font-size: 10px;text-align: left'>"
                + data.inDetailWrapperList[i].drgSpecification + "</td>\n" +
                "        <td style='width:4%;font-size: 10px;text-align: left'>"
                + data.inDetailWrapperList[i].drgPackingUnit + "</td>\n" +
                "        <td style='width:10%;font-size: 10px;text-align: right'>"
                + (data.inDetailWrapperList[i].drugBigQuantity).toFixed(3)
                + "</td>\n" +
                "        <td style='width:10%;font-size: 10px;text-align: right'>"
                + (data.inDetailWrapperList[i].drugBuyPrice).toFixed(4)
                + "</td>\n" +
                "        <td style='width:10%;font-size: 10px;text-align: right'>"
                + (data.inDetailWrapperList[i].drugTotalBuyPrice).toFixed(2)
                + "</td>\n" +
                "        <td style='width:10%;font-size: 10px;text-align: right'>"
                + (data.inDetailWrapperList[i].drugPrice).toFixed(4) + "</td>\n"
                +
                "        <td style='width:10%;font-size: 10px;text-align: right'>"
                + (data.inDetailWrapperList[i].drugTotalPrice).toFixed(2)
                + "</td>\n" +
                "        <td style='width:10%;font-size: 10px;text-align: right'>"
                + (data.inDetailWrapperList[i].netPrice).toFixed(2) + "</td>\n"
                +
                "    </tr>\n";
        }
        printHtml += "</tbody>\n"
        printHtml += "<tfoot>\n" +
            "  <tr>\n" +
            "    <td style='width:4%;font-size: 12px'></td>\n" +
            "    <td style='width:6%;font-size: 12px'></td>\n" +
            "    <td style='width:13%;font-size: 12px'></td>\n" +
            "<td  tdata=\"subCount\" format=\"#\" style='width:13%;font-size: 12px;text-align: center'>\n"
            +
            "<p align=\"center\"><b>共&nbsp&nbsp<b>#<b>&nbsp&nbsp笔</b></p>\n" +
            "</td>\n" +
            "<td style='width:4%;font-size: 12px'></td>\n" +
            "    <td style='width:10%;font-size: 12px'></td>\n" +
            "    <td style='width:10%;font-size: 12px'></td>\n" +
            "<td tdata=\"subSum\" format=\"0.00\" style='width:10%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            "<td style='width:10%;font-size: 12px'></td>\n" +
            "<td tdata=\"subSum\" format=\"0.00\" style='width:10%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            "<td tdata=\"subSum\" format=\"0.00\" style='width:10%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            " </tr>\n" +
            "  </tfoot>\n"
        // printHtml += "<tfoot><tr>\n"+
        //
        //     "<td colspan='2' style=''></td>"+
        //     "<td colspan='6' style=''></td>"+
        //     "<td colspan='10' style='font-size: 10px;text-align:center'>合计"+data.inDetailWrapperList.length+"笔</td>"+
        //     "<td colspan='15' style=''></td>"+
        //     "<td colspan='6' style=''></td>"+
        //     "<td colspan='10' style=''></td>"+
        //     "<td colspan='10' style=''></td>"+
        //     "<td colspan='10' style='font-size: 10px;text-align: right' tdata='subSum' format='#,##0.00' ></td>"+
        //     "<td colspan='10' style=''></td>"+
        //     "<td colspan='10' style='font-size: 10px;text-align: right'>"+(data.sumDrugTotalPrice).toFixed(2) +"</td>"+
        //     "<td colspan='10' style='font-size: 10px;text-align: right'>"+(data.sumDrugNetPrice).toFixed(2)+"</td>"+
        //     "</tr></tfoot>\n";
    }
    printHtml = printHtml + "</table>\n "
    log4j.info(data)
    log4j.info(printHtml)

    LODOP.PRINT_INITA(0, 0, 675, 796, "药库药品验收单打印模板");
    LODOP.SET_PRINT_PAGESIZE(1, 2420, 1500, "");
    LODOP.ADD_PRINT_TABLE(75, 32, '95%', 300, printHtml);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);

    LODOP.ADD_PRINT_HTM(23, 121, 387, 25, Param.user.hosptalName + "药库药品验收单");
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(52, 38, 183, 20, "单位： ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(52, 221, 156, 20, "验收日期：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(52, 378, 53, 20, "编号： ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(52, 433, 129, 20, "入库单号： ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);

    LODOP.ADD_PRINT_TEXT(389, 474, 100, 20, "制表：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(416, 44, 150, 20,
        "制表日期：" + year + "年" + month + "月" + day + "日" + "    ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(417, 210, 80, 20, "备注： ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(388, 42, 100, 20, "科室主管：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(389, 153, 95, 20, "采购：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(389, 257, 100, 20, "财会：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);
    LODOP.ADD_PRINT_TEXT(390, 360, 100, 20, "验收：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);

    LODOP.PRINT_SETUP();
}

//收费发票打印
function createFeePrint(data, index, res) {
    // 获取医保结算的信息（仙居暂时不处理）
    let provinceCityHtml = Param.user.hospitalId != 330004 ? provinceCityTemplate(res) : ''
    LODOP = getLodop();
    createFeePrint.data = {
        'patName': '',//姓名
        'billDate': '',//日
        'sum': '',//合计
        'loginNum': '',//收款人
        'patCardNum': '',//门诊号
        'medicareCard': '',//医保卡号
        'settleMentInformation': '',//医保信息
        'table': '',//文本
        'ticketFee0': '',
        'ticketFee1': '',
        'ticketFee2': '',
        'ticketFee3': '',
        'ticketFee4': '',
        'ticketFee5': '',
        'ticketFee6': '',
        'ticketFee7': '',
        'ticketFee8': '',
        'ticketFee9': '',
        'ticketFee10': '',
        'ticketFee11': '',
        'ticketFee12': '',
        'ticketFee13': '',
        'ticketFee14': '',
        'ticketFee15': '',
        'billCash': '',
        'billTotalFee': '',
        'billFavorFee': '',
    }
    var data = $.extend({}, createFeePrint.data, data);
    LODOP.PRINT_INITA(10, 1, 791, 470, "浙江省医疗服务(门诊)收费发票样式");
    LODOP.SET_PRINTER_INDEXA(index);
    LODOP.SET_PRINT_PAGESIZE(1, 2100, 1270, "CreateCustomPage");
    LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);

    LODOP.ADD_PRINT_TEXT(89, 86, 83, 20, data.medicareCard);
    LODOP.ADD_PRINT_TEXT(115, 95, 56, 20, data.patName);
    LODOP.ADD_PRINT_TEXT(92, 220, 90, 17, data.patCardNum);
    LODOP.ADD_PRINT_TEXT(109, 179, 110, 17, data.billDate);
    LODOP.ADD_PRINT_TABLE(74, 327, 407, 270, data.table);
    LODOP.ADD_PRINT_TEXT(370, 92, 85, 20, data.sffName);
    LODOP.ADD_PRINT_TEXT(346, 97, 193, 17, data.totalSettleAmount);

    LODOP.ADD_PRINT_TEXT(155, 99, 55, 17, data.ticketFee0);
    LODOP.ADD_PRINT_TEXT(179, 100, 55, 17, data.ticketFee1);
    LODOP.ADD_PRINT_TEXT(203, 99, 55, 17, data.ticketFee2);
    LODOP.ADD_PRINT_TEXT(228, 99, 55, 17, data.ticketFee3);
    LODOP.ADD_PRINT_TEXT(253, 98, 55, 17, data.ticketFee4);
    LODOP.ADD_PRINT_TEXT(276, 100, 55, 17, data.ticketFee5);
    LODOP.ADD_PRINT_TEXT(298, 99, 55, 17, data.billFavorFee == "0" ? '' : data.billFavorFee);
    LODOP.ADD_PRINT_TEXT(322, 100, 55, 17, data.ticketFee7);
    LODOP.ADD_PRINT_TEXT(154, 230, 50, 17, data.ticketFee8);
    LODOP.ADD_PRINT_TEXT(178, 227, 50, 17, data.ticketFee9);
    LODOP.ADD_PRINT_TEXT(201, 228, 50, 17, data.ticketFee10);
    LODOP.ADD_PRINT_TEXT(229, 229, 50, 17, data.ticketFee11);
    LODOP.ADD_PRINT_TEXT(253, 228, 50, 17, data.ticketFee12);
    LODOP.ADD_PRINT_TEXT(276, 230, 50, 17, data.ticketFee13);
    LODOP.ADD_PRINT_TEXT(299, 230, 50, 17, data.ticketFee14);
    LODOP.ADD_PRINT_TEXT(322, 229, 50, 17, data.ticketFee15);
    if (data.specialDiseaseSign == 2) LODOP.ADD_PRINT_TEXT(346, 445, 100, 17, "规定病种");
    if (data.billFavorFee != '0') LODOP.ADD_PRINT_TEXT(298, 33, 60, 17, "优惠金额");

    if (Param.user.hospitalId == 330004) {
        LODOP.ADD_PRINT_TEXT(364, 342, 392, 90, data.medicalInsuranceInformation ? data.medicalInsuranceInformation : '');
    } else {
        LODOP.ADD_PRINT_HTM(364, 332, 386, 85, provinceCityHtml);
    }
    LODOP.ADD_PRINT_TEXT(390, 35, 59, 18, "实收金额");
    LODOP.SET_PRINT_STYLEA(0, "FontColor", "#FF8040");
    LODOP.ADD_PRINT_TEXT(390, 93, 86, 17, (+data.settleMentInformation).toFixed(2));


    // LODOP.PRINT_DESIGN();
    LODOP.SET_PRINTER_INDEXA(index);
    LODOP.PRINT();
}

//收费明细列表打印
function inListPrint(data, index, res) {
    LODOP = getLodop();
    let provinceCityHtml = Param.user.hospitalId != 330004 ? provinceCityTemplate(res) : ''
    inListPrint.data = {
        'table': '',
        'patName': '',
        'sum': '',
        'billTotalFee': '',//总金额
        'billFavorFee': '',//优惠金额
        'loginNum': '',
        'billDate': '',
        'settleMentInformation': ''
    }
    var data = $.extend({}, inListPrint.data, data);
    data.table.forEach((item, num) => {
        LODOP.PRINT_INITA(0, 0, "148mm", "210mm", "处方笺打印模板");
        LODOP.SET_PRINT_PAGESIZE(1, 1480, 2100, "CreateCustomPage");
        LODOP.SET_PRINT_MODE("RESELECT_PRINTER", true);
        LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
        LODOP.ADD_PRINT_TEXT(33, 104, 327, 31, Param.user.hosptalName);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 16);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        LODOP.ADD_PRINT_TEXT(62, 214, 105, 30, "收费明细");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 14);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        LODOP.ADD_PRINT_BARCODE(44, 429, 100, 30, "128Auto", data.patCardNum);
        LODOP.ADD_PRINT_TEXT(95, 56, 90, 20, '姓名: ' + data.patName);
        LODOP.ADD_PRINT_TEXT(94, 410, 108, 20, '日期: ' + data.billDate.slice(0, 10));
        LODOP.ADD_PRINT_TEXT(95, 174, 105, 20, "门诊号: " + data.patCardNum);
        LODOP.ADD_PRINT_TABLE(127, 49, 460, 515, item);
        LODOP.ADD_PRINT_SHAPE(0, 116, 49, 468, 1, 0, 2, "#804000");
        LODOP.ADD_PRINT_SHAPE(0, 656, 54, 464, 1, 0, 2, "#804000");
        // LODOP.ADD_PRINT_TEXT(680,262,195,17,data.sum);
        // LODOP.ADD_PRINT_TEXT(678,93,90,17,data.loginNum);
        if ((num + 1) === data.table.length) {
            LODOP.ADD_PRINT_TEXT(635, 56, 100, 20, '总金额：' + data.billTotalFee.toFixed(2));
            LODOP.ADD_PRINT_TEXT(635, 164, 106, 20, '优惠金额：' + data.billFavorFee.toFixed(2));
            LODOP.ADD_PRINT_TEXT(635, 281, 111, 20, '应收金额：' + (data.billTotalFee - data.billFavorFee).toFixed(2));
            LODOP.ADD_PRINT_TEXT(663, 55, 64, 20, "医保信息:");
            LODOP.ADD_PRINT_HTM(680, 57, 446, 84, provinceCityHtml);
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 9);
        }
        LODOP.ADD_PRINT_TEXT(754, 394, 97, 20, `第${num + 1}页，共${data.table.length}页`);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);

        // LODOP.PRINT_DESIGN();
        LODOP.SET_PRINTER_INDEXA(index);
        LODOP.PRINT();
    })
}

//挂号日报表
function regPrint(data, index) {
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var day = new Date().getDate();
    regPrint.data = {
        chgNum: '',
        sffName: '',
        money: '',
        selfPayAmount: '',
        readyMoney: '',
        favorable: '',
        xjMedicareAmount: '',
        subtotal: '',
        people: '',
        departmentsWrappers: '',
        billClassifyWrappers: '',
    }
    var data = $.extend({}, regPrint.data, data);
    LODOP = getLodop();
    var depTableHeight;
    var typeTableHeight;
    var printDep
    printDep = "<style> table,td,th {border: 1px solid black;border-style: solid;border-collapse: collapse}table{width:500px;}</style><table border=\"1\" >\n"
    printDep += " <thead style='background: #DADADA'><tr>\n" +
        "<td style='width:20%;font-size: 12px;text-align: center'>开单项目</td>\n" +
        "<td style='width:15%;font-size: 12px;text-align: center'>挂号人次</td>\n" +
        "<td style='width:15%;font-size: 12px;text-align: center'>就诊人次</td>\n" +
        "<td style='width:15%;font-size: 12px;text-align: center'>退号人次</td>\n" +
        "<td style='width:15%;font-size: 12px;text-align: center'>挂号费</td>\n" +
        "<td style='width:10%;font-size: 12px;text-align: center'>诊疗费</td>\n" +
        "<td style='width:10%;font-size: 12px;text-align: center'>合计</td>\n" +
        "</tr></thead>\n"
    printDep += "<tbody>\n"
    if (data.departmentsWrappers) {
        depTableHeight = data.departmentsWrappers.length * 20;
        for (var i = 0; i < data.departmentsWrappers.length; i++) {
            printDep += "<tr>\n" +
                "        <td style='width:20%;font-size: 10px;text-align: center'>"
                + data.departmentsWrappers[i].depName + "</td>\n" +
                "        <td style='width:15%;font-size: 10px;text-align: center'>"
                + data.departmentsWrappers[i].regNums + "</td>\n" +
                "        <td style='width:15%;font-size: 10px;text-align: center'>"
                + data.departmentsWrappers[i].regVisitNums + "</td>\n" +
                "        <td style='width:15%;font-size: 10px;text-align: center'>"
                + data.departmentsWrappers[i].regReturnNums + "</td>\n" +
                "        <td style='width:15%;font-size: 10px;text-align: center'>"
                + (data.departmentsWrappers[i].regFee).toFixed(2) + "</td>\n" +
                "        <td style='width:10%;font-size: 10px;text-align: center'>"
                + (data.departmentsWrappers[i].therapyFee).toFixed(2) + "</td>\n" +
                "        <td style='width:10%;font-size: 10px;text-align: center'>"
                + (data.departmentsWrappers[i].summationFee).toFixed(2)
                + "</td>\n" +
                "    </tr>\n";
        }
        printDep += "</tbody>\n"
        printDep += "<tfoot>\n" +
            "  <tr>\n" +
            "    <td style='width:20%;font-size: 12px;text-align: center'>合计</td>\n" +
            "<td tdata='sum' format='#.##' style='width:15%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            "<td tdata=\"sum\" format=\"#.##\" style='width:15%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            "<td tdata=\"sum\" format=\"#.##\" style='width:15%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            "<td tdata=\"subSum\" format=\"0.00\" style='width:15%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            "<td tdata=\"subSum\" format=\"0.00\" style='width:10%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            "<td tdata=\"subSum\" format=\"0.00\" style='width:10%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            " </tr>\n" +
            "  </tfoot>\n"
    }
    printDep = printDep + "</table>\n "
    //按挂号类型统计
    var printType
    printType = "<style> table,td,th {border: 1px solid black;border-style: solid;border-collapse: collapse}table{width:500px;}</style><table border=\"1\">\n"
    printType += " <thead style='background: #DADADA'><tr>\n" +
        "<td style='width:20%;font-size: 12px;text-align: center'>费用类型</td>\n" +
        "<td style='width:15%;font-size: 12px;text-align: center'>初诊人次</td>\n" +
        "<td style='width:15%;font-size: 12px;text-align: center'>复诊人次</td>\n" +
        "<td style='width:15%;font-size: 12px;text-align: center'>挂号人次</td>\n" +
        "<td style='width:15%;font-size: 12px;text-align: center'>退号人次</td>\n" +
        "<td style='width:20%;font-size: 12px;text-align: center'>就诊人次</td>\n" +
        "</tr></thead>\n"
    printType += "<tbody>\n"
    if (data.billClassifyWrappers) {
        typeTableHeight = data.billClassifyWrappers.length * 20;
        for (var i = 0; i < data.billClassifyWrappers.length; i++) {
            printType += "<tr>\n" +
                "        <td style='width:20%;font-size: 10px;text-align: center'>"
                + data.billClassifyWrappers[i].blsName + "</td>\n" +
                "        <td style='width:15%;font-size: 10px;text-align: center'>"
                + data.billClassifyWrappers[i].firstVisit + "</td>\n" +
                "        <td style='width:15%;font-size: 10px;text-align: center'>"
                + data.billClassifyWrappers[i].visits + "</td>\n" +
                "        <td style='width:15%;font-size: 10px;text-align: center'>"
                + data.billClassifyWrappers[i].regNums + "</td>\n" +
                "        <td style='width:15%;font-size: 10px;text-align: center'>"
                + data.billClassifyWrappers[i].regReturnNums + "</td>\n" +
                "        <td style='width:20%;font-size: 10px;text-align: center'>"
                + data.billClassifyWrappers[i].visitNums + "</td>\n" +
                "    </tr>\n";
        }
        printType += "</tbody>\n"
        printType += "<tfoot>\n" +
            "  <tr>\n" +
            "    <td style='width:20%;font-size: 12px;text-align: center'>合计</td>\n" +
            "<td tdata='sum' format='#.##' style='width:15%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            "<td tdata=\"sum\" format=\"#.##\" style='width:15%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            "<td tdata=\"sum\" format=\"#.##\" style='width:15%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            "<td tdata=\"sum\" format=\"#.##\" style='width:15%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            "<td tdata=\"sum\" format=\"#.##\" style='width:20%;font-size: 12px;text-align: center'><b>###</b></td>\n"
            +
            " </tr>\n" +
            "  </tfoot>\n"
    }
    printType = printType + "</table>\n "
    //结账单信息
    var printMessage
    printMessage = "<style> table,td,th {border: 1px solid black;border-style: solid;border-collapse: collapse}table{width:500px;}</style><table border=\"1\">\n"
    printMessage += " <thead style='background: #DADADA'><tr>\n" +
        "<td style='width:25%;font-size: 12px;text-align: center'>项目</td>\n" +
        "<td style='width:25%;font-size: 12px;text-align: center'>金额</td>\n" +
        "<td style='width:25%;font-size: 12px;text-align: center'>项目</td>\n" +
        "<td style='width:25%;font-size: 12px;text-align: center'>金额</td>\n" +
        "</tr></thead>\n"
    printMessage += "<tbody>\n"
    if (data) {
        var selfPayAmount = data.selfPayAmount == null ? '0' : data.selfPayAmount
        var readyMoney = data.readyMoney == null ? '0' : data.readyMoney
        var favorable = data.favorable == null ? '0' : data.favorable
        var xjMedicareAmount = data.xjMedicareAmount == null ? '0' : data.xjMedicareAmount
        var subtotal = data.subtotal == null ? '0' : data.subtotal
        var people = data.people == null ? '0' : data.people
        printMessage += "<tr>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>自费金额</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>" + selfPayAmount + "</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>微信</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "</tr>" +
            "<tr>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>银行卡</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "</tr>" +
            "<tr>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>现金</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>" + readyMoney + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>作废票据</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>优惠</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>" + favorable + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>仙居</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>" + xjMedicareAmount + "</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>实收人次</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>" + people + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>支付宝</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>实收小计</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>" + subtotal + "</td>" +
            "</tr>"

    } else {
        printMessage += "<tr>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>自费金额</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>0</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>微信</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "</tr>" +
            "<tr>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>银行卡</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "</tr>" +
            "<tr>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>现金</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>0</td>" +
            "</tr>" +
            "<tr>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>作废票据</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>优惠</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>0</td>" +
            "</tr>" +
            "<tr>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>仙居</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>0</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>实收人次</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>0</td>" +
            "</tr>" +
            "<tr>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>支付宝</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'></td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>实收小计</td>" +
            "<td style='width:25%;font-size: 12px;text-align: center'>0</td>" +
            "</tr>"
    }
    printMessage = printMessage + "</table>\n "
    console.log(depTableHeight, typeTableHeight)
    LODOP.PRINT_INITA(10, 10, 800, 850, "套打挂号日报报表小票的模板");
    printZoom()  // 缩放纸张
    LODOP.ADD_PRINT_TEXT(6, 101, 365, 37, Param.user.hosptalName + "挂号日报报表");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 16);


    LODOP.ADD_PRINT_TABLE(103, 9, 510, depTableHeight + 24, printDep);
    LODOP.ADD_PRINT_TABLE(103 + depTableHeight + 24 + 5 + 20, 8, 510, typeTableHeight + 24, printType);
    LODOP.ADD_PRINT_TABLE(103 + depTableHeight + 24 + 5 + 20 + typeTableHeight + 24 + 5 + 20, 9, 510, 130, printMessage);
    LODOP.ADD_PRINT_TEXT(83, 10, 194, 20, "按科室统计");
    LODOP.ADD_PRINT_TEXT(103 + depTableHeight + 24 + 10, 12, 194, 20, "按挂号类型统计：");
    LODOP.ADD_PRINT_TEXT(103 + depTableHeight + 24 + 10 + 20 + typeTableHeight + 24 + 10, 11, 194, 20, "结账单信息：");
    LODOP.ADD_PRINT_TEXT(103 + depTableHeight + 24 + 5 + 20 + typeTableHeight + 24 + 5 + 20 + 130, 7, 199, 25, "人员签字：" + data.sffName + "");
    LODOP.ADD_PRINT_TEXT(103 + depTableHeight + 24 + 5 + 20 + typeTableHeight + 24 + 5 + 20 + 130, 235, 154, 25, "打印日期：" + year + "年" + month + "月" + day + "日" + "");

    LODOP.ADD_PRINT_TEXT(81, 382, 123, 23, "单位:(下至元角分)");
    LODOP.ADD_PRINT_TEXT(52, 13, 189, 20, "结账单号：" + data.chgNum + "");
    if (data.money == null || data.money == '' || data.money == undefined) {
        data.money = 0;
        var money = 0
        LODOP.ADD_PRINT_TEXT(56, 381, 121, 20, "实收现金小计：" + money + "");
    }


    // LODOP.PRINT_DESIGN();

    LODOP.SET_PRINTER_INDEXA(index);
    LODOP.PRINT();
}

/**
 * 收费/挂号报表
 * @param data 数据
 * @param arrHtml [发票表格html, 患者表格html, 结账单表格html]
 * @param index 打印机序号
 * @param billType 标题 （挂号 ：收费）
 * @param title 报表类型
 */
function chargePrint(data, arrHtml, index, billType, title) {
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var day = new Date().getDate();
    LODOP = getLodop();
    //单页打印三个表格？
    //按发票项目统计
    let baseCssHtml = "<style> table,td,th {border: 1px solid black;border-style: solid;border-collapse:collapse}table{width:100%;}td{font-size: 12px;text-align: center}</style><table border=\"1\">\n"
    var itemHtml = baseCssHtml + arrHtml[0] + "</table>"
    //按患者类型统计
    var patientHtml = baseCssHtml + arrHtml[1] + "</table>"
    //结账单信息
    var chargeMHtml = baseCssHtml + arrHtml[2] + "</table>"
    let itemTableHeight = data.ticInfoWrapperList.length * 20;
    let patientTableHeight = data.feeInfoWrapperList.length * 20;
    LODOP.PRINT_INITA(0, 0, "148mm", "210mm", "收费日报模板");
    printZoom()  // 缩放纸张
    LODOP.ADD_PRINT_TEXT(83, 13, 194, 20, "按发票项目统计");
    LODOP.ADD_PRINT_TABLE(103, 9, 531, itemTableHeight + 24, itemHtml);
    LODOP.ADD_PRINT_TEXT(103 + itemTableHeight + 24 + 10, 14, 194, 20, "按患者类型统计：");
    LODOP.ADD_PRINT_TABLE(103 + itemTableHeight + 24 + 5 + 20, 9, 531, patientTableHeight + 24, patientHtml);
    LODOP.ADD_PRINT_TEXT(103 + itemTableHeight + 24 + 10 + 20 + patientTableHeight + 24 + 10, 14, 194, 20, "结账单信息：");
    LODOP.ADD_PRINT_TABLE(103 + itemTableHeight + 24 + 5 + 20 + patientTableHeight + 24 + 5 + 25, 9, 531, 175, chargeMHtml);
    LODOP.ADD_PRINT_TEXT(103 + itemTableHeight + 24 + 5 + 20 + patientTableHeight + 24 + 5 + 35 + 175, 259, 106, 25, "人员签字：" + (data.sffName || Param.user.name));
    LODOP.ADD_PRINT_TEXT(103 + itemTableHeight + 24 + 5 + 20 + patientTableHeight + 24 + 5 + 35 + 175, 384, 154, 25, "打印日期：" + year + "-" + month + "-" + day);
    LODOP.ADD_PRINT_TEXT(103 + itemTableHeight + 24 + 5 + 20 + patientTableHeight + 24 + 5 + 35 + 175, 14, 154, 25, "日期：" + data.createDate);
    //根据dailyStatus判断标题
    let bt = billType == '1' ? "收费" : "挂号"
    let paperStyle;
    let summaryMonth;
    let cnNums = ['', '一', '二', '三', '四', '五', '六', '七', '八', '大', '九', '十', '十一', '十二']
    if (data.dailyStatus == '1') {
        paperStyle = "汇总日报表";
    } else if (data.dailyStatus == '2') {
        paperStyle = "汇总月报表";
        summaryMonth = cnNums[parseInt(data.createDate.slice(5, 7))] + "月"
    } else {
        paperStyle = title ? title : "个人日报表";
    }
    LODOP.ADD_PRINT_TEXT(17, 50, 444, 37, `${Param.user.hosptalName + bt + paperStyle}${summaryMonth ? '(' + summaryMonth + ')' : ''}`);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 14);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.ADD_PRINT_TEXT(79, 400, 123, 23, "单位：(下至元角分)");
    LODOP.ADD_PRINT_TEXT(59, 13, 236, 20, "结账单号：" + (data.businessSerialNumber || ''));
    LODOP.ADD_PRINT_TEXT(59, 361, 177, 20, "实收现金小计：" + (data.cashReceived || 0));

    // LODOP.PRINT_DESIGN();
    LODOP.SET_PRINTER_INDEXA(index);
    LODOP.PRINT();
}

//创建下拉列表
function createOption(setting) {
    var option = '';
    ajaxNormalPost(setting.url, setting.param, function (res) {
        if (res) {
            log4j.info(res)
            for (var i = 0; i < res.data.length; i++) {
                option += '<option value="' + res.data[i][setting.value] + '">' + res.data[i][setting.key] + '</option>'
            }
        }
        $('#' + setting.elId).append(option)
    })
}

//获得焦点
function getFirstFocus() {
    $('input[type="text"]').eq(0).focus()
}

// 获取页面等级
function getPageParam(param) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        //console.log(vars[i])
        var pair = vars[i].split("=");
        if (pair[0] == param) {
            return pair[1];
        }
    }
    return '';
}

/**
 * 打印领药证号
 * @param receiveNum 领药证号
 * @param patName 姓名
 * @param rcpPosts 贴数
 * @param condense 浓缩剂量
 * @param everyPackes 倍数 默认2
 * @param summary 备注
 * @param index 打印机序号
 * @param tisanes 是否煎药 1代煎 0不煎
 */
function getMedicinePrint800(receiveNum, {patName, rcpPosts, condense, everyPackes, summary}, index, tisanes = 0) {
    let amount = enrichment(condense);   // 浓缩量
    let numb = everyPackes || 2;
    let hosId = Param.user.hospitalId
    LODOP = getLodop();
    LODOP.PRINT_INITA(0, 0, "80mm", "40mm", "处方笺打印模板");
    LODOP.SET_PRINT_PAGESIZE(1, 800, 400, "CreateCustomPage");
    LODOP.SET_PRINT_MODE("RESELECT_PRINTER", true);
    LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
    LODOP.ADD_PRINT_TEXT("0.53mm", "4.76mm", "72.18mm", "7.67mm", '桐君堂(' + getPavilion() + ")领药证");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 14);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.ADD_PRINT_TEXT(25, 24, 248, 29, receiveNum);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 14);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.ADD_PRINT_TEXT(47, 1, 116, 29, "姓名：" + patName);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(69, 0, 53, 29, tisanes ? "代煎" : "不煎");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(48, 138, 68, 29, "中草药 ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.ADD_PRINT_TEXT(48, 196, 33, 29, rcpPosts);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(48, 220, 28, 29, "贴");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    if (tisanes) {
        LODOP.ADD_PRINT_TEXT(69, 41, 33, 29, rcpPosts);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(69, 63, 28, 29, "贴");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(69, 86, 33, 29, numb * rcpPosts);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(69, 115, 28, 29, "包");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    }
    if (amount != 200 && hosId == 330003) {
        LODOP.ADD_PRINT_TEXT(69, 151, 61, 29, "备注：");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(69, 199, 95, 29, "浓缩" + amount + "ml");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    }
    if (hosId == 330003) {
        LODOP.ADD_PRINT_TEXT(104, 0, 297, 36, "申明：代煎代送难免有破包现象, 如不能接受破包, 建议草药自煎(咨询电话: 0571-56095619)");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
    } else if (hosId == 330005) {
        LODOP.ADD_PRINT_TEXT(94, -1, 61, 29, "备注：");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(94, 45, 245, 46, summary);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
    }

    // LODOP.PRINT_DESIGN();
    LODOP.SET_PRINTER_INDEXA(index);
    LODOP.PRINT();
}

function getMedicinePrint1100(receiveNum, {patName, rcpPosts, condense, everyPackes, summary}, index, tisanes) {
    let amount = enrichment(condense);   // 浓缩量
    let numb = everyPackes || 2;
    let hosId = Param.user.hospitalId
    LODOP = getLodop();
    LODOP.PRINT_INITA(0, 0, "110.01mm", "60.01mm", "处方笺打印模板");
    LODOP.SET_PRINT_PAGESIZE(1, 1100, 600, "CreateCustomPage");
    LODOP.SET_PRINT_MODE("RESELECT_PRINTER", true);
    LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
    LODOP.ADD_PRINT_TEXT("4.23mm", "-3.97mm", "109.22mm", "7.67mm", Param.user.hosptalName + "领药证");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.ADD_PRINT_TEXT(43, -3, 386, 29, receiveNum);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 19);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.ADD_PRINT_TEXT(76, 39, 116, 29, "姓名：" + patName);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 13);
    LODOP.ADD_PRINT_TEXT(77, 179, 68, 29, "中草药 ");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 13);
    LODOP.ADD_PRINT_TEXT(77, 244, 33, 29, rcpPosts);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 13);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(77, 277, 28, 29, "贴");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 13);
    LODOP.ADD_PRINT_TEXT(102, 38, 53, 29, tisanes ? "代煎" : "不煎");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 13);
    if (tisanes) {
        LODOP.ADD_PRINT_TEXT(102, 86, 33, 29, rcpPosts);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 13);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(102, 113, 28, 29, "贴");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 13);
        LODOP.ADD_PRINT_TEXT(102, 136, 33, 29, numb * rcpPosts);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 13);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.ADD_PRINT_TEXT(102, 169, 28, 29, "包");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 13);
    }
    LODOP.ADD_PRINT_TEXT(129, 37, 62, 29, "备注：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 13);
    if (amount != 200 && hosId == 330003) {
        LODOP.ADD_PRINT_TEXT(129, 86, 122, 29, "浓缩" + amount + "ml");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 13);
    }
    if (hosId == 330005) {
        LODOP.ADD_PRINT_TEXT(129, 87, 296, 80, summary);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 13);
    } else if (hosId == 330003) {
        LODOP.ADD_PRINT_TEXT(161, 37, 344, 46, "申明：代煎代送难免有破包现象, 如不能接受破包, 建议草药自煎(咨询电话: 0571-56095619)");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    }

    // LODOP.PRINT_DESIGN();
    LODOP.SET_PRINTER_INDEXA(index);
    LODOP.PRINT();
}

/**
 * 浓缩量
 */
function enrichment(condense) {
    switch (condense) {
        case 2:
            return '150'
        case 3:
            return '120'
        case 4:
            return '100'
        case 5:
            return '80'
        case 6:
            return '60'
        default:
            return '200'
    }
}

/**
 * 打印另煎取药单
 * @param name 姓名
 * @param card 门诊号
 * @param data 代发药品数据
 * @param drugDate 打印/收费日期
 * @param index 打印机序号
 */
function drugReceivingPrint1100(name, card, data, drugDate, index) {
    var printHtml = "<style> table,td {font-size: 15px} </style><table>";
    data.forEach((item, index) => {
        let trEnd = !((index + 1) % 3) ? '</tr>' : ''
        if (item.drgName.indexOf('/') > 0) {
            item.drgName = item.drgName.slice(0, item.drgName.indexOf('/', item.drgName.indexOf('/') + 1))
        }
        printHtml += `<td>${item.drgName} ${item.redQuantity}${item.drgPackingUnit}</td>` + trEnd
    })
    printHtml += "</table>"
    console.log(printHtml)
    LODOP = getLodop();
    LODOP.PRINT_INITA(0, 0, "110.01mm", "60.01mm", "处方笺打印模板");
    LODOP.SET_PRINT_PAGESIZE(1, 1100, 600, "CreateCustomPage");
    LODOP.SET_PRINT_MODE("RESELECT_PRINTER", true);
    LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
    LODOP.ADD_PRINT_TEXT("6.35mm", "-3.97mm", "109.22mm", "7.67mm", Param.user.hosptalName + "另煎药品取药单");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 14);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.ADD_PRINT_TEXT("15.88mm", "20.9mm", "27.52mm", "5.29mm", card);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.ADD_PRINT_TEXT("23.02mm", "3.97mm", "17.46mm", "5.29mm", "姓 名：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.ADD_PRINT_TEXT("15.88mm", "3.7mm", "19.84mm", "5.29mm", "门诊号：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.ADD_PRINT_TEXT("23.28mm", "20.37mm", "20.11mm", "5.29mm", name);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.ADD_PRINT_TEXT(61, 229, 98, 20, "打印日期：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.ADD_PRINT_TEXT(85, 229, 86, 20, "收费日期：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.ADD_PRINT_TEXT(61, 302, 94, 20, drugDate);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.ADD_PRINT_TEXT(84, 304, 91, 20, drugDate);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.ADD_PRINT_TEXT(112, 16, 84, 20, "代发药品：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.ADD_PRINT_TEXT(194, 16, 91, 20, "发药人签名：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.ADD_PRINT_TEXT(195, 158, 71, 20, "病人签名：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.ADD_PRINT_TEXT(195, 284, 71, 20, "发药时间：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.ADD_PRINT_TABLE("28.05mm", "22.49mm", "128.59mm", "114.3mm", printHtml);

    // LODOP.PRINT_DESIGN();

    LODOP.SET_PRINTER_INDEXA(index);
    LODOP.PRINT();
}

function drugReceivingPrint800(name, data, drugDate, index) {
    var printHtml = "<style> table,td {font-size: 15px} </style><table>\n";
    data.forEach((item, index) => {
        let trEnd = !((index + 1) % 2) ? '</tr>' : ''
        if (item.drgName.indexOf('/') > 0) {
            item.drgName = item.drgName.slice(0, item.drgName.indexOf('/', item.drgName.indexOf('/') + 1))
        }
        printHtml += `<td>${item.drgName} ${item.redQuantity}${item.drgPackingUnit}</td>` + trEnd
    })
    printHtml += "</table>"
    LODOP = getLodop();
    LODOP.PRINT_INITA(0, 0, "80mm", "40mm", "处方笺打印模板");
    LODOP.SET_PRINT_PAGESIZE(1, 800, 400, "CreateCustomPage");
    LODOP.SET_PRINT_MODE("RESELECT_PRINTER", true);
    LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
    LODOP.ADD_PRINT_TEXT("1.85mm", "-2.12mm", "82.5mm", "7.67mm", '桐君堂(' + getPavilion() + ")另煎药取药单");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 14);
    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
    LODOP.ADD_PRINT_TEXT("9.26mm", "1.59mm", "29.63mm", "5.29mm", "姓 名：" + name);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.ADD_PRINT_TEXT(36, 139, 184, 20, "打印日期：" + drugDate);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.ADD_PRINT_TEXT(60, 4, 84, 20, "代发药品：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);
    LODOP.ADD_PRINT_TEXT(129, 3, 91, 20, "发药人签名：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.ADD_PRINT_TEXT(129, 102, 71, 20, "病人签名：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.ADD_PRINT_TEXT(129, 191, 71, 20, "发药时间：");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.ADD_PRINT_TABLE("14.29mm", "20.9mm", "128.59mm", "114.3mm", printHtml);

    // LODOP.PRINT_DESIGN();

    LODOP.SET_PRINTER_INDEXA(index);
    LODOP.PRINT();
}

/**
 * 打印煎药标签
 * @param patName 姓名
 * @param rcpPosts 贴数
 * @param receiveNum 领药证号
 * @param index 打印机序号
 */
function drugLabelPrint(patName, rcpPosts, receiveNum, index) {
    LODOP = getLodop();
    LODOP.PRINT_INITA(0, 0, "50mm", "28mm", "处方笺打印模板");
    LODOP.SET_PRINT_PAGESIZE(1, 500, 280, "CreateCustomPage");
    LODOP.SET_PRINT_MODE("RESELECT_PRINTER", true);
    LODOP.SET_PRINT_MODE("PRINT_NOCOLLATE", 1);
    LODOP.ADD_PRINT_TEXT("0.26mm", "0.79mm", "43.07mm", "6.35mm", "桐君堂国医馆(" + getPavilion() + ")");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
    LODOP.ADD_PRINT_TEXT("4.5mm", "1.06mm", "13.76mm", "5.29mm", "姓名:");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT("4.5mm", "10.32mm", "14.82mm", "5.29mm", patName);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.ADD_PRINT_TEXT("8.73mm", "1.06mm", "19.84mm", "5.29mm", "领药证号:");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT("11.91mm", "4.5mm", "42.86mm", "5.29mm", receiveNum);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.ADD_PRINT_TEXT(59, 4, 98, 20, "备注:冷藏保存");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(16, 152, 25, 20, "贴");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.ADD_PRINT_TEXT(16, 130, 23, 20, rcpPosts);
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
    LODOP.ADD_PRINT_TEXT(75, 4, 176, 20, "用量:每日2次,每次一袋或遵医嘱");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);
    LODOP.ADD_PRINT_TEXT(60, 106, 74, 20, todayDate());
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 8);

    // LODOP.PRINT_DESIGN();
    LODOP.SET_PRINTER_INDEXA(index);
    LODOP.PRINT();
}


// 获取拼音码 / 五笔码
// 文件中需引入 plugins/pinyin/pinyinjs-master/pinyinUtil.js
//             plugins/pinyin/pinyinjs-master/dict/pinyin_dict_firstletter.js
//             js/tools.js
function getPinyinTools(inPutValue) {
    return pinyinUtil.getFirstLetter(inPutValue, true)[0]
}

// 获取iframe的部分地址
function getIframeUrl() {
    let objName = $("iframe", parent.document).attr('src')
    // let objName = $(".i-frame:first",parent.document).attr('src')
    objName = [objName.split('/')[1], objName.split('/')[2]].join('/')
    return objName
}

/**
 * 记录搜索栏选择
 * @param arr   标签id数组
 * @param btn   搜索按钮
 * @param name  直接定义存session中的键名, 代替路由取值
 */
function recordPageSelect(arr, btn, name = '') {
    let objName = name || getIframeUrl()
    let obj = {}
    arr.forEach(item => {
        $("#" + item).on("change", function () {
            arr.forEach(i => {
                obj[i] = $('#' + i).val()
            })
            $(btn).click()
            sessionStorage.setItem(objName, JSON.stringify(obj))
        })
    })
}

/**
 * 读取搜索栏记录
 * @param arr  标签id数组
 * @param name 直接定义存session中的键名, 代替路由取值
 */
function readPageSelect(arr, name = '') {
    console.log('调用了')
    let objName = name || getIframeUrl()
    let obj = JSON.parse(sessionStorage.getItem(objName))
    if (obj) {
        arr.forEach(item => {
            $("#" + item).val(obj[item])
        })
    }
}

// 读取修改页面后的信息
function readPageNum() {
    return sessionStorage.getItem("recordPage");
}

// 表格滚动后thead固定（最外层id ，表格id）
function scrollSuckTheTop(content, table) {
    let copyHead = $(table + " thead")[0].outerHTML
    let tHead = `
			<div class="gray-table-body" id="copyThead" style="position: fixed;top: 0;display: none;height: auto;margin-right:20px;">
				<div class="table-radius">
					<table class="table table-fixed table-hover text-center">${copyHead}</table>
				</div>
			</div>`
    $(content).append(tHead)
    $(content).scroll(function () {
        let scrollTop = $(table).offset().top
        scrollTop <= 0 ? $("#copyThead").css('display', 'block') : $("#copyThead").css('display', 'none')
    })
}

// 返回之前xx天的日期
function getNowFormatDate(num) {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (num) {
        if ((strDate - num) >= 0 && (strDate - num) <= 9) {
            strDate = "0" + (strDate - num);
        } else {
            if (strDate - num <= 0) {
                strDate = 30 + strDate - num
                month = month - 1
            } else strDate = strDate - num
        }
    } else if (strDate >= 0 && strDate <= 9) strDate = "0" + strDate;
    if (month >= 1 && month <= 9) month = "0" + month;
    if (month == "02" && strDate > 28) strDate = 28
    var currentdate = year + "-" + month + "-" + strDate;
    return currentdate;
}

// 获取iframe地址
function getRouterTheListTwo() {
    let path = $(".i-frame", parent.document).attr('src')
    let pathArr = path.split('.html')[0].split('/')
    let res = [pathArr[pathArr.length - 2], pathArr[pathArr.length - 1]].join('/')
    return res
}

// form存在本地
function setFormToSession(that, name = '') {
    let path = name || getRouterTheListTwo()
    sessionStorage.setItem(path, JSON.stringify(that.form))
}

// 读取本地form数据
function getFormToSession(that, name = '') {
    let path = name || getRouterTheListTwo()
    let form = JSON.parse(sessionStorage.getItem(path))
    if (form) {
        for (let k in form) {
            if (k.includes('Start') && form[k]) that.date[0] = form[k]
            else if (k.includes('End') && form[k]) that.date[1] = form[k]
        }
        delete form.search_storeId
        that.form = Object.assign(that.form, form)
    }
}

/**
 * 根据feeID, 获取对应医保类型
 * @param {Number} feeId 标识
 * @return {String} 医保类型
 */
function getMedName(feeId) {
    let obj = {
        1: '自费',
        2: '市医保',
        3: '省医保',
        4: '一卡通',
        5: '仙居医保'
    }
    return obj[feeId]
}

/**
 * 返回星期
 * @param week 数字
 */
function getWeek(week) {
    let obj = {
        '1': '星期一',
        '2': '星期二',
        '3': '星期三',
        '4': '星期四',
        '5': '星期五',
        '6': '星期六',
        '7': '星期日'
    }
    return obj[week]
}

/**
 * 根据日期返回星期
 * @param dateString 例：’2020-02-02‘
 * @returns {string} 星期几
 */
function accordDateGetWeek(dateString) {
    let dateArray = dateString.split("-");
    let date = new Date(dateArray[0], parseInt(dateArray[1] - 1), dateArray[2]);
    return "周" + "日一二三四五六".charAt(date.getDay());
};

/**
 * 返回馆名
 */
function getPavilion() {
    let obj = {
        330000: '临安馆',
        330001: '城西馆',
        330002: '武林馆',
        330003: '东新馆',
        330004: '仙居馆',
        330005: '城站馆'
    }
    return obj[Param.user.hospitalId]
}

/**
 * 返回库房名称
 * @param storeId
 */
function getStoreName(storeId) {
    let obj = {
        '5001': '西药房',
        '5002': '中药房',
        '5003': '西药库',
        '5004': '中药库',
        '5005': '煎药房',
    }
    return obj[storeId]
}







