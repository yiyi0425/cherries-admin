// 浙江省阳光医保js
/**
 * 就诊信息查询
 * @param type 类型
 * @param param 参数
 * @returns {string}
 */
async function getCallAcureInput11(type,param) {

    ajaxNormalPostJson('/pub/dictionary/select_by_dic_code', param, function (res) {
        if(res.ret === 0){
            return res.data;
        }else{
            layer.msg(res.msg)
            return res.msg;
        }
    }).then().catch(e=>{})
    return JSON.stringify(param)
}

/**
 * 门诊医嘱实时提醒
 * @param recipeInfo
 * @param diagnoseInfoWrappers
 * @param examInfo
 * @param cardId
 * @param visId
 */
function getCallAcureInput13(recipeInfo,examInfo,diagnoseInfoWrappers,cardId,visId) {
    // let def = $.Deferred()

    return new Promise(((resolve, reject) => {
        let param = {}
        param.recipeInfo = recipeInfo
        param.cardId = cardId
        param.examineInfo = examInfo
        param.diagnoseInfoWrappers = diagnoseInfoWrappers;
        param.visId = visId
        ajaxNormalPostJson('/pro/sunshine/getCallAcureInput13', JSON.stringify(param), function (res) {
            if(res.ret == 0){
                resolve(res.data)
            }else{
                reject(res.msg)
            }
        })
    }))

}

